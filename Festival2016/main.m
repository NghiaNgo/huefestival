//
//  main.m
//  Festival2016
//
//  Created by Phan Phuoc Luong on 9/22/15.
//  Copyright (c) 2015 Phan Phuoc Luong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
