//
//  AppDelegate.h
//  Festival2016
//
//  Created by Phan Phuoc Luong on 9/22/15.
//  Copyright (c) 2015 Phan Phuoc Luong. All rights reserved.
//
#import "PPRevealSideViewController.h"
#import <GoogleMaps/GoogleMaps.h>

@class MapViewController;
@interface AppDelegate : UIResponder <UIApplicationDelegate, PPRevealSideViewControllerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (retain, nonatomic) PPRevealSideViewController  *revealSideViewController;
@property (strong, nonatomic) UINavigationController      *nav;
@property (strong, nonatomic) MapViewController      *mapViewController;

@property (nonatomic, assign) BOOL isIPhoneX;
@property (nonatomic, assign) int screenWidth;
@property (nonatomic, assign) int screenHeight;

@property (nonatomic, assign) CLLocationCoordinate2D myCoordinate;

@property (nonatomic, strong) NSMutableArray *arrLikedID;//chứa danh sách các chương trình được đánh dấu yêu thích
@property (nonatomic, strong) NSMutableArray *arrLikedArtistID;//chứa danh sách các nghệ sĩ được đánh dấu yêu thích

@property (nonatomic, strong) NSMutableArray *arrPrograms;//chứa danh sách các chương trình biểu diễn
@property (nonatomic, strong) NSMutableArray *arrTickets;//chứa danh sách các điểm bán vé
@property (nonatomic, strong) NSMutableArray *arrArts;//chứa danh sách các đoàn nghệ thuật
@property (nonatomic, strong) NSMutableArray *arrLocations;//chứa danh sách các địa điểm tổ chức
@property (nonatomic, strong) NSMutableArray *arrCalendar;//chứa danh sách lịch diễn

@property (nonatomic, strong) NSMutableArray *arrAboutList;//chứa danh sách menu hỗ trợ
@property (nonatomic, strong) NSMutableArray *arrNewsLikeList;//chứa danh sách các tin tức được like
@property (nonatomic, strong) NSMutableArray *arrDiaDiemLikeList;//chứa danh sách các danh lam được like

@property (nonatomic, assign) BOOL isMenuShowed;
@property (nonatomic, strong) NSString *language;
- (void) switchLanguage;
- (void) addLikeProgram:(BOOL) isLike andProgram:(id) programObj;
- (BOOL) checkProgramLike:(NSString *) pID;
- (void) addLikeArtist:(BOOL)isLike andArtObj:(id)obj;
- (BOOL) checkArtistLike:(NSString *)aID;
//kiem tra 1 program da co chua
- (BOOL) checkProgram:(NSString *)pID;
//đặt thông báo
- (void) setReminder:(NSString *)_id andTitle:(NSString *)title andMessage:(NSString *)message andTime:(NSString *)time andProgramID:(NSString *)pID timeBefore:(int)before;
- (void) removeReminder:(NSString *)_id;

//
- (void)setStatusbarColor:(UIColor *)color;
//
@end

@protocol PageControllerDelegate <NSObject>

@optional
- (void) nextController;
- (void) previousController;
- (void) focusToViewController:(int) index;
- (void) showMenu;
@end

@protocol MenuSliderDelegate <NSObject>
@optional
- (void) callToNewController:(int) controllerIndex;
@end

@protocol LoadImageDelegate <NSObject>
@optional
- (void) loadImageProgramFinish:(NSString *)_idProgram andIndex:(NSInteger)index andImage:(UIImage *)img;
- (void) loadImageArtFinish:(NSString *)_idArt andIndex:(NSInteger)index andImage:(UIImage *)img;
- (void) loadImageLocationFinish:(NSString *)_idLocation andIndex:(NSInteger)index andImage:(UIImage *)img;
- (void) loadImageNewsFinish:(NSString *)_idNews andIndex:(NSInteger)index andImage:(UIImage *)img;
- (void) loadImageTicketLocationFinish:(NSString *)_id andIndex:(NSInteger)index andImage:(UIImage *)img;
- (void) loadImageMenuListFinish:(NSString *)_id andSubID:(NSInteger)subID andImage:(UIImage *)img;
@end
