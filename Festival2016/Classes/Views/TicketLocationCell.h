//
//  TicketLocationCell.h
//  Festival2016
//
//  Created by Phan Phuoc Luong on 1/7/16.
//  Copyright © 2016 Phan Phuoc Luong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TicketLocationCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lblTitle;
@property (nonatomic, weak) IBOutlet UILabel *lblDesc;
@property (nonatomic, weak) IBOutlet UIImageView *imgThumb;
@property (nonatomic, weak) IBOutlet UIImageView *imgPhone;
@property (nonatomic, weak) IBOutlet UIButton *lblCap1;
@property (nonatomic, weak) IBOutlet UIImageView *imgAddress;
@property (nonatomic, weak) IBOutlet UIButton *lblCap2;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *progress;

@end
