//
//  MenuItem.m
//  UnitsScanner
//
//  Created by Phan Phuoc Luong on 10/31/14.
//  Copyright (c) 2014 Phan Phuoc Luong. All rights reserved.
//

#import "MenuItem.h"

@implementation MenuItem

- (id)initMenuItem:(int)mId andName:(NSString *)mName andIcon:(UIImage *)mIcon
{
    self = [super init];
    if(self){
        _menuID = mId;
        _menuName = mName;
        _menuIcon = mIcon;
    }
    
    return  self;
}

@end
