//
//  MenuItem.h
//  UnitsScanner
//
//  Created by Phan Phuoc Luong on 10/31/14.
//  Copyright (c) 2014 Phan Phuoc Luong. All rights reserved.
//

@interface MenuItem : NSObject

- (id) initMenuItem:(int) mId andName:(NSString *)mName andIcon:(UIImage *)mIcon;

@property (nonatomic, assign) int menuID;
@property (nonatomic, strong) NSString *menuName;
@property (nonatomic, strong) UIImage *menuIcon;

@end
