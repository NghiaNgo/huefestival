//
//  EventCell.h
//  Festival2016
//
//  Created by Phan Phuoc Luong on 11/16/15.
//  Copyright © 2015 Phan Phuoc Luong. All rights reserved.
//

#import "CalendarEventObj.h"

@interface EventCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lblDate;
@property (nonatomic, weak) IBOutlet UILabel *lblMoth;
@property (nonatomic, weak) IBOutlet UILabel *lblThu;//thứ
@property (nonatomic, weak) IBOutlet UIButton *btnDes;
@property (nonatomic, weak) IBOutlet UIView *line;

- (void) setContentForEvent:(NSString *)date andMonth:(NSString *)month andThu:(NSString *)thu andNumProgram:(int) num;
- (void) setObject:(CalendarEventObj *)calendarObj;
@end
