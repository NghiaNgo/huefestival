//
//  EventCell.m
//  Festival2016
//
//  Created by Phan Phuoc Luong on 11/16/15.
//  Copyright © 2015 Phan Phuoc Luong. All rights reserved.
//

#import "EventCell.h"

@implementation EventCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setContentForEvent:(NSString *)date andMonth:(NSString *)month andThu:(NSString *)thu andNumProgram:(int)num
{
    _lblDate.text = [date uppercaseString];
    _lblMoth.text = [month uppercaseString];
    _lblThu.text = [thu uppercaseString];
    NSString *des = [NSString stringWithFormat:MYLocalizedString(@"program.count", nil), num, num > 1?@"s":@""];
    [_btnDes setTitle:des forState: UIControlStateNormal | UIControlStateDisabled];
    
    CGSize strW = [thu sizeWithFont:_lblThu.font];
    CGRect r = _line.frame;
    r.size.width = strW.width;
    _line.frame = r;
}

- (void)setObject:(CalendarEventObj *)calendarObj
{
    _lblDate.text = calendarObj.cDD;
    _lblMoth.text = calendarObj.cMM;
    _lblThu.text = calendarObj.cLL;
    NSString *des = [NSString stringWithFormat:MYLocalizedString(@"program.count", nil), calendarObj.numProgram, calendarObj.numProgram > 1?@"s":@""];
    [_btnDes setTitle:des forState: UIControlStateNormal | UIControlStateDisabled];
    
    CGSize strW = [_lblThu.text sizeWithFont:_lblThu.font];
    CGRect r = _line.frame;
    r.size.width = strW.width;
    _line.frame = r;
}

@end
