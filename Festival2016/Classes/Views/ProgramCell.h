//
//  ProgramCell.h
//  Festival2016
//
//  Created by Phan Phuoc Luong on 11/9/15.
//  Copyright © 2015 Phan Phuoc Luong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProgramCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lblHueFes;
@property (nonatomic, weak) IBOutlet UILabel *lblTitle;
@property (nonatomic, weak) IBOutlet UIImageView *imgThumb;
@property (nonatomic, weak) IBOutlet UILabel *lblStartup;

@property (nonatomic, weak) IBOutlet UILabel *lblBegin;

@property (nonatomic, weak) IBOutlet UIView *viewDiachi;
@property (nonatomic, weak) IBOutlet UILabel *lblDiachiTitle;
@property (nonatomic, weak) IBOutlet UILabel *lblDiaChiValue;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *progress;

@end
