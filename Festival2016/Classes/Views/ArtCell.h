//
//  ArtCell.h
//  Festival2016
//
//  Created by Phan Phuoc Luong on 11/16/15.
//  Copyright © 2015 Phan Phuoc Luong. All rights reserved.
//

#import "MyButton.h"

@interface ArtCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIView *viewContent;
@property (nonatomic, weak) IBOutlet UILabel *lblTitle;
@property (nonatomic, weak) IBOutlet MyButton *btnList;
@property (nonatomic, weak) IBOutlet UIImageView *imgThumb;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *progress;

@end
