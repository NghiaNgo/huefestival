//
//  ArtCell.m
//  Festival2016
//
//  Created by Phan Phuoc Luong on 11/16/15.
//  Copyright © 2015 Phan Phuoc Luong. All rights reserved.
//

#import "ArtCell.h"

@implementation ArtCell

- (void)awakeFromNib {
    // Initialization code
    _btnList.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _btnList.titleLabel.textAlignment = NSTextAlignmentCenter;
    [_btnList setTitle:MYLocalizedString(@"art.btn.list", nil) forState:UIControlStateNormal];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
