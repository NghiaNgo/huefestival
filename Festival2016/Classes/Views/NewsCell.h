//
//  NewsCell.h
//  Festival2016
//
//  Created by Phan Phuoc Luong on 11/24/15.
//  Copyright © 2015 Phan Phuoc Luong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lblTitle;
@property (nonatomic, weak) IBOutlet UIImageView *imgThumb;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *progress;

@end
