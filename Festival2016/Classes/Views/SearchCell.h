//
//  SearchCell.h
//  Festival2016
//
//  Created by Phan Phuoc Luong on 1/20/16.
//  Copyright © 2016 Phan Phuoc Luong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lblHueFes;
@property (nonatomic, weak) IBOutlet UILabel *lblTitle;
@property (nonatomic, weak) IBOutlet UIImageView *iconView;

@end
