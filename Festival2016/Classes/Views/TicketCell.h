//
//  TicketCell.h
//  Festival2016
//
//  Created by Phan Phuoc Luong on 12/24/15.
//  Copyright © 2015 Phan Phuoc Luong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TicketCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *imgIcon;
@property (nonatomic, weak) IBOutlet UILabel *lblCaption;
@end
