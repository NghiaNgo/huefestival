//
//  MenuCell.h
//  Festival2016
//
//  Created by Phan Phuoc Luong on 9/28/15.
//  Copyright (c) 2015 Phan Phuoc Luong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *menuIcon;
@property (nonatomic, weak) IBOutlet UILabel *menuName;

@end
