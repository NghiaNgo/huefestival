//
//  NewsObj.h
//  Festival2016
//
//  Created by Phan Phuoc Luong on 11/24/15.
//  Copyright © 2015 Phan Phuoc Luong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NewsObj : NSObject

- (id) initWithDict:(NSDictionary *)dict;
- (void) setDataForDetail:(NSDictionary *)dict;
- (void)loadNewsImage:(NSString *)_id andDelegate:(id<LoadImageDelegate>)delegate;

@property (nonatomic, strong) id<LoadImageDelegate> loadImageDelegate;
@property (nonatomic, strong) NSString *nID;
@property (nonatomic, strong) NSString *nTitle;
@property (nonatomic, strong) NSString *nPathImage;
@property (nonatomic, strong) NSString *nSummary;
@property (nonatomic, strong) NSString *nPostDay;
@property (nonatomic, strong) NSString *nChangeDay;

@property (nonatomic, strong) UIImage *newsThumb;
@property (nonatomic, strong) NSDictionary *dict;

//for detail
@property (nonatomic, strong) NSString *content;
@property (nonatomic, strong) NSString *author;
@property (nonatomic, assign) NSInteger approved;
@property (nonatomic, strong) NSString *comment;
@property (nonatomic, assign) NSInteger isNew;
@property (nonatomic, assign) NSInteger isfocus;
@property (nonatomic, strong) NSString *Promotion;
@property (nonatomic, strong) NSString *appeardate;
@property (nonatomic, strong) NSString *groupid;
@property (nonatomic, assign) NSInteger view;

@end
