//
//  MenuSubObj.h
//  Festival2016
//
//  Created by Phan Phuoc Luong on 2/16/16.
//  Copyright © 2016 Phan Phuoc Luong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MenuSubObj : NSObject

- (id) initWithDict:(NSDictionary *)dict;
- (void)loadImage:(NSString *)_id andIndex:(int)index andDelegate:(id<LoadImageDelegate>)delegate;

@property (nonatomic, strong) id<LoadImageDelegate> loadImageDelegate;

@property (nonatomic, assign) int sID;
@property (nonatomic, strong) NSString *sTitle;
@property (nonatomic, strong) NSString *sLinkPath;
@property (nonatomic, assign) int sTypeData;
@property (nonatomic, assign) int sTypeID;
@property (nonatomic, strong) UIImage *subIcon;

@end
