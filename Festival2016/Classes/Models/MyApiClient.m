//
//  MyApiClient.m
//  FunnyClips
//
//  Created by Phan Phuoc Luong on 8/16/14.
//  Copyright (c) 2014 Phan Phuoc Luong. All rights reserved.
//

#import "MyApiClient.h"

@implementation MyApiClient

+ (instancetype)sharedClient
{
    static MyApiClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
    _sharedClient = [[MyApiClient alloc] initWithBaseURL:[NSURL URLWithString:BASE_URL]];
        _sharedClient.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    });
    
    return _sharedClient;
}

//lay danh sach chuong trinh
- (void)getAllPrograms:(NSString *)datas andSuccesBlock:(void (^)(AFHTTPRequestOperation *, id))successBlock andFailBlock:(void (^)(AFHTTPRequestOperation *, NSError *))failBlock
{
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:@"get_program_list", @"module", KEY_API, @"key", [self getLanguage], @"lang", datas, @"datas", nil];
    [self POST:@"/mobile_api/index.php" parameters:parameters success:successBlock failure:failBlock];
}

//lay danh sach lịch dien
- (void)getAllCalendarEvent:(NSString *)datas andSuccesBlock:(void (^)(AFHTTPRequestOperation *, id))successBlock andFailBlock:(void (^)(AFHTTPRequestOperation *, NSError *))failBlock
{
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:@"get_calendar_list", @"module", KEY_API, @"key", [self getLanguage], @"lang", datas, @"datas", nil];
    [self POST:@"/mobile_api/index.php" parameters:parameters success:successBlock failure:failBlock];
}

//lay danh sach cac doan nghe thuat
- (void)getAllArt:(NSString *)datas andSuccesBlock:(void (^)(AFHTTPRequestOperation *, id))successBlock andFailBlock:(void (^)(AFHTTPRequestOperation *, NSError *))failBlock
{
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:@"get_art_list", @"module", KEY_API, @"key", [self getLanguage], @"lang", datas, @"datas", nil];
    [self POST:@"/mobile_api/index.php" parameters:parameters success:successBlock failure:failBlock];
}

//lay danh sach cac diem bieu dien
- (void)getAllLocation:(NSString *)datas andSuccesBlock:(void (^)(AFHTTPRequestOperation *, id))successBlock andFailBlock:(void (^)(AFHTTPRequestOperation *, NSError *))failBlock
{
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:@"get_location_list", @"module", KEY_API, @"key", [self getLanguage], @"lang", datas, @"datas", nil];
    [self POST:@"/mobile_api/index.php" parameters:parameters success:successBlock failure:failBlock];
}

//lay danh sach tin tuc
- (void)getNews:(NSInteger)page numItem:(NSInteger)number andSuccesBlock:(void (^)(AFHTTPRequestOperation *, id))successBlock andFailBlock:(void (^)(AFHTTPRequestOperation *, NSError *))failBlock
{
    NSString *path = [NSString stringWithFormat:@"/mobile_api/index.php?module=get_news_list&key=%@&lang=%@&page_size=%zd&page_index=%zd", KEY_API, [self getLanguage], number, page];
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [self GET:path parameters:nil success:successBlock failure:failBlock];
}

//lay chi tiet 1 tin tuc
- (void)getNewsDetail:(NSString *)news_id andSuccesBlock:(void (^)(AFHTTPRequestOperation *, id))successBlock andFailBlock:(void (^)(AFHTTPRequestOperation *, NSError *))failBlock
{
    NSString *path = [NSString stringWithFormat:@"/mobile_api/index.php?module=get_news_detail&key=%@&news_id=%@", KEY_API, news_id];
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [self GET:path parameters:nil success:successBlock failure:failBlock];
}

//lấy danh sách các điểm bán vé
- (void) getTicketLocations:(NSString *)datas andSuccesBlock:(void (^)(AFHTTPRequestOperation *, id))successBlock andFailBlock:(void (^)(AFHTTPRequestOperation *, NSError *))failBlock
{
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:@"get_ticket_location", @"module", KEY_API, @"key", [self getLanguage], @"lang", datas, @"datas", nil];
    [self POST:@"/mobile_api/index.php" parameters:parameters success:successBlock failure:failBlock];
}

//lấy danh sách giá vé
- (void) getTicketPrice:(NSString *)datas andSuccesBlock:(void (^)(AFHTTPRequestOperation *, id))successBlock andFailBlock:(void (^)(AFHTTPRequestOperation *, NSError *))failBlock
{
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:@"get_program_price_list", @"module", KEY_API, @"key", [self getLanguage], @"lang", datas, @"datas", nil];
    [self POST:@"/mobile_api/index.php" parameters:parameters success:successBlock failure:failBlock];
}

- (void)getMenuList:(void (^)(AFHTTPRequestOperation *, id))successBlock andFailBlock:(void (^)(AFHTTPRequestOperation *, NSError *))failBlock
{
    NSString *path = [NSString stringWithFormat:@"/mobile_api/index.php?module=get_menu_list&key=%@&lang=%@", KEY_API, [self getLanguage]];
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [self GET:path parameters:nil success:successBlock failure:failBlock];
}

- (void)getListByID:(int)cate_id andTypeData:(int)typedata andPageIndex:(int)page_index andPageSize:(int)page_size andLat:(double)latitude andLng:(double)longtitude andSuccesBlock:(void (^)(AFHTTPRequestOperation *, id))successBlock andFailBlock:(void (^)(AFHTTPRequestOperation *, NSError *))failBlock
{
    NSString *path = [NSString stringWithFormat:@"/mobile_api/index.php?module=get_service_list&key=%@&lang=%@&cate_id=%i&typedata=%i&page_index=%i&page_size=%i&latitude=%lf&longtitude=%lf", KEY_API, [self getLanguage], cate_id, typedata, page_index, page_size, latitude, longtitude];
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [self GET:path parameters:nil success:successBlock failure:failBlock];
}

- (void)getServiceDetail:(int)service_id andTypeData:(int)typedata andSuccesBlock:(void (^)(AFHTTPRequestOperation *, id))successBlock andFailBlock:(void (^)(AFHTTPRequestOperation *, NSError *))failBlock
{
    NSString *path = [NSString stringWithFormat:@"/mobile_api/index.php?module=get_service_detail&key=%@&lang=%@&service_id=%i&typedata=%i", KEY_API, [self getLanguage], service_id, typedata];
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [self GET:path parameters:nil success:successBlock failure:failBlock];
}

//lây danh sach menu hỗ trợ
- (void) getAboutList:(void (^)(AFHTTPRequestOperation *, id))successBlock andFailBlock:(void (^)(AFHTTPRequestOperation *, NSError *))failBlock
{
    NSString *path = [NSString stringWithFormat:@"/mobile_api/index.php?module=get_about_list&key=%@&lang=%@", KEY_API, [self getLanguage]];
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [self GET:path parameters:nil success:successBlock failure:failBlock];
}

//lấy thôgn tin chi tiết cho 1 menu hỗ trợ
- (void) getAboutDetail:(int)about_id andSuccesBlock:(void (^)(AFHTTPRequestOperation *, id))successBlock andFailBlock:(void (^)(AFHTTPRequestOperation *, NSError *))failBlock
{
    NSString *path = [NSString stringWithFormat:@"/mobile_api/index.php?module=get_about_detail&key=%@&lang=%@&about_id=%i", KEY_API, [self getLanguage], about_id];
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [self GET:path parameters:nil success:successBlock failure:failBlock];
}

//============== common ============//
- (NSString *) getLanguage
{
    return [MyAppDelegate().language isEqualToString:@"en"] ? @"en" : @"vn";
}

@end
