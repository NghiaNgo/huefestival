//
//  TicketLocationObj.m
//  Festival2016
//
//  Created by Phan Phuoc Luong on 1/7/16.
//  Copyright © 2016 Phan Phuoc Luong. All rights reserved.
//

#import "TicketLocationObj.h"

@implementation TicketLocationObj

- (id)initWithDict:(NSDictionary *)dict andIndex:(int)index
{
    if(self = [super init]){
        _dict = dict;
        _id_ticketlocation = [dict objectForKey:@"id"];
        _title = [dict objectForKey:@"title"];
        _content = [dict objectForKey:@"content"];
        _pLinkImage = [dict objectForKey:@"pathimage"];
        _phone = [dict objectForKey:@"phone"];
        _address = [dict objectForKey:@"address"];
        _md5 = [dict objectForKey:@"md5"];
        NSString *latitude = [dict objectForKey:@"latitude"];
        NSString *longtitude = [dict objectForKey:@"longtitude"];
        _sumary = [dict objectForKey:@"summary"];
        
        if(!_id_ticketlocation || [_id_ticketlocation isKindOfClass:[NSNull class]])
            _id_ticketlocation = @"";
        if(!_title || [_title isKindOfClass:[NSNull class]])
            _title = @"";
        if(!_content || [_content isKindOfClass:[NSNull class]])
            _content = @"";
        if(!_phone || [_phone isKindOfClass:[NSNull class]])
            _phone = @"";
        if(!_address || [_address isKindOfClass:[NSNull class]])
            _address = @"";
        if(!_md5 || [_md5 isKindOfClass:[NSNull class]])
            _md5 = @"";
        if(!_sumary || [_sumary isKindOfClass:[NSNull class]])
            _sumary = @"";
        if(!_pLinkImage || [_pLinkImage isKindOfClass:[NSNull class]])
            _pLinkImage = @"";
        if(!latitude || [latitude isKindOfClass:[NSNull class]])
            latitude = @"0.0";
        if(!longtitude || [longtitude isKindOfClass:[NSNull class]])
            longtitude = @"0.0";
        _lat = [latitude doubleValue];
        _lng = [longtitude doubleValue];
        
        [self loadImage:_id_ticketlocation andIndex:index andDelegate:_loadImageDelegate];
    }
    
    return self;
}

- (void)updateNewData:(TicketLocationObj *)newObj
{
    _id_ticketlocation = newObj.id_ticketlocation;
    _title = newObj.title;
    _content = newObj.content;
    _pLinkImage = newObj.pLinkImage;
    _pThumb = newObj.pThumb;
    _lat = newObj.lat;
    _lng = newObj.lng;
    _md5 = newObj.md5;
}

- (void)loadImage:(NSString *)_id andIndex:(int)index andDelegate:(id<LoadImageDelegate>)delegate
{
    _loadImageDelegate = delegate;
    if(_pLinkImage.length == 0){
        UIImage *image = [UIImage imageNamed:@"test_thumb0"];
        _pThumb = image;
        [_loadImageDelegate loadImageTicketLocationFinish:_id andIndex:index andImage:image];
        return;
    }
    
    
    NSString *link = [_pLinkImage stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [self getPhoto:_id andLink:[NSURL URLWithString:link] andIndex:index];
}

- (void)getPhoto:(NSString *)_id andLink:(NSURL *) link andIndex:(NSInteger)index
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:link];
    request.cachePolicy = NSURLRequestReturnCacheDataElseLoad;
    AFHTTPRequestOperation *posterOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    posterOperation.responseSerializer = [AFImageResponseSerializer serializer];
    posterOperation.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"image/jpg", @"image/png", @"image/jpeg", nil];
    [posterOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id image) {
        _pThumb = image;
        
        if(_loadImageDelegate)
            [_loadImageDelegate loadImageTicketLocationFinish:_id andIndex:index andImage:image];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //NSLog(@"Now showing poster request failed with error: %@", error);
    }];
    [posterOperation start];
}

@end
