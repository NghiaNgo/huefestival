//
//  NewsObj.m
//  Festival2016
//
//  Created by Phan Phuoc Luong on 11/24/15.
//  Copyright © 2015 Phan Phuoc Luong. All rights reserved.
//

#import "NewsObj.h"

@implementation NewsObj

- (id)initWithDict:(NSDictionary *)dict
{
    if(self = [super init]){
        _dict = dict;
        _nID = [dict objectForKey:@"id"];
        _nTitle = [dict objectForKey:@"title"];
        _nPathImage = [dict objectForKey:@"pathimage"];
        _nSummary = [dict objectForKey:@"summary"];
        _nPostDay = [dict objectForKey:@"postdate"];
        _nChangeDay = [dict objectForKey:@"changedate"];
        
        _content = @"";
        if(!_nID || [_nID isKindOfClass:[NSNull class]])
            _nID = @"";
        if(!_nTitle || [_nTitle isKindOfClass:[NSNull class]])
            _nTitle = @"";
        if(!_nPathImage || [_nPathImage isKindOfClass:[NSNull class]])
            _nPathImage = @"";
        if(!_nSummary || [_nSummary isKindOfClass:[NSNull class]])
            _nSummary = @"";
        if(!_nPostDay || [_nPostDay isKindOfClass:[NSNull class]])
            _nPostDay = @"";
        if(!_nChangeDay || [_nChangeDay isKindOfClass:[NSNull class]])
            _nChangeDay = @"";
        
        if(_nTitle.length > 0)
            _nTitle = [_nTitle stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        [self loadNewsImage:_nID andDelegate:_loadImageDelegate];
    }
    
    return self;
}

- (void)setDataForDetail:(NSDictionary *)dict
{
    _content = [dict objectForKey:@"content"];
    _author = [dict objectForKey:@"author"];
    _approved = [[dict objectForKey:@"approved"] integerValue];
    _comment = [dict objectForKey:@"comment"];
    _isNew = [[dict objectForKey:@"isNew"] integerValue];
    _isfocus = [[dict objectForKey:@"isfocus"] integerValue];
    _Promotion = [dict objectForKey:@"Promotion"];
    _appeardate = [dict objectForKey:@"appeardate"];
    _groupid = [dict objectForKey:@"groupid"];
    _view = [[dict objectForKey:@"view"] integerValue];
        
    if(!_content || [_content isKindOfClass:[NSNull class]])
        _content = @"";
    if(!_author || [_author isKindOfClass:[NSNull class]])
        _author = @"";
    if(!_comment || [_comment isKindOfClass:[NSNull class]])
        _comment = @"";
    if(!_Promotion || [_Promotion isKindOfClass:[NSNull class]])
        _Promotion = @"";
    if(!_appeardate || [_appeardate isKindOfClass:[NSNull class]])
        _appeardate = @"";
    if(!_groupid || [_groupid isKindOfClass:[NSNull class]])
        _groupid = @"";
}

- (void)loadNewsImage:(NSString *)_id andDelegate:(id<LoadImageDelegate>)delegate
{
    _loadImageDelegate = delegate;
    if(_nPathImage.length == 0){
        NSString *imgName = [NSString stringWithFormat:@"test_thumb3"];
        UIImage *image = [UIImage imageNamed:imgName];
        _newsThumb = image;
        [_loadImageDelegate loadImageNewsFinish:_id andIndex:0 andImage:image];
        return;
    }
    
    NSString *link = [_nPathImage stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [self getPhoto:_id andLink:[NSURL URLWithString:link] andIndex:0];
}

- (void)getPhoto:(NSString *)_id andLink:(NSURL *) link andIndex:(NSInteger)index
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:link];
    request.cachePolicy = NSURLRequestReturnCacheDataElseLoad;
    AFHTTPRequestOperation *posterOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    posterOperation.responseSerializer = [AFImageResponseSerializer serializer];
    posterOperation.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"image/jpg", @"image/png", @"image/jpeg", nil];
    [posterOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id image) {
        _newsThumb = image;
        if(_loadImageDelegate)
            [_loadImageDelegate loadImageNewsFinish:_id andIndex:0 andImage:image];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //NSLog(@"Now showing poster request failed with error: %@", error);
    }];
    [posterOperation start];
}

@end
