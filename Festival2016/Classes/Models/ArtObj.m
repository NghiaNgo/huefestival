//
//  ArtObj.m
//  Festival2016
//
//  Created by Phan Phuoc Luong on 11/16/15.
//  Copyright © 2015 Phan Phuoc Luong. All rights reserved.
//

#import "ArtObj.h"

@implementation ArtObj

- (id)initWithDict:(NSDictionary *)dict
{
    if(self = [super init]){
        _aID = [dict objectForKey:@"id_doan"];
        _aName = [dict objectForKey:@"doan_name"];
        _aContent = [dict objectForKey:@"doan_content"];
        _md5 = [dict objectForKey:@"md5"];
        
        if(!_aID || [_aID isKindOfClass:[NSNull class]])
            _aID = @"";
        if(!_aName || [_aName isKindOfClass:[NSNull class]])
            _aName = @"";
        if(!_aContent || [_aContent isKindOfClass:[NSNull class]])
            _aContent = @"";
        if(!_md5 || [_md5 isKindOfClass:[NSNull class]])
            _md5 = @"";
        
        NSArray *arrDict = (NSArray *)[dict objectForKey:@"detail_list"];
        _art_detail_list = [[NSMutableArray alloc] initWithCapacity:arrDict.count];
        DetailListObj *subObj = nil;
        for(NSDictionary *d in arrDict){
            subObj = [[DetailListObj alloc] initWithArtDict:d];
            [_art_detail_list addObject:subObj];
        }
        
        _arrImageLink = [[NSMutableArray alloc] init];
        _arrImage = [[NSMutableArray alloc] init];
        UIImage *image = [UIImage imageNamed:@"test_thumb0.png"];
        NSDictionary *arrImages = [dict objectForKey:@"pathimage_list"];
        for(NSString *str in arrImages){
            [_arrImageLink addObject:str];
            [_arrImage addObject:image];
        }
        
        [self loadArtImage:_aID andDelegate:_loadImageDelegate];
    }
    
    return self;
}

- (void)updateNewData:(ArtObj *)newArt
{
    _aID = newArt.aID;
    _aName = newArt.aName;
    _aContent = newArt.aContent;
    _md5 = newArt.md5;
    
    [_art_detail_list removeAllObjects];
    [_art_detail_list addObjectsFromArray:newArt.art_detail_list];
    
    //[_arrImageLink removeAllObjects];
    //[_arrImageLink addObjectsFromArray:newArt.arrImageLink];
    
    //[_arrImage removeAllObjects];
    //[_arrImage addObjectsFromArray:newArt.arrImage];
}

- (void)loadArtImage:(NSString *)_id andDelegate:(id<LoadImageDelegate>)delegate
{
    _loadImageDelegate = delegate;
    if(_arrImageLink.count == 0){
        UIImage *image = [UIImage imageNamed:@"test_thumb0.png"];
        [_arrImage addObject:image];
        [_loadImageDelegate loadImageArtFinish:_id andIndex:0 andImage:image];
        return;
    }
    
    NSInteger index = 0;
    for(NSString *strLink in _arrImageLink){
        NSString *link = [strLink stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [self getPhoto:_id andLink:[NSURL URLWithString:link] andIndex:index];
        index++;
    }
}

- (void)getPhoto:(NSString *)_id andLink:(NSURL *) link andIndex:(NSInteger)index
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:link];
    request.cachePolicy = NSURLRequestReturnCacheDataElseLoad;
    AFHTTPRequestOperation *posterOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    posterOperation.responseSerializer = [AFImageResponseSerializer serializer];
    posterOperation.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"image/jpg", @"image/png", @"image/jpeg", nil];
    [posterOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id image) {
        if(index < _arrImage.count)
            [_arrImage replaceObjectAtIndex:index withObject:image];
        else if(_arrImage.count < _arrImageLink.count)
            [_arrImage addObject:image];
        
        if(_loadImageDelegate)
            [_loadImageDelegate loadImageArtFinish:_id andIndex:index andImage:image];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //NSLog(@"Now showing poster request failed with error: %@", error);
    }];
    [posterOperation start];
}

@end
