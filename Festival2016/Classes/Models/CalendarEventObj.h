//
//  CalendarEventObj.h
//  Festival2016
//
//  Created by Phan Phuoc Luong on 11/16/15.
//  Copyright © 2015 Phan Phuoc Luong. All rights reserved.
//

#import "DetailListObj.h"

@interface CalendarEventObj : NSObject

- (id) initWithDict:(NSDictionary *)dict;
- (void) updateData:(CalendarEventObj *)newObj;

@property (nonatomic, strong) NSString *fDate;//full day
@property (nonatomic, strong) NSString *md5;//md5
//extend
@property (nonatomic, strong) NSString *cDD;//ngày
@property (nonatomic, strong) NSString *cMM;//tháng
@property (nonatomic, strong) NSString *cLL;//thứ
@property (nonatomic, assign) int numProgram;//so chuong trinh
@property (nonatomic, strong) NSMutableArray *calendar_detail_list;//danh sach chuong trinh

@end
