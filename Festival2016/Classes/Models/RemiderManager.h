//
//  RemiderManager.h
//  Festival2016
//
//  Created by Phan Phuoc Luong on 11/5/15.
//  Copyright © 2015 Phan Phuoc Luong. All rights reserved.
//

#import <EventKit/EventKit.h>

@interface RemiderManager : NSObject
{
    BOOL isGranted;
}

@property (nonatomic, strong) EKEventStore *eventStore;

- (id) initReminderManager;
- (void) addNewReminder;
@end
