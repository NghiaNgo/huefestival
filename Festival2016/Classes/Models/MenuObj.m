//
//  MenuObj.m
//  Festival2016
//
//  Created by Phan Phuoc Luong on 2/16/16.
//  Copyright © 2016 Phan Phuoc Luong. All rights reserved.
//

#import "MenuObj.h"

@implementation MenuObj

- (id)initWithDict:(NSDictionary *)dict
{
    if(self = [super init]){
        _mID = [[dict objectForKey:@"id"] intValue];
        _mTitle = [dict objectForKey:@"title"];
        _mTypeData = [[dict objectForKey:@"typedata"] intValue];
        _mLinkPath = [dict objectForKey:@"pathicon"];
        
        _mSubMenu = [[NSMutableArray alloc] init];
        NSArray *arrSub = [dict objectForKey:@"submenu"];
        MenuSubObj *subObj = nil;
        for(NSDictionary *d in arrSub){
            subObj = [[MenuSubObj alloc] initWithDict:d];
            subObj.loadImageDelegate = _loadImageDelegate;
            [_mSubMenu addObject:subObj];
        }
        
        if(!_mTitle || [_mTitle isKindOfClass:[NSNull class]])
            _mTitle = @"";
        if(!_mLinkPath || [_mLinkPath isKindOfClass:[NSNull class]])
            _mLinkPath = @"";
        
        [self loadImage:[NSString stringWithFormat:@"%i", _mID] andIndex:0 andDelegate:_loadImageDelegate];
    }
    
    return self;
}

- (id)initWithDictForAbout:(NSDictionary *)dict
{
    if(self = [super init]){
        _mID = [[dict objectForKey:@"id"] intValue];
        _mTitle = [dict objectForKey:@"title"];
    }
    
    return self;
}

- (void)loadImage:(NSString *)_id andIndex:(int)index andDelegate:(id<LoadImageDelegate>)delegate
{
    _loadImageDelegate = delegate;
    if(_mLinkPath.length == 0){
        UIImage *image = [UIImage imageNamed:@"test_thumb0"];
        _menuIcon = image;
        [_loadImageDelegate loadImageMenuListFinish:_id andSubID:0 andImage:image];
        return;
    }
    
    NSString *link = [_mLinkPath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [self getPhoto:_id andLink:[NSURL URLWithString:link] andIndex:index];
}

- (void)getPhoto:(NSString *)_id andLink:(NSURL *) link andIndex:(NSInteger)index
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:link];
    request.cachePolicy = NSURLRequestReturnCacheDataElseLoad;
    AFHTTPRequestOperation *posterOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    posterOperation.responseSerializer = [AFImageResponseSerializer serializer];
    posterOperation.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"image/jpg", @"image/png", @"image/jpeg", nil];
    [posterOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id image) {
        _menuIcon = image;
        if(_loadImageDelegate)
            [_loadImageDelegate loadImageMenuListFinish:_id andSubID:0 andImage:image];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //NSLog(@"Now showing poster request failed with error: %@", error);
    }];
    [posterOperation start];
}

@end
