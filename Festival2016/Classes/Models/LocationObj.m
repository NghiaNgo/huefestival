//
//  LocationObj.m
//  Festival2016
//
//  Created by Phan Phuoc Luong on 11/20/15.
//  Copyright © 2015 Phan Phuoc Luong. All rights reserved.
//

#import "LocationObj.h"

@implementation LocationObj

- (id)initWithDict:(NSDictionary *)dict
{
    if(self = [super init]){
        _id_diadiem = [dict objectForKey:@"id_diadiem"];
        _diadiem_name = [dict objectForKey:@"diadiem_name"];
        _diadiem_content = [dict objectForKey:@"diadiem_content"];
        _pLinkImage = [dict objectForKey:@"pathimage"];
        _arrange = [[dict objectForKey:@"arrange"] intValue];
        _md5 = [dict objectForKey:@"md5"];
        NSString *latitude = [dict objectForKey:@"latitude"];
        NSString *longtitude = [dict objectForKey:@"longtitude"];
        
        _detail_list = [[NSMutableArray alloc] init];
        NSArray *arr = (NSArray *)[dict objectForKey:@"detail_list"];
        DetailListObj *detail = nil;
        for (NSDictionary *d in arr) {
            detail = [[DetailListObj alloc] initWithProgramDict:d];
            [_detail_list addObject:detail];
        }
        
        if(!_id_diadiem || [_id_diadiem isKindOfClass:[NSNull class]])
            _id_diadiem = @"";
        if(!_diadiem_name || [_diadiem_name isKindOfClass:[NSNull class]])
            _diadiem_name = @"";
        if(!_diadiem_content || [_diadiem_content isKindOfClass:[NSNull class]])
            _diadiem_content = @"";
        if(!_md5 || [_md5 isKindOfClass:[NSNull class]])
            _md5 = @"";
        if(!_pLinkImage || [_pLinkImage isKindOfClass:[NSNull class]])
            _pLinkImage = @"";
        if(!latitude || [latitude isKindOfClass:[NSNull class]])
            latitude = @"0.0";
        if(!longtitude || [longtitude isKindOfClass:[NSNull class]])
            longtitude = @"0.0";
        _lat = [latitude doubleValue];
        _lng = [longtitude doubleValue];
        
        [self loadImage:_id_diadiem andDelegate:_loadImageDelegate];
    }
    
    return self;
}

- (void)updateNewData:(LocationObj *)newObj
{
    _id_diadiem = newObj.id_diadiem;
    _diadiem_name = newObj.diadiem_name;
    _diadiem_content = newObj.diadiem_content;
    _lat = newObj.lat;
    _lng = newObj.lng;
    _pLinkImage = newObj.pLinkImage;
    _pThumb = newObj.pThumb;
    _md5 = newObj.md5;
    
    [_detail_list removeAllObjects];
    [_detail_list addObjectsFromArray:newObj.detail_list];
}

- (void)loadImage:(NSString *)_id andDelegate:(id<LoadImageDelegate>)delegate
{
    _loadImageDelegate = delegate;
    if(_pLinkImage.length == 0){
        NSString *imgName = [NSString stringWithFormat:@"test_thumb0.png"];
        UIImage *image = [UIImage imageNamed:imgName];
        _pThumb = image;
        [_loadImageDelegate loadImageLocationFinish:_id andIndex:0 andImage:image];
        return;
    }
    
    
    NSString *link = [_pLinkImage stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [self getPhoto:_id andLink:[NSURL URLWithString:link] andIndex:0];
}

- (void)getPhoto:(NSString *)_id andLink:(NSURL *) link andIndex:(NSInteger)index
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:link];
    request.cachePolicy = NSURLRequestReturnCacheDataElseLoad;
    AFHTTPRequestOperation *posterOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    posterOperation.responseSerializer = [AFImageResponseSerializer serializer];
    posterOperation.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"image/jpg", @"image/png", @"image/jpeg", nil];
    [posterOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id image) {
        _pThumb = image;
        
        if(_loadImageDelegate)
            [_loadImageDelegate loadImageLocationFinish:_id andIndex:index andImage:image];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //NSLog(@"Now showing poster request failed with error: %@", error);
    }];
    [posterOperation start];
}

@end
