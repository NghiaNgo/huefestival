//
//  CalendarEventObj.m
//  Festival2016
//
//  Created by Phan Phuoc Luong on 11/16/15.
//  Copyright © 2015 Phan Phuoc Luong. All rights reserved.
//

#import "CalendarEventObj.h"

@implementation CalendarEventObj

- (id)initWithDict:(NSDictionary *)dict
{
    if(self = [super init]){
        _fDate = [dict objectForKey:@"fdate"];
        _md5 = [dict objectForKey:@"md5"];
        
        if(!_fDate || [_fDate isKindOfClass:[NSNull class]])
            _fDate = @"2016-05-01";
        if(!_md5 || [_md5 isKindOfClass:[NSNull class]])
            _md5 = @"";
        
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setDateFormat:@"yyyy-MM-dd"];
        NSDate *date = [format dateFromString:_fDate];
        [format setDateFormat:@"dd/MM/yyyy"];
        NSString *s = [format stringFromDate:date];
        date = [format dateFromString:s];
        
        [format setDateFormat:@"dd"];
        NSString *strDay = [format stringFromDate:date];
        _cDD = strDay;
        
        if(![MyAppDelegate().language isEqualToString:@"en"]){
            [format setDateFormat:@"MM"];
            strDay = [NSString stringWithFormat:@"THÁNG %@", [format stringFromDate:date]];
        } else {
            [format setDateFormat:@"LLLL"];
            strDay = [[format stringFromDate:date] uppercaseString];
        }
        _cMM = strDay;
        
        [format setDateFormat:@"EEEE"];
        strDay = [[format stringFromDate:date] uppercaseString];
        if(![MyAppDelegate().language isEqualToString:@"en"]) {
            if([strDay isEqualToString:@"MONDAY"])
                strDay = @"THỨ 2";
            if([strDay isEqualToString:@"TUESDAY"])
                strDay = @"THỨ 3";
            if([strDay isEqualToString:@"WEDNESDAY"])
                strDay = @"THỨ 4";
            if([strDay isEqualToString:@"THURSDAY"])
                strDay = @"THỨ 5";
            if([strDay isEqualToString:@"FRIDAY"])
                strDay = @"THỨ 6";
            if([strDay isEqualToString:@"SATURDAY"])
                strDay = @"THỨ 7";
            if([strDay isEqualToString:@"SUNDAY"])
                strDay = @"CHỦ NHẬT";
        }
        _cLL = strDay;
        
        
        ///List Detail
        [format setDateFormat:@"MM"];
        NSString *month = [format stringFromDate:date];
        
        NSArray *arrDict = (NSArray *)[dict objectForKey:@"detail_list"];
        DetailListObj *subObj = nil;
        _calendar_detail_list = [[NSMutableArray alloc] initWithCapacity:arrDict.count];
        for(NSDictionary *d in arrDict){
            subObj = [[DetailListObj alloc] initWithCalendarDict:d];
            subObj.day = [NSString stringWithFormat:@"%@/%@",_cDD, month];
            [_calendar_detail_list addObject:subObj];
        }
        _numProgram = (int)_calendar_detail_list.count;
    }
    
    return self;
}

- (void)updateData:(CalendarEventObj *)newObj
{
    _fDate = newObj.fDate;
    _md5 = newObj.md5;
    
    _cDD = newObj.cDD;
    _cMM = newObj.cMM;
    _cLL = newObj.cLL;
    [_calendar_detail_list removeAllObjects];
    [_calendar_detail_list addObjectsFromArray:newObj.calendar_detail_list];
    _numProgram = newObj.numProgram;
}
@end
