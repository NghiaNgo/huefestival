//
//  ProgramObj.m
//  Festival2016
//
//  Created by Phan Phuoc Luong on 11/9/15.
//  Copyright © 2015 Phan Phuoc Luong. All rights reserved.
//

#import "ProgramObj.h"

@implementation ProgramObj

- (id)initWithDict:(NSDictionary *)dict
{
    if(self = [super init]){
        _pID = [dict objectForKey:@"id_chuongtrinh"];
        _pName = [dict objectForKey:@"chuongtrinh_name"];
        _pContent = [dict objectForKey:@"chuongtrinh_content"];
        _pPrice = [dict objectForKey:@"price"];
        _pType = [[dict objectForKey:@"type_program"] intValue];
        _type = [[dict objectForKey:@"type_inoff"] intValue];
        _md5 = [dict objectForKey:@"md5"];
        
        _detail_list = [[NSMutableArray alloc] init];
        NSArray *arr = (NSArray *)[dict objectForKey:@"detail_list"];
        DetailListObj *detail = nil;
        for (NSDictionary *d in arr) {
            detail = [[DetailListObj alloc] initWithProgramDict:d];
            [_detail_list addObject:detail];
        }
        
        if(!_pID || [_pID isKindOfClass:[NSNull class]])
            _pID = @"";
        if(!_pName || [_pName isKindOfClass:[NSNull class]])
            _pName = @"";
        if(!_pContent || [_pContent isKindOfClass:[NSNull class]])
            _pContent = @"";
        if(!_pPrice || [_pPrice isKindOfClass:[NSNull class]])
            _pPrice = @"0";
        if(!_md5 || [_md5 isKindOfClass:[NSNull class]])
            _md5 = @"";
    
        //load image
        _arrImage = [[NSMutableArray alloc] init];
        UIImage *image = [UIImage imageNamed:@"test_thumb0.png"];
        _arrLinkImage = [[NSMutableArray alloc] init];
        NSDictionary *arrImages = [dict objectForKey:@"pathimage_list"];
        for(NSString *str in arrImages){
            [_arrLinkImage addObject:str];
            [_arrImage addObject:image];
        }
        
        [self loadImage:_pID andDelegate:_loadImageDelegate];
    }
    
    return self;
}

- (void)updateNewData:(ProgramObj *)newObj
{
    _pID = newObj.pID;
    _pName = newObj.pName;
    _pContent = newObj.pContent;
    _pPrice = newObj.pPrice;
    _type = newObj.type;
    _md5 = newObj.md5;
    
    [_detail_list removeAllObjects];
    [_detail_list addObjectsFromArray:newObj.detail_list];
    
    //[_arrLinkImage removeAllObjects];
    //[_arrLinkImage addObjectsFromArray:newObj.arrLinkImage];
    
    //[_arrImage removeAllObjects];
    //[_arrImage addObjectsFromArray:newObj.arrImage];
}

- (void)loadImage:(NSString *)_id andDelegate:(id<LoadImageDelegate>)delegate
{
    _loadImageDelegate = delegate;
    if(_arrLinkImage.count == 0){
        UIImage *image = [UIImage imageNamed:@"test_thumb0.png"];
        [_arrImage addObject:image];
        [_loadImageDelegate loadImageProgramFinish:_id andIndex:0 andImage:image];
        return;
    }
    
    NSInteger index = 0;
    for(NSString *strLink in _arrLinkImage){
        NSString *link = [strLink stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [self getPhoto:_id andLink:[NSURL URLWithString:link] andIndex:index];
        index++;
    }
}

- (void)getPhoto:(NSString *)_id andLink:(NSURL *) link andIndex:(NSInteger)index
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:link];
    request.cachePolicy = NSURLRequestReturnCacheDataElseLoad;
    AFHTTPRequestOperation *posterOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    posterOperation.responseSerializer = [AFImageResponseSerializer serializer];
    posterOperation.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"image/jpg", @"image/png", @"image/jpeg", nil];
    [posterOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id image) {
        if(index < _arrImage.count)
            [_arrImage replaceObjectAtIndex:index withObject:image];
        else if(_arrImage.count < _arrLinkImage.count)
            [_arrImage addObject:image];
        
        if(_loadImageDelegate)
            [_loadImageDelegate loadImageProgramFinish:_id andIndex:index andImage:image];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //NSLog(@"Now showing poster request failed with error: %@", error);
    }];
    [posterOperation start];
}
@end
