//
//  MenuObj.h
//  Festival2016
//
//  Created by Phan Phuoc Luong on 2/16/16.
//  Copyright © 2016 Phan Phuoc Luong. All rights reserved.
//

#import "MenuSubObj.h"

@interface MenuObj : NSObject

- (id) initWithDict:(NSDictionary *)dict;
- (id) initWithDictForAbout:(NSDictionary *)dict;

@property (nonatomic, strong) id<LoadImageDelegate> loadImageDelegate;

@property (nonatomic, assign) int mID;
@property (nonatomic, strong) NSString *mTitle;
@property (nonatomic, strong) NSString *mLinkPath;
@property (nonatomic, assign) int mTypeData;
@property (nonatomic, strong) NSMutableArray *mSubMenu;
@property (nonatomic, strong) UIImage *menuIcon;
@end
