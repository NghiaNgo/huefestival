//
//  RemiderManager.m
//  Festival2016
//
//  Created by Phan Phuoc Luong on 11/5/15.
//  Copyright © 2015 Phan Phuoc Luong. All rights reserved.
//

#import "RemiderManager.h"

@implementation RemiderManager

- (id)initReminderManager
{
    if(self = [super init]){
        if(!_eventStore)
            _eventStore = [[EKEventStore alloc] init];
    }
    return self;
}

- (void)addNewReminder
{
    [_eventStore requestAccessToEntityType:EKEntityTypeReminder completion:^(BOOL granted, NSError *error) {
        if (granted)
        {
            EKReminder *reminder = [EKReminder reminderWithEventStore:_eventStore];
            
            NSCalendar *gregorian = [NSCalendar currentCalendar];
            
            //input date from server
            NSDate *currentTime = [NSDate date];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSString *timeStr = [dateFormatter stringFromDate:currentTime];
            
            //convert to date
            NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
            [inputFormatter setCalendar:gregorian];
            [inputFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSDate *parseDate = [inputFormatter dateFromString:timeStr];
            
            NSDateComponents *dateComponents = [gregorian components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:parseDate];
            NSDateComponents *timeComponents = [gregorian components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:parseDate];
            NSDateComponents *dateComps = [[NSDateComponents alloc] init];
            int year = (int)[dateComponents year];
            int month = (int) [dateComponents month];
            int day = (int) [dateComponents day];
            
            int hour = (int)[timeComponents hour];
            int minute = (int) [timeComponents minute];
            int seconds = (int) [timeComponents second];
            
            [dateComps setDay:day];
            [dateComps setMonth:month];
            [dateComps setYear:year];
            [dateComps setHour:hour];
            [dateComps setMinute:minute];
            [dateComps setSecond:seconds];
            [dateComps setTimeZone:[NSTimeZone localTimeZone]];
            
            //for alarm
            timeStr = [NSString stringWithFormat:@"%i-%i-%i %i:%i:%i", year, month, day, hour, minute + 5, seconds];
            NSDate *date = [inputFormatter dateFromString:timeStr];
            EKAlarm *alarm = [EKAlarm alarmWithAbsoluteDate:date];
            //set evetnt
            reminder.title = [NSString stringWithFormat:@"Reminder %@", timeStr];
            //reminder.startDateComponents = dateComponents;
            reminder.calendar = [_eventStore defaultCalendarForNewReminders];
            [reminder addAlarm:alarm];
            
            NSError *error = nil;
            [_eventStore saveReminder:reminder commit:YES error:&error];
            if (error)
                NSLog(@"error = %@", error);
        } else {
            [UIAlertView showWithTitle:MYLocalizedString(@"alert.title", nil) message:MYLocalizedString(@"reminder.notallow", nil) handler:nil];
        }
    }];
}
@end
