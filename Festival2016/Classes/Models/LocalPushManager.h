//
//  LocalPushManager.h
//  Festival2016
//
//  Created by Phan Phuoc Luong on 11/6/15.
//  Copyright © 2015 Phan Phuoc Luong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocalPushManager : NSObject

- (void)addNewEvent:(NSString *)notificationID andDate:(NSString *)strDate alertTitle:(NSString *)title alertBody:(NSString *)alertBody action:(NSString *)action andProgramID:(NSString *)pID andBefore:(int) before;

- (void)updateEvent:(NSString *)notificationID andDate:(NSString *)strDate alertBody:(NSString *)alertBody andProgramID:(NSString *)pID;

- (void) canceEvent:(NSString*)notificationID;

- (void) canceAllEvent;

- (NSMutableArray *) getAllEvent;

@end
