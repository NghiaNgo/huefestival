//
//  CategoryObj.m
//  MyStudentSurvey
//
//  Created by Phan Phuoc Luong on 3/10/15.
//  Copyright (c) 2015 Phan Phuoc Luong. All rights reserved.
//

#import "CategoryObj.h"

@implementation CategoryObj

- (id)initWithDict:(NSDictionary *)dict
{
    if(self = [super init]){
        _isLoading = YES;
        _isSelected = NO;
        if(nil != [dict objectForKey: @"category_approved"]){
            _cID = [[dict objectForKey: @"category_approved"] intValue];
        }
        
        if(nil != [dict objectForKey:@"category_name"]){
            _cName = [dict objectForKey:@"category_name"];
        }
        
        if(nil != [dict objectForKey:@"icon"]){
            _cLinkIcon = [dict objectForKey:@"icon"];
        }
    }
    
    return self;
}

- (id)initWithData:(int)cID andName:(NSString *)name andIcon:(UIImage *)img
{
    if(self = [super init]){
        _isLoading = YES;
        _isSelected = NO;
        
        _cID = cID;
        _cName = name;
        _cImage = img;
    }
    
    return self;
}

- (void)getPhoto:(int)cID andLink:(NSURL *) link
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:link];
    request.cachePolicy = NSURLRequestReturnCacheDataElseLoad;
    AFHTTPRequestOperation *posterOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    posterOperation.responseSerializer = [AFImageResponseSerializer serializer];
    [posterOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id image) {
        _cImage = image;
        [_delegate loadImageFinish:image andCID:cID];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
       //NSLog(@"Now showing poster request failed with error: %@", error);
    }];
    [posterOperation start];
}

@end
