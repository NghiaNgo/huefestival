//
//  MenuSubObj.m
//  Festival2016
//
//  Created by Phan Phuoc Luong on 2/16/16.
//  Copyright © 2016 Phan Phuoc Luong. All rights reserved.
//

#import "MenuSubObj.h"

@implementation MenuSubObj

- (id)initWithDict:(NSDictionary *)dict
{
    if(self = [super init]){
        _sID = [[dict objectForKey:@"id"] intValue];
        _sTitle = [dict objectForKey:@"title"];
        _sTypeID = [[dict objectForKey:@"ptypeid"] intValue];
        _sTypeData = [[dict objectForKey:@"typedata"] intValue];
        _sLinkPath = [dict objectForKey:@"pathicon"];
        
        if(!_sTitle || [_sTitle isKindOfClass:[NSNull class]])
            _sTitle = @"";
        if(!_sLinkPath || [_sLinkPath isKindOfClass:[NSNull class]])
            _sLinkPath = @"";
        
        [self loadImage:[NSString stringWithFormat:@"%i", _sID] andIndex:_sTypeID andDelegate:_loadImageDelegate];
    }
    
    return self;
}

- (void)loadImage:(NSString *)_id andIndex:(int)index andDelegate:(id<LoadImageDelegate>)delegate
{
    _loadImageDelegate = delegate;
    if(_sLinkPath.length == 0){
        UIImage *image = [UIImage imageNamed:@"test_thumb0"];
        _subIcon = image;
        [_loadImageDelegate loadImageMenuListFinish:_id andSubID:index andImage:image];
        return;
    }
    
    NSString *link = [_sLinkPath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [self getPhoto:_id andLink:[NSURL URLWithString:link] andIndex:index];
}

- (void)getPhoto:(NSString *)_id andLink:(NSURL *) link andIndex:(NSInteger)index
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:link];
    request.cachePolicy = NSURLRequestReturnCacheDataElseLoad;
    AFHTTPRequestOperation *posterOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    posterOperation.responseSerializer = [AFImageResponseSerializer serializer];
    posterOperation.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"image/jpg", @"image/png", @"image/jpeg", nil];
    [posterOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id image) {
        _subIcon = image;
        if(_loadImageDelegate)
            [_loadImageDelegate loadImageMenuListFinish:_id andSubID:index andImage:image];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //NSLog(@"Now showing poster request failed with error: %@", error);
    }];
    [posterOperation start];
}

@end
