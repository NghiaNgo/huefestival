//
//  DetailListObj.h
//  Festival2016
//
//  Created by Phan Phuoc Luong on 11/20/15.
//  Copyright © 2015 Phan Phuoc Luong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DetailListObj : NSObject

- (id) initWithProgramDict:(NSDictionary *)dict;
- (id) initWithArtDict:(NSDictionary *)dict;
- (id) initWithCalendarDict:(NSDictionary *)dict;

//dung cho detail list trong chuong trinh
@property (nonatomic, strong) NSString *doan_name;
@property (nonatomic, assign) int id_doan;
@property (nonatomic, strong) NSString *nhom_name;
@property (nonatomic, assign) int id_nhom;
@property (nonatomic, strong) NSString *fdate;
@property (nonatomic, strong) NSString *tdate;

//dung cho art và calendar
@property (nonatomic, strong) NSString *day;
@property (nonatomic, strong) NSString *id_chuongtrinh;//id chuong trinh
@property (nonatomic, strong) NSString *chuongtrinh_name;//name chuong trinh
@property (nonatomic, assign) int type_inoff;

//dung chung cho ca 3
@property (nonatomic, strong) NSString *time;
@property (nonatomic, strong) NSString *id_diadiem;//id dia diem
@property (nonatomic, strong) NSString *diadiem_name;//nam dia diem

@end
