//
//  TicketLocationObj.h
//  Festival2016
//
//  Created by Phan Phuoc Luong on 1/7/16.
//  Copyright © 2016 Phan Phuoc Luong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TicketLocationObj : NSObject

- (id) initWithDict:(NSDictionary *)dict andIndex:(int)index;
- (void) updateNewData:(TicketLocationObj *)newObj;
- (void)loadImage:(NSString *)_id andIndex:(int)index andDelegate:(id<LoadImageDelegate>)delegate;

@property (nonatomic, strong) id<LoadImageDelegate> loadImageDelegate;
@property (nonatomic, strong) NSString *id_ticketlocation;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *content;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *pLinkImage;
@property (nonatomic, strong) UIImage *pThumb;
@property (nonatomic, assign) double lat;
@property (nonatomic, assign) double lng;
@property (nonatomic, strong) NSString *md5;

@property (nonatomic, strong) NSDictionary *dict;

//su dung cho muc dia diem
@property (nonatomic, strong) NSString *sumary;
@property (nonatomic, strong) NSString *website;
@property (nonatomic, assign) int typedata;

@end
