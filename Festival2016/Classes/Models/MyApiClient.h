//
//  MyApiClient.h
//  FunnyClips
//
//  Created by Phan Phuoc Luong on 8/16/14.
//  Copyright (c) 2014 Phan Phuoc Luong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPRequestOperationManager.h"

@interface MyApiClient : AFHTTPRequestOperationManager


/**
 * ===================
 *  for admin server
 * ===================
 */

+ (instancetype) sharedClient;

//lay danh sach chuong trinh bieu dien
- (void) getAllPrograms:(NSString *)datas andSuccesBlock:(void (^)(AFHTTPRequestOperation *, id))successBlock andFailBlock:(void (^)(AFHTTPRequestOperation *, NSError *))failBlock;

//lay danh sach lich dien
- (void) getAllCalendarEvent:(NSString *)datas andSuccesBlock:(void (^)(AFHTTPRequestOperation *, id))successBlock andFailBlock:(void (^)(AFHTTPRequestOperation *, NSError *))failBlock;

//lay danh sach cac doan nghe thuat
- (void) getAllArt:(NSString *)datas andSuccesBlock:(void (^)(AFHTTPRequestOperation *, id))successBlock andFailBlock:(void (^)(AFHTTPRequestOperation *, NSError *))failBlock;


//lay danh sach cac diem bieu dien
- (void) getAllLocation:(NSString *)datas andSuccesBlock:(void (^)(AFHTTPRequestOperation *, id))successBlock andFailBlock:(void (^)(AFHTTPRequestOperation *, NSError *))failBlock;

//lay danh sach tin tuc
- (void) getNews:(NSInteger) page numItem:(NSInteger)number andSuccesBlock:(void (^)(AFHTTPRequestOperation *, id))successBlock andFailBlock:(void (^)(AFHTTPRequestOperation *, NSError *))failBlock;

//lay chi tiet 1 tin tuc
- (void) getNewsDetail:(NSString *)news_id andSuccesBlock:(void (^)(AFHTTPRequestOperation *, id))successBlock andFailBlock:(void (^)(AFHTTPRequestOperation *, NSError *))failBlock;

//lấy danh sách các điểm bán vé
- (void) getTicketLocations:(NSString *)datas andSuccesBlock:(void (^)(AFHTTPRequestOperation *, id))successBlock andFailBlock:(void (^)(AFHTTPRequestOperation *, NSError *))failBlock;

//lấy danh sách giá vé
- (void) getTicketPrice:(NSString *)datas andSuccesBlock:(void (^)(AFHTTPRequestOperation *, id))successBlock andFailBlock:(void (^)(AFHTTPRequestOperation *, NSError *))failBlock;

//lây danh sach cac menu con
- (void) getMenuList:(void (^)(AFHTTPRequestOperation *, id))successBlock andFailBlock:(void (^)(AFHTTPRequestOperation *, NSError *))failBlock;

//lây danh sach cac bai viet trong dia diem
- (void) getListByID:(int)cate_id andTypeData:(int)typedata andPageIndex:(int)page_index andPageSize:(int)page_size andLat:(double)latitude andLng:(double)longtitude andSuccesBlock:(void (^)(AFHTTPRequestOperation *, id))successBlock andFailBlock:(void (^)(AFHTTPRequestOperation *, NSError *))failBlock;

//lây chi tiet 1 dia diem
- (void) getServiceDetail:(int)service_id andTypeData:(int)typedata andSuccesBlock:(void (^)(AFHTTPRequestOperation *, id))successBlock andFailBlock:(void (^)(AFHTTPRequestOperation *, NSError *))failBlock;

//lây danh sach menu hỗ trợ
- (void) getAboutList:(void (^)(AFHTTPRequestOperation *, id))successBlock andFailBlock:(void (^)(AFHTTPRequestOperation *, NSError *))failBlock;

//lấy thôgn tin chi tiết cho 1 menu hỗ trợ
- (void) getAboutDetail:(int)about_id andSuccesBlock:(void (^)(AFHTTPRequestOperation *, id))successBlock andFailBlock:(void (^)(AFHTTPRequestOperation *, NSError *))failBlock;
@end
