//
//  CategoryObj.h
//  MyStudentSurvey
//
//  Created by Phan Phuoc Luong on 3/10/15.
//  Copyright (c) 2015 Phan Phuoc Luong. All rights reserved.
//

@protocol CategoryDelegate <NSObject>

@optional
- (void) loadImageFinish:(UIImage *)img andCID:(int) cID;

@end

@interface CategoryObj : NSObject

- (id) initWithDict:(NSDictionary *) dict;
- (id) initWithData:(int) cID andName:(NSString *) name andIcon:(UIImage *) img;

@property (nonatomic, strong) id<CategoryDelegate> delegate;
@property (nonatomic, assign) int cID;
@property (nonatomic, strong) NSString *cName;
@property (nonatomic, strong) NSString *cLinkIcon;
@property (nonatomic, strong) UIImage *cImage;
@property (nonatomic, assign) BOOL isLoading;
@property (nonatomic, assign) BOOL isSelected;

- (void)getPhoto:(int)cID andLink:(NSURL *) link;

@end
