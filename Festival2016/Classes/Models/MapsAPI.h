//
//  MapsAPI.h
//  Festival2016
//
//  Created by Phan Phuoc Luong on 1/18/16.
//  Copyright © 2016 Phan Phuoc Luong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPRequestOperationManager.h"

@interface MapsAPI : AFHTTPRequestOperationManager

+ (instancetype) sharedClient;

- (void) searchPlaceAPI:(double)lat andLng:(double)lng andRadius:(int) radius andType:(NSString *)type andName:(NSString *)name andSuccesBlock:(void (^)(AFHTTPRequestOperation *, id))successBlock andFailBlock:(void (^)(AFHTTPRequestOperation *, NSError *))failBlock;

- (void) searchDirectionAPI:(NSString *)fromLocation andTo:(NSString *)toLocation andSuccesBlock:(void (^)(AFHTTPRequestOperation *, id))successBlock andFailBlock:(void (^)(AFHTTPRequestOperation *, NSError *))failBlock;

@end
