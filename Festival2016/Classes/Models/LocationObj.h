//
//  LocationObj.h
//  Festival2016
//
//  Created by Phan Phuoc Luong on 11/20/15.
//  Copyright © 2015 Phan Phuoc Luong. All rights reserved.
//

#import "DetailListObj.h"

@interface LocationObj : NSObject

- (id) initWithDict:(NSDictionary *)dict;
- (void) updateNewData:(LocationObj *)newObj;
- (void)loadImage:(NSString *)_id andDelegate:(id<LoadImageDelegate>)delegate;

@property (nonatomic, strong) id<LoadImageDelegate> loadImageDelegate;
@property (nonatomic, strong) NSString *id_diadiem;
@property (nonatomic, strong) NSString *diadiem_name;
@property (nonatomic, strong) NSString *diadiem_content;
@property (nonatomic, strong) NSString *md5;
@property (nonatomic, strong) NSMutableArray *detail_list;
//bo sung
@property (nonatomic, strong) NSString *pLinkImage;
@property (nonatomic, strong) UIImage *pThumb;
@property (nonatomic, assign) double lat;
@property (nonatomic, assign) double lng;
@property (nonatomic, assign) int arrange;

@end
