//
//  DetailListObj.m
//  Festival2016
//
//  Created by Phan Phuoc Luong on 11/20/15.
//  Copyright © 2015 Phan Phuoc Luong. All rights reserved.
//

#import "DetailListObj.h"

@implementation DetailListObj

//parse cho list detail trong danh sach chuong trinh
- (id)initWithProgramDict:(NSDictionary *)dict
{
    if(self = [super init]){
        _id_diadiem = [dict objectForKey:@"id_diadiem"];
        _diadiem_name = [dict objectForKey:@"diadiem_name"];
        _id_doan = [[dict objectForKey:@"id_doan"] intValue];
        _doan_name = [dict objectForKey:@"doan_name"];
        _id_nhom = [[dict objectForKey:@"id_nhom"] intValue];
        _nhom_name = [dict objectForKey:@"nhom_name"];
        _fdate = [dict objectForKey:@"fdate"];
        _tdate = [dict objectForKey:@"tdate"];
        _time = [dict objectForKey:@"time"];
        
        if(!_diadiem_name || [_diadiem_name isKindOfClass:[NSNull class]])
            _diadiem_name = @"";
        if(!_doan_name || [_doan_name isKindOfClass:[NSNull class]])
            _doan_name = @"";
        if(!_nhom_name || [_nhom_name isKindOfClass:[NSNull class]])
            _nhom_name = @"";
        if(!_fdate || [_fdate isKindOfClass:[NSNull class]])
            _fdate = @"";
        if(!_tdate || [_tdate isKindOfClass:[NSNull class]])
            _tdate = @"";
        if(!_time || [_time isKindOfClass:[NSNull class]])
            _time = @"";
    }
    
    return self;
}

//parser cho list detail trong lich dien
- (id)initWithCalendarDict:(NSDictionary *)dict
{
    if(self = [super init]){
        _time = [dict objectForKey:@"time"];
        _id_chuongtrinh = [dict objectForKey:@"id_chuongtrinh"];
        _chuongtrinh_name = [dict objectForKey:@"chuongtrinh_name"];
        _id_diadiem = [dict objectForKey:@"id_diadiem"];
        _diadiem_name = [dict objectForKey:@"diadiem_name"];
        _type_inoff = [[dict objectForKey:@"type_inoff"] intValue];
        
        if(!_time || [_time isKindOfClass:[NSNull class]])
            _time = @"";
        if(!_id_chuongtrinh || [_id_chuongtrinh isKindOfClass:[NSNull class]])
            _id_chuongtrinh = @"-1";
        if(!_chuongtrinh_name || [_chuongtrinh_name isKindOfClass:[NSNull class]])
            _chuongtrinh_name = @"";
        if(!_id_diadiem || [_id_diadiem isKindOfClass:[NSNull class]])
            _id_diadiem = @"-1";
        if(!_diadiem_name || [_diadiem_name isKindOfClass:[NSNull class]])
            _diadiem_name = @"";
    }
    
    return self;
}

//parser cho list detail trong doan nghe thuat
- (id)initWithArtDict:(NSDictionary *)dict
{
    if(self = [super init]){
        _id_chuongtrinh = [dict objectForKey:@"id_chuongtrinh"];
        _chuongtrinh_name = [dict objectForKey:@"chuongtrinh_name"];
        _id_diadiem = [dict objectForKey:@"id_diadiem"];
        _diadiem_name = [dict objectForKey:@"diadiem_name"];
        
        if(!_id_chuongtrinh || [_id_chuongtrinh isKindOfClass:[NSNull class]])
            _id_chuongtrinh = @"-1";
        if(!_chuongtrinh_name || [_chuongtrinh_name isKindOfClass:[NSNull class]])
            _chuongtrinh_name = @"";
        if(!_id_diadiem || [_id_diadiem isKindOfClass:[NSNull class]])
            _id_diadiem = @"-1";
        if(!_diadiem_name || [_diadiem_name isKindOfClass:[NSNull class]])
            _diadiem_name = @"";
    }
    
    return self;
}
@end
