//
//  ArtObj.h
//  Festival2016
//
//  Created by Phan Phuoc Luong on 11/16/15.
//  Copyright © 2015 Phan Phuoc Luong. All rights reserved.
//

#import "DetailListObj.h"

@interface ArtObj : NSObject

- (id) initWithDict:(NSDictionary *)dict;
- (void) updateNewData:(ArtObj *)newArt;
- (void) loadArtImage:(NSString *)_id andDelegate:(id<LoadImageDelegate>)delegate;

@property (nonatomic, strong) id<LoadImageDelegate> loadImageDelegate;
@property (nonatomic, strong) NSString *aID;
@property (nonatomic, strong) NSString *md5;
@property (nonatomic, strong) NSString *aName;
@property (nonatomic, strong) NSString *aContent;
@property (nonatomic, strong) NSMutableArray *arrImageLink;
@property (nonatomic, strong) NSMutableArray *arrImage;
@property (nonatomic, strong) NSMutableArray *art_detail_list;

@end
