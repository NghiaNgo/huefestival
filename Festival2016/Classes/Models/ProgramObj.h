//
//  ProgramObj.h
//  Festival2016
//
//  Created by Phan Phuoc Luong on 11/9/15.
//  Copyright © 2015 Phan Phuoc Luong. All rights reserved.
//

#import "DetailListObj.h"

@interface ProgramObj : NSObject

- (id) initWithDict:(NSDictionary *)dict;
- (void) updateNewData:(ProgramObj *)newObj;
- (void)loadImage:(NSString *)_id andDelegate:(id<LoadImageDelegate>)delegate;

@property (nonatomic, strong) id<LoadImageDelegate> loadImageDelegate;
@property (nonatomic, strong) NSString *pID;
@property (nonatomic, strong) NSString *pName;
@property (nonatomic, strong) NSString *pContent;
@property (nonatomic, strong) NSString *pPrice;
@property (nonatomic, assign) int pType;//0 bt, 1 tieu diem, 2 bieu dien nghe thuat, 3 cong dong
@property (nonatomic, strong) NSString *md5;
@property (nonatomic, strong) NSMutableArray *arrLinkImage;
@property (nonatomic, strong) NSMutableArray *arrImage;
@property (nonatomic, strong) NSMutableArray *detail_list;
@property (nonatomic, assign) int type;//1 IN, 0 OFF

@end
