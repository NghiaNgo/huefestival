//
//  LocalPushManager.m
//  Festival2016
//
//  Created by Phan Phuoc Luong on 11/6/15.
//  Copyright © 2015 Phan Phuoc Luong. All rights reserved.
//

#import "LocalPushManager.h"

@implementation LocalPushManager

- (void)addEvent:(NSString *)strDate alertTitle:(NSString *)title alertBody:(NSString *)alertBody action:(NSString *)action notificationID:(NSString *)notificationID andProgramID:(NSString *)pID andBefore:(int)before
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    NSDate *parseDate = [dateFormatter dateFromString:strDate];
    
    NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:parseDate];
    NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:parseDate];
    NSDateComponents *dateComps = [[NSDateComponents alloc] init];
    int year = (int)[dateComponents year];
    int month = (int) [dateComponents month];
    int day = (int) [dateComponents day];
    int hour = (int)[timeComponents hour];
    int minute = (int) [timeComponents minute];
    int seconds = (int) [timeComponents second];
    //nếu ngày có 00:00:00
    if(hour == 0 && minute == 0){
        hour = 7;
        minute = 30;
    }
    
    if(before >= 60){
        --hour;
        if(hour < 0){
            --day;
            hour += 24;
        }
    } else {
        minute -= before;
        if(minute < 0){
            hour -= 1;
            if(hour < 0){
                --day;
                hour += 24;
            }
            minute += 60;
        }
    }
    
    [dateComps setDay:day];
    [dateComps setMonth:month];
    [dateComps setYear:year];
    [dateComps setHour:hour];
    [dateComps setMinute:minute];
    [dateComps setSecond:seconds];
    
    NSDate *itemDate = [calendar dateFromComponents:dateComps];
    
    UILocalNotification *localNotif = [[UILocalNotification alloc] init];
    if (localNotif == nil)
        return;
    localNotif.fireDate = [itemDate dateByAddingTimeInterval:-1];
    localNotif.timeZone = [NSTimeZone defaultTimeZone];
    localNotif.repeatInterval = NSYearCalendarUnit;
    localNotif.alertBody = alertBody;
    localNotif.alertAction = action;
    //localNotif.alertTitle = title;
    
    localNotif.soundName = UILocalNotificationDefaultSoundName;
    localNotif.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
    
    //lấy ngày giờ của alarm
    NSString *strDay = [NSString stringWithFormat:@"%i/%i/%i", day, month, year];
    NSString *strTime = [NSString stringWithFormat:@"%@%i:%@%i", hour < 10?@"0":@"", hour, minute < 10?@"0":@"", minute];
    
    NSDictionary *infoDict = [NSDictionary dictionaryWithObjectsAndKeys:notificationID , @"id", pID, @"program_id", alertBody, @"content", strDay, @"strDay", strTime, @"strTime", nil];
    localNotif.userInfo = infoDict;
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotif];
}

- (void)updateEvent:(NSString *)strDate alertBody:(NSString *)alertBody notificationID:(NSString *)notificationID andProgramID:(NSString *)pID
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm dd/MM/yyyy"];
    
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    NSDate *parseDate = [dateFormatter dateFromString:strDate];
    
    NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:parseDate];
    NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:parseDate];
    NSDateComponents *dateComps = [[NSDateComponents alloc] init];
    int year = (int)[dateComponents year];
    int month = (int) [dateComponents month];
    int day = (int) [dateComponents day];
    int hour = (int)[timeComponents hour];
    int minute = (int) [timeComponents minute];
    int seconds = (int) [timeComponents second];
    
    [dateComps setDay:day];
    [dateComps setMonth:month];
    [dateComps setYear:year];
    [dateComps setHour:hour];
    [dateComps setMinute:minute];
    [dateComps setSecond:seconds];
    
    NSDate *itemDate = [calendar dateFromComponents:dateComps];
    
    UILocalNotification *localNotif = [[UILocalNotification alloc] init];
    if (localNotif == nil)
        return;
    localNotif.fireDate = [itemDate dateByAddingTimeInterval:-1];
    localNotif.timeZone = [NSTimeZone defaultTimeZone];
    localNotif.repeatInterval = NSYearCalendarUnit;
    localNotif.alertBody = alertBody;
    localNotif.alertAction = MYLocalizedString(@"view", nil);
    
    localNotif.soundName = UILocalNotificationDefaultSoundName;
    localNotif.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
    
    //lấy ngày giờ của alarm
    NSString *strDay = [NSString stringWithFormat:@"%i/%i/%i", day, month, year];
    NSString *strTime = [NSString stringWithFormat:@"%@%i:%@%i", hour < 10?@"0":@"", hour, minute < 10?@"0":@"", minute];
    
    NSDictionary *infoDict = [NSDictionary dictionaryWithObjectsAndKeys:notificationID , @"id", pID, @"program_id", alertBody, @"content", strDay, @"strDay", strTime, @"strTime", nil];
    localNotif.userInfo = infoDict;
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotif];
}

- (void)addNewEvent:(NSString *)notificationID andDate:(NSString *)strDate alertTitle:(NSString *)title alertBody:(NSString *)alertBody action:(NSString *)action andProgramID:(NSString *)pID andBefore:(int) before
{
    for(UILocalNotification *aNotif in [[UIApplication sharedApplication] scheduledLocalNotifications]) {
        if([[aNotif.userInfo objectForKey:@"id"] isEqualToString:notificationID]) {
            [[UIApplication sharedApplication]cancelLocalNotification:aNotif];
            break;
        }
    }
    
    [self addEvent:strDate alertTitle:title alertBody:alertBody action:action notificationID:notificationID andProgramID:pID andBefore:before];
}

- (void)updateEvent:(NSString *)notificationID andDate:(NSString *)strDate alertBody:(NSString *)alertBody andProgramID:(NSString *)pID
{
    for(UILocalNotification *aNotif in [[UIApplication sharedApplication] scheduledLocalNotifications]) {
        if([[aNotif.userInfo objectForKey:@"id"] isEqualToString:notificationID]) {
            [[UIApplication sharedApplication]cancelLocalNotification:aNotif];
            break;
        }
    }
    
    [self updateEvent:strDate alertBody:alertBody notificationID:notificationID andProgramID:pID];
}

- (void) canceEvent:(NSString *)notificationID
{
    for(UILocalNotification *aNotif in [[UIApplication sharedApplication] scheduledLocalNotifications]) {
        if([[aNotif.userInfo objectForKey:@"id"] isEqualToString:notificationID]) {
            [[UIApplication sharedApplication]cancelLocalNotification:aNotif];
            break;
        }
    }
}

- (void)canceAllEvent
{
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
}

- (NSMutableArray *) getAllEvent {
    NSMutableArray *arrIDs = [[NSMutableArray alloc] init];
    for(UILocalNotification *aNotif in [[UIApplication sharedApplication] scheduledLocalNotifications]) {
        [arrIDs addObject:aNotif.userInfo];
    }
    
    return arrIDs;
}

- (BOOL) checkNotificationByID:(NSString *)notificationID
{
    for(UILocalNotification *aNotif in [[UIApplication sharedApplication] scheduledLocalNotifications]) {
        if([[aNotif.userInfo objectForKey:@"id"] isEqualToString:notificationID]) {
            return YES;
        }
    }
    return NO;
}

@end
