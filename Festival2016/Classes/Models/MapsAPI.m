//
//  MapsAPI.m
//  Festival2016
//
//  Created by Phan Phuoc Luong on 1/18/16.
//  Copyright © 2016 Phan Phuoc Luong. All rights reserved.
//

#import "MapsAPI.h"

@implementation MapsAPI

+ (instancetype)sharedClient
{
    static MapsAPI *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[MapsAPI alloc] initWithBaseURL:[NSURL URLWithString:@"https://maps.googleapis.com"]];
        _sharedClient.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    });
    
    return _sharedClient;
}

///place/nearbysearch/json?location=-33.8670,151.1957&radius=500&types=food&name=cruise&key=
- (void)searchPlaceAPI:(double)lat andLng:(double)lng andRadius:(int)radius andType:(NSString *)type andName:(NSString *)name andSuccesBlock:(void (^)(AFHTTPRequestOperation *, id))successBlock andFailBlock:(void (^)(AFHTTPRequestOperation *, NSError *))failBlock
{
    NSString *path = [NSString stringWithFormat:@"/maps/api/place/nearbysearch/json?location=%lf,%lf&radius=%i&types=%@&name=%@&key=%@", lat, lng, radius, type, name, GOOGLE_API_KEY];
    [self GET:path parameters:nil success:successBlock failure:failBlock];
}

///directions/json?origin=Toronto&destination=Montreal&key=YOUR_API_KEY
- (void)searchDirectionAPI:(NSString *)fromLocation andTo:(NSString *)toLocation andSuccesBlock:(void (^)(AFHTTPRequestOperation *, id))successBlock andFailBlock:(void (^)(AFHTTPRequestOperation *, NSError *))failBlock
{
    NSString *path = [NSString stringWithFormat:@"/maps/api/directions/json?origin=%@&destination=%@&sensor=false&key=%@", fromLocation, toLocation, GOOGLE_API_KEY];
    [self GET:path parameters:nil success:successBlock failure:failBlock];
}
@end
