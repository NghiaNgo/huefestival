
//The MIT License (MIT)
//
//Copyright (c) 2014 Rafał Augustyniak
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of
//this software and associated documentation files (the "Software"), to deal in
//the Software without restriction, including without limitation the rights to
//use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//the Software, and to permit persons to whom the Software is furnished to do so,
//subject to the following conditions:
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
//FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
//IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
//CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

#import "RATableViewCell.h"

@interface RATableViewCell ()

@end

@implementation RATableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
  
    self.selectedBackgroundView = [UIView new];
    self.selectedBackgroundView.backgroundColor = [UIColor clearColor];

    UITapGestureRecognizer *_tapNext = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapNext)];
    [_viewNext addGestureRecognizer:_tapNext];
    
    UITapGestureRecognizer *_tapLocation = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapLocation)];
    [_viewAddress addGestureRecognizer:_tapLocation];
    
    UITapGestureRecognizer *_tapNotification = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapNotification)];
    [_viewTime addGestureRecognizer:_tapNotification];
}

- (void)prepareForReuse
{
  [super prepareForReuse];
  //self.isSelected = NO;
}


- (void)setupProgram:(NSString *)title icon:(UIImage *)img date:(NSString *)strDate time:(NSString *)time address:(NSString *)strAddress isLiked:(BOOL)isLike isExpand:(BOOL)isExpand andProgram:(ProgramObj *)obj
{
    self.backgroundColor = [UIColor whiteColor];
    
    CGRect r = _title.frame;
    CGRect textRect = [title boundingRectWithSize:CGSizeMake(r.size.width - 10, 9999) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica Neue" size:15]} context:nil];
    int h = (int)textRect.size.height;
    if(h > 21){
        h = h + 2;
        if(_isSelected){
            if(h > 100)
                h = 100;
        } else {
            if(h > 42)
                h = 42;
        }
        r.size.height = h;
    }
    _title.frame = r;
    _title.text = title;
    
    _imgIcon.image = img;
    [_btnLike setSelected:isLike];
    [_btnExpand setSelected:isExpand];
    //_lblTimeTitle.text = MYLocalizedString(@"program.start", nil);
    if([time isEqualToString:@"00:00:00"])
        time = MYLocalizedString(@"program.time0", nil);
    _lblTime.text = time;
    //_lblAddressTitle.text = MYLocalizedString(@"location.title", nil);
    _lblAddress.text = strAddress;
    _lblDate.text = strDate;
    _pObj = obj;
    [self refresh];
}

- (void)refresh
{
    if(_isSelected){
        _lblDate.hidden = YES;
        _viewTime.hidden = YES;
        _viewAddress.hidden = YES;
        [_btnExpand setSelected:YES];
        _line.hidden = YES;
    } else {
        _lblDate.hidden = NO;
        _viewTime.hidden = NO;
        _viewAddress.hidden = NO;
        [_btnExpand setSelected:NO];
        _line.hidden = NO;
    }
    
    CGRect r = _title.frame;
    CGRect textRect = [_title.text boundingRectWithSize:CGSizeMake(r.size.width - 10, 9999) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica Neue" size:15]} context:nil];
    int h = (int)textRect.size.height;
    if(h > 21){
        h = h + 2;
        if(_isSelected){
            if(h > 100)
                h = 100;
        } else {
            if(h > 42)
                h = 42;
        }
        r.size.height = h;
    }
    _title.frame = r;
}

- (void)setupSub:(NSString *)strDate time:(NSString *)time address:(NSString *)address index:(int)index isLast:(BOOL)isLast diadiemID:(NSString *)id_diadiem
{
    self.backgroundColor = index%2 == 0? UIColorFromRGB(0xdfe3e6):UIColorFromRGB(0xFFFFFF);

    _lblTimeTitle.text = MYLocalizedString(@"program.start", nil);
    _lblAddressTitle.text = MYLocalizedString(@"location.title", nil);
    
    _lblDate.text = strDate;
    if([time isEqualToString:@"00:00:00"])
        time = MYLocalizedString(@"program.time0", nil);
    _lblTime.text = time;
    _lblAddress.text = address;
    _viewAddress.tag = [id_diadiem intValue];
    
    if(isLast){
        _line.hidden = NO;
    }
}

- (IBAction)clickLike:(id)sender
{
    if(_pObj){
        [_btnLike setSelected:!_btnLike.isSelected];
        [MyAppDelegate() addLikeProgram:_btnLike.isSelected andProgram:_pObj];
        
    }
}

- (void) tapNext
{
    [_delegate callDetail:_pObj];
}

- (void) tapLocation
{
    [_delegate callLocation:[NSString stringWithFormat:@"%i", (int)_viewAddress.tag] object:_pObj];
}

- (void) tapNotification
{
    [_delegate callNotification:_lblTime.text date:_lblDate.text address:_lblAddress.text object:_pObj detail_obj:_detail_obj];
}
@end
