
//The MIT License (MIT)
//
//Copyright (c) 2014 Rafał Augustyniak
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of
//this software and associated documentation files (the "Software"), to deal in
//the Software without restriction, including without limitation the rights to
//use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//the Software, and to permit persons to whom the Software is furnished to do so,
//subject to the following conditions:
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
//FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
//IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
//CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

#import <UIKit/UIKit.h>
#import "FLAnimatedImageView.h"
#import "FLAnimatedImage.h"
#import "ProgramObj.h"
#import "DetailListObj.h"

@protocol RATableDelegate <NSObject>

- (void) callDetail:(ProgramObj *)obj;
- (void) callNotification:(NSString *)time date:(NSString *)strDate address:(NSString *)address object:(ProgramObj *)obj detail_obj:(DetailListObj *)detail_obj;
- (void) callLocation:(NSString *)id_diadiem object:(ProgramObj *)obj;


@end
@interface RATableViewCell : UITableViewCell

@property (nonatomic, copy) void (^additionButtonTapAction)(id sender);
@property (nonatomic) BOOL isSelected;
@property (nonatomic, strong) ProgramObj *pObj;
@property (nonatomic, strong) DetailListObj *detail_obj;
@property (nonatomic, strong) id<RATableDelegate> delegate;
//parent
- (void)setupProgram:(NSString *)title icon:(UIImage *)img date:(NSString *)date time:(NSString *)time address:(NSString *)strDate isLiked:(BOOL) isLike isExpand:(BOOL) isExpand andProgram:(ProgramObj *)obj;
//sub
- (void)setupSub:(NSString *) strDate time:(NSString *)time address:(NSString *)address index:(int)index isLast:(BOOL) isLast diadiemID:(NSString *)id_diadiem;
- (void) refresh;

@property (weak, nonatomic) IBOutlet UIView *viewNext;
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;
@property (weak, nonatomic) IBOutlet UIImageView *line;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UIView *viewTime;
@property (weak, nonatomic) IBOutlet UILabel *lblTimeTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UIView *viewAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblAddressTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UIButton *btnLike;
@property (weak, nonatomic) IBOutlet UIButton *btnExpand;

@end
