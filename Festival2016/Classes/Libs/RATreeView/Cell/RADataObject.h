
//The MIT License (MIT)
//
//Copyright (c) 2013 Rafał Augustyniak
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of
//this software and associated documentation files (the "Software"), to deal in
//the Software without restriction, including without limitation the rights to
//use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//the Software, and to permit persons to whom the Software is furnished to do so,
//subject to the following conditions:
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
//FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
//IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
//CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

#import <Foundation/Foundation.h>
#import "ProgramObj.h"
#import "DetailListObj.h"

@interface RADataObject : NSObject

@property (strong, nonatomic) NSString *pID;
@property (strong, nonatomic) NSString *pName;
@property (strong, nonatomic) NSString *strDate;
@property (strong, nonatomic) NSString *strTime;
@property (strong, nonatomic) NSString *strAddress;
@property (strong, nonatomic) UIImage *imgIcon;
@property (strong, nonatomic) NSMutableArray *children;

@property (assign, nonatomic) BOOL isCanTranfer;
@property (assign, nonatomic) BOOL isLike;
@property (assign, nonatomic) int subIndex;
@property (assign, nonatomic) BOOL isLast;
@property (nonatomic, strong) ProgramObj *obj;
@property (nonatomic, strong) NSString *diadiem_id;
@property (nonatomic, strong) DetailListObj *detail_obj;

@property (assign, nonatomic) BOOL isExpand;
//for parent
- (id)initWithName:(NSString *)pid andName:(NSString *)pname andIcon:(UIImage *)icon andDate:(NSString *)strDate andTime:(NSString *)strTime andAddress:(NSString *)strAddress andLike:(BOOL)islike children:(NSArray *)array;
//for child
- (id)initWithName:(NSString *)strDate andTime:(NSString *)strTime andAddress:(NSString *)strAddress andDiaDiemID:(NSString *)id_dia_diem andIndex:(int)index;

- (void)addChild:(id)child;
- (void)removeChild:(id)child;

@end
