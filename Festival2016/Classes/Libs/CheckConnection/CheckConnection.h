//
//  Connection.h
//  NoozGrab
//
//  Created by nguyen nguyen on 3/2/12.
//  Copyright (c) 2012 GOS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CheckConnection : NSObject
+ (BOOL) isConnected; 
+ (BOOL)hasConnectivity ;
@end
