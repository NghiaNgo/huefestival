//
//  MyTextField.h
//  Rewardz
//
//  Created by Phan Phuoc Luong on 7/23/13.
//  Copyright (c) 2013 Phan Phuoc Luong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTextField : UITextField
{
    BOOL isEnablePadding;
    float paddingLeft;
    float paddingRight;
    float paddingTop;
    float paddingBottom;
}

- (void)setPadding:(BOOL)enable top:(float)top right:(float)right bottom:(float)bottom left:(float)left;
@end
