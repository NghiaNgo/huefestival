//
//  CouchbaseEvents.m
//  Appzitov1.2
//
//  Created by Phan Phuoc Luong on 8/13/15.
//  Copyright (c) 2015 Phan Phuoc Luong. All rights reserved.
//

#import "CouchbaseEvents.h"

@implementation CouchbaseEvents

- (id) init
{
    self = [super init];
    if (self){
        CBObjects *obj = [CBObjects sharedInstance];
        if(!dbNewsLiked || !dbLikedDiaDiem){
            dbNewsLiked     = obj.dbLikeNews;
            dbLikedDiaDiem  = obj.dbLikeDiaDiem;
        }
        
        if([self checkLanguageVN]) {
            if(!dbProgram || !dbCalendar || !dbArt || !dbLocation || !dbTicket){
                dbProgram = obj.dbProgram;
                dbCalendar = obj.dbCalendar;
                dbArt = obj.dbArt;
                dbLocation = obj.dbLocation;
                dbTicket = obj.dbTicket;
            }
        } else {
            if(!dbProgram_en || !dbCalendar_en || !dbArt_en || !dbLocation_en || !dbTicket_en){
                dbProgram_en = obj.dbProgram_en;
                dbCalendar_en = obj.dbCalendar_en;
                dbArt_en = obj.dbArt_en;
                dbLocation_en = obj.dbLocation_en;
                dbTicket_en = obj.dbTicket_en;
            }
        }
    }
    
    return self;
}

+ (CouchbaseEvents *)sharedInstance
{
    static CouchbaseEvents *sharedInstance = nil;
    if (sharedInstance == nil) {
        sharedInstance = [[CouchbaseEvents alloc] init];
    }
    return sharedInstance;
}

//=============== function for Program ===============//
//add a document
- (void) addProgram:(NSDictionary *)dict
{
    //kiem tra app ở trạng thái remove ko
    NSString *md5 = [dict objectForKey:@"md5"];
    if(!md5 || [md5 isKindOfClass:[NSNull class]] || md5.length == 0){
        //call delete
        [self deleteProgram:[dict objectForKey:@"id_chuongtrinh"]];
        return;
    }
    
    NSError *error;
    CBLDocument *doc = nil;
    NSString *program_id = [dict objectForKey:@"id_chuongtrinh"];
    if([self checkLanguageVN]){
        if([self checkDocument:dbProgram andDocumentID:program_id])//đã tồn tại add mới
            [self deleteProgram:program_id];
        doc = [dbProgram documentWithID:program_id];
        [doc putProperties:dict error:&error];
    } else {
        if([self checkDocument:dbProgram_en andDocumentID:program_id])//đã tồn tại add mới
            [self deleteProgram:program_id];
        doc = [dbProgram_en documentWithID:program_id];
        [doc putProperties:dict error:&error];
    }
}

//add a list documents
- (void) addProgramList:(NSArray *)arr_dict
{
    NSError *error;
    CBLDocument *doc = nil;
    for(NSDictionary *dict in arr_dict){
        //get key id from dict
        NSString *program_id = [dict objectForKey:@"id_chuongtrinh"];
        
        //kiem tra app ở trạng thái remove ko
        NSString *md5 = [dict objectForKey:@"md5"];
        if(!md5 || [md5 isKindOfClass:[NSNull class]] || md5.length == 0){
            //app da remove thì cần xoá ở local
            [self deleteProgram:program_id];
            continue;
        }
        
        if([self checkLanguageVN]){
            if([self checkDocument:dbProgram andDocumentID:program_id])//đã tồn tại add mới
                [self deleteProgram:program_id];
            doc = [dbProgram documentWithID:program_id];
            [doc putProperties:dict error:&error];
        } else {
            if([self checkDocument:dbProgram_en andDocumentID:program_id])//đã tồn tại add mới
                [self deleteProgram:program_id];
            doc = [dbProgram_en documentWithID:program_id];
            [doc putProperties:dict error:&error];
        }
    }
}

//delete document
- (BOOL)deleteProgram:(NSString *)program_id
{
    if(![self checkDocument:[self checkLanguageVN] ? dbProgram : dbProgram_en andDocumentID:program_id]){
        //NSLog(@"This document not exist yet");
        return NO;
    }
    
    NSError* error;
    CBLDocument* document = nil;
    if([self checkLanguageVN])
        document = [dbProgram documentWithID:program_id];
    else
        document = [dbProgram_en documentWithID:program_id];
    
    [document deleteDocument:&error];
    
    if (!error) {
        //NSLog(@"Deleted document, deletion status is %d", [document isDeleted]);
        return NO;
    }
    
    return YES;
}

//lưu photo cua chuong trỉnh
- (void) saveProgramPhoto:(NSString *)name andImage:(UIImage *)image andProgramID:(NSString *)program_id
{
    [self deleteProgramPhoto:name andProgramID:program_id];
    
    CBLDocument *doc = nil;
    if([self checkLanguageVN])
        doc = [dbProgram documentWithID:program_id];
    else
        doc = [dbProgram_en documentWithID:program_id];
    
    CBLUnsavedRevision *newRev = [doc.currentRevision createRevision];
    NSData *imageData = nil;
    if([name hasSuffix:@".jpg"] || [name hasSuffix:@".jpeg"]){
        imageData = UIImageJPEGRepresentation(image, 0.75);
        [newRev setAttachmentNamed:name
                   withContentType: @"image/jpeg"
                           content: imageData];
    } else if([name hasSuffix:@".png"]){
        imageData = UIImagePNGRepresentation(image);
        [newRev setAttachmentNamed:name
                   withContentType: @"image/png"
                           content: imageData];
    }
    // (You could also update newRev.properties while you're here)
    if(imageData && newRev){
        NSError *error;
        assert([newRev save: &error]);
    }
}

- (UIImage *)getProgramPhoto:(NSString *)name andProgramID:(NSString *)program_id
{
    UIImage* photo = [UIImage imageNamed:@"test_thumb0.png"];
    if(!name || [name isKindOfClass:[NSNull class]] || name.length == 0)
        return photo;
    
    CBLDocument* doc = nil;
    if([self checkLanguageVN])
        doc = [dbProgram documentWithID:program_id];
    else
        doc = [dbProgram_en documentWithID:program_id];
    CBLRevision* rev = doc.currentRevision;
    if(!rev)
        return photo;
    CBLAttachment* att = [rev attachmentNamed:name];
    if (att != nil) {
        NSData* imageData = att.content;
        photo = [[UIImage alloc] initWithData: imageData];
    }

    return photo;
}

- (void)deleteProgramPhoto:(NSString *)name andProgramID:(NSString *)program_id
{
    // Remove an attachment from a document:
    CBLDocument* doc = nil;
    if([self checkLanguageVN])
        doc = [dbProgram documentWithID:program_id];
    else
        doc = [dbProgram documentWithID:program_id];
    if(doc) {
        CBLUnsavedRevision* newRev = [doc.currentRevision createRevision];
        if(newRev){
            [newRev removeAttachmentNamed:name];
            NSError* error;
            assert([newRev save: &error]);
        }
    }
}

//load all program at local
- (NSMutableArray *) loadAllProgram
{
    NSError *error;
    
    NSMutableArray *arrDicts = [[NSMutableArray alloc] init];
    CBLQuery* query = nil;
    if([self checkLanguageVN])
        query = [dbProgram createAllDocumentsQuery];
    else
        query = [dbProgram_en createAllDocumentsQuery];
    //query.descending = NO;

    NSDictionary *dict = nil;
    CBLDocument *document = nil;
    CBLQueryEnumerator* result = [query run: &error];
    for (CBLQueryRow* row in result) {
        document = row.document;
        dict = document.properties;
        [arrDicts addObject:dict];
    }
    
    return arrDicts;
}

- (NSDictionary *)getProgramByID:(NSString *)program_id
{
    NSError *error;
    //register view
    CBLView *view = [[self checkLanguageVN]?dbProgram:dbProgram_en viewNamed:[NSString stringWithFormat:@"by_%@", program_id]];
    [view setMapBlock: MAPBLOCK({
        id p_id = doc[@"id_chuongtrinh"];
        if ([(NSString *)p_id isEqualToString:program_id])
            emit(p_id , doc);
    }) reduceBlock: nil version: @"1"];
    
    CBLQuery *query = [view createQuery];
    [query setKeys:[[NSArray alloc] initWithObjects:program_id, nil]];
    
    NSDictionary *dict = nil;
    CBLDocument *document = nil;
    CBLQueryEnumerator* result = [query run: &error];
    for (CBLQueryRow* row in result) {
        document = row.document;
        dict = document.properties;
    }
    
    return dict;
}

//=====================================//
//=========== Lịch Diễn ===============//
//=====================================//
- (void) addCalendar:(NSDictionary *)dict
{
    //kiem tra app ở trạng thái remove ko
    NSString *md5 = [dict objectForKey:@"md5"];
    if(!md5 || [md5 isKindOfClass:[NSNull class]] || md5.length == 0){
        //call delete
        [self deleteCalendar:[dict objectForKey:@"fdate"]];
        return;
    }
    
    NSError *error;
    CBLDocument *doc = nil;
    NSString *fdate = [dict objectForKey:@"fdate"];
    if([self checkLanguageVN])
        doc = [dbCalendar documentWithID:fdate];
    else
        doc = [dbCalendar_en documentWithID:fdate];
    
    [doc putProperties:dict error:&error];
}

//add a list documents
- (void) addCalendarList:(NSArray *)arr_dict
{
    NSError *error;
    CBLDocument *doc = nil;
    for(NSDictionary *dict in arr_dict){
        //get key id from dict
        NSString *fdate = [dict objectForKey:@"fdate"];
        
        //kiem tra app ở trạng thái remove ko
        NSString *md5 = [dict objectForKey:@"md5"];
        if(!md5 || [md5 isKindOfClass:[NSNull class]] || md5.length == 0){
            //app da remove thì cần xoá ở local
            [self deleteCalendar:fdate];
            continue;
        }
        
        if([self checkLanguageVN]){
            if([self checkDocument:dbCalendar andDocumentID:fdate])//đã tồn tại add mới
                [self deleteCalendar:fdate];
            doc = [dbCalendar documentWithID:fdate];
            [doc putProperties:dict error:&error];
        } else {
            if([self checkDocument:dbCalendar_en andDocumentID:fdate])//đã tồn tại add mới
                [self deleteCalendar:fdate];
            doc = [dbCalendar_en documentWithID:fdate];
            [doc putProperties:dict error:&error];
        }
    }
}

//delete document
- (BOOL)deleteCalendar:(NSString *)fdate
{
    if(![self checkDocument:[self checkLanguageVN]?dbCalendar:dbCalendar_en andDocumentID:fdate]){
        NSLog(@"This document not exist yet");
        return NO;
    }
    
    NSError* error;
    CBLDocument* document = nil;
    if([self checkLanguageVN])
        document = [dbCalendar documentWithID:fdate];
    else
        document = [dbCalendar_en documentWithID:fdate];
    
    [document deleteDocument:&error];
    
    if (!error) {
        NSLog(@"Deleted document, deletion status is %d", [document isDeleted]);
        return NO;
    }
    
    return YES;
}

//load all program at local
- (NSMutableArray *) loadAllCalendar
{
    NSError *error;
    
    NSMutableArray *arrDicts = [[NSMutableArray alloc] init];
    CBLQuery* query = nil;
    if([self checkLanguageVN])
        query = [dbCalendar createAllDocumentsQuery];
    else
        query = [dbCalendar_en createAllDocumentsQuery];
    query.descending = NO;
    
    NSDictionary *dict = nil;
    CBLDocument *document = nil;
    CBLQueryEnumerator* result = [query run: &error];
    for (CBLQueryRow* row in result) {
        document = row.document;
        dict = document.properties;
        [arrDicts addObject:dict];
    }
    
    return arrDicts;
}

//============================================//
//=============== Doan Nghe Sy ===============//
//============================================//
//add a document
- (void) addArt:(NSDictionary *)dict
{
    //kiem tra app ở trạng thái remove ko
    NSString *md5 = [dict objectForKey:@"md5"];
    if(!md5 || [md5 isKindOfClass:[NSNull class]] || md5.length == 0){
        //call delete
        [self deleteArt:[dict objectForKey:@"id_doan"]];
        return;
    }
    
    NSError *error;
    CBLDocument *doc = nil;
    NSString *art_id = [dict objectForKey:@"id_doan"];
    if([self checkLanguageVN])
        doc = [dbArt documentWithID:art_id];
    else
        doc = [dbArt_en documentWithID:art_id];
    
    [doc putProperties:dict error:&error];
}

//add a list documents
- (void) addArtList:(NSArray *)arr_dict
{
    NSError *error;
    CBLDocument *doc = nil;
    for(NSDictionary *dict in arr_dict){
        //get key id from dict
        NSString *_id = [dict objectForKey:@"id_doan"];
        
        //kiem tra app ở trạng thái remove ko
        NSString *md5 = [dict objectForKey:@"md5"];
        if(!md5 || [md5 isKindOfClass:[NSNull class]] || md5.length == 0){
            [self deleteArt:_id];
            continue;
        }
        
        if([self checkLanguageVN]){
            if([self checkDocument:dbArt andDocumentID:_id])//đã tồn tại add mới
                [self deleteArt:_id];
            doc = [dbArt documentWithID:_id];
            [doc putProperties:dict error:&error];
        } else {
            if([self checkDocument:dbArt_en andDocumentID:_id])//đã tồn tại add mới
                [self deleteArt:_id];
            doc = [dbArt_en documentWithID:_id];
            [doc putProperties:dict error:&error];
        }
    }
}

//delete document
- (BOOL)deleteArt:(NSString *)art_id
{
    if(![self checkDocument:[self checkLanguageVN] ? dbArt : dbArt_en andDocumentID:art_id]){
        return NO;
    }
    
    NSError* error;
    CBLDocument* document = nil;
    if([self checkLanguageVN])
        document = [dbArt documentWithID:art_id];
    else
        document = [dbArt_en documentWithID:art_id];
    
    [document deleteDocument:&error];
    
    if (!error) {
        return NO;
    }
    
    return YES;
}

//lưu photo cua chuong trỉnh
- (void) saveArtPhoto:(NSString *)name andImage:(UIImage *)image andArtID:(NSString *)art_id
{
    [self deleteArtPhoto:name andArtID:art_id];
    
    CBLDocument *doc = nil;
    if([self checkLanguageVN])
        doc = [dbArt documentWithID:art_id];
    else
        doc = [dbArt_en documentWithID:art_id];
    
    CBLUnsavedRevision *newRev = [doc.currentRevision createRevision];
    NSData *imageData = nil;
    if([name hasSuffix:@".jpg"] || [name hasSuffix:@".jpeg"]){
        imageData = UIImageJPEGRepresentation(image, 0.75);
        [newRev setAttachmentNamed:name
                   withContentType: @"image/jpeg"
                           content: imageData];
    } else if([name hasSuffix:@".png"]){
        imageData = UIImagePNGRepresentation(image);
        [newRev setAttachmentNamed:name
                   withContentType: @"image/png"
                           content: imageData];
    }
    // (You could also update newRev.properties while you're here)
    if(imageData && newRev){
        NSError *error;
        assert([newRev save: &error]);
    }
}

- (UIImage *)getArtPhoto:(NSString *)name andArtID:(NSString *)art_id
{
    UIImage* photo = [UIImage imageNamed:@"test_thumb0.png"];
    if(!name || [name isKindOfClass:[NSNull class]] || name.length == 0)
        return photo;
    
    CBLDocument* doc = nil;
    if([self checkLanguageVN])
        doc = [dbArt documentWithID:art_id];
    else
        doc = [dbArt_en documentWithID:art_id];
    CBLRevision* rev = doc.currentRevision;
    if(!rev)
        return photo;
    CBLAttachment* att = [rev attachmentNamed:name];
    if (att != nil) {
        NSData* imageData = att.content;
        photo = [[UIImage alloc] initWithData: imageData];
    }
    
    return photo;
}

- (void)deleteArtPhoto:(NSString *)name andArtID:(NSString *)art_id
{
    // Remove an attachment from a document:
    CBLDocument* doc = nil;
    if([self checkLanguageVN])
        doc = [dbArt documentWithID:art_id];
    else
        doc = [dbArt_en documentWithID:art_id];
    if(doc) {
        CBLUnsavedRevision* newRev = [doc.currentRevision createRevision];
        if(newRev){
            [newRev removeAttachmentNamed:name];
            NSError* error;
            assert([newRev save: &error]);
        }
    }
}

//load all program at local
- (NSMutableArray *) loadAllArt
{
    NSError *error;
    
    NSMutableArray *arrDicts = [[NSMutableArray alloc] init];
    CBLQuery* query = nil;
    if([self checkLanguageVN])
        query = [dbArt createAllDocumentsQuery];
    else
        query = [dbArt_en createAllDocumentsQuery];
    //query.descending = NO;
    
    NSDictionary *dict = nil;
    CBLDocument *document = nil;
    CBLQueryEnumerator* result = [query run: &error];
    for (CBLQueryRow* row in result) {
        document = row.document;
        dict = document.properties;
        [arrDicts addObject:dict];
    }
    
    return arrDicts;
}

- (NSDictionary *)getArtByID:(NSString *)art_id
{
    NSError *error;
    //register view
    CBLView *view = [[self checkLanguageVN]?dbArt:dbArt_en viewNamed:[NSString stringWithFormat:@"by_%@", art_id]];
    [view setMapBlock: MAPBLOCK({
        id a_id = doc[@"id_doan"];
        if ([(NSString *)a_id isEqualToString:art_id])
            emit(a_id , doc);
    }) reduceBlock: nil version: @"1"];
    
    CBLQuery *query = [view createQuery];
    [query setKeys:[[NSArray alloc] initWithObjects:art_id, nil]];
    
    NSDictionary *dict = nil;
    CBLDocument *document = nil;
    CBLQueryEnumerator* result = [query run: &error];
    for (CBLQueryRow* row in result) {
        document = row.document;
        dict = document.properties;
    }
    
    return dict;
}

//============================================//
//=============== Diem bieu dien==============//
//============================================//
//add a document
- (void) addLocation:(NSDictionary *)dict
{
    //kiem tra app ở trạng thái remove ko
    NSString *md5 = [dict objectForKey:@"md5"];
    if(!md5 || [md5 isKindOfClass:[NSNull class]] || md5.length == 0){
        //call delete
        [self deleteLocation:[dict objectForKey:@"id_diadiem"]];
        return;
    }
    
    NSError *error;
    CBLDocument *doc = nil;
    NSString *_id = [dict objectForKey:@"id_diadiem"];
    if([self checkLanguageVN])
        doc = [dbLocation documentWithID:_id];
    else
        doc = [dbLocation_en documentWithID:_id];
    
    [doc putProperties:dict error:&error];
}

//add a list documents
- (void) addLocationList:(NSArray *)arr_dict
{
    NSError *error;
    CBLDocument *doc = nil;
    for(NSDictionary *dict in arr_dict){
        //get key id from dict
        NSString *_id = [dict objectForKey:@"id_diadiem"];
        
        //kiem tra app ở trạng thái remove ko
        NSString *md5 = [dict objectForKey:@"md5"];
        if(!md5 || [md5 isKindOfClass:[NSNull class]] || md5.length == 0){
            [self deleteLocation:_id];
            continue;
        }
        
        if([self checkLanguageVN]){
            if([self checkDocument:dbLocation andDocumentID:_id])//đã tồn tại add mới
                [self deleteLocation:_id];
            doc = [dbLocation documentWithID:_id];
            [doc putProperties:dict error:&error];
        } else {
            if([self checkDocument:dbLocation_en andDocumentID:_id])//đã tồn tại add mới
                [self deleteLocation:_id];
            doc = [dbLocation_en documentWithID:_id];
            [doc putProperties:dict error:&error];
        }
    }
}

//delete document
- (BOOL)deleteLocation:(NSString *)_id
{
    if(![self checkDocument:[self checkLanguageVN] ? dbLocation : dbLocation_en andDocumentID:_id]){
        return NO;
    }
    
    NSError* error;
    CBLDocument* document = nil;
    if([self checkLanguageVN])
        document = [dbLocation documentWithID:_id];
    else
        document = [dbLocation_en documentWithID:_id];
    
    [document deleteDocument:&error];
    
    if (!error) {
        return NO;
    }
    
    return YES;
}

//lưu photo cua chuong trỉnh
- (void) saveLocationPhoto:(NSString *)name andImage:(UIImage *)image andLocationID:(NSString *)_id
{
    [self deleteLocationPhoto:name andLocationID:_id];
    
    CBLDocument *doc = nil;
    if([self checkLanguageVN])
        doc = [dbLocation documentWithID:_id];
    else
        doc = [dbLocation_en documentWithID:_id];
    
    CBLUnsavedRevision *newRev = [doc.currentRevision createRevision];
    NSData *imageData = nil;
    if([name hasSuffix:@".jpg"] || [name hasSuffix:@".jpeg"]){
        imageData = UIImageJPEGRepresentation(image, 0.75);
        [newRev setAttachmentNamed:name
                   withContentType: @"image/jpeg"
                           content: imageData];
    } else if([name hasSuffix:@".png"]){
        imageData = UIImagePNGRepresentation(image);
        [newRev setAttachmentNamed:name
                   withContentType: @"image/png"
                           content: imageData];
    }
    // (You could also update newRev.properties while you're here)
    if(imageData && newRev){
        NSError *error;
        assert([newRev save: &error]);
    }
}

- (UIImage *)getLocationPhoto:(NSString *)name andLocationID:(NSString *)_id
{
    UIImage* photo = [UIImage imageNamed:@"test_thumb0.png"];
    if(!name || [name isKindOfClass:[NSNull class]] || name.length == 0)
        return photo;
    
    CBLDocument* doc = nil;
    if([self checkLanguageVN])
        doc = [dbLocation documentWithID:_id];
    else
        doc = [dbLocation_en documentWithID:_id];
    CBLRevision* rev = doc.currentRevision;
    if(!rev)
        return photo;
    CBLAttachment* att = [rev attachmentNamed:name];
    if (att != nil) {
        NSData* imageData = att.content;
        photo = [[UIImage alloc] initWithData: imageData];
    }
    
    return photo;
}

- (void)deleteLocationPhoto:(NSString *)name andLocationID:(NSString *)_id
{
    // Remove an attachment from a document:
    CBLDocument* doc = nil;
    if([self checkLanguageVN])
        doc = [dbLocation documentWithID:_id];
    else
        doc = [dbLocation_en documentWithID:_id];
    if(doc) {
        CBLUnsavedRevision* newRev = [doc.currentRevision createRevision];
        if(newRev){
            [newRev removeAttachmentNamed:name];
            NSError* error;
            assert([newRev save: &error]);
        }
    }
}

//load all program at local
- (NSMutableArray *) loadAllLocation
{
    NSError *error;
    
    NSMutableArray *arrDicts = [[NSMutableArray alloc] init];
    CBLQuery* query = nil;
    if([self checkLanguageVN])
        query = [dbLocation createAllDocumentsQuery];
    else
        query = [dbLocation_en createAllDocumentsQuery];
    //query.descending = NO;
    
    NSDictionary *dict = nil;
    CBLDocument *document = nil;
    CBLQueryEnumerator* result = [query run: &error];
    for (CBLQueryRow* row in result) {
        document = row.document;
        dict = document.properties;
        [arrDicts addObject:dict];
    }
    
    return arrDicts;
}


//============================================//
//=============== Diem ban ve   ==============//
//============================================//
//add a document
- (void) addTicketLocation:(NSDictionary *)dict
{
    //kiem tra app ở trạng thái remove ko
    NSString *md5 = [dict objectForKey:@"md5"];
    if(!md5 || [md5 isKindOfClass:[NSNull class]] || md5.length == 0){
        //call delete
        [self deleteTicketLocation:[dict objectForKey:@"id"]];
        return;
    }
    
    NSError *error;
    CBLDocument *doc = nil;
    NSString *_id = [dict objectForKey:@"id"];
    if([self checkLanguageVN])
        doc = [dbTicket documentWithID:_id];
    else
        doc = [dbTicket_en documentWithID:_id];
    
    [doc putProperties:dict error:&error];
}

- (void) addTicketLocationList:(NSArray *)arr_dict
{
    NSError *error;
    CBLDocument *doc = nil;
    for(NSDictionary *dict in arr_dict){
        //get key id from dict
        NSString *_id = [dict objectForKey:@"id"];
        
        //kiem tra app ở trạng thái remove ko
        NSString *md5 = [dict objectForKey:@"md5"];
        if(!md5 || [md5 isKindOfClass:[NSNull class]] || md5.length == 0){
            [self deleteTicketLocation:_id];
            continue;
        }
        
        if([self checkLanguageVN]){
            if([self checkDocument:dbTicket andDocumentID:_id])//đã tồn tại add mới
                [self deleteTicketLocation:_id];
            doc = [dbTicket documentWithID:_id];
            [doc putProperties:dict error:&error];
        } else {
            if([self checkDocument:dbTicket_en andDocumentID:_id])//đã tồn tại add mới
                [self deleteTicketLocation:_id];
            doc = [dbTicket_en documentWithID:_id];
            [doc putProperties:dict error:&error];
        }
    }
}

- (BOOL) deleteTicketLocation:(NSString *)_id
{
    if(![self checkDocument:[self checkLanguageVN] ? dbTicket : dbTicket_en andDocumentID:_id]){
        return NO;
    }
    
    NSError* error;
    CBLDocument* document = nil;
    if([self checkLanguageVN])
        document = [dbTicket documentWithID:_id];
    else
        document = [dbTicket_en documentWithID:_id];
    
    [document deleteDocument:&error];
    
    if (!error) {
        return NO;
    }
    
    return YES;
}

- (void) saveTicketLocationPhoto:(NSString *)name andImage:(UIImage *)image andTicketLocationID:(NSString *)_id
{
    [self deleteTicketLocationPhoto:name andLocationID:_id];
    
    CBLDocument *doc = nil;
    if([self checkLanguageVN])
        doc = [dbTicket documentWithID:_id];
    else
        doc = [dbTicket_en documentWithID:_id];
    
    CBLUnsavedRevision *newRev = [doc.currentRevision createRevision];
    NSData *imageData = nil;
    if([name hasSuffix:@".jpg"] || [name hasSuffix:@".jpeg"]){
        imageData = UIImageJPEGRepresentation(image, 0.75);
        [newRev setAttachmentNamed:name
                   withContentType: @"image/jpeg"
                           content: imageData];
    } else if([name hasSuffix:@".png"]){
        imageData = UIImagePNGRepresentation(image);
        [newRev setAttachmentNamed:name
                   withContentType: @"image/png"
                           content: imageData];
    }
    // (You could also update newRev.properties while you're here)
    if(imageData && newRev){
        NSError *error;
        assert([newRev save: &error]);
    }
}

- (UIImage *)getTicketLocationPhoto:(NSString *)name andLocationID:(NSString *)_id
{
    UIImage* photo = [UIImage imageNamed:@"test_thumb0.png"];
    if(!name || [name isKindOfClass:[NSNull class]] || name.length == 0)
        return photo;
    
    CBLDocument* doc = nil;
    if([self checkLanguageVN])
        doc = [dbTicket documentWithID:_id];
    else
        doc = [dbTicket_en documentWithID:_id];
    CBLRevision* rev = doc.currentRevision;
    if(!rev)
        return photo;
    CBLAttachment* att = [rev attachmentNamed:name];
    if (att != nil) {
        NSData* imageData = att.content;
        photo = [[UIImage alloc] initWithData: imageData];
    }
    
    return photo;
}

- (void) deleteTicketLocationPhoto:(NSString *)name andLocationID:(NSString *)_id
{
    // Remove an attachment from a document:
    CBLDocument* doc = nil;
    if([self checkLanguageVN])
        doc = [dbTicket documentWithID:_id];
    else
        doc = [dbTicket_en documentWithID:_id];
    if(doc) {
        CBLUnsavedRevision* newRev = [doc.currentRevision createRevision];
        if(newRev){
            [newRev removeAttachmentNamed:name];
            NSError* error;
            assert([newRev save: &error]);
        }
    }
}

- (NSMutableArray *) loadAllTicketLocation
{
    NSError *error;
    
    NSMutableArray *arrDicts = [[NSMutableArray alloc] init];
    CBLQuery* query = nil;
    if([self checkLanguageVN])
        query = [dbTicket createAllDocumentsQuery];
    else
        query = [dbTicket_en createAllDocumentsQuery];
    //query.descending = NO;
    
    NSDictionary *dict = nil;
    CBLDocument *document = nil;
    CBLQueryEnumerator* result = [query run: &error];
    for (CBLQueryRow* row in result) {
        document = row.document;
        dict = document.properties;
        [arrDicts addObject:dict];
    }
    
    return arrDicts;
}


//============================================//
//=============== News Liked    ==============//
//============================================//
//add a document
- (void) addNewsLike:(NSDictionary *)dict
{
    NSError *error;
    CBLDocument *doc = nil;
    NSString *_id = [dict objectForKey:@"id"];
    if([self checkDocument:dbNewsLiked andDocumentID:_id])
        return;
    
    doc = [dbNewsLiked documentWithID:_id];
    [doc putProperties:dict error:&error];
}

- (BOOL) deleteNewLike:(NSString *)_id
{
    if(![self checkDocument:dbNewsLiked andDocumentID:_id]){
        return NO;
    }
    
    NSError* error;
    CBLDocument* document = nil;
    document = [dbNewsLiked documentWithID:_id];
    [document deleteDocument:&error];
    
    if (!error) {
        return NO;
    }
    
    return YES;
}

- (void) saveNewsPhoto:(NSString *)name andImage:(UIImage *)image andID:(NSString *)_id
{
    [self deleteNewsPhoto:name andID:_id];
    CBLDocument *doc = [dbNewsLiked documentWithID:_id];
    
    CBLUnsavedRevision *newRev = [doc.currentRevision createRevision];
    NSData *imageData = nil;
    if([name hasSuffix:@".jpg"] || [name hasSuffix:@".jpeg"]){
        imageData = UIImageJPEGRepresentation(image, 0.75);
        [newRev setAttachmentNamed:name
                   withContentType: @"image/jpeg"
                           content: imageData];
    } else if([name hasSuffix:@".png"]){
        imageData = UIImagePNGRepresentation(image);
        [newRev setAttachmentNamed:name
                   withContentType: @"image/png"
                           content: imageData];
    }
    // (You could also update newRev.properties while you're here)
    if(imageData && newRev){
        NSError *error;
        assert([newRev save: &error]);
    }
}

- (UIImage *)getNewsPhoto:(NSString *)name andID:(NSString *)_id
{
    UIImage* photo = [UIImage imageNamed:@"test_thumb0.png"];
    if(!name || [name isKindOfClass:[NSNull class]] || name.length == 0)
        return photo;
    
    CBLDocument* doc = [dbNewsLiked documentWithID:_id];
    CBLRevision* rev = doc.currentRevision;
    if(!rev)
        return photo;
    CBLAttachment* att = [rev attachmentNamed:name];
    if (att != nil) {
        NSData* imageData = att.content;
        photo = [[UIImage alloc] initWithData: imageData];
    }
    
    return photo;
}

- (void) deleteNewsPhoto:(NSString *)name andID:(NSString *)_id
{
    CBLDocument* doc = [dbNewsLiked documentWithID:_id];
    if(doc) {
        CBLUnsavedRevision* newRev = [doc.currentRevision createRevision];
        if(newRev){
            [newRev removeAttachmentNamed:name];
            NSError* error;
            assert([newRev save: &error]);
        }
    }
}

- (NSMutableArray *) loadAllNewsLiked
{
    NSError *error;
    NSMutableArray *arrDicts = [[NSMutableArray alloc] init];
    CBLQuery* query = [dbNewsLiked createAllDocumentsQuery];
    //query.descending = NO;
    
    NSDictionary *dict = nil;
    CBLDocument *document = nil;
    CBLQueryEnumerator* result = [query run: &error];
    for (CBLQueryRow* row in result) {
        document = row.document;
        dict = document.properties;
        [arrDicts addObject:dict];
    }
    
    return arrDicts;
}

//============================================//
//=============== Dia Diem Liked ==============//
//============================================//
//add a document
- (void) addDiaDiemLike:(NSDictionary *)dict
{
    NSError *error;
    CBLDocument *doc = nil;
    NSString *_id = [dict objectForKey:@"id"];
    if([self checkDocument:dbLikedDiaDiem andDocumentID:_id])
        return;
    
    doc = [dbLikedDiaDiem documentWithID:_id];
    [doc putProperties:dict error:&error];
}

- (BOOL) deleteDiaDiemLike:(NSString *)_id
{
    if(![self checkDocument:dbLikedDiaDiem andDocumentID:_id]){
        return NO;
    }
    
    NSError* error;
    CBLDocument* document = [dbLikedDiaDiem documentWithID:_id];
    [document deleteDocument:&error];
    
    if (!error) {
        return NO;
    }
    
    return YES;
}

- (void) saveDiaDiemPhoto:(NSString *)name andImage:(UIImage *)image andID:(NSString *)_id
{
    //[self deleteDiaDiemPhoto:name andID:_id];
    CBLDocument *doc = [dbLikedDiaDiem documentWithID:_id];
    
    CBLUnsavedRevision *newRev = [doc.currentRevision createRevision];
    NSData *imageData = nil;
    if([name hasSuffix:@".jpg"] || [name hasSuffix:@".jpeg"]){
        imageData = UIImageJPEGRepresentation(image, 0.5);
        [newRev setAttachmentNamed:name
                   withContentType:@"image/jpeg"
                           content:imageData];
    } else if([name hasSuffix:@".png"]){
        imageData = UIImagePNGRepresentation(image);
        [newRev setAttachmentNamed:name
                   withContentType: @"image/png"
                           content: imageData];
    }
    // (You could also update newRev.properties while you're here)
    if(imageData && newRev){
        NSError *error;
        assert([newRev save: &error]);
    }
}

- (UIImage *)getDiaDiemPhoto:(NSString *)name andID:(NSString *)_id
{
    UIImage* photo = [UIImage imageNamed:@"test_thumb0.png"];
    if(!name || [name isKindOfClass:[NSNull class]] || name.length == 0)
        return photo;
    
    CBLDocument* doc = [dbLikedDiaDiem documentWithID:_id];
    CBLRevision* rev = doc.currentRevision;
    if(!rev)
        return photo;
    CBLAttachment* att = [rev attachmentNamed:name];
    if (att != nil) {
        NSData* imageData = att.content;
        photo = [[UIImage alloc] initWithData: imageData];
    }
    
    return photo;
}

- (void) deleteDiaDiemPhoto:(NSString *)name andID:(NSString *)_id
{
    CBLDocument* doc = [dbLikedDiaDiem documentWithID:_id];
    if(doc) {
        CBLUnsavedRevision* newRev = [doc.currentRevision createRevision];
        if(newRev){
            [newRev removeAttachmentNamed:name];
            NSError* error;
            assert([newRev save: &error]);
        }
    }
}

- (NSMutableArray *) loadAllDiaDiemLiked
{
    NSError *error;
    NSMutableArray *arrDicts = [[NSMutableArray alloc] init];
    CBLQuery* query = [dbLikedDiaDiem createAllDocumentsQuery];
    //query.descending = NO;
    
    NSDictionary *dict = nil;
    CBLDocument *document = nil;
    CBLQueryEnumerator* result = [query run: &error];
    for (CBLQueryRow* row in result) {
        document = row.document;
        dict = document.properties;
        [arrDicts addObject:dict];
    }
    
    return arrDicts;
}

//==================================//
//=========== Common ===============//
//==================================//
- (BOOL) checkLanguageVN
{
    return [[[LanguagesManager sharedInstance] getCurrentLanguage] isEqualToString:@"vi"];
}

//check document in database
- (BOOL) checkDocument:(CBLDatabase *)_db andDocumentID:(NSString *) documentID
{
    CBLDocument *doc = [_db existingDocumentWithID:documentID];
    return !doc ? NO : YES;
}

@end
