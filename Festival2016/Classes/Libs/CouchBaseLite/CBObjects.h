//
//  CBObjects.h
//  CoachBaseLite-Example
//
//  Created by Phan Phuoc Luong on 8/12/15.
//  Copyright (c) 2015 Phan Phuoc Luong. All rights reserved.
//

#import <CouchbaseLite/CouchbaseLite.h>

@interface CBObjects : NSObject

@property (nonatomic, strong) CBLDatabase *dbProgram;//chuong trinh
@property (nonatomic, strong) CBLDatabase *dbCalendar;//lich dien
@property (nonatomic, strong) CBLDatabase *dbArt;//doan nghe thuat
@property (nonatomic, strong) CBLDatabase *dbLocation;//dia diem
@property (nonatomic, strong) CBLDatabase *dbTicket;//diem ban ve

//danh cho cac table tieng anh
@property (nonatomic, strong) CBLDatabase *dbProgram_en;
@property (nonatomic, strong) CBLDatabase *dbCalendar_en;
@property (nonatomic, strong) CBLDatabase *dbArt_en;
@property (nonatomic, strong) CBLDatabase *dbLocation_en;
@property (nonatomic, strong) CBLDatabase *dbTicket_en;


//danh cho cac table like
@property (nonatomic, strong) CBLDatabase *dbLikeNews;//like tin tuc
@property (nonatomic, strong) CBLDatabase *dbLikeDiaDiem;//like mục địa điểm

@property (nonatomic, strong) CBLManager *manager;

+(CBObjects *) sharedInstance;

@end
