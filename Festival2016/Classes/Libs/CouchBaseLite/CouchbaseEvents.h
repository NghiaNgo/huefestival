//
//  CouchbaseEvents.h
//  Appzitov1.2
//
//  Created by Phan Phuoc Luong on 8/13/15.
//  Copyright (c) 2015 Phan Phuoc Luong. All rights reserved.
//

#import <CouchbaseLite/CouchbaseLite.h>
#import "CBObjects.h"

@interface CouchbaseEvents : NSObject
{
    CBLDatabase *dbProgram;
    CBLDatabase *dbCalendar;
    CBLDatabase *dbArt;
    CBLDatabase *dbLocation;
    CBLDatabase *dbTicket;
    
    //tieng anh
    CBLDatabase *dbProgram_en;
    CBLDatabase *dbCalendar_en;
    CBLDatabase *dbArt_en;
    CBLDatabase *dbLocation_en;
    CBLDatabase *dbTicket_en;
    
    //like
    CBLDatabase *dbNewsLiked;
    CBLDatabase *dbLikedDiaDiem;
}

+ (CouchbaseEvents *) sharedInstance;

//======== chương trình =========//
- (void) addProgram:(NSDictionary *)dict;
- (void) addProgramList:(NSArray *)arr_dict;
- (BOOL) deleteProgram:(NSString *)program_id;
- (void) saveProgramPhoto:(NSString *)name andImage:(UIImage *)image andProgramID:(NSString *)program_id;
- (UIImage *)getProgramPhoto:(NSString *)name andProgramID:(NSString *)program_id;
- (void) deleteProgramPhoto:(NSString *)name andProgramID:(NSString *)program_id;
- (NSMutableArray *) loadAllProgram;
- (NSDictionary *)getProgramByID:(NSString *)program_id;

//======== Lịch diễn ============//
- (void) addCalendar:(NSDictionary *)dict;
- (void) addCalendarList:(NSArray *)arr_dict;
- (BOOL) deleteCalendar:(NSString *)fdate;
- (NSMutableArray *) loadAllCalendar;

//======== doan nghe sy =========//
- (void) addArt:(NSDictionary *)dict;
- (void) addArtList:(NSArray *)arr_dict;
- (BOOL) deleteArt:(NSString *)art_id;
- (void) saveArtPhoto:(NSString *)name andImage:(UIImage *)image andArtID:(NSString *)art_id;
- (UIImage *)getArtPhoto:(NSString *)name andArtID:(NSString *)art_id;
- (void) deleteArtPhoto:(NSString *)name andArtID:(NSString *)art_id;
- (NSMutableArray *) loadAllArt;
- (NSDictionary *)getArtByID:(NSString *)art_id;

//======== diem bieu dien =========//
- (void) addLocation:(NSDictionary *)dict;
- (void) addLocationList:(NSArray *)arr_dict;
- (BOOL) deleteLocation:(NSString *)_id;
- (void) saveLocationPhoto:(NSString *)name andImage:(UIImage *)image andLocationID:(NSString *)_id;
- (UIImage *)getLocationPhoto:(NSString *)name andLocationID:(NSString *)_id;
- (void) deleteLocationPhoto:(NSString *)name andLocationID:(NSString *)_id;
- (NSMutableArray *) loadAllLocation;

//======== diem ban ve =========//
- (void) addTicketLocation:(NSDictionary *)dict;
- (void) addTicketLocationList:(NSArray *)arr_dict;
- (BOOL) deleteTicketLocation:(NSString *)_id;
- (void) saveTicketLocationPhoto:(NSString *)name andImage:(UIImage *)image andTicketLocationID:(NSString *)_id;
- (UIImage *)getTicketLocationPhoto:(NSString *)name andLocationID:(NSString *)_id;
- (void) deleteTicketLocationPhoto:(NSString *)name andLocationID:(NSString *)_id;
- (NSMutableArray *) loadAllTicketLocation;

//======= Like Tin tuc =========//
- (void) addNewsLike:(NSDictionary *)dict;
- (BOOL) deleteNewLike:(NSString *)_id;
- (void) saveNewsPhoto:(NSString *)name andImage:(UIImage *)image andID:(NSString *)_id;
- (UIImage *)getNewsPhoto:(NSString *)name andID:(NSString *)_id;
- (void) deleteNewsPhoto:(NSString *)name andID:(NSString *)_id;
- (NSMutableArray *) loadAllNewsLiked;

//======= Like Dia Diem =========//
- (void) addDiaDiemLike:(NSDictionary *)dict;
- (BOOL) deleteDiaDiemLike:(NSString *)_id;
- (void) saveDiaDiemPhoto:(NSString *)name andImage:(UIImage *)image andID:(NSString *)_id;
- (UIImage *)getDiaDiemPhoto:(NSString *)name andID:(NSString *)_id;
- (void) deleteDiaDiemPhoto:(NSString *)name andID:(NSString *)_id;
- (NSMutableArray *) loadAllDiaDiemLiked;

@end
