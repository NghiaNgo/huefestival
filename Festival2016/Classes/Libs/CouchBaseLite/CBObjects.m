//
//  CBObjects.m
//  CoachBaseLite-Example
//
//  Created by Phan Phuoc Luong on 8/12/15.
//  Copyright (c) 2015 Phan Phuoc Luong. All rights reserved.
//

#import "CBObjects.h"

@implementation CBObjects

- (id) init
{
    self = [super init];
    if (self){
        NSError *error;
        _manager = [CBLManager sharedInstance];
        if(!_manager){
            NSLog(@"Cannot create shared instance of CBLManager");
            return nil;
        }
        
        _dbProgram = [_manager databaseNamed:@"fes_program" error:&error];//chương trình
        if(!_dbProgram){
            NSLog(@"Cannot create database program. Error mesages: %@", error.localizedDescription);
            return nil;
        }
        
        _dbCalendar = [_manager databaseNamed:@"fes_calendar" error:&error];//lich dien
        if(!_dbCalendar){
            NSLog(@"Cannot create database calendar. Error mesages: %@", error.localizedDescription);
            return nil;
        }
        
        _dbArt = [_manager databaseNamed:@"fes_art" error:&error];//đoàn nghệ thuật
        if(!_dbArt){
            NSLog(@"Cannot create database Art. Error mesages: %@", error.localizedDescription);
            return nil;
        }
        
        _dbLocation = [_manager databaseNamed:@"fes_location" error:&error];//địa điểm
        if(!_dbLocation){
            NSLog(@"Cannot create database Location. Error mesages: %@", error.localizedDescription);
            return nil;
        }
        
        _dbTicket = [_manager databaseNamed:@"fes_ticket" error:&error];//diem ban ve
        if(!_dbTicket){
            NSLog(@"Cannot create database Ticket. Error mesages: %@", error.localizedDescription);
            return nil;
        }
        
        //================= cac table danh cho tieng anh ======================//
        _dbProgram_en = [_manager databaseNamed:@"fes_program_en" error:&error];//chương trình
        if(!_dbProgram_en){
            NSLog(@"Cannot create database program. Error mesages: %@", error.localizedDescription);
            return nil;
        }
        
        _dbCalendar_en = [_manager databaseNamed:@"fes_calendar_en" error:&error];//lich dien
        if(!_dbCalendar_en){
            NSLog(@"Cannot create database calendar. Error mesages: %@", error.localizedDescription);
            return nil;
        }
        
        _dbArt_en = [_manager databaseNamed:@"fes_art_en" error:&error];//đoàn nghệ thuật
        if(!_dbArt_en){
            NSLog(@"Cannot create database Art. Error mesages: %@", error.localizedDescription);
            return nil;
        }
        
        _dbLocation_en = [_manager databaseNamed:@"fes_location_en" error:&error];//địa điểm
        if(!_dbLocation_en){
            NSLog(@"Cannot create database Location. Error mesages: %@", error.localizedDescription);
            return nil;
        }
        
        _dbTicket_en = [_manager databaseNamed:@"fes_ticket_en" error:&error];//diem ban ve
        if(!_dbTicket_en){
            NSLog(@"Cannot create database Tocket. Error mesages: %@", error.localizedDescription);
            return nil;
        }
        
        //================= cac table danh cho muc like ======================//
        _dbLikeNews = [_manager databaseNamed:@"fes_news_liked" error:&error];//like tin tuc
        if(!_dbLikeNews){
            NSLog(@"Cannot create database news liked. Error mesages: %@", error.localizedDescription);
            return nil;
        }
        
        _dbLikeDiaDiem = [_manager databaseNamed:@"fes_diadiem_liked" error:&error];//like dia diem
        if(!_dbLikeDiaDiem){
            NSLog(@"Cannot create database dia diem liked. Error mesages: %@", error.localizedDescription);
            return nil;
        }
    }
    
    return self;
}

+ (CBObjects *)sharedInstance {
    static CBObjects *sharedInstance = nil;
    if (sharedInstance == nil) {
        sharedInstance = [[CBObjects alloc] init];
    }
    return sharedInstance;
}

@end
