//
//  ViewController.h
//  PageViewController
//
//  Created by Tom Fewster on 11/01/2012.
//

#import <UIKit/UIKit.h>

@interface PagerViewController : UIViewController <UIScrollViewDelegate>

@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) IBOutlet UIPageControl *pageControl;
@property (assign) int page;

- (IBAction)changePage:(id)sender;
- (void) setToPage:(int) page;
- (void) setFirstPage;

- (void)previousPage;
- (void)nextPage;

@end
