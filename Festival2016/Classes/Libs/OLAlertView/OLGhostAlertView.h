//
//  OLGhostAlertView.h
//
//  Originally created by Radu Dutzan.
//  (c) 2012 Onda.
//

#import <UIKit/UIKit.h>

@protocol OLGhostAlertDelegate;
@interface OLGhostAlertView : UIView

@property (nonatomic, retain) NSString *olID;
@property (nonatomic, strong) id<OLGhostAlertDelegate> olDelegate;

- (id)initWithTitle:(NSString *)title;
- (id)initWithTitle:(NSString *)title message:(NSString *)message;
- (id)initWithTitle:(NSString *)title message:(NSString *)message timeout:(NSTimeInterval)timeout dismissible:(BOOL)dismissible;
- (id)initWithCustomTitle:(NSString *)title message:(NSString *)message timeout:(NSTimeInterval)timeout dismissible:(BOOL)dismissible;

- (void)show;
- (void)showCustom;
- (void)hide;
- (void)hideCustom;

@end

@protocol OLGhostAlertDelegate <NSObject>

- (void) touchOnOLAlert:(NSString *)olID;

@end