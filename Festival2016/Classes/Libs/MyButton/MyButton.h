//
//  MyButton.h
//  MeHereWhereYou
//
//  Created by Phan Phuoc Luong on 4/12/14.
//  Copyright (c) 2014 Phan Phuoc Luong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyButton : UIButton
{
    UIColor *currentColor;
}
@property (nonatomic, strong) UIColor *colorHighlighted;
@property (nonatomic, strong) UIColor *colorNormal;
@property (nonatomic, retain) id myDatas;


@end
