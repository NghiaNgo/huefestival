//
//  MyButton.m
//  MeHereWhereYou
//
//  Created by Phan Phuoc Luong on 4/12/14.
//  Copyright (c) 2014 Phan Phuoc Luong. All rights reserved.
//

#import "MyButton.h"

@implementation MyButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];
    
    if (highlighted) {
        self.backgroundColor = _colorHighlighted;
    }
    else {
        if(!currentColor)
            currentColor = _colorNormal;
        self.backgroundColor = currentColor;
    }
}

- (void)setSelected:(BOOL)selected
{
    if (selected)
    {
        currentColor = _colorHighlighted;
    }
    else
    {
        currentColor = _colorNormal;
    }
    self.backgroundColor = currentColor;
}

@end
