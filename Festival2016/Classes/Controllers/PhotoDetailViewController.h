//
//  PhotoDetailViewController.h
//  Festival2016
//
//  Created by Phan Phuoc Luong on 2/25/16.
//  Copyright © 2016 Phan Phuoc Luong. All rights reserved.
//

#import "SMPageControl.h"

@interface PhotoDetailViewController : UIViewController<UIScrollViewDelegate>
{
    NSMutableArray *_arrPhotos;
    int indexSelected;
    AppDelegate *appDelegate;
}

- (id) initWithData:(NSMutableArray*)arrPhotos andIndex:(int)index andNibName:(NSString *) nibName;
@property (nonatomic, weak) IBOutlet UIView *viewMain;
//
@property (nonatomic, assign) BOOL isPresented;
@property (nonatomic, weak) IBOutlet UIScrollView *scrollImages;
@property (nonatomic, weak) IBOutlet SMPageControl *pageControl;

@end
