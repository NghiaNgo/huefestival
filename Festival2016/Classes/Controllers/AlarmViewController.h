//
//  AlarmViewController.h
//  Festival2016
//
//  Created by Phan Phuoc Luong on 2/18/16.
//  Copyright © 2016 Phan Phuoc Luong. All rights reserved.
//

#import "FXBlurView.h"

@interface AlarmViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>
{
       AppDelegate *appDelegate; 
}

- (id) initWithDatas:(NSString *)nibName andData:(NSMutableArray *)arrAlarms;
@property (nonatomic, weak) IBOutlet UIView *viewMain;
//
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UILabel *lblTitle;
@property (nonatomic, weak) IBOutlet FXBlurView *viewBlur;


@property (nonatomic, weak) IBOutlet FXBlurView *viewEdit;
@property (nonatomic, weak) IBOutlet UIDatePicker *myDatePicker;
@property (nonatomic, weak) IBOutlet UILabel *lblTimeEdit;
@property (nonatomic, weak) IBOutlet UILabel *lblDateEdit;
@property (nonatomic, weak) IBOutlet UILabel *lblContentEdit;
@property (nonatomic, weak) IBOutlet UIButton *btnOK;

@end
