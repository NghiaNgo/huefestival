//
//  ProgramFillterByLocationIDViewController.m
//  Festival2016
//
//  Created by Phan Phuoc Luong on 1/11/16.
//  Copyright © 2016 Phan Phuoc Luong. All rights reserved.
//

#import "ProgramFillterByLocationIDViewController.h"
#import "ProgramDetailViewController.h"
#import "ProgramObj.h"
#import "ProgramCell.h"

@interface ProgramFillterByLocationIDViewController ()

@end

@implementation ProgramFillterByLocationIDViewController

- (id)initWithLocationIDSelected:(NSString *)id_diadiem
{
    if(self = [super initWithNibName:@"ProgramFillterByLocationIDViewController" bundle:nil])
    {
        id_diadiemSelected = id_diadiem;
        arrDatas = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)initSafeArea {
    _viewMain.translatesAutoresizingMaskIntoConstraints = NO;
    
    if (@available(iOS 11, *)) {
        UILayoutGuide *guide = self.view.safeAreaLayoutGuide;
        [_viewMain.leadingAnchor constraintEqualToAnchor:guide.leadingAnchor].active = YES;
        [_viewMain.trailingAnchor constraintEqualToAnchor:guide.trailingAnchor].active = YES;
        [_viewMain.topAnchor constraintEqualToAnchor:guide.topAnchor].active = YES;
        [_viewMain.bottomAnchor constraintEqualToAnchor:guide.bottomAnchor].active = YES;
    } else {
        UILayoutGuide *margins = self.view.layoutMarginsGuide;
        [_viewMain.leadingAnchor constraintEqualToAnchor:margins.leadingAnchor].active = YES;
        [_viewMain.trailingAnchor constraintEqualToAnchor:margins.trailingAnchor].active = YES;
        [_viewMain.topAnchor constraintEqualToAnchor:self.topLayoutGuide.bottomAnchor].active = YES;
        [_viewMain.bottomAnchor constraintEqualToAnchor:self.bottomLayoutGuide.topAnchor].active = YES;
    }
    // Refresh myView and/or main view
    [self.view layoutIfNeeded];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.frame = [UIScreen mainScreen].bounds;
    //
    [self initSafeArea];
    //
    appDelegate = MyAppDelegate();
    _lblTitle.text = MYLocalizedString(@"program.list", nil);
    [self loadProgramBy];
}

- (IBAction) clickBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) loadProgramBy
{
    [arrDatas removeAllObjects];
    if(appDelegate.arrPrograms.count == 0){
        //load data at local
        NSMutableArray *arrDict = [[CouchbaseEvents sharedInstance] loadAllProgram];
        NSArray *arrSort = [[NSMutableArray alloc] init];
        if(arrDict.count > 0){
            arrSort = [arrDict sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *a, NSDictionary *b) {
                int arr1 = [[a objectForKey:@"arrange"] intValue];
                int arr2 = [[b objectForKey:@"arrange"] intValue];
                return arr1 > arr2;
            }];
        }
        NSString *strJson = @"[";
        ProgramObj *obj = nil;
        int i = 0;
        for(NSDictionary *d in arrSort){
            obj = [[ProgramObj alloc] initWithDict:d];
            //======= load photo offline =====//
            if(obj.arrLinkImage.count == 0){
                UIImage *image = [[CouchbaseEvents sharedInstance] getProgramPhoto:[NSString stringWithFormat:@"program_%@_%i.jpg", obj.pID, 0] andProgramID:obj.pID];
                [obj.arrImage addObject:image];
            } else {
                for(NSInteger i = 0; i < obj.arrLinkImage.count; i++){
                    UIImage *image = [[CouchbaseEvents sharedInstance] getProgramPhoto:[NSString stringWithFormat:@"program_%@_%zd.jpg", obj.pID, i] andProgramID:obj.pID];
                    [obj.arrImage addObject:image];
                }
            }
            
            NSString *id_chuongtrinh = [NSString stringWithFormat:@"{\"id_chuongtrinh\":\"%@\"", obj.pID];
            NSString *md5 = [NSString stringWithFormat:@"\"md5\":\"%@\"},", obj.md5];
            if(i == arrSort.count - 1)
                md5 = [md5 stringByReplacingOccurrencesOfString:@"," withString:@""];
            strJson = [strJson stringByAppendingString:[NSString stringWithFormat:@"%@,%@", id_chuongtrinh, md5]];
            i++;
            
            [appDelegate.arrPrograms addObject:obj];
        }
        strJson = [strJson stringByAppendingString:@"]"];//bổ sung mốc ] vào cuối
        for(ProgramObj *p in appDelegate.arrPrograms){
            for(DetailListObj *d in p.detail_list){
                if([d.id_diadiem isEqualToString:id_diadiemSelected]){
                    [arrDatas addObject:p];
                    break;
                }
            }
        }
        //call load on server
        [self connectToServer:0 andDatas:strJson];
    } else {
        for(ProgramObj *p in appDelegate.arrPrograms){
            for(DetailListObj *d in p.detail_list){
                if([d.id_diadiem isEqualToString:id_diadiemSelected]){
                    [arrDatas addObject:p];
                    break;
                }
            }
        }
    }
    
    [_tableView reloadData];
}

#pragma mark - UItableview
#pragma mark tableview Delegate method
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Indentifier";
    
    ProgramCell *cell = (ProgramCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:IS_IPAD?@"ProgramCellIPad":@"ProgramCell" owner:self options:nil];
        for(id currentObject in topLevelObjects) {
            if([currentObject isKindOfClass:[UITableViewCell class]])
            {
                cell = (ProgramCell *)currentObject;
                cell.backgroundColor = [UIColor clearColor];
                break;
            }
        }
    }
    
    ProgramObj *obj = [arrDatas objectAtIndex:indexPath.row];
    if(obj){
        cell.lblTitle.text = obj.pName;
        if(obj.arrImage.count > 0){
            cell.imgThumb.image = [obj.arrImage objectAtIndex:0];
            [cell.progress stopAnimating];
        } else
            cell.imgThumb.image = [UIImage imageNamed:@"test_thumb0.png"];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_tableView deselectRowAtIndexPath:indexPath animated:NO];
 
    ProgramObj *obj = [arrDatas objectAtIndex:indexPath.row];
    ProgramDetailViewController *detail = [[ProgramDetailViewController alloc] initWithObjectSelected:obj];
    [self.navigationController pushViewController:detail animated:YES];
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return nil;
}

#pragma TableDatasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(!IS_IPAD){
        float hCell = 200.0f;
        ProgramObj *obj = nil;
        obj = [arrDatas objectAtIndex:indexPath.row];
        CGRect textRect = [obj.pName boundingRectWithSize:CGSizeMake(_tableView.frame.size.width - 22, 9999)
                                                          options:NSStringDrawingUsesLineFragmentOrigin
                                                       attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica Neue" size:15]}
                                                          context:nil];
                
                
        CGSize size = textRect.size;
        float h = size.height;
        
        if(h <= 21)
            return hCell;
        else
            return hCell + (h - 21) + h/21;
    } else {
        float hCell = 400.0f;
        ProgramObj *obj = nil;
        obj = [arrDatas objectAtIndex:indexPath.row];
        CGRect textRect = [obj.pName boundingRectWithSize:CGSizeMake(_tableView.frame.size.width - 22, 9999)
                            options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica Neue" size:24]}
                                                          context:nil];
                
                
        CGSize size = textRect.size;
        float h = size.height;
                
        if(h <= 50)
            return hCell;
        else
            return hCell + (h - 50) + h/50;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrDatas.count;
}

//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////// Connect with Server /////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
- (void) connectToServer:(int)typeReg andDatas:(NSString *)datas
{
    if(![CheckConnection hasConnectivity]){
        if(arrDatas.count == 0)
            [UIAlertView showWithTitle:MYLocalizedString(@"error.internet.title", nil) message:MYLocalizedString(@"error.internet.content", nil) handler:nil];
        else {
            [appDelegate.arrPrograms removeAllObjects];
            [appDelegate.arrPrograms addObjectsFromArray:arrDatas];
        }
        return;
    }
    
    if(arrDatas.count == 0)
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    id successBlock = ^(AFHTTPRequestOperation *operation, id jsonResult)
    {
        //NSLog(@"%@", jsonResult);
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        switch (typeReg) {
            case 0:{//get program
                int type = [[jsonResult objectForKey:@"type"] intValue];
                if(type == 1){
                    NSArray *arrDict = (NSArray *)[jsonResult objectForKey:@"list"];
                    if(arrDict.count == 0)
                        return;
                    NSArray *arrSort = [arrDict sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *a, NSDictionary *b) {
                        int arr1 = [[a objectForKey:@"arrange"] intValue];
                        int arr2 = [[b objectForKey:@"arrange"] intValue];
                        return arr1 > arr2;
                    }];
                    
                    [[CouchbaseEvents sharedInstance] addProgramList:arrDict];
                    
                    ProgramObj *obj = nil;
                    for (NSDictionary *d in arrSort) {
                        obj = [[ProgramObj alloc] initWithDict:d];
                        
                        if(obj.md5.length == 0){//remove item at local
                            [self removeItemAtLocal:obj.pID andType:0];
                            continue;
                        }
                        obj.loadImageDelegate = (id)appDelegate;
                        [appDelegate.arrPrograms addObject:obj];
                    }
                    
                    [arrDatas removeAllObjects];
                    for(ProgramObj *p in appDelegate.arrPrograms){
                        for(DetailListObj *d in p.detail_list){
                            if([d.id_diadiem isEqualToString:id_diadiemSelected]){
                                [arrDatas addObject:p];
                                break;
                            }
                        }
                    }
                    [_tableView reloadData];
                }
                break;
            }
            default:
                break;
        }
    };
    
    id failureBlock = ^(AFHTTPRequestOperation *operation, NSError *error) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        if(arrDatas.count == 0)
            [UIAlertView showWithTitle:MYLocalizedString(@"error.fail.title", nil) message:MYLocalizedString(@"error.fail.content", nil) handler:nil];
    };
    
    switch (typeReg) {
        case 0:{//get all program
            [[MyApiClient sharedClient] getAllPrograms:datas andSuccesBlock:successBlock andFailBlock:failureBlock];
            break;
        }
        default:
            break;
    }
}

#pragma common
- (void) showOLGhostAlertView: (NSString *) title content:(NSString *) strContent time: (int) timeout
{
    OLGhostAlertView *olAlert = [[OLGhostAlertView alloc] initWithTitle:title message:strContent timeout:timeout dismissible:YES];
    [olAlert show];
}

- (void)reloadDataTable:(NSInteger) index
{
    NSString *strIndex = [NSString stringWithFormat:@"%zd", index];
    [self performSelectorOnMainThread:@selector(refreshTable:) withObject:strIndex waitUntilDone:NO];
}

- (void) refreshTable:(NSString *) strIndex
{
    NSInteger index = [strIndex integerValue];
    if(index >= arrDatas.count || index < 0)
        return;
    
    [_tableView beginUpdates];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
    [_tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
    [_tableView endUpdates];
}

- (void) removeItemAtLocal:(NSString *)_id andType:(int)tab
{
    switch (tab) {
        case 0:{//xoa 1 program
            for(ProgramObj *obj in arrDatas){
                if([obj.pID isEqualToString:_id]){
                    [arrDatas removeObject:obj];
                    break;
                }
            }
            break;
        }
        default:
            break;
    }
}

@end
