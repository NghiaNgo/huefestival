//
//  ProgramFillterByLocationIDViewController.h
//  Festival2016
//
//  Created by Phan Phuoc Luong on 1/11/16.
//  Copyright © 2016 Phan Phuoc Luong. All rights reserved.
//

#import "MyButton.h"
#import "CouchbaseEvents.h"

@interface ProgramFillterByLocationIDViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    AppDelegate *appDelegate;
    
    NSString *id_diadiemSelected;
    NSMutableArray *arrDatas;
}

- (id) initWithLocationIDSelected:(NSString *)id_diadiem;
@property (nonatomic, weak) IBOutlet UIView *viewMain;
//
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UIView *viewHeader;
@property (nonatomic, weak) IBOutlet UILabel *lblTitle;

@end
