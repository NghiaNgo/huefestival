//
//  MapViewController.m
//  Festival2016
//
//  Created by Phan Phuoc Luong on 11/4/15.
//  Copyright © 2015 Phan Phuoc Luong. All rights reserved.
//

#import "MapViewController.h"
#import "CouchbaseEvents.h"
#import "LocationObj.h"
#import "MapsAPI.h"
#import <objc/runtime.h>
//xem danh sach các chương trình diễn ra tại địa điểm ABC
#import "ProgramFillterByLocationIDViewController.h"
//
#import "ProgramObj.h"

@interface MapViewController ()
{
    int DISTANCE;
}
@end

static const CGFloat CalloutYOffset = 57.0f;
@implementation MapViewController

- (id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if(self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]){
        appDelegate = MyAppDelegate();
        arrDatas = [[NSMutableArray alloc] init];
        arrMarkers = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    [_mapView setMapType:kGMSTypeNone];
    [_mapView setMapType:kGMSTypeNormal];
}

- (void) setDatas:(int)type andDatas:(NSMutableArray *)datas
{
    typeHeader = type;
    if(_mapView)
        [_mapView clear];
    [arrDatas removeAllObjects];
    [arrMarkers removeAllObjects];
    if(myMarker && _mapView){
        myMarker.map = _mapView;
        [arrMarkers addObject:myMarker];
    }
    [arrDatas addObjectsFromArray:datas];
}

- (void)initSafeArea {
    _viewMain.translatesAutoresizingMaskIntoConstraints = NO;
    
    if (@available(iOS 11, *)) {
        UILayoutGuide *guide = self.view.safeAreaLayoutGuide;
        [_viewMain.leadingAnchor constraintEqualToAnchor:guide.leadingAnchor].active = YES;
        [_viewMain.trailingAnchor constraintEqualToAnchor:guide.trailingAnchor].active = YES;
        [_viewMain.topAnchor constraintEqualToAnchor:guide.topAnchor].active = YES;
        [_viewMain.bottomAnchor constraintEqualToAnchor:guide.bottomAnchor].active = YES;
    } else {
        UILayoutGuide *margins = self.view.layoutMarginsGuide;
        [_viewMain.leadingAnchor constraintEqualToAnchor:margins.leadingAnchor].active = YES;
        [_viewMain.trailingAnchor constraintEqualToAnchor:margins.trailingAnchor].active = YES;
        [_viewMain.topAnchor constraintEqualToAnchor:self.topLayoutGuide.bottomAnchor].active = YES;
        [_viewMain.bottomAnchor constraintEqualToAnchor:self.bottomLayoutGuide.topAnchor].active = YES;
    }
    // Refresh myView and/or main view
    [self.view layoutIfNeeded];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.frame = [UIScreen mainScreen].bounds;
    //
    [self initSafeArea];
    //
    DISTANCE = 500;
    //icon for mymarker
    pinIconOnMap = [UIImage imageNamed:@"ic_you3.png"];
    icPinNoBearing = [self createAnimationMarkerNoBearing];
    icPinBearing = [self createAnimationMarkerBearing];
    
    title = MYLocalizedString(@"location.map", nil);
    _lblTitle.text = title;
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:16.467712
                                                            longitude:107.579099
                                                                 zoom:15];
    _mapView.camera = camera;
    _mapView.myLocationEnabled = NO;
    _mapView.settings.compassButton = NO;
    _mapView.delegate = self;
    
    //location manager
    if(!_locationManager)
        _locationManager  = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    [_locationManager startUpdatingLocation];
    if([CLLocationManager headingAvailable]){
        isBearing = NO;//co dung kiem tra trang thai ban do co dang hien thi dang driving
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
        {
            [_locationManager requestWhenInUseAuthorization];
            [_locationManager requestAlwaysAuthorization];
        }
        [_locationManager startUpdatingHeading];
        [self addButtonBearing];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //regis disable sleep for this view
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    
    Ivar ivar =  class_getInstanceVariable([UITextField class], "_placeholderLabel");
    UILabel *placeholderLabel = object_getIvar(_txtSearch, ivar);
    placeholderLabel.textColor = [UIColor colorWithWhite:1.0 alpha:0.8f];
    //
    [_txtSearch setPlaceholder:MYLocalizedString(@"search", nil)];
    _txtSearch.hidden = YES;
    
    if(typeHeader == 0)
        [_btnMoreProgram setTitle:MYLocalizedString(@"location.fillterprogram", nil) forState:UIControlStateNormal];
    else
        [_btnMoreProgram setTitle:MYLocalizedString(@"location.nearby", nil) forState:UIControlStateNormal];
    [_btnMoreProgram setHidden:(typeHeader == 1)];
    [self updateLocationBearing];
    
    [self addMarkerToMapView];
    [_locationManager startUpdatingLocation];
    //detech location service on/off
    if(![CLLocationManager locationServicesEnabled]){
        [UIAlertView showTurnOnLocationAlert:MYLocalizedString(@"locationservice.title", nil) message:MYLocalizedString(@"location.off", nil) handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
            if(buttonIndex == 1){
                [[UIApplication sharedApplication] openURL: [NSURL URLWithString: UIApplicationOpenSettingsURLString]];
            }
        }];
    } else if([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied){
        [UIAlertView showTurnOnLocationAlert:MYLocalizedString(@"locationservice.title", nil) message:MYLocalizedString(@"location.denie", nil) handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
            if(buttonIndex == 1){
                [[UIApplication sharedApplication] openURL: [NSURL URLWithString: UIApplicationOpenSettingsURLString]];
            }
        }];
    }
    
    //find direction
    if(myMarker && !polyline){
        for(GMSMarker *marker in arrMarkers){
            if(marker != myMarker){
                NSString *toLocation = [NSString stringWithFormat:@"%lf,%lf", marker.position.latitude, marker.position.longitude];
                [self searchDirectionOnMap:toLocation];
                break;
            }
        }
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    //regis disable sleep for this view
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
}

//============= Click able ============//
- (IBAction) clickBack:(id)sender
{
    polyline = nil;
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction) clickSearch:(id)sender
{
    [_txtSearch becomeFirstResponder];
}

#pragma TexyField Search Delegate
-(void)textFieldDidChange :(UITextField *)theTextField
{
    NSString *str = [theTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if(str.length > 0)
        [self searchAPIonMap:str];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_txtSearch resignFirstResponder];
    return YES;
}

/**
 * zoom cac marker de xem dc tren man hinh
 */
- (void)fitBoundMapView
{
    if(arrMarkers.count <= 1)
        return;
    GMSCoordinateBounds *bounds;
    for (GMSMarker *marker in arrMarkers) {
        if (bounds == nil) {
            bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:marker.position
                                                          coordinate:marker.position];
        }
        bounds = [bounds includingCoordinate:marker.position];
    }
    GMSCameraUpdate *update = [GMSCameraUpdate fitBounds:bounds withPadding:100.0f];
    [_mapView animateWithCameraUpdate:update];
}

#pragma mark - CLLocationManager
#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    //NSLog(@"%@", error.description);
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    if(newLocation){
        appDelegate.myCoordinate = newLocation.coordinate;
        angle = [self calculateAngle:newLocation];
        if(!myMarker){
            myMarker = [GMSMarker markerWithPosition:appDelegate.myCoordinate];
            myMarker.icon = icPinNoBearing;
            myMarker.groundAnchor = CGPointMake(myMarker.groundAnchor.x, myMarker.groundAnchor.y - 0.5);
            myMarker.appearAnimation = kGMSMarkerAnimationNone;
            myMarker.map = _mapView;
            [CATransaction begin];
            [CATransaction setAnimationDuration:1.f];
            [_mapView animateToLocation:myMarker.position];
            [CATransaction commit];
            [arrMarkers addObject:myMarker];
            [self fitBoundMapView];
        } else {
            myMarker.position = newLocation.coordinate;
        }
    }
}

#pragma mark - convert location to marker
- (void) addMarkerToMapView
{
    if(typeHeader != 2){
        for(LocationObj *o in arrDatas){
            BOOL isNew = YES;
            for(GMSMarker *m in arrMarkers){
                NSString *mID = (NSString *)m.userData;
                if([mID isEqualToString:o.id_diadiem]){
                    isNew = NO;
                    m.position = CLLocationCoordinate2DMake(o.lat, o.lng);
                    m.title = o.diadiem_name;
                    break;
                }
            }
        
            if(isNew && o.lat > 0 && o.lng > 0) {
                GMSMarker *maker = [GMSMarker markerWithPosition:CLLocationCoordinate2DMake(o.lat, o.lng)];
                maker.title = o.diadiem_name;
                maker.userData = o.id_diadiem;
                maker.icon = [self creatMarkerView:o.diadiem_name andDesc:o.diadiem_content andIcon:pinIconOnMap];
                maker.appearAnimation = kGMSMarkerAnimationNone;
                maker.map = _mapView;
                [arrMarkers addObject:maker];
                //gán gia trị id dia diem duoc chon
                id_diadiem_selected = o.id_diadiem;
            }
        }
    } else {//danh sách các điểm diễn đang diễn ra bán kính 500m
        if(appDelegate.arrPrograms.count == 0){
            [self loadProgramAtLocal];
            return;
        }
        CLLocation *location1 = [[CLLocation alloc] initWithLatitude:appDelegate.myCoordinate.latitude longitude:appDelegate.myCoordinate.longitude];
        CLLocation *location2 = nil;
        for(LocationObj *o in arrDatas){
            location2 = [[CLLocation alloc] initWithLatitude:o.lat longitude:o.lng];
            
            CLLocationDistance distanceInMeters = [location1 distanceFromLocation:location2];
            if(distanceInMeters < DISTANCE){//nho hon 500m
                for(ProgramObj *p in appDelegate.arrPrograms){//tim chuong trinh có thoi gian hien tai
                    for(DetailListObj *d in p.detail_list ){
                        if([d.id_diadiem isEqualToString:o.id_diadiem] && [self checkTimeProgram:d]){
                            BOOL isNew = YES;
                            for(GMSMarker *m in arrMarkers){
                                NSString *mID = (NSString *)m.userData;
                                if([mID isEqualToString:o.id_diadiem]){
                                    isNew = NO;
                                    m.position = CLLocationCoordinate2DMake(o.lat, o.lng);
                                    m.title = o.diadiem_name;
                                    break;
                                }
                            }
                            
                            if(isNew && o.lat > 0 && o.lng > 0) {
                                GMSMarker *maker = [GMSMarker markerWithPosition:CLLocationCoordinate2DMake(o.lat, o.lng)];
                                maker.title = o.diadiem_name;
                                maker.userData = o.id_diadiem;
                                maker.icon = [self creatMarkerView:o.diadiem_name andDesc:o.diadiem_content andIcon:pinIconOnMap];
                                maker.appearAnimation = kGMSMarkerAnimationNone;
                                maker.map = _mapView;
                                [arrMarkers addObject:maker];
                            }
                            break;
                        }
                    }
                }
                
            }
        }
        NSString *metter = @"m";
        int numDistance = DISTANCE;
        if(numDistance >= 1000){
            numDistance /= 1000;
            metter = @"km";
        }
        int number = (int)arrMarkers.count - 1;
        if(number < 0)
            number = 0;
        [_btnMoreProgram setTitle:[NSString stringWithFormat:MYLocalizedString(@"location.nearby", nil), number, numDistance, metter] forState:UIControlStateNormal];
    }
    
    [self fitBoundMapView];
}

- (BOOL)checkTimeProgram:(DetailListObj *)obj
{
    double secondsInMinute = 60;
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"dd/MM/yyyy"];
    [format setTimeZone:[NSTimeZone systemTimeZone]];
    NSDate* currentdate = [NSDate date];
    NSString *cString = [format stringFromDate:currentdate];
    currentdate = [format dateFromString:cString];
    
    NSString *fdate = obj.fdate;
    NSString *tdate = obj.tdate;
    
    NSDateFormatter *format1 = [[NSDateFormatter alloc] init];
    [format1 setDateFormat:@"yyyy-MM-dd"];
    
    NSDate *fd = [format1 dateFromString:fdate];
    fdate = [format stringFromDate:fd];
    fd = [format dateFromString:fdate];
    
    NSDate *td = [format1 dateFromString:tdate];
    tdate = [format stringFromDate:td];
    td = [format dateFromString:tdate];
    
    if(![fdate isEqualToString:tdate]){
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSUInteger unitFlags = NSMonthCalendarUnit | NSDayCalendarUnit;
        NSDateComponents *components = [calendar components:unitFlags fromDate:fd toDate:td options:0];
        NSInteger numDays = [components day];
        NSRange days = [calendar rangeOfUnit:NSDayCalendarUnit inUnit:NSMonthCalendarUnit forDate:fd];
        
        components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:fd];
        NSInteger fday = [components day];
        NSInteger fmonth = [components month];
        NSInteger fyear = [components year];
        components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:td];
        NSInteger tday = [components day];
        NSInteger tmonth = [components month];
        for(int i = 0; i < numDays; i++){
            fday += 1;
            if(fmonth == tmonth){
                if(fday > tday)
                    break;
            } else if(fmonth < tmonth){
                int daysInGivenMonth = (int)days.length;
                if(fday > daysInGivenMonth){
                    fmonth = tmonth;
                    fday = 1;
                }
            }
            
            NSString *strDate = [NSString stringWithFormat:@"%zd/%zd/%zd", fday, fmonth, fyear];
            NSDate *enddate = [format dateFromString:strDate];
            
            NSTimeInterval distanceBetweenDates = [enddate timeIntervalSinceDate:currentdate];
            NSInteger secondsBetweenDates = distanceBetweenDates / secondsInMinute;
            if (secondsBetweenDates == 0){
                if([obj.time isEqualToString:@"00:00:00"])
                    return YES;
                
                [format setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
                cString = [format stringFromDate:[NSDate date]];
                currentdate = [format dateFromString:cString];
                strDate = [NSString stringWithFormat:@"%@ %@", enddate, obj.time];
                enddate = [format dateFromString:strDate];
                NSTimeInterval distanceBetweenDates = [currentdate timeIntervalSinceDate:enddate];
                NSInteger secondsBetweenDates = distanceBetweenDates / secondsInMinute;
                if(secondsBetweenDates <= 60)//nhỏ hon 1 giờ
                    return YES;
            }
        }
    } else {
        NSTimeInterval distanceBetweenDates = [fd timeIntervalSinceDate:currentdate];
        NSInteger secondsBetweenDates = distanceBetweenDates / secondsInMinute;
        if (secondsBetweenDates == 0){
            if([obj.time isEqualToString:@"00:00:00"])//cả ngày
                return YES;
            [format setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
            cString = [format stringFromDate:[NSDate date]];
            currentdate = [format dateFromString:cString];
            
            NSString *strDate = [NSString stringWithFormat:@"%@ %@", fdate, obj.time];
            NSDate *enddate = [format dateFromString:strDate];
            NSTimeInterval distanceBetweenDates = [currentdate timeIntervalSinceDate:enddate];
            NSInteger secondsBetweenDates = distanceBetweenDates / secondsInMinute;
            if(secondsBetweenDates <= 60)//nhỏ hơn 1 giờ
                return YES;
        }
    }
    return NO;
}

- (void) drawLineOnMap:(NSMutableArray *)arrLocations
{
    GMSMutablePath *path = [GMSMutablePath path];
    for(NSDictionary *dict in arrLocations){
        double lat = [[dict objectForKey:@"lat"] doubleValue];
        double lng = [[dict objectForKey:@"lng"] doubleValue];
        [path addLatitude:lat longitude:lng];
    }
    if(!polyline)
        polyline = [GMSPolyline polylineWithPath:path];
    else
        [polyline setPath:path];
    polyline.strokeWidth = 5.f;
    polyline.strokeColor = [UIColor redColor];
    polyline.map = _mapView;
}

/////////////////////////////////////////////////////////////////
/////////////////////// add button bearing //////////////////////
/////////////////////////////////////////////////////////////////
- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading
{
    direction = newHeading.magneticHeading;
    
    if(isBearing){
        //cho xoay ban do theo huong compass
        [CATransaction begin];
        [CATransaction setAnimationDuration:0.5f];
        float f = RadiansToDegrees(DegreesToRadians(direction)) - angle;
        [_mapView animateToBearing:f];
        [CATransaction commit];
    }
}

/**
 * Ham tinh goc de ve compass point
 */
// Caculate the angle between the north and the direction to observed geo-location
-(float)calculateAngle:(CLLocation *)userlocation
{
    float userLocationLatitude = DegreesToRadians(userlocation.coordinate.latitude);
    float userLocationLongitude = DegreesToRadians(userlocation.coordinate.longitude);
    
    float targetedPointLatitude = DegreesToRadians(myMarker.position.latitude);
    float targetedPointLongitude = DegreesToRadians(myMarker.position.longitude);
    
    float longitudeDifference = targetedPointLongitude - userLocationLongitude;
    
    float y = sin(longitudeDifference) * cos(targetedPointLatitude);
    float x = cos(userLocationLatitude) * sin(targetedPointLatitude) - sin(userLocationLatitude) * cos(targetedPointLatitude) * cos(longitudeDifference);
    float radiansValue = atan2(y, x);
    if (radiansValue >= 0) {
        return radiansValue;
    } else {
        return 360+radiansValue;
    }
    return radiansValue;
}

- (void) addButtonBearing
{
    btnBearing = [[MyButton alloc] init];
    CGRect mR = self.view.frame;
    float y = mR.size.height - 90;
    float x = mR.size.width - 45;
    btnBearing.frame = CGRectMake(x, y, 40, 40);
    [btnBearing setBackgroundImage:[UIImage imageNamed:@"ic_bearing.png"] forState:UIControlStateNormal];
    btnBearing.layer.cornerRadius = 4;
    btnBearing.layer.borderWidth = 1;
    btnBearing.layer.borderColor = [UIColor colorWithRed:129/255.0 green:129/255.0 blue:129/255.0 alpha:0.5f].CGColor;
    btnBearing.myDatas = @"NO";
    [btnBearing addTarget:self action:@selector(clickBearingMapView:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnBearing];
}

- (void) updateLocationBearing
{
    CGRect r = btnBearing.frame;
    r.origin.y = appDelegate.screenHeight - (_btnMoreProgram.isHidden?45:90);
    btnBearing.frame = r;
}

- (void) clickBearingMapView:(id) sender
{
    if(![CLLocationManager locationServicesEnabled] || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied){
        [UIAlertView showTurnOnLocationAlert:MYLocalizedString(@"locationservice.title", nil) message:MYLocalizedString(@"location.off", nil) handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
            if(buttonIndex == 1){
                [[UIApplication sharedApplication] openURL: [NSURL URLWithString: UIApplicationOpenSettingsURLString]];
            }
        }];
        return;
    }
    
    MyButton *myBtn = (MyButton *)sender;
    NSString *data = (NSString *)myBtn.myDatas;
    if([data isEqualToString:@"NO"]){
        myBtn.myDatas = @"YES";
        [myBtn setBackgroundImage:[UIImage imageNamed:@"ic_bearing_focus.png"] forState:UIControlStateNormal];
        
        isBearing = YES;
        [CATransaction begin];
        [CATransaction setAnimationDuration:1.f];
        [_mapView animateToLocation:myMarker.position];
        [_mapView animateToZoom:_mapView.camera.zoom + 3];
        [_mapView animateToBearing:RadiansToDegrees(DegreesToRadians(direction)) - angle];
        [_mapView animateToViewingAngle:15];
        [CATransaction commit];
        // Rotate the arrow image
        [UIView animateWithDuration:1.0f animations:^{
            myMarker.icon = icPinBearing;
        }];
    } else {
        myBtn.myDatas = @"NO";
        [myBtn setBackgroundImage:[UIImage imageNamed:@"ic_bearing.png"] forState:UIControlStateNormal];
        
        isBearing = NO;
        [CATransaction begin];
        [CATransaction setAnimationDuration:1.f];
        [_mapView animateToLocation:myMarker.position];
        [_mapView animateToZoom:_mapView.camera.zoom - 3];
        [_mapView animateToBearing:0];
        [_mapView animateToViewingAngle:0];
        [CATransaction commit];
        // Rotate the arrow image
        [UIView animateWithDuration:1.0f animations:^{
            myMarker.icon = icPinNoBearing;
        }];
    }
}

//=========== create Marker ================//
- (UIImage *) createAnimationMarkerNoBearing
{
    NSMutableArray *frames = [NSMutableArray array];
    for (int index = 0; index < 22; index++) {
        int i = index;
        if(index > 8)
            i = 8 - index%8;
        if(index >= 16)
            i = 0;
        NSString *img = [NSString stringWithFormat:@"pi%i copy.png", i];
        [frames addObject:[UIImage imageNamed:img]];
    }
    return [UIImage animatedImageWithImages:frames duration:3];
}

- (UIImage *) createAnimationMarkerBearing
{
    NSMutableArray *frames = [NSMutableArray array];
    for (int index = 0; index < 22; index++) {
        int i = index;
        if(index > 8)
            i = 8 - index%8;
        if(index >= 16)
            i = 0;
        NSString *img = [NSString stringWithFormat:@"pi%i.png", i];
        [frames addObject:[UIImage imageNamed:img]];
    }
    return [UIImage animatedImageWithImages:frames duration:3];
}

//=========================================================//
//================= GMSMapViewDelegate ====================//
//=========================================================//
#pragma mark - GMSMapViewDelegate

- (UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker
{
    return _emptyCalloutView;
}

- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker
{
    // Animate to the marker
    [CATransaction begin];
    [CATransaction setAnimationDuration:0.25f];
    
    GMSCameraPosition *camera = [[GMSCameraPosition alloc] initWithTarget:marker.position
                                         zoom:_mapView.camera.zoom
                                      bearing:0
                                 viewingAngle:0];
    [_mapView animateToCameraPosition:camera];
    [CATransaction commit];
    
    if (marker != _mapView.selectedMarker && marker == myMarker) {
        //show popup create favorite when click on me
        return NO;
    } else {//tim đường đi
        polyline.map = nil;
        NSString *toLocation = [NSString stringWithFormat:@"%lf,%lf", marker.position.latitude, marker.position.longitude];
        [self searchDirectionOnMap:toLocation];
        if(typeHeader == 2){
            id_diadiem_selected = (NSString *)marker.userData;
            [self performSelector:@selector(clickMoreProgram:) withObject:nil afterDelay:0.0];
        }
    }
    return YES;
}

- (void)mapView:(GMSMapView *)pMapView didChangeCameraPosition:(GMSCameraPosition *)position
{
    /* move callout with map drag */
    if (pMapView.selectedMarker != nil && !_calloutView.hidden) {
        CLLocationCoordinate2D anchor = [pMapView.selectedMarker position];
        
        CGPoint arrowPt = self.calloutView.backgroundView.arrowPoint;
        
        CGPoint pt = [pMapView.projection pointForCoordinate:anchor];
        pt.x -= arrowPt.x;
        pt.y -= arrowPt.y + CalloutYOffset;
        
        _calloutView.frame = (CGRect) {.origin = pt, .size = _calloutView.frame.size };
    } else {
        [_calloutView dismissCalloutAnimated:YES];
    }
}

- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    [_calloutView dismissCalloutAnimated:YES];
}

- (void)calloutViewClicked:(SMCalloutView *)calloutView
{
}

- (IBAction) clickMoreProgram:(id)sender
{
    if(typeHeader != 2 || !sender){
        if(id_diadiem_selected > 0){
            ProgramFillterByLocationIDViewController *program  = [[ProgramFillterByLocationIDViewController alloc] initWithLocationIDSelected:id_diadiem_selected];
            [self.navigationController pushViewController:program animated:YES];
        }
    } else if(typeHeader == 2 && sender){
        UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:MYLocalizedString(@"location.distance1", nil) delegate:self cancelButtonTitle:MYLocalizedString(@"close", nil) destructiveButtonTitle:nil otherButtonTitles:
                                MYLocalizedString(@"location.distance2", nil),
                                MYLocalizedString(@"location.distance3", nil),
                                MYLocalizedString(@"location.distance4", nil),
                                MYLocalizedString(@"location.distance5", nil),
                                nil];
        if(!IS_IPAD)
            [popup showInView:[UIApplication sharedApplication].keyWindow];
        else
            [popup showFromRect:_btnMoreProgram.frame inView:self.view animated:YES];
    }
}

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(buttonIndex == popup.cancelButtonIndex)
        return;
    switch (buttonIndex) {
        case 0:{
            DISTANCE = 100;
            break;
        }
        case 1:{
            DISTANCE = 500;
            break;
        }
        case 2:{
            DISTANCE = 1000;
            break;
        }
        case 3:{
            DISTANCE = 5000;
            break;
        }
    }
    if(_mapView)
        [_mapView clear];
    [arrMarkers removeAllObjects];
    if(myMarker && _mapView){
        myMarker.map = _mapView;
        [arrMarkers addObject:myMarker];
    }
    [self addMarkerToMapView];
}

//=========================================================//
//=================== Tạo Marker View =====================//
//=========================================================//
- (UIImage *) creatMarkerView:(NSString *)strTitle andDesc:(NSString *)des andIcon:(UIImage *)pinIcon
{
    int padding = 10.0f; int x = 0, y = 0; int w = 150, h = 100;
    UIFont *font = [UIFont boldSystemFontOfSize:13.0f];
    UIFont *font1 = [UIFont systemFontOfSize:11.0f];
    
    CGSize strSize1;
    CGSize strSize2;
        
    strSize1 = [strTitle sizeWithFont:font];
    strSize2 = [des sizeWithFont:font1];
        
    float max = MAX(strSize1.width, strSize2.width);
    w = max + 2 * padding + 30;
    if(w < 150)
        w = 150;
    else if(w > self.view.frame.size.width - 40)
        w = self.view.frame.size.width - 40;
    
    //dung de kiem tra co show des ko
    BOOL isShowDescription = des.length > 0;
    
    UIView *markerView = [[UIView alloc] initWithFrame:CGRectMake(x,y,w,h)];
    markerView.backgroundColor = [UIColor clearColor];
    //================ add background phia tren ==============//
    UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(x, y, w, h - 40)];
    contentView.backgroundColor = [UIColor clearColor];
    [markerView addSubview:contentView];
    //back ground for callout
    UIImageView *bgImageView = [[UIImageView alloc] initWithFrame:contentView.bounds];
    bgImageView.image = [UIImage imageNamed:@"bg_callout1.png"];
    [contentView addSubview:bgImageView];
    
    x = padding;
    y = 5;
    int lblH = 18;
    int lblW = w - 2*padding;
    if(!isShowDescription)
        y = (h - 50 - lblH)/2;
    UILabel *lblName = [[UILabel alloc] initWithFrame:CGRectMake(x, y, lblW, lblH)];
    lblName.text = strTitle;
    lblName.textAlignment = NSTextAlignmentLeft;
    lblName.textColor = [UIColor blackColor];
    lblName.font = font;
    lblName.backgroundColor = [UIColor clearColor];
    [contentView addSubview:lblName];
    //truong hop neu co noi dung thi show
    if(isShowDescription){
        y = 23;
        UILabel *lblDes = [[UILabel alloc] initWithFrame:CGRectMake(x, y, lblW, lblH)];
        lblDes.text = des;
        lblDes.textColor = UIColorFromRGB(COLOR_RED);
        lblDes.textAlignment = NSTextAlignmentLeft;
        lblDes.font = font1;
        lblDes.backgroundColor = [UIColor clearColor];
        [contentView addSubview:lblDes];
    }
    //========= end name ===========//
    //======= add icon size 28x40 =========//
    UIImageView *iconView = [[UIImageView alloc] initWithFrame:CGRectMake(w/2 - pinIcon.size.width/2, h - 40, pinIcon.size.width, 40)];
    iconView.image = pinIcon;
    [markerView addSubview:iconView];
    //add button next
    UIButton *btnNext = [[UIButton alloc] initWithFrame:CGRectMake(w - 30, 0, 30, h - 50)];
    btnNext.backgroundColor = [UIColor clearColor];
    btnNext.layer.cornerRadius = 4;
    [btnNext setImage:[UIImage imageNamed:@"ic_direction.png"] forState:UIControlStateNormal];
    [btnNext addTarget:self action:@selector(clickMoreProgram:) forControlEvents:UIControlEventTouchUpInside];
    [markerView addSubview:btnNext];
    //============ end avatar ==========//
    UIImage *mkIcon = [self imageWithView:markerView];
    return mkIcon;
}

- (UIImage *) imageWithView:(UIView *)view
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, NO, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return img;
}

//=================================================//
//===============  SEARCH API  ====================//
//=================================================//
- (void) searchAPIonMap:(NSString *)name
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    id successBlock = ^(AFHTTPRequestOperation *operation, id jsonResult)
    {
        NSLog(@"%@", jsonResult);
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    };
    
    id failureBlock = ^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@", error.description);
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    };
    
    [[MapsAPI sharedClient] searchPlaceAPI:myMarker.position.latitude andLng:myMarker.position.longitude andRadius:500 andType:@"foods" andName:name andSuccesBlock:successBlock andFailBlock:failureBlock];
}

- (void) searchDirectionOnMap:(NSString *)toLocation
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    id successBlock = ^(AFHTTPRequestOperation *operation, id jsonResult)
    {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        NSString *status = [jsonResult objectForKey:@"status"];
        if(!status || [status isKindOfClass:[NSNull class]] || ![status isEqualToString:@"OK"])
            [UIAlertView showWithTitle:MYLocalizedString(@"search.direction.title", nil) message:MYLocalizedString(@"search.direction.content", nil) handler:nil];
        else {//hiển thị đường đi
            @try {
                NSArray *routes = [jsonResult objectForKey:@"routes"];
                NSDictionary *route = [routes lastObject];
                if (route) {
                    NSString *overviewPolyline = [[route objectForKey: @"overview_polyline"] objectForKey:@"points"];
                    NSMutableArray *_path = [self decodePolyLine:overviewPolyline];
                
                    NSInteger numberOfSteps = _path.count;
        
                    GMSMutablePath *path = [GMSMutablePath path];
                    for (NSInteger index = 0; index < numberOfSteps; index++) {
                        CLLocation *location = [_path objectAtIndex:index];
                        double lat = location.coordinate.latitude;
                        double lng = location.coordinate.longitude;
                        [path addLatitude:lat longitude:lng];
                    }
                    if(!polyline)
                        polyline = [GMSPolyline polylineWithPath:path];
                    else
                        [polyline setPath:path];
                    
                    polyline.strokeWidth = 5.f;
                    polyline.strokeColor = [UIColor redColor];
                    polyline.map = _mapView;
                }
                
            }
            @catch (NSException *exception) {}
        }
    };
    
    id failureBlock = ^(AFHTTPRequestOperation *operation, NSError *error) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    };
    
    [[MapsAPI sharedClient] searchDirectionAPI:[NSString stringWithFormat:@"%lf,%lf", myMarker.position.latitude, myMarker.position.longitude] andTo:toLocation andSuccesBlock:successBlock andFailBlock:failureBlock];
}

-(NSMutableArray *)decodePolyLine:(NSString *)encodedStr {
    NSMutableString *encoded = [[NSMutableString alloc] initWithCapacity:[encodedStr length]];
    [encoded appendString:encodedStr];
    [encoded replaceOccurrencesOfString:@"\\\\" withString:@"\\"
                                options:NSLiteralSearch
                                  range:NSMakeRange(0, [encoded length])];
    NSInteger len = [encoded length];
    NSInteger index = 0;
    NSMutableArray *array = [[NSMutableArray alloc] init];
    NSInteger lat=0;
    NSInteger lng=0;
    while (index < len) {
        NSInteger b;
        NSInteger shift = 0;
        NSInteger result = 0;
        do {
            b = [encoded characterAtIndex:index++] - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);
        NSInteger dlat = ((result & 1) ? ~(result >> 1) : (result >> 1));
        lat += dlat;
        shift = 0;
        result = 0;
        do {
            b = [encoded characterAtIndex:index++] - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);
        NSInteger dlng = ((result & 1) ? ~(result >> 1) : (result >> 1));
        lng += dlng;
        NSNumber *latitude = [[NSNumber alloc] initWithFloat:lat * 1e-5];
        NSNumber *longitude = [[NSNumber alloc] initWithFloat:lng * 1e-5];
        
        CLLocation *location = [[CLLocation alloc] initWithLatitude:[latitude floatValue] longitude:[longitude floatValue]];
        [array addObject:location];
    }
    
    return array;
}

//load danh sách các chương trình ở local
- (void) loadProgramAtLocal
{
    if(appDelegate.arrPrograms.count == 0){
        //load data at local
        NSMutableArray *arrDict = [[CouchbaseEvents sharedInstance] loadAllProgram];
        NSArray *arrSort = [arrDict sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *a, NSDictionary *b) {
            int arr1 = [[a objectForKey:@"arrange"] intValue];
            int arr2 = [[b objectForKey:@"arrange"] intValue];
            return arr1 > arr2;
        }];
        NSString *strJson = @"[";
        ProgramObj *obj = nil;
        int i = 0;
        for(NSDictionary *d in arrSort){
            obj = [[ProgramObj alloc] initWithDict:d];
            [appDelegate.arrPrograms addObject:obj];
            
            NSString *id_chuongtrinh = [NSString stringWithFormat:@"{\"id_chuongtrinh\":\"%@\"", obj.pID];
            NSString *md5 = [NSString stringWithFormat:@"\"md5\":\"%@\"},", obj.md5];
            if(i == arrSort.count - 1)
                md5 = [md5 stringByReplacingOccurrencesOfString:@"," withString:@""];
            strJson = [strJson stringByAppendingString:[NSString stringWithFormat:@"%@,%@", id_chuongtrinh, md5]];
            i++;
            
            //======= load photo offline =====//
            if(obj.arrLinkImage.count == 0){
                UIImage *image = [[CouchbaseEvents sharedInstance] getProgramPhoto:[NSString stringWithFormat:@"program_%@_%i.jpg", obj.pID, 0] andProgramID:obj.pID];
                [obj.arrImage addObject:image];
            } else {
                for(NSInteger i = 0; i < obj.arrLinkImage.count; i++){
                    UIImage *image = [[CouchbaseEvents sharedInstance] getProgramPhoto:[NSString stringWithFormat:@"program_%@_%zd.jpg", obj.pID, i] andProgramID:obj.pID];
                    [obj.arrImage addObject:image];
                }
            }
        }
        strJson = [strJson stringByAppendingString:@"]"];//bổ sung mốc ] vào cuối
        //call load on server
        [self connectToServer:0 andDatas:strJson];
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////// Connect with Server /////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
- (void) connectToServer:(int)typeReg andDatas:(NSString *)datas
{
    if(![CheckConnection hasConnectivity])
        return;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    id successBlock = ^(AFHTTPRequestOperation *operation, id jsonResult)
    {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        switch (typeReg) {
            case 0:{//get program
                int type = [[jsonResult objectForKey:@"type"] intValue];
                if(type == 1){
                    NSArray *arrDict = (NSArray *)[jsonResult objectForKey:@"list"];
                    if(arrDict.count == 0){
                        [self addMarkerToMapView];
                        return;
                    }
                    
                    NSArray *arrSort = [arrDict sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *a, NSDictionary *b) {
                        int arr1 = [[a objectForKey:@"arrange"] intValue];
                        int arr2 = [[b objectForKey:@"arrange"] intValue];
                        return arr1 > arr2;
                    }];
                    
                    [[CouchbaseEvents sharedInstance] addProgramList:arrDict];
                    ProgramObj *obj = nil;
                    for (NSDictionary *d in arrSort) {
                        obj = [[ProgramObj alloc] initWithDict:d];
                        
                        if(obj.md5.length == 0){
                            //remove item at local
                            [self removeItemAtLocal:obj.pID];
                            continue;
                        }
                        obj.loadImageDelegate = (id)appDelegate;
                        [appDelegate.arrPrograms addObject:obj];
                    }
                
                    [self addMarkerToMapView];
                }
                break;
            }

            default:
                break;
        }
    };
    
    id failureBlock = ^(AFHTTPRequestOperation *operation, NSError *error) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [UIAlertView showWithTitle:MYLocalizedString(@"error.fail.title", nil) message:MYLocalizedString(@"error.fail.content", nil) handler:nil];
    };
    
    switch (typeReg) {
        case 0:{//get all program
            [[MyApiClient sharedClient] getAllPrograms:datas andSuccesBlock:successBlock andFailBlock:failureBlock];
            break;
        }
        default:
            break;
    }
}

- (void) removeItemAtLocal:(NSString *)_id
{
    //xoa 1 program
    for(ProgramObj *obj in arrDatas){
        if([obj.pID isEqualToString:_id]){
            [arrDatas removeObject:obj];
            break;
        }
    }
}

#pragma common
- (void) showOLGhostAlertView: (NSString *) alertTitle content:(NSString *) strContent time: (int) timeout
{
    OLGhostAlertView *olAlert = [[OLGhostAlertView alloc] initWithTitle:alertTitle message:strContent timeout:timeout dismissible:YES];
    [olAlert show];
}

@end
