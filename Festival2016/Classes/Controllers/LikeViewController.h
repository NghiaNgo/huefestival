//
//  LikeViewController.h
//  Festival2016
//
//  Created by Phan Phuoc Luong on 2/20/16.
//  Copyright © 2016 Phan Phuoc Luong. All rights reserved.
//

#import "FXBlurView.h"

@interface LikeViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    AppDelegate *appDelegate; 
    NSMutableArray *arrDataProgram;
    NSMutableArray *arrDataArtist;
    NSMutableArray *arrDataNews;
    NSMutableArray *arrDataDiaDiem;
}
- (id) initWithDatas:(NSString *)nibName andData:(NSMutableArray *)arrPrograms andData1:(NSMutableArray *)arrArtist andData2:(NSMutableArray *)arrNews andData3:(NSMutableArray *)arrDiaDiem;
@property (nonatomic, weak) IBOutlet UIView *viewMain;
//
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UILabel *lblTitle;
@property (nonatomic, weak) IBOutlet FXBlurView *viewBlur;
@end
