//
//  AboutDetailViewController.h
//  Festival2016
//
//  Created by Phan Phuoc Luong on 2/18/16.
//  Copyright © 2016 Phan Phuoc Luong. All rights reserved.
//

#import "FXBlurView.h"

@interface AboutDetailViewController : UIViewController<UIWebViewDelegate>
{
    AppDelegate *appDelegate;
}

- (id) initWithDatas:(NSString *)nibName andType:(int)type andTitle:(NSString *)title;
@property (nonatomic, weak) IBOutlet UIView *viewMain;
//
@property (nonatomic, weak) IBOutlet UILabel *lblTitle;
@property (nonatomic, weak) IBOutlet FXBlurView *viewBlur;
@property (nonatomic, weak) IBOutlet UIWebView *webView;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *indicatorView;
@end
