//
//  AboutDetailViewController.m
//  Festival2016
//
//  Created by Phan Phuoc Luong on 2/18/16.
//  Copyright © 2016 Phan Phuoc Luong. All rights reserved.
//

#import "AboutDetailViewController.h"
#import "NSString+HTML.h"

@interface AboutDetailViewController ()
{
    int _typeFile;
    NSString *_title;
}
@end

@implementation AboutDetailViewController
- (void)initSafeArea {
    _viewMain.translatesAutoresizingMaskIntoConstraints = NO;
    
    if (@available(iOS 11, *)) {
        UILayoutGuide *guide = self.view.safeAreaLayoutGuide;
        [_viewMain.leadingAnchor constraintEqualToAnchor:guide.leadingAnchor].active = YES;
        [_viewMain.trailingAnchor constraintEqualToAnchor:guide.trailingAnchor].active = YES;
        [_viewMain.topAnchor constraintEqualToAnchor:guide.topAnchor].active = YES;
        [_viewMain.bottomAnchor constraintEqualToAnchor:guide.bottomAnchor].active = YES;
    } else {
        UILayoutGuide *margins = self.view.layoutMarginsGuide;
        [_viewMain.leadingAnchor constraintEqualToAnchor:margins.leadingAnchor].active = YES;
        [_viewMain.trailingAnchor constraintEqualToAnchor:margins.trailingAnchor].active = YES;
        [_viewMain.topAnchor constraintEqualToAnchor:self.topLayoutGuide.bottomAnchor].active = YES;
        [_viewMain.bottomAnchor constraintEqualToAnchor:self.bottomLayoutGuide.topAnchor].active = YES;
    }
    // Refresh myView and/or main view
    [self.view layoutIfNeeded];
}

- (id)initWithDatas:(NSString *)nibName andType:(int)type andTitle:(NSString *)title
{
    if(self = [super initWithNibName:nibName bundle:nil]){
        _typeFile = type;
        _title = title;
        appDelegate = MyAppDelegate();
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.frame = [UIScreen mainScreen].bounds;
    //
    [self initSafeArea];
    //
    _viewBlur.blurRadius = 15;
    _lblTitle.text = _title;

    _webView.delegate = self;
    [self connectToServer:0];
}

- (void) loadContent:(NSString *)content
{
    content = [content stringByDecodingHTMLEntities];
    if(!IS_IPAD)
        content = [NSString stringWithFormat:@"<html>""<style type=\"text/css\">"
               "a:link{color:#000; text-decoration: none}"
               "body{background-color:transparent; font-size:16; font-family: Helvetica Neue; color: #000; text-align:justify; margin:0; padding:10}""</style>"
               "<body>%@</body></html>", content];
    else
        content = [NSString stringWithFormat:@"<html>""<style type=\"text/css\">"
                   "a:link{color:#000; text-decoration: none}"
                   "body{background-color:transparent; font-size:25; font-family: Helvetica Neue; color: #000; text-align:justify; margin:0; padding:20}""</style>"
                   "<body>%@</body></html>", content];
    [_webView loadHTMLString:content baseURL: nil];
    
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    [_indicatorView startAnimating];
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [_indicatorView stopAnimating];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [_indicatorView stopAnimating];
}

- (IBAction) clickBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////// Connect with Server /////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
- (void) connectToServer:(int)typeReg
{
    if(![CheckConnection hasConnectivity])
        return;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    id successBlock = ^(AFHTTPRequestOperation *operation, id jsonResult)
    {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        switch (typeReg) {
            case 0:{//get program
                int type = [[jsonResult objectForKey:@"type"] intValue];
                if(type == 1){
                    NSDictionary *detail = [jsonResult objectForKey:@"detail"];
                    NSString *content = [detail objectForKey:@"content"];
                    if(!content || [content isKindOfClass:[NSNull class]] || content.length == 0)
                        content = @" ";
                    [self loadContent:content];
                }
                break;
            }
                
            default:
                break;
        }
    };
    
    id failureBlock = ^(AFHTTPRequestOperation *operation, NSError *error) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [UIAlertView showWithTitle:MYLocalizedString(@"error.fail.title", nil) message:MYLocalizedString(@"error.fail.content", nil) handler:nil];
    };
    
    switch (typeReg) {
        case 0:{//get list menu
            [[MyApiClient sharedClient] getAboutDetail:_typeFile andSuccesBlock:successBlock andFailBlock:failureBlock];
            break;
        }
        default:
            break;
    }
}

@end
