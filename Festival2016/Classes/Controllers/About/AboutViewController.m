//
//  AboutViewController.m
//  Festival2016
//
//  Created by Phan Phuoc Luong on 2/5/16.
//  Copyright © 2016 Phan Phuoc Luong. All rights reserved.
//

#import "AboutViewController.h"
#import "MenuObj.h"

@interface AboutViewController ()

@end

@implementation AboutViewController

- (void)initSafeArea {
    _viewMain.translatesAutoresizingMaskIntoConstraints = NO;
    
    if (@available(iOS 11, *)) {
        UILayoutGuide *guide = self.view.safeAreaLayoutGuide;
        [_viewMain.leadingAnchor constraintEqualToAnchor:guide.leadingAnchor].active = YES;
        [_viewMain.trailingAnchor constraintEqualToAnchor:guide.trailingAnchor].active = YES;
        [_viewMain.topAnchor constraintEqualToAnchor:guide.topAnchor].active = YES;
        [_viewMain.bottomAnchor constraintEqualToAnchor:guide.bottomAnchor].active = YES;
    } else {
        UILayoutGuide *margins = self.view.layoutMarginsGuide;
        [_viewMain.leadingAnchor constraintEqualToAnchor:margins.leadingAnchor].active = YES;
        [_viewMain.trailingAnchor constraintEqualToAnchor:margins.trailingAnchor].active = YES;
        [_viewMain.topAnchor constraintEqualToAnchor:self.topLayoutGuide.bottomAnchor].active = YES;
        [_viewMain.bottomAnchor constraintEqualToAnchor:self.bottomLayoutGuide.topAnchor].active = YES;
    }
    // Refresh myView and/or main view
    [self.view layoutIfNeeded];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //
    [self initSafeArea];
    //
    appDelegate = MyAppDelegate();
    if(appDelegate.arrAboutList.count == 0){
        [self connectToServer:0];
    } else {
        [_tableView reloadData];
    }
    [self refreshLanguage];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    float w = screenViewWidth*3/4;
    if(IS_IPAD)
        w = screenViewWidth/2;
    CGRect r = _viewHoTro.frame;
    r.size.width = w;
    r.origin.x = screenViewWidth - r.size.width;
    _viewHoTro.frame = r;
    r = _tableView.frame;
    r.size.width = w;
    r.origin.x = screenViewWidth - r.size.width;
    _tableView.frame = r;
}

//refresh language
- (void)refreshLanguage
{
    _lblHoTro.text = MYLocalizedString(@"menu.item0", nil);
}

#pragma mark - UItableview
#pragma mark tableview Delegate method
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Indentifier";
    
    AboutCell *cell = (AboutCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"AboutCell" owner:self options:nil];
        for(id currentObject in topLevelObjects) {
            if([currentObject isKindOfClass:[UITableViewCell class]])
            {
                cell = (AboutCell *)currentObject;
                cell.backgroundColor = [UIColor clearColor];
                break;
            }
        }
    }
    
    MenuObj *obj = [appDelegate.arrAboutList objectAtIndex:indexPath.row];
    if(obj){
        cell.lblCaption.text = obj.mTitle;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_tableView deselectRowAtIndexPath:indexPath animated:NO];
    int row = (int)indexPath.row;
    MenuObj *obj = [appDelegate.arrAboutList objectAtIndex:row];
    [_delegate clickItemOnMenu:obj.mTitle andRow:obj.mID];
    [self.revealSideViewController popViewControllerAnimated:YES completion:^{}];
}

#pragma TableDatasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return appDelegate.arrAboutList.count;
}

//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////// Connect with Server /////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
- (void) connectToServer:(int)typeReg
{
    if(![CheckConnection hasConnectivity])
        return;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    id successBlock = ^(AFHTTPRequestOperation *operation, id jsonResult)
    {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        switch (typeReg) {
            case 0:{//get program
                int type = [[jsonResult objectForKey:@"type"] intValue];
                if(type == 1){
                    NSArray *arrDict = (NSArray *)[jsonResult objectForKey:@"list"];
                    MenuObj *obj = nil;
                    for(NSDictionary *d in arrDict){
                        obj = [[MenuObj alloc] initWithDictForAbout:d];
                        [appDelegate.arrAboutList addObject:obj];
                    }
                    [_tableView reloadData];
                }
                break;
            }
                
            default:
                break;
        }
    };
    
    id failureBlock = ^(AFHTTPRequestOperation *operation, NSError *error) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [UIAlertView showWithTitle:MYLocalizedString(@"error.fail.title", nil) message:MYLocalizedString(@"error.fail.content", nil) handler:nil];
    };
    
    switch (typeReg) {
        case 0:{//get list menu
            [[MyApiClient sharedClient] getAboutList:successBlock andFailBlock:failureBlock];
            break;
        }
        default:
            break;
    }
}
@end
