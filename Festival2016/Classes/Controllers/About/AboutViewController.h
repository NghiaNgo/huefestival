//
//  AboutViewController.h
//  Festival2016
//
//  Created by Phan Phuoc Luong on 2/5/16.
//  Copyright © 2016 Phan Phuoc Luong. All rights reserved.
//

#import "AboutCell.h"
@protocol SlideMenuDelegate <NSObject>
- (void) clickItemOnMenu:(NSString *)title andRow:(int)row;

@end

@interface AboutViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>
{
    AppDelegate *appDelegate;
}
@property (nonatomic, weak) IBOutlet UIView *viewMain;
//
@property (nonatomic, strong) id<SlideMenuDelegate> delegate;
@property (nonatomic, weak) IBOutlet UILabel *lblHoTro;
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UIView *viewHoTro;


@end
