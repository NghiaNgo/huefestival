//
//  PlaceDetailViewController.m
//  Festival2016
//
//  Created by Phan Phuoc Luong on 2/17/16.
//  Copyright © 2016 Phan Phuoc Luong. All rights reserved.
//

#import "PlaceDetailViewController.h"
#import "ProgramCell.h"
#import "MapViewController.h"
#import "LocationObj.h"
#import "PhotoDetailViewController.h"
#import "CouchbaseEvents.h"

@interface PlaceDetailViewController ()
{
    NSString *phoneNumber;
}
@end

@implementation PlaceDetailViewController

- (id)initWithObject:(NSString *)nibName andObj:(TicketLocationObj *)obj andArr:(NSMutableArray *)data
{
    if(self = [super initWithNibName:nibName bundle:nil])
    {
        objSelect = obj;
        appDelegate = MyAppDelegate();
        arrDatas = [[NSMutableArray alloc] initWithArray:data];
    }
    
    return self;
}

- (void)initSafeArea {
    _viewMain.translatesAutoresizingMaskIntoConstraints = NO;
    
    if (@available(iOS 11, *)) {
        UILayoutGuide *guide = self.view.safeAreaLayoutGuide;
        [_viewMain.leadingAnchor constraintEqualToAnchor:guide.leadingAnchor].active = YES;
        [_viewMain.trailingAnchor constraintEqualToAnchor:guide.trailingAnchor].active = YES;
        [_viewMain.topAnchor constraintEqualToAnchor:guide.topAnchor].active = YES;
        [_viewMain.bottomAnchor constraintEqualToAnchor:guide.bottomAnchor].active = YES;
    } else {
        UILayoutGuide *margins = self.view.layoutMarginsGuide;
        [_viewMain.leadingAnchor constraintEqualToAnchor:margins.leadingAnchor].active = YES;
        [_viewMain.trailingAnchor constraintEqualToAnchor:margins.trailingAnchor].active = YES;
        [_viewMain.topAnchor constraintEqualToAnchor:self.topLayoutGuide.bottomAnchor].active = YES;
        [_viewMain.bottomAnchor constraintEqualToAnchor:self.bottomLayoutGuide.topAnchor].active = YES;
    }
    // Refresh myView and/or main view
    [self.view layoutIfNeeded];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.frame = [UIScreen mainScreen].bounds;
    //
    [self initSafeArea];
    //
    _pageControl.currentPage = 0;
    _pageControl.indicatorMargin = 3.0f;
    _pageControl.pageIndicatorTintColor = [[UIColor whiteColor] colorWithAlphaComponent:0.9f];
    _pageControl.currentPageIndicatorTintColor = [UIColorFromRGB(COLOR_RED) colorWithAlphaComponent:1.f];
    _pageControl.currentPageIndicatorImage = [UIImage imageNamed:@"ic_slide_focus.png"];
    _pageControl.pageIndicatorMaskImage = [UIImage imageNamed:@"ic_slide.png"];
    //init langague
    [self refreshLanguage];
    //touch on view lien quan
    _viewRelate.hidden = YES;
    UITapGestureRecognizer *tapShowRelate = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                    action:@selector(showRelateView)];
    [_viewBottom addGestureRecognizer:tapShowRelate];
    UITapGestureRecognizer *tabHideRelate = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                    action:@selector(hideRelateView)];
    [_viewTopRelate addGestureRecognizer:tabHideRelate];
    
    UITapGestureRecognizer *tabMobile = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                    action:@selector(callMobile)];
    [_viewPhone addGestureRecognizer:tabMobile];
    UITapGestureRecognizer *tabWebsite = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                action:@selector(callWebsite)];
    [_viewWebsite addGestureRecognizer:tabWebsite];
    
    UITapGestureRecognizer *tabScrollImage = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                     action:@selector(showPhotoDetail)];
    [_scrollImages addGestureRecognizer:tabScrollImage];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.view.frame = [UIScreen mainScreen].bounds;
    if(IS_IPAD){
        CGRect frame = _viewImages.frame;
        frame.size.height = 320;
        _viewImages.frame= frame;
        
        frame = _viewButton.frame;
        frame.origin.y = _viewImages.frame.origin.y + _viewImages.frame.size.height;
        _viewButton.frame = frame;
        
        frame = _scrollContent.frame;
        frame.origin.y = _viewButton.frame.origin.y + _viewButton.frame.size.height;
        frame.size.height = _scrollMain.frame.size.height - frame.origin.y;
        _scrollContent.frame = frame;
    }
    [self loadScrollImages];
    [self loadContent];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return YES;
}

//refresh language
- (void)refreshLanguage
{
    _lblMap.text = MYLocalizedString(@"location.navigation", nil);
    _lblLike.text = MYLocalizedString(@"detail.like", nil);
    _lblSave.text = MYLocalizedString(@"detail.save", nil);
    _lblMore.text = MYLocalizedString(@"detail.more", nil);
    _lblMore2.text = MYLocalizedString(@"detail.more", nil);
    //for content
    _lblAddressTitle.text = MYLocalizedString(@"location.address", nil);
    _lblPhoneTitle.text = MYLocalizedString(@"location.phone", nil);
}

- (void) loadScrollImages
{
    CGRect r;
    float h = _scrollImages.frame.size.height;
    r = CGRectMake(0, 0, screenViewWidth, h);
    UIImageView *imageview = [[UIImageView alloc] initWithFrame:r];
    imageview.contentMode = UIViewContentModeScaleAspectFill;
    imageview.image = objSelect.pThumb;
    [_scrollImages addSubview:imageview];
    
    [_pageControl setNumberOfPages:1];
    _pageControl.currentPage = 0;
    _scrollImages.contentSize = CGSizeMake(screenViewWidth, _scrollImages.frame.size.height);
}

- (void) loadContent
{
    [_btnLike setSelected:[self detechDiaDiemLiked:objSelect.id_ticketlocation]];
    _lblLike.textColor = _btnLike.isSelected?UIColorFromRGB(COLOR_YELLOW):UIColorFromRGB(COLOR_WHITE);
    
    NSString *_title = objSelect.title;
    NSString *_address = objSelect.address;
    NSString *_phone = objSelect.phone;
    NSString *_website = objSelect.website;
    NSString *_sumary = @"";
    NSString *_content = @"";
    if(objSelect.content.length == 0){
        _sumary = [objSelect.sumary stringByDecodingHTMLEntities];
        //call load detail
        [self connectToServer:0];
    } else {
        _sumary = [objSelect.sumary stringByDecodingHTMLEntities];
        _content = [objSelect.content stringByDecodingHTMLEntities];
    }
    
    float y = 5;
    CGRect r;
    r = [_title boundingRectWithSize:CGSizeMake(screenViewWidth - 20, 9999)
                                options:NSStringDrawingUsesLineFragmentOrigin
                             attributes:@{NSFontAttributeName:_lblTitle.font}
                                context:nil];
    CGRect frame = _lblTitle.frame;
    frame.origin.y = y;
    frame.size.height = r.size.height;
    _lblTitle.frame = frame;
    _lblTitle.text = _title;
    y += _lblTitle.frame.size.height;
    
    if(_address.length > 0){
        frame = _viewAddress.frame;
        y += 5;
        frame.origin.y = y;
        _viewAddress.frame = frame;
        _lblAddressValue.text = _address;
        y += _viewAddress.frame.size.height;
    } else {
        _viewAddress.hidden = YES;
    }
    
    if(_phone.length > 0){
        frame = _viewPhone.frame;
        y += 5;
        frame.origin.y = y;
        _viewPhone.frame = frame;
        _lblPhoneValue.text = _phone;
        y += _viewPhone.frame.size.height;
    } else {
        _viewPhone.hidden = YES;
    }
    
    if(_website.length > 0){
        _viewWebsite.hidden = NO;
        frame = _viewWebsite.frame;
        y += 5;
        frame.origin.y = y;
        _viewWebsite.frame = frame;
        _lblWebsiteValue.text = _website;
        y += _viewWebsite.frame.size.height;
    } else {
        _viewWebsite.hidden = YES;
    }
    
    NSString *content = [NSString stringWithFormat:@"<html>""<style type=\"text/css\">"
               "a:link{color:#000; text-decoration: none}"
               "body{background-color:transparent; font-size:15; font-family: Helvetica Neue; color: #000; text-align:justify; margin:0; padding:0}""</style>"
               "<body>%@<br/><br/>%@</body></html>", _sumary, _content];
    [_webContent loadHTMLString:content baseURL: nil];
    _webContent.scrollView.scrollEnabled = NO;
    _webContent.scrollView.bounces = NO;
    _webContent.delegate = self;
    
    frame = _webContent.frame;
    y += 5;
    frame.origin.y = y;
    _webContent.frame = frame;
    
    [self updateDownOfContent];
}

- (void) updateDownOfContent
{
    CGRect frame = _scrollContent.frame;
    frame.size.height = _webContent.frame.origin.y + _webContent.frame.size.height + 10;
    _scrollContent.frame = frame;
    
    frame = _scrollMain.frame;
    frame.size.height = self.viewMain.frame.size.height - (_viewHeader.frame.size.height + _viewBottom.frame.size.height);
    _scrollMain.frame = frame;
    //update content size to scroll
    _scrollMain.contentSize = CGSizeMake(screenViewWidth, _scrollImages.frame.size.height + _viewButton.frame.size.height + _scrollContent.frame.size.height + 10);
}

- (void) callMobile
{
    phoneNumber = [_lblPhoneValue.text stringByTrimmingCharactersInSet:
                         [NSCharacterSet whitespaceCharacterSet]];
    if(phoneNumber.length > 0){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:MYLocalizedString(@"art.alert.call.title", nil), phoneNumber] message:MYLocalizedString(@"art.alert.call.content", nil) delegate:self cancelButtonTitle:MYLocalizedString(@"nothanks", nil) otherButtonTitles:MYLocalizedString(@"call", nil), nil];
        alert.tag = 100;
        [alert show];
    }
}

- (void) callWebsite
{
    NSString *urlSite = [_lblWebsiteValue.text stringByTrimmingCharactersInSet:
                         [NSCharacterSet whitespaceCharacterSet]];
    if(urlSite.length > 0){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlSite]];
    }
}

#pragma mark - Webview Delegate
-(void)webViewDidFinishLoad:(UIWebView *)webView {
    CGRect webViewFrame = webView.frame;
    webViewFrame.size.height = 1;
    webView.frame = webViewFrame;
    CGSize fittingSize = [webView sizeThatFits:CGSizeZero];
    webViewFrame.size = fittingSize;
    webView.frame = webViewFrame;
    
    [self updateDownOfContent];
}

//============== Click Button ==============//
- (IBAction) clickBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction) clickShare:(id)sender
{
    NSString *message = objSelect.title;
    NSArray *activityProviders = @[message, [NSURL URLWithString:BASE_URL]];
    UIActivityViewController *activityViewController =
    [[UIActivityViewController alloc] initWithActivityItems:activityProviders  applicationActivities:nil];
    if (SYSTEM_VERSION_GREATER_THAN(@"7.0")) {
        activityViewController.excludedActivityTypes = @[UIActivityTypeAddToReadingList,UIActivityTypePostToTencentWeibo, UIActivityTypeAirDrop,UIActivityTypePrint,  UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact];
    } else {
        activityViewController.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact];
    }
    activityViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    if (!IS_IPAD) {
        [self presentViewController:activityViewController animated:YES completion:nil];
    } else {
        UIButton *btn = (UIButton *)sender;
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:activityViewController];
        [popup presentPopoverFromRect:CGRectMake(btn.frame.origin.x + btn.frame.size.width/2, _viewHeader.frame.size.height, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
}

- (IBAction) clickSave:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:MYLocalizedString(@"program.save.alert.title", nil) message:MYLocalizedString(@"program.save.alert.content", nil) delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1){
        if(alertView.tag == 100){
            phoneNumber = [phoneNumber stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", phoneNumber]]];
        }
        else
            UIImageWriteToSavedPhotosAlbum(objSelect.pThumb,nil,NULL,NULL);
    }
}

- (IBAction) clickLike:(id)sender
{
    [_btnLike setSelected:!_btnLike.isSelected];
    _lblLike.textColor = _btnLike.isSelected?UIColorFromRGB(COLOR_YELLOW):UIColorFromRGB(COLOR_WHITE);
    [self addLikeOrDisLike:_btnLike.isSelected andID:objSelect.id_ticketlocation];
}

- (IBAction)showMapView:(id)sender
{
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    LocationObj *lObj = [[LocationObj alloc] init];
    lObj.id_diadiem = objSelect.id_ticketlocation;
    lObj.diadiem_name = objSelect.title;
    lObj.diadiem_content = [NSString stringWithFormat:@"%@ (%@)", objSelect.address, objSelect.phone];
    lObj.pThumb = objSelect.pThumb;
    lObj.lat = objSelect.lat;
    lObj.lng = objSelect.lng;
    [arr addObject:lObj];
    
    [appDelegate.mapViewController setDatas:1 andDatas:arr];
    [self.navigationController pushViewController:appDelegate.mapViewController animated:YES];
}

- (void) showRelateView
{
    if(!_viewRelate.isHidden)
        return;
    _viewRelate.hidden = NO;
    [_tableView reloadData];
    
    [UIView animateWithDuration:0.25
                          delay:0.0
                        options: (int) UIViewAnimationCurveEaseOut
                     animations:^{
                         CGRect frame = _viewRelate.frame;
                         frame.origin.y = 0;
                         _viewRelate.frame = frame;
                     }
                     completion:^(BOOL finished){}];
}

- (void) hideRelateView
{
    if(_viewRelate.isHidden)
        return;
    
    [UIView animateWithDuration:0.25
                          delay:0.0
                        options: (int) UIViewAnimationCurveEaseOut
                     animations:^{
                         CGRect frame = _viewRelate.frame;
                         frame.origin.y = self.viewMain.frame.size.height;
                         _viewRelate.frame = frame;
                     }
                     completion:^(BOOL finished){
                         _viewRelate.hidden = YES;
                     }];
}

- (void) showPhotoDetail
{
    PhotoDetailViewController *photo = [[PhotoDetailViewController alloc] initWithData:[[NSMutableArray alloc] initWithObjects:objSelect.pThumb, nil] andIndex:0 andNibName:@"PhotoDetailViewController"];
    [self.navigationController presentViewController:photo animated:NO completion:nil];
}

#pragma mark -
#pragma mark UIScrollViewDelegate methods
- (void)scrollViewDidScroll:(UIScrollView *)sender {
    // Switch the indicator when more than 50% of the previous/next page is visible
    CGFloat pageWidth = _scrollImages.frame.size.width;
    int page = floor((_scrollImages.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    _pageControl.currentPage = page;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Indentifier";
    
    //chuong trinh
    ProgramCell *cell = (ProgramCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ProgramCell1" owner:self options:nil];
        for(id currentObject in topLevelObjects) {
            if([currentObject isKindOfClass:[UITableViewCell class]])
            {
                cell = (ProgramCell *)currentObject;
                cell.backgroundColor = [UIColor clearColor];
                break;
            }
        }
    }
    
    TicketLocationObj *obj = [arrDatas objectAtIndex:indexPath.row];
    cell.lblHueFes.text = [NSString stringWithFormat:@"%@, %@", MYLocalizedString(@"fes.title", nil), MYLocalizedString(@"location.title", nil)];
    if(obj){
        NSString *name = [obj.title stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        CGRect r = cell.lblTitle.frame;
        //update vi tri title
        CGRect textRect = [name boundingRectWithSize:CGSizeMake(r.size.width - 20, 9999) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica Neue" size:16]} context:nil];
        int h = (int)textRect.size.height;
        
        if(h > 21){
            h = h + 2;
            if(h > 80)
                h = 80;
            r.size.height = h;
            cell.lblTitle.frame = r;
        }
        
        cell.lblTitle.text = name;
        cell.imgThumb.image = obj.pThumb;
        cell.imgThumb.contentMode = UIViewContentModeScaleToFill;
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_tableView deselectRowAtIndexPath:indexPath animated:NO];
    [self hideRelateView];
    objSelect = [arrDatas objectAtIndex:indexPath.row];
    [self loadScrollImages];
    [self loadContent];
    [_scrollMain setContentOffset:CGPointMake(0, 0) animated:YES];
}

#pragma TableDatasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrDatas.count;
}

//////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////// Connect with Server ////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
- (void) connectToServer:(int)typeReg
{
    if(![CheckConnection hasConnectivity]){
        [UIAlertView showWithTitle:MYLocalizedString(@"error.internet.title", nil) message:MYLocalizedString(@"error.internet.content", nil) handler:nil];
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    id successBlock = ^(AFHTTPRequestOperation *operation, id jsonResult)
    {
        //NSLog(@"%@", jsonResult);
        switch (typeReg) {
            case 0:{//get news detail
                int type = [[jsonResult objectForKey:@"type"] intValue];
                if(type == 1){
                    NSDictionary *d = [jsonResult objectForKey:@"detail"];
                    NSString *website = [d objectForKey:@"website"];
                    NSString *content = [d objectForKey:@"content"];
                    if(!website || [website isKindOfClass:[NSNull class]])
                        website = @"";
                    if(!content || [content isKindOfClass:[NSNull class]] || content.length == 0)
                        content = @" ";
                    objSelect.website = website;
                    objSelect.content = content;
                    [self loadContent];
                }
                
                break;
            }
            default:
                break;
        }
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
    };
    
    id failureBlock = ^(AFHTTPRequestOperation *operation, NSError *error) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        [UIAlertView showWithTitle:MYLocalizedString(@"error.fail.title", nil) message:MYLocalizedString(@"error.fail.content", nil) handler:nil];
    };
    
    switch (typeReg) {
        case 0:{//
            [[MyApiClient sharedClient] getServiceDetail:[objSelect.id_ticketlocation intValue] andTypeData:objSelect.typedata andSuccesBlock:successBlock andFailBlock:failureBlock];
            break;
        }
            
        default:
            break;
    }
}

#pragma common
- (void) showOLGhostAlertView: (NSString *) oltitle content:(NSString *) strContent time: (int) timeout
{
    OLGhostAlertView *olAlert = [[OLGhostAlertView alloc] initWithTitle:oltitle message:strContent timeout:timeout dismissible:YES];
    [olAlert show];
}

- (BOOL) detechDiaDiemLiked:(NSString *)_ID
{
    for(TicketLocationObj *obj in appDelegate.arrDiaDiemLikeList){
        if([obj.id_ticketlocation isEqualToString:_ID])
            return YES;
    }
    return NO;
}

- (void) addLikeOrDisLike:(BOOL) isLike andID:(NSString *)ID
{
    CouchbaseEvents *manager = [CouchbaseEvents sharedInstance];
    if(isLike){//tick like
        NSDictionary *d = objSelect.dict;
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithDictionary:d];
        [dict setObject:[NSString stringWithFormat:@"%i", objSelect.typedata] forKey:@"stypedata"];
        [manager addDiaDiemLike:dict];
        [manager saveDiaDiemPhoto:objSelect.pLinkImage andImage:objSelect.pThumb andID:ID];
        [appDelegate.arrDiaDiemLikeList addObject:objSelect];
    } else {//dislike xoá dữ liệu
        [manager deleteDiaDiemLike:ID];
        [manager deleteDiaDiemPhoto:objSelect.pLinkImage andID:ID];
        [appDelegate.arrDiaDiemLikeList removeObject:objSelect];
    }
}

@end
