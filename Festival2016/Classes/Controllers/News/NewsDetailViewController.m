//
//  NewsDetailViewController.m
//  Festival2016
//
//  Created by Phan Phuoc Luong on 11/24/15.
//  Copyright © 2015 Phan Phuoc Luong. All rights reserved.
//

#import "NewsDetailViewController.h"
#import "ProgramCell.h"
#import "PhotoDetailViewController.h"
#import "CouchbaseEvents.h"

@interface NewsDetailViewController ()

@end

@implementation NewsDetailViewController
- (id)initWithObject:(NSString *)nibName andObj:(NewsObj *)obj andArr:(NSMutableArray *)data
{
    if(self = [super initWithNibName:nibName bundle:nil]){
        objSelect = obj;
        arrNews = [[NSMutableArray alloc] initWithArray:data];
        appDelegate = MyAppDelegate();
    }
    
    return self;
}

- (void)initSafeArea {
    _viewMain.translatesAutoresizingMaskIntoConstraints = NO;
    
    if (@available(iOS 11, *)) {
        UILayoutGuide *guide = self.view.safeAreaLayoutGuide;
        [_viewMain.leadingAnchor constraintEqualToAnchor:guide.leadingAnchor].active = YES;
        [_viewMain.trailingAnchor constraintEqualToAnchor:guide.trailingAnchor].active = YES;
        [_viewMain.topAnchor constraintEqualToAnchor:guide.topAnchor].active = YES;
        [_viewMain.bottomAnchor constraintEqualToAnchor:guide.bottomAnchor].active = YES;
    } else {
        UILayoutGuide *margins = self.view.layoutMarginsGuide;
        [_viewMain.leadingAnchor constraintEqualToAnchor:margins.leadingAnchor].active = YES;
        [_viewMain.trailingAnchor constraintEqualToAnchor:margins.trailingAnchor].active = YES;
        [_viewMain.topAnchor constraintEqualToAnchor:self.topLayoutGuide.bottomAnchor].active = YES;
        [_viewMain.bottomAnchor constraintEqualToAnchor:self.bottomLayoutGuide.topAnchor].active = YES;
    }
    // Refresh myView and/or main view
    [self.view layoutIfNeeded];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //
    [self initSafeArea];
    //
    //init langague
    [self refreshLanguage];
    //
    _pageControl.currentPage = 0;
    _pageControl.indicatorMargin = 3.0f;
    _pageControl.pageIndicatorTintColor = [[UIColor whiteColor] colorWithAlphaComponent:0.9f];
    _pageControl.currentPageIndicatorTintColor = [UIColorFromRGB(COLOR_RED) colorWithAlphaComponent:1.f];
    _pageControl.currentPageIndicatorImage = [UIImage imageNamed:@"ic_slide_focus.png"];
    _pageControl.pageIndicatorMaskImage = [UIImage imageNamed:@"ic_slide.png"];
    //touch on view lien quan
    _viewRelate.hidden = YES;
    UITapGestureRecognizer *tapShowRelate = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                    action:@selector(showRelateView)];
    [_viewBottom addGestureRecognizer:tapShowRelate];
    UITapGestureRecognizer *tabHideRelate = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                    action:@selector(hideRelateView)];
    [_viewTopRelate addGestureRecognizer:tabHideRelate];
    
    UITapGestureRecognizer *tabScrollImage = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                     action:@selector(showPhotoDetail)];
    [_scrollImages addGestureRecognizer:tabScrollImage];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if(IS_IPAD){
        CGRect frame = _viewImages.frame;
        frame.size.height = 320;
        _viewImages.frame= frame;
        
        frame = _viewButton.frame;
        frame.origin.y = _viewImages.frame.origin.y + _viewImages.frame.size.height;
        _viewButton.frame = frame;
        
        frame = _scrollContent.frame;
        frame.origin.y = _viewButton.frame.origin.y + _viewButton.frame.size.height;
        frame.size.height = _scrollMain.frame.size.height - frame.origin.y;
        _scrollContent.frame = frame;
    }
    
    [self loadScrollImages];
    [self loadContent];
}

//refresh language
- (void)refreshLanguage
{
    _lblLike.text = MYLocalizedString(@"detail.like", nil);
    _lblSave.text = MYLocalizedString(@"detail.save", nil);
    _lblMorenews.text = MYLocalizedString(@"detail.more", nil);
    _lblMorenews2.text = MYLocalizedString(@"detail.more", nil);
}

- (void) loadScrollImages
{
    float h = _scrollImages.frame.size.height;
    CGRect r = CGRectMake(0, 0, screenViewWidth, h);
    UIImageView *imageview = [[UIImageView alloc] initWithFrame:r];
    [imageview setContentMode:UIViewContentModeScaleAspectFill];
    imageview.image = objSelect.newsThumb;
    [_scrollImages addSubview:imageview];
    
    [_pageControl setNumberOfPages:1];
    _pageControl.currentPage = 0;
    _scrollImages.contentSize = CGSizeMake(screenViewWidth, _scrollImages.frame.size.height);
}

- (void) loadContent
{
    [_btnLike setSelected:[self detechNewsLiked:objSelect.nID]];
    _lblLike.textColor = _btnLike.isSelected?UIColorFromRGB(COLOR_YELLOW):UIColorFromRGB(COLOR_WHITE);
    
    NSString *new_title = objSelect.nTitle;
    NSString *new_date = objSelect.nPostDay;
    NSString *new_sumary = @"";
    NSString *new_content = @"";
    if(objSelect.content.length == 0){
        new_sumary = [objSelect.nSummary stringByDecodingHTMLEntities];
        //call load detail
        [self connectToServer:0];
    } else {
        new_sumary = [objSelect.nSummary stringByDecodingHTMLEntities];
        new_content = [objSelect.content stringByDecodingHTMLEntities];
    }
    
    CGRect r;
    r = [new_title boundingRectWithSize:CGSizeMake(screenViewWidth - 20, 9999)
                                    options:NSStringDrawingUsesLineFragmentOrigin
                                 attributes:@{NSFontAttributeName:_lblTitle.font}
                                    context:nil];
    CGRect frame = _lblTitle.frame;
    frame.origin.y = 5;
    frame.size.height = r.size.height;
    _lblTitle.frame = frame;
    _lblTitle.text = new_title;
    
    frame = _lblDate.frame;
    frame.origin.y = _lblTitle.frame.origin.y + _lblTitle.frame.size.height + 5;
    _lblDate.frame = frame;
    _lblDate.text = new_date;
    
    NSString *content = [NSString stringWithFormat:@"<html>""<style type=\"text/css\">"
               "a:link{color:#000; text-decoration: none}"
               "body{background-color:transparent; font-size:15; font-family: Helvetica Neue; color: #000; text-align:justify; margin:0; padding:0}""</style>"
               "<body>%@<br/><br/>%@</body></html>", new_sumary, new_content];
    [_webContent loadHTMLString:content baseURL: nil];
    _webContent.scrollView.scrollEnabled = NO;
    _webContent.scrollView.bounces = NO;
    _webContent.delegate = self;
    
    frame = _webContent.frame;
    frame.origin.y = _lblDate.frame.origin.y + _lblDate.frame.size.height + 5;
    _webContent.frame = frame;
    
    [self updateDownOfContent];
}

- (void) updateDownOfContent
{
    CGRect frame = _scrollContent.frame;
    frame.size.height = _webContent.frame.origin.y + _webContent.frame.size.height + 10;
    _scrollContent.frame = frame;
    
    frame = _scrollMain.frame;
    frame.size.height = self.viewMain.frame.size.height - (_viewHeader.frame.size.height + _viewBottom.frame.size.height);
    _scrollMain.frame = frame;
    //update content size to scroll
    _scrollMain.contentSize = CGSizeMake(screenViewWidth, _scrollImages.frame.size.height + _viewButton.frame.size.height + _scrollContent.frame.size.height + 10);
}

#pragma mark - Webview Delegate
-(void)webViewDidFinishLoad:(UIWebView *)webView {
    CGRect webViewFrame = webView.frame;
    webViewFrame.size.height = 1;
    webView.frame = webViewFrame;
    CGSize fittingSize = [webView sizeThatFits:CGSizeZero];
    webViewFrame.size = fittingSize;
    webView.frame = webViewFrame;
    
    [self updateDownOfContent];
}

//============== Click Button ==============//
- (IBAction) clickBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction) clickShare:(id)sender
{
    NSString *message = objSelect.nTitle;
    NSArray *activityProviders = @[message, [NSURL URLWithString:BASE_URL]];
    UIActivityViewController *activityViewController =
    [[UIActivityViewController alloc] initWithActivityItems:activityProviders  applicationActivities:nil];
    if (SYSTEM_VERSION_GREATER_THAN(@"7.0")) {
        activityViewController.excludedActivityTypes = @[UIActivityTypeAddToReadingList,UIActivityTypePostToTencentWeibo, UIActivityTypeAirDrop,UIActivityTypePrint,  UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact];
    } else {
        activityViewController.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact];
    }
    activityViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    if (!IS_IPAD) {
        [self presentViewController:activityViewController animated:YES completion:nil];
    } else {
        UIButton *btn = (UIButton *)sender;
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:activityViewController];
        [popup presentPopoverFromRect:CGRectMake(btn.frame.origin.x + btn.frame.size.width/2, _viewHeader.frame.size.height, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
}

- (IBAction) clickSave:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:MYLocalizedString(@"program.save.alert.title", nil) message:MYLocalizedString(@"program.save.alert.content", nil) delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1){
        UIImageWriteToSavedPhotosAlbum(objSelect.newsThumb,nil,NULL,NULL);
    }
}

- (IBAction) clickLike:(id)sender
{
    [_btnLike setSelected:!_btnLike.isSelected];
    _lblLike.textColor = _btnLike.isSelected?UIColorFromRGB(COLOR_YELLOW):UIColorFromRGB(COLOR_WHITE);
    [self addLikeOrDisLike:_btnLike.isSelected andNewsID:objSelect.nID andName:objSelect.nTitle];
}

- (void) showRelateView
{
    if(!_viewRelate.isHidden)
        return;
    _viewRelate.hidden = NO;
    [_tableView reloadData];
    
    [UIView animateWithDuration:0.25
                          delay:0.0
                        options: (int) UIViewAnimationCurveEaseOut
                     animations:^{
                         CGRect frame = _viewRelate.frame;
                         frame.origin.y = 0;
                         _viewRelate.frame = frame;
                     }
                     completion:^(BOOL finished){}];
}

- (void) hideRelateView
{
    if(_viewRelate.isHidden)
        return;
    
    [UIView animateWithDuration:0.25
                          delay:0.0
                        options: (int) UIViewAnimationCurveEaseOut
                     animations:^{
                         CGRect frame = _viewRelate.frame;
                         frame.origin.y = self.viewMain.frame.size.height;
                         _viewRelate.frame = frame;
                     }
                     completion:^(BOOL finished){
                         _viewRelate.hidden = YES;
                     }];
}

- (void) showPhotoDetail
{
    PhotoDetailViewController *photo = [[PhotoDetailViewController alloc] initWithData:[[NSMutableArray alloc] initWithObjects:objSelect.newsThumb, nil] andIndex:0 andNibName:@"PhotoDetailViewController"];
    [self.navigationController presentViewController:photo animated:NO completion:nil];
}

#pragma mark -
#pragma mark UIScrollViewDelegate methods
- (void)scrollViewDidScroll:(UIScrollView *)sender {
    // Switch the indicator when more than 50% of the previous/next page is visible
    CGFloat pageWidth = _scrollImages.frame.size.width;
    int page = floor((_scrollImages.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    _pageControl.currentPage = page;
}

#pragma mark - UItableview
#pragma mark tableview Delegate method
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Indentifier";
    
    //chuong trinh
    ProgramCell *cell = (ProgramCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ProgramCell1" owner:self options:nil];
        for(id currentObject in topLevelObjects) {
            if([currentObject isKindOfClass:[UITableViewCell class]])
            {
                cell = (ProgramCell *)currentObject;
                cell.backgroundColor = [UIColor clearColor];
                break;
            }
        }
    }
    
    NewsObj *obj = [arrNews objectAtIndex:indexPath.row];
    cell.lblHueFes.text = [NSString stringWithFormat:@"%@, %@", MYLocalizedString(@"fes.title", nil), MYLocalizedString(@"news.title", nil)];
    if(obj){
        NSString *name = [obj.nTitle stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        CGRect r = cell.lblTitle.frame;
        //update vi tri title
        CGRect textRect = [name boundingRectWithSize:CGSizeMake(r.size.width - 20, 9999) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica Neue" size:16]} context:nil];
        int h = (int)textRect.size.height;
        
        if(h > 21){
            h = h + 2;
            if(h > 80)
                h = 80;
            r.size.height = h;
            cell.lblTitle.frame = r;
        }
        
        cell.lblTitle.text = name;
        cell.imgThumb.image = obj.newsThumb;
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_tableView deselectRowAtIndexPath:indexPath animated:NO];
    [self hideRelateView];
    objSelect = [arrNews objectAtIndex:indexPath.row];
    [self loadScrollImages];
    [self loadContent];
    [_scrollMain setContentOffset:CGPointMake(0, 0) animated:YES];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return nil;
}

#pragma TableDatasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrNews.count;
}

//////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////// Connect with Server ////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
- (void) connectToServer:(int)typeReg
{
    if(![CheckConnection hasConnectivity]){
        [UIAlertView showWithTitle:MYLocalizedString(@"error.internet.title", nil) message:MYLocalizedString(@"error.internet.content", nil) handler:nil];
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    id successBlock = ^(AFHTTPRequestOperation *operation, id jsonResult)
    {
        //NSLog(@"%@", jsonResult);
        switch (typeReg) {
            case 0:{//get news detail
                int type = [[jsonResult objectForKey:@"type"] intValue];
                if(type == 1){
                    NSDictionary *d = [jsonResult objectForKey:@"detail"];
                    [objSelect setDataForDetail:d];
                    [self loadContent];
                }
                
                break;
            }
            default:
                break;
        }
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
    };
    
    id failureBlock = ^(AFHTTPRequestOperation *operation, NSError *error) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        [UIAlertView showWithTitle:MYLocalizedString(@"error.fail.title", nil) message:MYLocalizedString(@"error.fail.content", nil) handler:nil];
    };
    
    switch (typeReg) {
        case 0:{//lay danh sach cac diem bieu dien
            [[MyApiClient sharedClient] getNewsDetail:objSelect.nID andSuccesBlock:successBlock andFailBlock:failureBlock];
            break;
        }
            
        default:
            break;
    }
}

#pragma common
- (void) showOLGhostAlertView: (NSString *) oltitle content:(NSString *) strContent time: (int) timeout
{
    OLGhostAlertView *olAlert = [[OLGhostAlertView alloc] initWithTitle:oltitle message:strContent timeout:timeout dismissible:YES];
    [olAlert show];
}

- (BOOL) detechNewsLiked:(NSString *)newsID
{
    for(NewsObj *obj in appDelegate.arrNewsLikeList){
        if([obj.nID isEqualToString:newsID])
            return YES;
    }
    return NO;
}

- (void) addLikeOrDisLike:(BOOL) isLike andNewsID:(NSString *)newsID andName:(NSString *)name
{
    CouchbaseEvents *manager = [CouchbaseEvents sharedInstance];
    if(isLike){//tick like
        [manager addNewsLike:objSelect.dict];
        [manager saveNewsPhoto:[NSString stringWithFormat:@"%@.jpg", name] andImage:objSelect.newsThumb andID:newsID];
        [appDelegate.arrNewsLikeList addObject:objSelect];
    } else {//dislike xoá dữ liệu
        [manager deleteNewLike:newsID];
        [manager deleteNewsPhoto:[NSString stringWithFormat:@"%@.jpg", name] andID:newsID];
        [appDelegate.arrNewsLikeList removeObject:objSelect];
    }
}

@end
