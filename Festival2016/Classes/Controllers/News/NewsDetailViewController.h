//
//  NewsDetailViewController.h
//  Festival2016
//
//  Created by Phan Phuoc Luong on 11/24/15.
//  Copyright © 2015 Phan Phuoc Luong. All rights reserved.
//

#import "SMPageControl.h"
#import "NSString+HTML.h"
#import "NewsObj.h"

@interface NewsDetailViewController : UIViewController<UIScrollViewDelegate, UIWebViewDelegate, UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate>
{
    AppDelegate *appDelegate;
    NewsObj *objSelect;
    NSMutableArray *arrNews;
}

- (id) initWithObject:(NSString *)nibName andObj:(NewsObj *)obj andArr:(NSMutableArray *)data;
@property (nonatomic, weak) IBOutlet UIView *viewMain;
//
@property (nonatomic, weak) IBOutlet UIScrollView *scrollMain;
@property (nonatomic, weak) IBOutlet UIScrollView *scrollImages;
@property (nonatomic, weak) IBOutlet SMPageControl *pageControl;
@property (nonatomic, weak) IBOutlet UIScrollView *scrollContent;
@property (nonatomic, weak) IBOutlet UIView *viewButton;
@property (nonatomic, weak) IBOutlet UIView *viewHeader;
@property (nonatomic, weak) IBOutlet UIView *viewBottom;
@property (nonatomic, weak) IBOutlet UIView *viewImages;

@property (nonatomic, weak) IBOutlet UIButton *btnLike;
@property (nonatomic, weak) IBOutlet UILabel *lblLike;
@property (nonatomic, weak) IBOutlet UILabel *lblSave;
@property (nonatomic, weak) IBOutlet UILabel *lblMorenews;
//content
@property (nonatomic, weak) IBOutlet UILabel *lblTitle;
@property (nonatomic, weak) IBOutlet UILabel *lblDate;
@property (nonatomic, weak) IBOutlet UIWebView *webContent;

//view các tin liên quan
@property (nonatomic, weak) IBOutlet UIView *viewRelate;
@property (nonatomic, weak) IBOutlet UIView *viewTopRelate;
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UILabel *lblMorenews2;

@end
