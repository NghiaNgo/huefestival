//
//  NewsViewController.m
//  Festival2016
//
//  Created by Phan Phuoc Luong on 11/13/15.
//  Copyright © 2015 Phan Phuoc Luong. All rights reserved.
//

#import "NewsViewController.h"
#import "NewsDetailViewController.h"
#import "SearchViewController.h"
#import "NewsCell.h"
#import "NewsObj.h"
#import "CouchbaseEvents.h"

@interface NewsViewController ()

@end

@implementation NewsViewController

- (id)initWithDatas:(NSString *)nibName andType:(int)type
{
    if(self = [super initWithNibName:nibName bundle:nil]){
        typeHeader = type;
        page_index = 0;
        arrDatas = [[NSMutableArray alloc] init];
        arrDatasSearch = [[NSMutableArray alloc] init];
        appDelegate = MyAppDelegate();
    }
    
    return self;
}

- (void)initSafeArea {
    _viewMain.translatesAutoresizingMaskIntoConstraints = NO;
    
    if (@available(iOS 11, *)) {
        UILayoutGuide *guide = self.view.safeAreaLayoutGuide;
        [_viewMain.leadingAnchor constraintEqualToAnchor:guide.leadingAnchor].active = YES;
        [_viewMain.trailingAnchor constraintEqualToAnchor:guide.trailingAnchor].active = YES;
        [_viewMain.topAnchor constraintEqualToAnchor:guide.topAnchor].active = YES;
        [_viewMain.bottomAnchor constraintEqualToAnchor:guide.bottomAnchor].active = YES;
    } else {
        UILayoutGuide *margins = self.view.layoutMarginsGuide;
        [_viewMain.leadingAnchor constraintEqualToAnchor:margins.leadingAnchor].active = YES;
        [_viewMain.trailingAnchor constraintEqualToAnchor:margins.trailingAnchor].active = YES;
        [_viewMain.topAnchor constraintEqualToAnchor:self.topLayoutGuide.bottomAnchor].active = YES;
        [_viewMain.bottomAnchor constraintEqualToAnchor:self.bottomLayoutGuide.topAnchor].active = YES;
    }
    // Refresh myView and/or main view
    [self.view layoutIfNeeded];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.frame = [UIScreen mainScreen].bounds;
    //
    [self initSafeArea];
    //
    //init langague
    [self refreshLanguage];
    //load
    [self loadTable];
    
    pullToRefreshManager_ = [[MNMBottomPullToRefreshManager alloc] initWithPullToRefreshViewHeight:60.0f tableView:_tableView withClient:self];
    
    //load news just liked
    if(appDelegate.arrNewsLikeList.count == 0)
        [self loadNewsLiked];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [_optionIndices removeAllIndexes];
    if(typeHeader == 0){
        _optionIndices = [NSMutableIndexSet indexSetWithIndex:4];
        [self initMenuLeft];
        [_btnMenu setHidden:NO];
        [_btnBack setHidden:YES];
    } else {
        [_btnMenu setHidden:YES];
        [_btnBack setHidden:NO];
    }
}

//refresh language
- (void)refreshLanguage
{
    [_btnSearch setTitle:MYLocalizedString(@"search", nil) forState:UIControlStateNormal];
}

- (void)refreshImage:(NSString *)newID andImage:(UIImage *)image
{
    NSInteger index = 0;
    for(NewsObj *o in arrDatasSearch){
        if([o.nID isEqualToString:newID]){
            o.newsThumb = image;
            [self reloadDataTable:index];
            break;
        }
        index++;
    }
}

- (IBAction) clickBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction) clickSearch:(id)sender
{
    SearchViewController *search = [[SearchViewController alloc] initWithDatas:@"SearchViewController" andType:search_news andDatas:arrDatasSearch];
    [self.navigationController pushViewController:search animated:NO];
}

- (IBAction) clickMenuLeft:(id)sender
{
    [menuLeft setIsSingleSelect:YES];
    [menuLeft show];
}

- (void) initMenuLeft
{
    NSString *sub = appDelegate.language;
    if(sub.length == 0)
        sub = @"vi";
    NSArray *images = @[[UIImage imageNamed:@"ic_menu_focus.png"],
                        [UIImage imageNamed:[NSString stringWithFormat:@"ic_home_%@.png", sub]],
                        [UIImage imageNamed:[NSString stringWithFormat:@"ic_program_%@.png", sub]],
                        [UIImage imageNamed:[NSString stringWithFormat:@"ic_place_%@.png", sub]],
                        [UIImage imageNamed:[NSString stringWithFormat:@"ic_news_%@.png", sub]],
                        [UIImage imageNamed:[NSString stringWithFormat:@"ic_profile_%@.png", sub]],
                        ];
    
    NSArray *colors = @[
                        [UIColor colorWithRed:215/255.f green:169/255.f blue:27/255.f alpha:0],
                        [UIColor colorWithRed:215/255.f green:169/255.f blue:27/255.f alpha:1],
                        [UIColor colorWithRed:215/255.f green:169/255.f blue:27/255.f alpha:1],
                        [UIColor colorWithRed:215/255.f green:169/255.f blue:27/255.f alpha:1],
                        [UIColor colorWithRed:215/255.f green:169/255.f blue:27/255.f alpha:1],
                        [UIColor colorWithRed:215/255.f green:169/255.f blue:27/255.f alpha:1],
                        ];
    
    
    menuLeft = [[RNFrostedSidebar alloc] initWithImages:images selectedIndices:_optionIndices borderColors:colors];
    menuLeft.delegate = self;
}

#pragma mark - RNFrostedSidebarDelegate

- (void)sidebar:(RNFrostedSidebar *)sidebar didTapItemAtIndex:(NSUInteger)index {
    [sidebar dismissAnimated:YES completion:^(BOOL finished) {
        if(index != 0 && index != 4){
            [self.navigationController popToRootViewControllerAnimated:NO];
            [_menuSliderDelegate callToNewController:(int)index];
        }
    }];
}

- (void)sidebar:(RNFrostedSidebar *)sidebar didEnable:(BOOL)itemEnabled itemAtIndex:(NSUInteger)index {
    if (itemEnabled) {
        [self.optionIndices addIndex:index];
    } else {
        [self.optionIndices removeIndex:index];
    }
}
#pragma mark - UItableview
#pragma mark tableview Delegate method
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Indentifier";
    
    NewsCell *cell = (NewsCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    NewsObj *obj = [arrDatasSearch objectAtIndex:indexPath.row];
    if (cell == nil) {
        NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:IS_IPAD?@"NewsCellIPad":@"NewsCell" owner:self options:nil];
        for(id currentObject in topLevelObjects) {
            if([currentObject isKindOfClass:[UITableViewCell class]])
            {
                cell = (NewsCell *)currentObject;
                cell.backgroundColor = [UIColor clearColor];
                break;
            }
        }
    }
            
    if(obj){
        cell.lblTitle.text = obj.nTitle;
        if(obj.newsThumb){
            cell.imgThumb.image = obj.newsThumb;
            [cell.progress stopAnimating];
        } else {
            cell.imgThumb.image = [UIImage imageNamed:@"test_thumb0.png"];
        }
    }
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_tableView deselectRowAtIndexPath:indexPath animated:NO];
    NewsObj *obj = [arrDatasSearch objectAtIndex:indexPath.row];
    NewsDetailViewController *detail = [[NewsDetailViewController alloc] initWithObject:@"NewsDetailViewController" andObj:obj andArr:arrDatas];
    [self.navigationController pushViewController:detail animated:YES];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return nil;
}

#pragma TableDatasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!IS_IPAD){
        //for news
        float hCell = 200.0f;
        NewsObj *obj = nil;
        obj = [arrDatasSearch objectAtIndex:indexPath.row];
        CGRect textRect = [obj.nTitle boundingRectWithSize:CGSizeMake(_tableView.frame.size.width - 22, 9999)
                                                      options:NSStringDrawingUsesLineFragmentOrigin
                                                   attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica Neue" size:15]}
                                                      context:nil];
            
            
        CGSize size = textRect.size;
        float h = size.height;
            
        if(h <= 21)
            return hCell;
        else
            return hCell + (h - 21) + h/21;
    } else {
        float hCell = 400.0f;
        NewsObj *obj = nil;
        obj = [arrDatasSearch objectAtIndex:indexPath.row];
        CGRect textRect = [obj.nTitle boundingRectWithSize:CGSizeMake(_tableView.frame.size.width - 22, 9999)
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica Neue" size:24]}
                                                  context:nil];
        
        
        CGSize size = textRect.size;
        float h = size.height;
        
        if(h <= 50)
            return hCell;
        else
            return hCell + (h - 50) + h/50;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrDatasSearch.count;
}

#pragma mark -
#pragma mark MNMBottomPullToRefreshManagerClient
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [pullToRefreshManager_ tableViewScrolled];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [pullToRefreshManager_ tableViewReleased];
}

- (void)bottomPullToRefreshTriggered:(MNMBottomPullToRefreshManager *)manager {
}

- (void)MNMBottomPullToRefreshManagerClientReloadTable
{
    [self performSelector:@selector(loadTable) withObject:nil afterDelay:1.0f];
}

#pragma mark -
#pragma mark Aux view methods

/*
 * Loads the table
 */
- (void)loadTable {
    page_index++;
    [self connectToServer:0];
    [_tableView reloadData];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [pullToRefreshManager_ relocatePullToRefreshView];
}

//////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////// Connect with Server ////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
- (void) connectToServer:(int)typeReg
{
    if(![CheckConnection hasConnectivity]){
        [UIAlertView showWithTitle:MYLocalizedString(@"error.internet.title", nil) message:MYLocalizedString(@"error.internet.content", nil) handler:nil];
        return;
    }
    if(page_index == 1)
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    id successBlock = ^(AFHTTPRequestOperation *operation, id jsonResult)
    {
        //NSLog(@"%@", jsonResult);
        switch (typeReg) {
            case 0:{//get news
                int type = [[jsonResult objectForKey:@"type"] intValue];
                if(type == 1){
                    NSArray *arrDict = (NSArray *)[jsonResult objectForKey:@"list"];
                    NewsObj *obj = nil;
                    for (NSDictionary *d in arrDict) {
                        obj = [[NewsObj alloc] initWithDict:d];
                        obj.loadImageDelegate = (id)appDelegate;
                        
                        [arrDatas addObject:obj];
                    }
                    
                    [arrDatasSearch removeAllObjects];
                    [arrDatasSearch addObjectsFromArray:arrDatas];
                    [_tableView reloadData];
                }
                
                break;
            }
            default:
                break;
        }
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [pullToRefreshManager_ tableViewReloadFinished];
    };
    
    id failureBlock = ^(AFHTTPRequestOperation *operation, NSError *error) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [pullToRefreshManager_ tableViewReloadFinished];
        
        [UIAlertView showWithTitle:MYLocalizedString(@"error.fail.title", nil) message:MYLocalizedString(@"error.fail.content", nil) handler:nil];
    };
    
    switch (typeReg) {
        case 0:{//lay danh sach cac diem bieu dien
            [[MyApiClient sharedClient] getNews:page_index numItem:PAGE_COUNT andSuccesBlock:successBlock andFailBlock:failureBlock];
            break;
        }
            
        default:
            break;
    }
}

#pragma common
- (void) showOLGhostAlertView: (NSString *) oltitle content:(NSString *) strContent time: (int) timeout
{
    OLGhostAlertView *olAlert = [[OLGhostAlertView alloc] initWithTitle:oltitle message:strContent timeout:timeout dismissible:YES];
    [olAlert show];
}

- (void)reloadDataTable:(NSInteger) index
{
    NSString *strIndex = [NSString stringWithFormat:@"%zd", index];
    [self performSelectorOnMainThread:@selector(refreshTable:) withObject:strIndex waitUntilDone:NO];
}

- (void) refreshTable:(NSString *) strIndex
{
    NSInteger index = [strIndex integerValue];
    if(index >= arrDatasSearch.count || index < 0)
        return;
    
    [_tableView beginUpdates];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
    [_tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
    [_tableView endUpdates];
}

//load like new
- (void) loadNewsLiked
{
    NSMutableArray *arrDict = [[CouchbaseEvents sharedInstance] loadAllNewsLiked];
    for(NSDictionary *d in arrDict){
        NewsObj *obj = [[NewsObj alloc] initWithDict:d];
        [appDelegate.arrNewsLikeList addObject:obj];
    }
}

@end
