//
//  NewsViewController.h
//  Festival2016
//
//  Created by Phan Phuoc Luong on 11/13/15.
//  Copyright © 2015 Phan Phuoc Luong. All rights reserved.
//

#import "RNFrostedSidebar.h"
#import "MNMBottomPullToRefreshManager.h"

@interface NewsViewController : UIViewController<RNFrostedSidebarDelegate, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate, MNMBottomPullToRefreshManagerClient>
{
    AppDelegate *appDelegate;
    RNFrostedSidebar *menuLeft;
    int typeHeader;//để xác định nút back hay menu
    NSInteger page_index;
    
    NSMutableArray *arrDatas;
    NSMutableArray *arrDatasSearch;
    /**
     * Pull to refresh manager
     */
    MNMBottomPullToRefreshManager *pullToRefreshManager_;
}

- (id) initWithDatas:(NSString *)nibName andType:(int)typeHeader;
@property (nonatomic, weak) IBOutlet UIView *viewMain;
//
@property (nonatomic, strong) id<MenuSliderDelegate> menuSliderDelegate;

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, weak) IBOutlet UIButton *btnMenu;
@property (nonatomic, weak) IBOutlet UIButton *btnBack;

//for search
@property (nonatomic, weak) IBOutlet UIButton *btnSearch;
//for slide
@property (nonatomic, strong) NSMutableIndexSet *optionIndices;

- (void) refreshLanguage;
- (void) refreshImage:(NSString *)newID andImage:(UIImage *)image;
@end
