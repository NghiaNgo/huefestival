//
//  LikeViewController.m
//  Festival2016
//
//  Created by Phan Phuoc Luong on 2/20/16.
//  Copyright © 2016 Phan Phuoc Luong. All rights reserved.
//

#import "LikeViewController.h"
#import "ProgramCell.h"
#import "ProgramObj.h"
#import "ProgramDetailViewController.h"
#import "ArtObj.h"
#import "ArtDetailViewController.h"
#import "TicketLocationObj.h"
#import "PlaceDetailViewController.h"
#import "NewsObj.h"
#import "NewsDetailViewController.h"


@interface LikeViewController ()

@end

@implementation LikeViewController

- (id)initWithDatas:(NSString *)nibName andData:(NSMutableArray *)arrPrograms andData1:(NSMutableArray *)arrArtist andData2:(NSMutableArray *)arrNews andData3:(NSMutableArray *)arrDiaDiem
{
    if(self = [super initWithNibName:nibName bundle:nil]){
        arrDataProgram = [[NSMutableArray alloc] initWithArray:arrPrograms];
        arrDataArtist = [[NSMutableArray alloc] initWithArray:arrArtist];
        arrDataNews = [[NSMutableArray alloc] initWithArray:arrNews];
        arrDataDiaDiem = [[NSMutableArray alloc] initWithArray:arrDiaDiem];
        appDelegate = MyAppDelegate();
    }
    
    return self;
}

- (void)initSafeArea {
    _viewMain.translatesAutoresizingMaskIntoConstraints = NO;
    
    if (@available(iOS 11, *)) {
        UILayoutGuide *guide = self.view.safeAreaLayoutGuide;
        [_viewMain.leadingAnchor constraintEqualToAnchor:guide.leadingAnchor].active = YES;
        [_viewMain.trailingAnchor constraintEqualToAnchor:guide.trailingAnchor].active = YES;
        [_viewMain.topAnchor constraintEqualToAnchor:guide.topAnchor].active = YES;
        [_viewMain.bottomAnchor constraintEqualToAnchor:guide.bottomAnchor].active = YES;
    } else {
        UILayoutGuide *margins = self.view.layoutMarginsGuide;
        [_viewMain.leadingAnchor constraintEqualToAnchor:margins.leadingAnchor].active = YES;
        [_viewMain.trailingAnchor constraintEqualToAnchor:margins.trailingAnchor].active = YES;
        [_viewMain.topAnchor constraintEqualToAnchor:self.topLayoutGuide.bottomAnchor].active = YES;
        [_viewMain.bottomAnchor constraintEqualToAnchor:self.bottomLayoutGuide.topAnchor].active = YES;
    }
    // Refresh myView and/or main view
    [self.view layoutIfNeeded];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.frame = [UIScreen mainScreen].bounds;
    //
    [self initSafeArea];
    //
    _viewBlur.blurRadius = 15;
    //init langague
    [self refreshLanguage];
    [_tableView reloadData];
}

//refresh language
- (void)refreshLanguage
{
    _lblTitle.text = MYLocalizedString(@"profile.liked1", nil);
}

- (IBAction) clickBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UItableview
#pragma mark tableview Delegate method
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Indentifier";
    
    ProgramCell *cell = (ProgramCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:IS_IPAD?@"ProgramCellIPad":@"ProgramCell" owner:self options:nil];
        for(id currentObject in topLevelObjects) {
            if([currentObject isKindOfClass:[UITableViewCell class]])
            {
                cell = (ProgramCell *)currentObject;
                cell.backgroundColor = [UIColor clearColor];
                break;
            }
        }
    }
    
    NSInteger section = indexPath.section;
    switch (section) {
        case 0:{
            ProgramObj *obj = [arrDataProgram objectAtIndex:indexPath.row];
            if(obj && [obj isKindOfClass:[ProgramObj class]]){
                cell.lblTitle.text = obj.pName;
                if(obj.arrImage.count > 0){
                    cell.imgThumb.image = [obj.arrImage objectAtIndex:0];
                    [cell.progress stopAnimating];
                } else
                    cell.imgThumb.image = [UIImage imageNamed:@"test_thumb0.png"];
            }
            break;
        }
        case 1:{
            ArtObj *obj = [arrDataArtist objectAtIndex:indexPath.row];
            if(obj && [obj isKindOfClass:[ArtObj class]]){
                cell.lblTitle.text = obj.aName;
                if(obj.arrImage.count > 0){
                    cell.imgThumb.image = [obj.arrImage objectAtIndex:0];
                    [cell.progress stopAnimating];
                } else
                    cell.imgThumb.image = [UIImage imageNamed:@"test_thumb0.png"];
            }
            break;
        }
        case 2:{
            NewsObj *obj = [arrDataNews objectAtIndex:indexPath.row];
            if(obj && [obj isKindOfClass:[NewsObj class]]){
                cell.lblTitle.text = obj.nTitle;
                if(obj.newsThumb){
                    cell.imgThumb.image = obj.newsThumb;
                    [cell.progress stopAnimating];
                } else
                    cell.imgThumb.image = [UIImage imageNamed:@"test_thumb0.png"];
            }
            break;
        }
        case 3:{
            TicketLocationObj *obj = [arrDataDiaDiem objectAtIndex:indexPath.row];
            if(obj && [obj isKindOfClass:[TicketLocationObj class]]){
                cell.lblTitle.text = obj.title;
                if(obj.pThumb)
                    cell.imgThumb.image = obj.pThumb;
                else
                    cell.imgThumb.image = [UIImage imageNamed:@"test_thumb0.png"];
            }
            break;
        }
        default:
            break;
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_tableView deselectRowAtIndexPath:indexPath animated:NO];
    NSInteger section = indexPath.section;
    switch (section) {
        case 0:{
            ProgramObj *obj = [arrDataProgram objectAtIndex:indexPath.row];
            ProgramDetailViewController *detail = [[ProgramDetailViewController alloc] initWithObjectSelected:obj];
            [self.navigationController pushViewController:detail animated:YES];
            break;
        }
        case 1:{
            ArtObj *obj = [arrDataArtist objectAtIndex:indexPath.row];
            ArtDetailViewController *detail = [[ArtDetailViewController alloc] initWithObjectSelected:obj];
            [self.navigationController pushViewController:detail animated:YES];
            break;
        }
        case 2:{
            NewsObj *obj = [arrDataNews objectAtIndex:indexPath.row];
            NewsDetailViewController *detail = [[NewsDetailViewController alloc] initWithObject:@"NewsDetailViewController" andObj:obj andArr:arrDataNews];
            [self.navigationController pushViewController:detail animated:YES];
            break;
        }
        case 3:{
            TicketLocationObj *obj = [arrDataDiaDiem objectAtIndex:indexPath.row];
            PlaceDetailViewController *detail = [[PlaceDetailViewController alloc] initWithObject:@"PlaceDetailViewController" andObj:obj andArr:arrDataDiaDiem];
            [self.navigationController pushViewController:detail animated:YES];
            break;
        }
        default:
            break;
    }
}

#pragma TableDatasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *content = @"";
    NSInteger section = indexPath.section;
    switch (section) {
        case 0:{
            ProgramObj *obj = nil;
            obj = [arrDataProgram objectAtIndex:indexPath.row];
            content = obj.pName;
            break;
        }
        case 1:{
            ArtObj *obj = nil;
            obj = [arrDataArtist objectAtIndex:indexPath.row];
            content = obj.aName;
            break;
        }
        case 2:{
            NewsObj *obj = nil;
            obj = [arrDataNews objectAtIndex:indexPath.row];
            content = obj.nTitle;
            break;
        }
        case 3:{
            TicketLocationObj *obj = nil;
            obj = [arrDataDiaDiem objectAtIndex:indexPath.row];
            content = obj.title;
            break;
        }
    }
    
    if(!IS_IPAD){
        float hCell = 200.0f;
        CGRect textRect = [content boundingRectWithSize:CGSizeMake(_tableView.frame.size.width - 22, 9999)
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica Neue" size:15]}
                                                  context:nil];
        
        
        CGSize size = textRect.size;
        float h = size.height;
        
        if(h <= 21)
            return hCell;
        else
            return hCell + (h - 21) + h/21;
    } else {
        float hCell = 400.0f;
        CGRect textRect = [content boundingRectWithSize:CGSizeMake(_tableView.frame.size.width - 22, 9999)
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica Neue" size:24]}
                                                  context:nil];
        
        
        CGSize size = textRect.size;
        float h = size.height;
        
        if(h <= 50)
            return hCell;
        else
            return hCell + (h - 50) + h/50;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return arrDataProgram.count;
            break;
        case 1:
            return arrDataArtist.count;
            break;
        case 2:
            return arrDataNews.count;
            break;
        case 3:
            return arrDataDiaDiem.count;
            break;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    int h = 44;
    switch (section) {
        case 0:
            return arrDataProgram.count > 0 ? h : 0;
        case 1:
            return arrDataArtist.count > 0 ? h : 0;
        case 2:
            return arrDataNews.count > 0 ? h : 0;
        case 3:
            return arrDataDiaDiem.count > 0 ? h : 0;
    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenViewWidth, 44)];
    view.backgroundColor = [UIColor colorWithRed:194/255.0 green:34/255.0 blue:35/255.0 alpha:1.f];
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, screenViewWidth - 20, view.frame.size.height)];
    lbl.backgroundColor = [UIColor clearColor];
    lbl.textColor = UIColorFromRGB(COLOR_WHITE);
    lbl.font = [UIFont boldSystemFontOfSize:15];
    switch (section) {
        case 0:
            lbl.text = [MYLocalizedString(@"like.program", nil) uppercaseString];
            break;
        case 1:
            lbl.text = [MYLocalizedString(@"like.artist", nil) uppercaseString];
            break;
        case 2:
            lbl.text = [MYLocalizedString(@"like.news", nil) uppercaseString];
            break;
        case 3:
            lbl.text = [MYLocalizedString(@"like.location", nil) uppercaseString];
            break;
    }
    [view addSubview:lbl];
    
    return view;
}

@end
