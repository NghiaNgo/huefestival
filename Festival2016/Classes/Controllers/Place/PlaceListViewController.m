//
//  PlaceListViewController.m
//  Festival2016
//
//  Created by Phan Phuoc Luong on 2/17/16.
//  Copyright © 2016 Phan Phuoc Luong. All rights reserved.
//

#import "PlaceListViewController.h"
#import "SearchViewController.h"
#import "LocationObj.h"
#import "MapViewController.h"
#import "PlaceDetailViewController.h"

@interface PlaceListViewController ()

@end

@implementation PlaceListViewController

- (id)initWithDatas:(NSString *)nibName andObject:(MenuSubObj *)obj
{
    if(self = [super initWithNibName:nibName bundle:nil]){
        objSelected = obj;
        page_index = 0;
        arrDatas = [[NSMutableArray alloc] init];
        appDelegate = MyAppDelegate();
    }
    
    return self;
}

- (void)initSafeArea {
    _viewMain.translatesAutoresizingMaskIntoConstraints = NO;
    
    if (@available(iOS 11, *)) {
        UILayoutGuide *guide = self.view.safeAreaLayoutGuide;
        [_viewMain.leadingAnchor constraintEqualToAnchor:guide.leadingAnchor].active = YES;
        [_viewMain.trailingAnchor constraintEqualToAnchor:guide.trailingAnchor].active = YES;
        [_viewMain.topAnchor constraintEqualToAnchor:guide.topAnchor].active = YES;
        [_viewMain.bottomAnchor constraintEqualToAnchor:guide.bottomAnchor].active = YES;
    } else {
        UILayoutGuide *margins = self.view.layoutMarginsGuide;
        [_viewMain.leadingAnchor constraintEqualToAnchor:margins.leadingAnchor].active = YES;
        [_viewMain.trailingAnchor constraintEqualToAnchor:margins.trailingAnchor].active = YES;
        [_viewMain.topAnchor constraintEqualToAnchor:self.topLayoutGuide.bottomAnchor].active = YES;
        [_viewMain.bottomAnchor constraintEqualToAnchor:self.bottomLayoutGuide.topAnchor].active = YES;
    }
    // Refresh myView and/or main view
    [self.view layoutIfNeeded];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.frame = [UIScreen mainScreen].bounds;
    //
    [self initSafeArea];
    //
    //init langague
    [self refreshLanguage];
    
    [self loadTable];
    pullToRefreshManager_ = [[MNMBottomPullToRefreshManager alloc] initWithPullToRefreshViewHeight:60.0f tableView:_tableView withClient:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

//refresh language
- (void)refreshLanguage
{
    [_btnSearch setTitle:MYLocalizedString(@"search", nil) forState:UIControlStateNormal];
}

- (void)loadImageTicketLocationFinish:(NSString *)_id andIndex:(NSInteger)index andImage:(UIImage *)img
{
    [_tableView reloadData];
}

- (IBAction) clickBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction) clickSearch:(id)sender
{
    SearchViewController *search = [[SearchViewController alloc] initWithDatas:@"SearchViewController" andType:search_content andDatas:arrDatas];
    [self.navigationController pushViewController:search animated:NO];
}

#pragma mark - UItableview
#pragma mark tableview Delegate method
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Indentifier";
    
    //chuong trinh
    TicketLocationCell *cell = (TicketLocationCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"TicketLocationCell" owner:self options:nil];
        for(id currentObject in topLevelObjects) {
            if([currentObject isKindOfClass:[UITableViewCell class]])
            {
                cell = (TicketLocationCell *)currentObject;
                cell.backgroundColor = [UIColor clearColor];
                break;
            }
        }
    }
    
    TicketLocationObj *obj = [arrDatas objectAtIndex:indexPath.row];
    if(obj){
        NSString *name = [obj.title stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        CGRect r = cell.lblTitle.frame;
        //update vi tri title
        CGRect textRect = [name boundingRectWithSize:CGSizeMake(r.size.width - 10, 9999) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica Neue" size:15]} context:nil];
        int h = (int)textRect.size.height;
            
        if(h > 21){
            h = h + 2;
            if(h > 52)
                h = 52;
            r.size.height = h;
            cell.lblTitle.frame = r;
        }
            
        cell.lblTitle.text = name;
        if(obj.pThumb){
            cell.imgThumb.image = obj.pThumb;
            cell.imgThumb.contentMode = UIViewContentModeScaleToFill;
            [cell.progress stopAnimating];
        }
        
        if(obj.address.length > 0){
            [cell.lblCap1 setTitle:obj.address forState:UIControlStateNormal];
            cell.lblCap1.tag = indexPath.row;
            [cell.lblCap1 addTarget:self action:@selector(callLocation:) forControlEvents:UIControlEventTouchUpInside];
        } else {
            cell.imgAddress.hidden = YES;;
        }
        
        if(obj.phone.length > 0){
            [cell.lblCap2 setTitle:obj.phone forState:UIControlStateNormal];
            cell.lblCap2.tag = indexPath.row;
            [cell.lblCap2 addTarget:self action:@selector(callPhone:) forControlEvents:UIControlEventTouchUpInside];
        } else {
            cell.imgPhone.hidden = YES;;
        }
        
        if(cell.imgAddress.isHidden && cell.imgPhone.isHidden && obj.sumary.length > 0){
            cell.lblDesc.hidden = NO;
            r = cell.lblDesc.frame;
            textRect = [obj.sumary boundingRectWithSize:CGSizeMake(r.size.width - 10, 9999) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica Neue" size:13]} context:nil];
            int h = (int)textRect.size.height;
            if(h > 21){
                h = h + 2;
                if(h > 80)
                    h = 80 - (cell.lblTitle.frame.size.height - 21);
                r.origin.y = cell.lblTitle.frame.origin.y + cell.lblTitle.frame.size.height + 3;
                r.size.height = h;
                cell.lblDesc.frame = r;
            }
            cell.lblDesc.text = obj.sumary;
        }
    }

    [cell bringSubviewToFront:cell.lblCap1];
    [cell bringSubviewToFront:cell.lblCap2];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void) callLocation:(id) sender
{
    int index = (int)((UIButton *)sender).tag;
    if(index >= 0 && index < arrDatas.count){
        TicketLocationObj *obj = [arrDatas objectAtIndex:index];
        if(obj)
            [self showMapView:obj];
    }
}

- (void) showMapView:(TicketLocationObj *)obj
{
    if(obj.lat > 0 && obj.lng > 0){
        NSMutableArray *arr = [[NSMutableArray alloc] init];
        LocationObj *lObj = [[LocationObj alloc] init];
        lObj.id_diadiem = obj.id_ticketlocation;
        lObj.diadiem_name = obj.title;
        lObj.diadiem_content = [NSString stringWithFormat:@"%@ (%@)", obj.address, obj.phone];
        lObj.pThumb = obj.pThumb;
        lObj.lat = obj.lat;
        lObj.lng = obj.lng;
        [arr addObject:lObj];
        //push inview
        [appDelegate.mapViewController setDatas:1 andDatas:arr];
        [self.navigationController pushViewController:appDelegate.mapViewController animated:YES];
    }
}

- (void) callPhone:(id) sender
{
    int index = (int)((UIButton *)sender).tag;
    if(index >= 0 && index < arrDatas.count){
        TicketLocationObj *obj = [arrDatas objectAtIndex:index];
        phone = obj.phone;
        if(phone.length > 8){
            phone = [[[phone componentsSeparatedByString:@"-"] objectAtIndex:0] stringByReplacingOccurrencesOfString:@" " withString:@""];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:MYLocalizedString(@"art.alert.call.title", nil), phone] message:MYLocalizedString(@"art.alert.call.content", nil) delegate:self cancelButtonTitle:MYLocalizedString(@"nothanks", nil) otherButtonTitles:MYLocalizedString(@"call", nil), nil];
            [alert show];
        }
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1){
        phone = [phone stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", phone]]];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_tableView deselectRowAtIndexPath:indexPath animated:NO];
    TicketLocationObj *obj = [arrDatas objectAtIndex:indexPath.row];
    obj.typedata = objSelected.sTypeData;
    PlaceDetailViewController *detail = [[PlaceDetailViewController alloc] initWithObject:@"PlaceDetailViewController" andObj:obj andArr:arrDatas];
    [self.navigationController pushViewController:detail animated:YES];
}

#pragma TableDatasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrDatas.count;
}

#pragma mark -
#pragma mark MNMBottomPullToRefreshManagerClient
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [pullToRefreshManager_ tableViewScrolled];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [pullToRefreshManager_ tableViewReleased];
}

- (void)bottomPullToRefreshTriggered:(MNMBottomPullToRefreshManager *)manager {
}

- (void)MNMBottomPullToRefreshManagerClientReloadTable
{
    [self performSelector:@selector(loadTable) withObject:nil afterDelay:1.0f];
}

#pragma mark -
#pragma mark Aux view methods

/*
 * Loads the table
 */
- (void)loadTable {
    page_index++;
    [self connectToServer:0];
    [_tableView reloadData];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [pullToRefreshManager_ relocatePullToRefreshView];
}

//////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////// Connect with Server ////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
- (void) connectToServer:(int)typeReg
{
    if(![CheckConnection hasConnectivity]){
        [UIAlertView showWithTitle:MYLocalizedString(@"error.internet.title", nil) message:MYLocalizedString(@"error.internet.content", nil) handler:nil];
        return;
    }
    if(page_index == 1)
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    id successBlock = ^(AFHTTPRequestOperation *operation, id jsonResult)
    {
        //NSLog(@"%@", jsonResult);
        switch (typeReg) {
            case 0:{//get list
                int type = [[jsonResult objectForKey:@"type"] intValue];
                if(type == 1){
                    NSArray *arrDict = (NSArray *)[jsonResult objectForKey:@"list"];
                    TicketLocationObj *obj = nil;
                    for (NSDictionary *d in arrDict) {
                        obj = [[TicketLocationObj alloc] initWithDict:d andIndex:((int)arrDatas.count)];
                        obj.loadImageDelegate = self;
                        obj.typedata = objSelected.sTypeData;
                        [arrDatas addObject:obj];
                    }
                    
                    [_tableView reloadData];
                }
                
                break;
            }
            default:
                break;
        }
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [pullToRefreshManager_ tableViewReloadFinished];
    };
    
    id failureBlock = ^(AFHTTPRequestOperation *operation, NSError *error) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [pullToRefreshManager_ tableViewReloadFinished];
        
        [UIAlertView showWithTitle:MYLocalizedString(@"error.fail.title", nil) message:MYLocalizedString(@"error.fail.content", nil) handler:nil];
    };
    
    switch (typeReg) {
        case 0:{//lay danh sach cac
            double lat = appDelegate.myCoordinate.latitude;
            double lng = appDelegate.myCoordinate.longitude;
            [[MyApiClient sharedClient] getListByID:objSelected.sID andTypeData:objSelected.sTypeData andPageIndex:(int)page_index andPageSize:PAGE_COUNT andLat:lat andLng:lng andSuccesBlock:successBlock andFailBlock:failureBlock];
            break;
        }
            
        default:
            break;
    }
}

#pragma common
- (void) showOLGhostAlertView: (NSString *) oltitle content:(NSString *) strContent time: (int) timeout
{
    OLGhostAlertView *olAlert = [[OLGhostAlertView alloc] initWithTitle:oltitle message:strContent timeout:timeout dismissible:YES];
    [olAlert show];
}

- (void)reloadDataTable:(NSInteger) index
{
    NSString *strIndex = [NSString stringWithFormat:@"%zd", index];
    [self performSelectorOnMainThread:@selector(refreshTable:) withObject:strIndex waitUntilDone:NO];
}

- (void) refreshTable:(NSString *) strIndex
{
    NSInteger index = [strIndex integerValue];
    if(index >= arrDatas.count || index < 0)
        return;
    
    [_tableView beginUpdates];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
    [_tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
    [_tableView endUpdates];
}

@end
