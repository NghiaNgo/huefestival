//
//  PlaceViewController.m
//  Festival2016
//
//  Created by Phan Phuoc Luong on 2/16/16.
//  Copyright © 2016 Phan Phuoc Luong. All rights reserved.
//

#import "PlaceViewController.h"
#import "TicketCell.h"
#import "TicketLocationViewController.h"
#import "DiaDiemCell.h"
#import "LocationObj.h"
#import "PlaceListViewController.h"
#import "MapViewController.h"
#import "CouchbaseEvents.h"
#import "TicketLocationObj.h"

@interface PlaceViewController ()
{
    UIColor *normalColor1;
    UIColor *highLighColor;
}
@end

@implementation PlaceViewController
- (void)initSafeArea {
    _viewMain.translatesAutoresizingMaskIntoConstraints = NO;
    
    if (@available(iOS 11, *)) {
        UILayoutGuide *guide = self.view.safeAreaLayoutGuide;
        [_viewMain.leadingAnchor constraintEqualToAnchor:guide.leadingAnchor].active = YES;
        [_viewMain.trailingAnchor constraintEqualToAnchor:guide.trailingAnchor].active = YES;
        [_viewMain.topAnchor constraintEqualToAnchor:guide.topAnchor].active = YES;
        [_viewMain.bottomAnchor constraintEqualToAnchor:guide.bottomAnchor].active = YES;
    } else {
        UILayoutGuide *margins = self.view.layoutMarginsGuide;
        [_viewMain.leadingAnchor constraintEqualToAnchor:margins.leadingAnchor].active = YES;
        [_viewMain.trailingAnchor constraintEqualToAnchor:margins.trailingAnchor].active = YES;
        [_viewMain.topAnchor constraintEqualToAnchor:self.topLayoutGuide.bottomAnchor].active = YES;
        [_viewMain.bottomAnchor constraintEqualToAnchor:self.bottomLayoutGuide.topAnchor].active = YES;
    }
    // Refresh myView and/or main view
    [self.view layoutIfNeeded];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.frame = [UIScreen mainScreen].bounds;
    //
    [self initSafeArea];
    //
    appDelegate = MyAppDelegate();
    _optionIndices = [NSMutableIndexSet indexSetWithIndex:3];
    //color for button
    normalColor1 = UIColorFromRGB(TAB_COLOR);
    highLighColor = UIColorFromRGB(COLOR_YELLOW);
    
    _scrollTab.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.8f];
    _scrollTab.delegate = self;
    [_scrollTab setShowsHorizontalScrollIndicator:NO];
    [_scrollTab setShowsVerticalScrollIndicator:NO];
    
    _btnDiemDien.colorNormal = normalColor1;
    _btnDiemDien.colorHighlighted = highLighColor;
    _btnVe.colorNormal = normalColor1;
    _btnVe.colorHighlighted = highLighColor;
    
    arrDatas = [[NSMutableArray alloc] init];
    //lấy danh mục menu con
    arrMenuButton = [[NSMutableArray alloc] init];
    arrMenuCaption = [[NSMutableArray alloc] init];
    
    [self refreshLanguage];
    [self changeButtonSelected:0];
    [self performSelector:@selector(clickDiemDien:) withObject:nil];
    [self connectToServer:0 andDatas:@""];
    
    //load news just liked
    if(appDelegate.arrDiaDiemLikeList.count == 0)
        [self loadDiaDiemLiked];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //init langague
    [self refreshLanguage];
    [_optionIndices removeAllIndexes];
    [_optionIndices addIndex:3];
    [self initMenuLeft];
    
    CGRect r = _btnDiemDien.frame;
    float w = r.size.width;
    if(IS_IPAD && w < screenViewWidth/7)
        w = screenViewWidth/7;
    r.origin.x = 0;
    r.size.width = w;
    _btnDiemDien.frame = r;
    r = _lblDiemDien.frame;
    r.origin.x = 0;
    r.size.width = w;
    _lblDiemDien.frame = r;
    
    r = _btnVe.frame;
    r.origin.x = _btnDiemDien.frame.size.width;
    r.size.width = w;
    _btnVe.frame = r;
    r = _lblVe.frame;
    r.origin.x = _btnVe.frame.origin.x;
    r.size.width = w;
    _lblVe.frame = r;
}

- (IBAction) clickMenuLeft:(id)sender
{
    [menuLeft setIsSingleSelect:YES];
    [menuLeft show];
}

//refresh language
- (void)refreshLanguage
{
    _lblTitle.text = [MYLocalizedString(@"location.title", nil) uppercaseString];
    _lblDiemDien.text = [MYLocalizedString(@"tab_diemdien", nil) uppercaseString];
    _lblVe.text = [MYLocalizedString(@"tab_ve", nil) uppercaseString];
}

- (void) changeButtonSelected:(int) index
{
    tabIndex = index;
    [_btnDiemDien setSelected:NO];
    [_btnVe setSelected:NO];
    for(MyButton *btn in arrMenuButton)
        [btn setSelected:NO];
    //set title color
    _lblDiemDien.textColor = [UIColor blackColor];
    _lblVe.textColor = [UIColor blackColor];
    for(UILabel *lbl in arrMenuCaption)
        lbl.textColor = [UIColor blackColor];
    
    switch (index) {
        case 0://diem dien
            _lblDiemDien.textColor = UIColorFromRGB(TEXT_COLOR_FOCUS);
            [_btnDiemDien setSelected:YES];
            if(_scrollTab.contentOffset.x > 0){
                float x = 0;
                [_scrollTab setContentOffset:CGPointMake(x, 0) animated:NO];
            }
            break;
        case 1://ve
            _lblVe.textColor = UIColorFromRGB(TEXT_COLOR_FOCUS);
            [_btnVe setSelected:YES];
            if(_scrollTab.contentOffset.x > _btnDiemDien.frame.size.width){
                float x = _btnDiemDien.frame.size.width;
                [_scrollTab setContentOffset:CGPointMake(x, 0) animated:NO];
            }
            break;
        default:{
            MyButton *myBtn = [arrMenuButton objectAtIndex:index - 2];
            [myBtn setSelected:YES];
            UILabel *lbl = [arrMenuCaption objectAtIndex:index - 2];
            lbl.textColor = UIColorFromRGB(TEXT_COLOR_FOCUS);
            if(myBtn.frame.origin.x + myBtn.frame.size.width > screenViewWidth){
                float x = myBtn.frame.origin.x + myBtn.frame.size.width - screenViewWidth;
                [_scrollTab setContentOffset:CGPointMake(x, 0) animated:NO];
            }
            break;
        }
    }
}

#pragma mark - UItableview
#pragma mark tableview Delegate method
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(scrollView.tag != 100)
        return;
    if(scrollView.contentOffset.x > 0)
        _iconArrLeft.hidden = NO;
    else
        _iconArrLeft.hidden = YES;
    
    if(scrollView.contentSize.width - scrollView.contentOffset.x > screenViewWidth)
        _iconArrRight.hidden = NO;
    else
        _iconArrRight.hidden = YES;
}

///////////////////////////////////////////////////
/////////////////////// MENU //////////////////////
///////////////////////////////////////////////////
- (void) initMenuLeft
{
    NSString *sub = appDelegate.language;
    if(sub.length == 0)
        sub = @"vi";
    NSArray *images = @[[UIImage imageNamed:@"ic_menu_focus.png"],
                        [UIImage imageNamed:[NSString stringWithFormat:@"ic_home_%@.png", sub]],
                        [UIImage imageNamed:[NSString stringWithFormat:@"ic_program_%@.png", sub]],
                        [UIImage imageNamed:[NSString stringWithFormat:@"ic_place_%@.png", sub]],
                        [UIImage imageNamed:[NSString stringWithFormat:@"ic_news_%@.png", sub]],
                        [UIImage imageNamed:[NSString stringWithFormat:@"ic_profile_%@.png", sub]],
                        ];
    
    NSArray *colors = @[
                        [UIColor colorWithRed:215/255.f green:169/255.f blue:27/255.f alpha:0],
                        [UIColor colorWithRed:215/255.f green:169/255.f blue:27/255.f alpha:1],
                        [UIColor colorWithRed:215/255.f green:169/255.f blue:27/255.f alpha:1],
                        [UIColor colorWithRed:215/255.f green:169/255.f blue:27/255.f alpha:1],
                        [UIColor colorWithRed:215/255.f green:169/255.f blue:27/255.f alpha:1],
                        [UIColor colorWithRed:215/255.f green:169/255.f blue:27/255.f alpha:1],
                        ];
    
    menuLeft = [[RNFrostedSidebar alloc] initWithImages:images selectedIndices:_optionIndices borderColors:colors];
    menuLeft.delegate = self;
}

#pragma mark - RNFrostedSidebarDelegate

- (void)sidebar:(RNFrostedSidebar *)sidebar didTapItemAtIndex:(NSUInteger)index {
    [sidebar dismissAnimated:YES completion:^(BOOL finished) {
        if(index != 0 && index != 3){
            [self.navigationController popToRootViewControllerAnimated:NO];
            [_menuSliderDelegate callToNewController:(int)index];
        }
    }];
}

- (void)sidebar:(RNFrostedSidebar *)sidebar didEnable:(BOOL)itemEnabled itemAtIndex:(NSUInteger)index {
    if (itemEnabled) {
        [self.optionIndices addIndex:index];
    } else {
        [self.optionIndices removeIndex:index];
    }
}

- (void) initMenuTab
{
    MyButton *btn = nil;
    UILabel *lbl = nil;
    float x = _btnVe.frame.origin.x + _btnVe.frame.size.width;
    float w = _btnVe.frame.size.width;
    float h = _btnVe.frame.size.height;
    for(MenuObj *item in arrMenuList){
        btn = [[MyButton alloc] initWithFrame:CGRectMake(x, 0, w, h)];
        btn.tag = item.mID;
        btn.backgroundColor = normalColor1;
        btn.colorNormal = normalColor1;
        btn.colorHighlighted = highLighColor;
        if(item.menuIcon)
            [btn setImage:item.menuIcon forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(clickButtonOnTab:) forControlEvents:UIControlEventTouchUpInside];
        [arrMenuButton addObject:btn];
        
        lbl = [[UILabel alloc] initWithFrame:CGRectMake(x, h, w, 20)];
        lbl.tag = item.mID;
        lbl.text = [item.mTitle uppercaseString];
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.font = _lblDiemDien.font;
        [arrMenuCaption addObject:lbl];

        x += w;
        [_scrollTab addSubview:btn];
        [_scrollTab addSubview:lbl];
    }
    
    if(btn.frame.origin.x + btn.frame.size.width > screenViewWidth)
        _iconArrRight.hidden = NO;
    else
        _iconArrRight.hidden = YES;
    _scrollTab.contentSize = CGSizeMake(btn.frame.size.width + btn.frame.origin.x, _scrollTab.frame.size.height);
}

- (void)loadImageMenuListFinish:(NSString *)_id andSubID:(NSInteger)subID andImage:(UIImage *)img
{
    if(subID == 0){
        for(MyButton *btn in arrMenuButton){
            if(btn && btn.tag == [_id intValue]){
                [btn setImage:img forState:UIControlStateNormal];
            }
        }
    } else {
        int index = 2;
        for(MyButton *btn in arrMenuButton){
            if(btn && btn.tag == subID){
                for(MenuObj *menu in arrMenuList){
                    if(menu.mID == subID){
                        for(MenuSubObj *sub in menu.mSubMenu){
                            if(sub.sID == [_id intValue]){
                                sub.subIcon = img;
                                break;
                            }
                        }
                        break;
                    }
                }
            }
            index++;
        }
        if(tabIndex == index)
            [_tableView reloadData];
    }
}

- (IBAction) clickDiemDien:(id)sender
{
    _tableView.tag = 0;
    [self changeButtonSelected:0];
    
    [arrDatas removeAllObjects];
    if(appDelegate.arrLocations.count > 0){
        [arrDatas addObjectsFromArray:appDelegate.arrLocations];
    }
    [_tableView reloadData];
}

- (IBAction) clickVe:(id)sender
{
    _tableView.tag = 1;
    [self changeButtonSelected:1];
    
    [arrDatas removeAllObjects];
    [arrDatas addObject:[MYLocalizedString(@"ticket.list", nil) uppercaseString]];
    [arrDatas addObject:[MYLocalizedString(@"ticket.price", nil) uppercaseString]];
    [_tableView reloadData];
}

- (void) clickButtonOnTab:(id)sender
{
    MyButton *myBtn = (MyButton *) sender;
    NSInteger bTag = myBtn.tag;
    int index = 0;
    BOOL isOk = NO;
    for(MyButton *b in arrMenuButton){
        if(b.tag == bTag){
            isOk = YES;
            break;
        }
        ++index;
    }
    
    if(isOk){
        //reload table
        index += 2;
        _tableView.tag = index;
        [self changeButtonSelected:index];
        
        MenuObj *menuObj = [arrMenuList objectAtIndex:index - 2];
        if(menuObj){
            [arrDatas removeAllObjects];
            for(MenuSubObj *sub in menuObj.mSubMenu)
                [arrDatas addObject:sub];
            [_tableView reloadData];
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Indentifier";
    
    switch (tableView.tag) {
        case 0:{//diem dien
            DiaDiemCell *cell = (DiaDiemCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"DiaDiemCell" owner:self options:nil];
                for(id currentObject in topLevelObjects) {
                    if([currentObject isKindOfClass:[UITableViewCell class]])
                    {
                        cell = (DiaDiemCell *)currentObject;
                        cell.backgroundColor = [UIColor clearColor];
                        break;
                    }
                }
            }
            LocationObj *obj = [arrDatas objectAtIndex:indexPath.row];
            if(obj){
                cell.lblCaption.text = [obj.diadiem_name uppercaseString];
                cell.imgView.image = obj.pThumb;
            }

            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
        case 1:{// ve/ mua ve
            TicketCell *cell = (TicketCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"TicketCell" owner:self options:nil];
                for(id currentObject in topLevelObjects) {
                    if([currentObject isKindOfClass:[UITableViewCell class]])
                    {
                        cell = (TicketCell *)currentObject;
                        cell.backgroundColor = [UIColor clearColor];
                        break;
                    }
                }
            }
            NSString *caption = [arrDatas objectAtIndex:indexPath.row];
            cell.lblCaption.text = [caption uppercaseString];
            if(indexPath.row == 0){
                cell.imgIcon.image = [UIImage imageNamed:@"ic_flag.png"];
            } else if(indexPath.row == 1){
                cell.imgIcon.image = [UIImage imageNamed:@"ic_dola.png"];
            }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
        default:{
            DiaDiemCell *cell = (DiaDiemCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"DiaDiemCell" owner:self options:nil];
                for(id currentObject in topLevelObjects) {
                    if([currentObject isKindOfClass:[UITableViewCell class]])
                    {
                        cell = (DiaDiemCell *)currentObject;
                        cell.backgroundColor = [UIColor clearColor];
                        break;
                    }
                }
            }
            MenuSubObj *obj = [arrDatas objectAtIndex:indexPath.row];
            if(obj){
                cell.lblCaption.text = [obj.sTitle uppercaseString];
                if(obj.subIcon)
                    cell.imgView.image = obj.subIcon;
                else
                    cell.imgView.image = [UIImage imageNamed:@"test_thumb0.png"];
            }
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_tableView deselectRowAtIndexPath:indexPath animated:NO];
    switch (tableView.tag) {
        case 0:{//chọn 1 diem dien
            LocationObj *obj = [arrDatas objectAtIndex:indexPath.row];
            if(obj){
                NSMutableArray *arr = [[NSMutableArray alloc] init];
                [arr addObject:obj];
                //push inview
                [appDelegate.mapViewController setDatas:0 andDatas:arr];
                [self.navigationController pushViewController:appDelegate.mapViewController animated:YES];
            }
            break;
        }
        case 1:{//cho tab ban ve
            if(indexPath.row == 0){
                TicketLocationViewController *ticketViewController = [[TicketLocationViewController alloc] initWithData:@"TicketLocationViewController" andTypeList:1];
                [self.navigationController pushViewController:ticketViewController animated:YES];
            } else if(indexPath.row == 1){
                TicketLocationViewController *ticketViewController = [[TicketLocationViewController alloc] initWithData:@"TicketLocationViewController" andTypeList:2];
                [self.navigationController pushViewController:ticketViewController animated:YES];
            }
            break;
        }
        default:{
            MenuSubObj *obj = [arrDatas objectAtIndex:indexPath.row];
            if(obj){
                //NSLog(@"%@", obj.sTitle);
                PlaceListViewController *placeList = [[PlaceListViewController alloc] initWithDatas:@"PlaceListViewController" andObject:obj];
                [self.navigationController pushViewController:placeList animated:YES];
            }
            break;
        }
    }
}

#pragma TableDatasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (tableView.tag) {
        case 0:
            return IS_IPAD ? 300 : 150;
        case 1://ve /diem ban ve
            return 130;
        default:
            return IS_IPAD ? 300 : 150;
    }
    
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrDatas.count;
}
//////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////// Connect with Server ////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
- (void) connectToServer:(int)typeReg andDatas:(NSString *)datas
{
    if(![CheckConnection hasConnectivity])
        return;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    id successBlock = ^(AFHTTPRequestOperation *operation, id jsonResult)
    {
        //NSLog(@"%@", jsonResult);
        switch (typeReg) {
            case 0:{//get địa điểm tổ chức
                int type = [[jsonResult objectForKey:@"type"] intValue];
                if(type == 1){
                    arrMenuList = [[NSMutableArray alloc] init];
                    NSArray *arrDict = (NSArray *)[jsonResult objectForKey:@"list"];
                    MenuObj *obj = nil;
                    for(NSDictionary *dict in arrDict){
                        obj = [[MenuObj alloc] initWithDict:dict];
                        obj.loadImageDelegate = self;
                        [arrMenuList addObject:obj];
                    }
                    
                    [self initMenuTab];
                }
                
                break;
            }
            default:
                break;
        }
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
    };
    
    id failureBlock = ^(AFHTTPRequestOperation *operation, NSError *error) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    };
    
    switch (typeReg) {
        case 0:{//lay danh sach cac menu list
            [[MyApiClient sharedClient] getMenuList:successBlock andFailBlock:failureBlock];
            break;
        }
        default:
            break;
    }
}

- (void)refreshImageLocation:(NSString *)_idLocation andImage:(UIImage *)img
{
    if(tabIndex == 0){
        NSInteger index = 0;
        for(LocationObj *o in arrDatas){
            if([o.id_diadiem isEqualToString:_idLocation]){
                o.pThumb = img;
                [self reloadDataTable:index];
                break;
            }
            index++;
        }
    }
}

- (void)reloadDataTable:(NSInteger) index
{
    NSString *strIndex = [NSString stringWithFormat:@"%zd", index];
    [self performSelectorOnMainThread:@selector(refreshTable:) withObject:strIndex waitUntilDone:NO];
}

- (void) refreshTable:(NSString *) strIndex
{
    NSInteger index = [strIndex integerValue];
    if(index >= arrDatas.count || index < 0)
        return;
    
    [_tableView beginUpdates];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
    [_tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
    [_tableView endUpdates];
}

//load like new
- (void) loadDiaDiemLiked
{
    NSMutableArray *arrDict = [[CouchbaseEvents sharedInstance] loadAllDiaDiemLiked];
    for(NSDictionary *d in arrDict){
        TicketLocationObj *obj = [[TicketLocationObj alloc] initWithDict:d andIndex:((int)appDelegate.arrDiaDiemLikeList.count)];
        [appDelegate.arrDiaDiemLikeList addObject:obj];
    }
}

@end
