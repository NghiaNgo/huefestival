//
//  PlaceViewController.h
//  Festival2016
//
//  Created by Phan Phuoc Luong on 2/16/16.
//  Copyright © 2016 Phan Phuoc Luong. All rights reserved.
//
#import "MyButton.h"
#import "MenuObj.h"
#import "RNFrostedSidebar.h"

@interface PlaceViewController : UIViewController<RNFrostedSidebarDelegate, LoadImageDelegate, UIScrollViewDelegate>
{
    //app delegate
    AppDelegate *appDelegate;
    //menu left
    RNFrostedSidebar *menuLeft;
    
    NSMutableArray *arrDatas;
    int tabIndex;
    
    //for menu list
    NSMutableArray *arrMenuList;
    NSMutableArray *arrMenuButton;
    NSMutableArray *arrMenuCaption;
}
@property (nonatomic, weak) IBOutlet UIView *viewMain;
//
@property (nonatomic, strong) id<MenuSliderDelegate> menuSliderDelegate;

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UIScrollView *scrollTab;

@property (nonatomic, weak) IBOutlet UILabel *lblTitle;

@property (nonatomic, weak) IBOutlet UIButton *btnMenu;
@property (nonatomic, weak) IBOutlet MyButton *btnDiemDien;
@property (nonatomic, weak) IBOutlet MyButton *btnVe;
@property (nonatomic, weak) IBOutlet UILabel *lblDiemDien;
@property (nonatomic, weak) IBOutlet UILabel *lblVe;

//for slide
@property (nonatomic, strong) NSMutableIndexSet *optionIndices;
@property (nonatomic, weak) IBOutlet UIImageView *iconArrLeft;
@property (nonatomic, weak) IBOutlet UIImageView *iconArrRight;

- (void) refreshImageLocation:(NSString *)_idLocation andImage:(UIImage *)img;

@end
