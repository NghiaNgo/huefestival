//
//  PlaceListViewController.h
//  Festival2016
//
//  Created by Phan Phuoc Luong on 2/17/16.
//  Copyright © 2016 Phan Phuoc Luong. All rights reserved.
//

#import "MNMBottomPullToRefreshManager.h"
#import "MenuObj.h"
#import "TicketLocationCell.h"
#import "TicketLocationObj.h"

@interface PlaceListViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, MNMBottomPullToRefreshManagerClient, LoadImageDelegate>
{
    AppDelegate *appDelegate;
    NSInteger page_index;
    
    MenuSubObj *objSelected;
    NSString *phone;
    
    NSMutableArray *arrDatas;
    /**
     * Pull to refresh manager
     */
    MNMBottomPullToRefreshManager *pullToRefreshManager_;
}

- (id) initWithDatas:(NSString *)nibName andObject:(MenuSubObj *)obj;
@property (nonatomic, weak) IBOutlet UIView *viewMain;
//
@property (nonatomic, weak) IBOutlet UITableView *tableView;

//for search
@property (nonatomic, weak) IBOutlet UIButton *btnSearch;

@end
