//
//  DashboardViewController.m
//  Festival2016
//
//  Created by Phan Phuoc Luong on 11/9/15.
//  Copyright © 2015 Phan Phuoc Luong. All rights reserved.
//

#import "DashboardViewController.h"
#import "NewsViewController.h"
#import "FesHomeViewController.h"
#import "PlaceViewController.h"
#import "ProfileViewController.h"
#import "AboutViewController.h"
#import "AboutDetailViewController.h"
//
#import "LocationObj.h"


@interface DashboardViewController ()<SlideMenuDelegate>
{
    PlaceViewController *place;
    FesHomeViewController *fes;
    NewsViewController *news;
    AboutViewController *about;
}

@end

@implementation DashboardViewController

- (void)initSafeArea {
    _viewMain.translatesAutoresizingMaskIntoConstraints = NO;
    
    if (@available(iOS 11, *)) {
        UILayoutGuide *guide = self.view.safeAreaLayoutGuide;
        [_viewMain.leadingAnchor constraintEqualToAnchor:guide.leadingAnchor].active = YES;
        [_viewMain.trailingAnchor constraintEqualToAnchor:guide.trailingAnchor].active = YES;
        [_viewMain.topAnchor constraintEqualToAnchor:guide.topAnchor].active = YES;
        [_viewMain.bottomAnchor constraintEqualToAnchor:guide.bottomAnchor].active = YES;
    } else {
        UILayoutGuide *margins = self.view.layoutMarginsGuide;
        [_viewMain.leadingAnchor constraintEqualToAnchor:margins.leadingAnchor].active = YES;
        [_viewMain.trailingAnchor constraintEqualToAnchor:margins.trailingAnchor].active = YES;
        [_viewMain.topAnchor constraintEqualToAnchor:self.topLayoutGuide.bottomAnchor].active = YES;
        [_viewMain.bottomAnchor constraintEqualToAnchor:self.bottomLayoutGuide.topAnchor].active = YES;
    }
    // Refresh myView and/or main view
    [self.view layoutIfNeeded];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.frame = [UIScreen mainScreen].bounds;
    //
    [self initSafeArea];
    //
    appDelegate = MyAppDelegate();
    [_segLanguage setSelectedSegmentIndex:[MyAppDelegate().language isEqualToString:@"en"]? 1 : 0];
    [_segLanguage addTarget:self action:@selector(changeLanguage) forControlEvents:UIControlEventValueChanged];
    //init langague
    [self refreshLanguage];
    [self loadDataAtLocal];
    //color for button
    _viewBlur.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.35f];
    UIColor *normalColor = UIColorFromRGB(COLOR_RED);
    UIColor *highLighColor = UIColorFromRGB(COLOR_YELLOW);
    
    float corner = 4;
    _btnFes.colorNormal = normalColor;
    _btnFes.colorHighlighted = highLighColor;
    _btnFes.layer.cornerRadius = corner;
    _btnNews.colorNormal = normalColor;
    _btnNews.colorHighlighted = highLighColor;
    _btnNews.layer.cornerRadius = corner;
    _btnLocations.colorNormal = normalColor;
    _btnLocations.colorHighlighted = highLighColor;
    _btnLocations.layer.cornerRadius = corner;
    _btnProfile.colorNormal = normalColor;
    _btnProfile.colorHighlighted = highLighColor;
    _btnProfile.layer.cornerRadius = corner;
    
    //for floating view
    _viewFloatingBlur.backgroundColor = [UIColor colorWithRed:40/255.0 green:40/255.0 blue:40/255.0 alpha:1.f];
    _viewFloatingBlur.tintColor = [UIColor colorWithRed:40/255.0 green:40/255.0 blue:40/255.0 alpha:1.f];
    _viewFloatingBlur.blurRadius = 50;
    _viewFloatingBlur.contentMode = UIViewContentModeRight;
    _viewFloatingBlur.layer.contentsGravity = kCAGravityBottomLeft;
    [_viewFloatingBlur setClipsToBounds:YES];
    dispatch_block_t block = ^{
        [_viewFloatingBlur updateAsynchronously:YES completion:nil];
    };
    //
    if ([NSThread isMainThread]) {
        block();
    } else {
        dispatch_sync(dispatch_get_main_queue(), block);
    }
    _btnTieuDiem.colorNormal = normalColor;
    _btnTieuDiem.colorHighlighted = highLighColor;
    _btnCongDong.colorNormal = normalColor;
    _btnCongDong.colorHighlighted = highLighColor;
    _btnLichDien.colorNormal = normalColor;
    _btnLichDien.colorHighlighted = highLighColor;
    _btnNgheSi.colorNormal = normalColor;
    _btnNgheSi.colorHighlighted = highLighColor;
    //update location for view floating
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc]
                                             initWithTarget:self action:@selector(clickCloseFloating:)];
    [_viewFloatingBlur addGestureRecognizer:tapRecognizer];

    CGRect frame = _viewFloatingBlur.frame;
    frame.origin.x = _viewBlur.frame.origin.x + _btnFes.frame.origin.x;
    frame.origin.y = _viewBlur.frame.origin.y + _btnFes.frame.origin.y;
    frame.size.width = _btnFes.frame.size.width;
    frame.size.height = _btnFes.frame.size.height;
    _viewFloatingBlur.frame = frame;
    _viewFloatingBlur.hidden = YES;
    _btnTieuDiem.frame = _btnClose.frame;
    _btnTieuDiem.hidden = YES;
    _btnCongDong.frame = _btnClose.frame;
    _btnCongDong.hidden = YES;
    _btnLichDien.frame = _btnClose.frame;
    _btnLichDien.hidden = YES;
    _btnNgheSi.frame = _btnClose.frame;
    _btnNgheSi.hidden = YES;
    
    CGRect r = _segLanguage.frame;
    r.size.height = IS_IPAD?50:32;
    _segLanguage.frame = r;
    _segLanguage.layer.cornerRadius = 4;
    _segLanguage.layer.borderColor = UIColorFromRGB(COLOR_RED).CGColor;
    _segLanguage.layer.borderWidth = 3;
    _segLanguage.tintColor = UIColorFromRGB(0xD7A91B);
    [_segLanguage setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica Neue" size:IS_IPAD?17.0:13.0],
                                           NSForegroundColorAttributeName:UIColorFromRGB(0xDB3C3C)} forState:UIControlStateSelected];
    [_segLanguage setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica Neue" size:IS_IPAD?15.0:11.0],
                                           NSForegroundColorAttributeName:[UIColor lightGrayColor]} forState:UIControlStateNormal];
    _viewDisable.hidden = YES;
    
    //handle swipse
    UISwipeGestureRecognizer *leftSwipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipes:)];
    leftSwipeGestureRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:leftSwipeGestureRecognizer];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self->appDelegate setStatusbarColor:UIColorFromRGB(0xDB3C3C)];
    
}

- (void)handleSwipes:(UISwipeGestureRecognizer *)sender
{
    [self performSelector:@selector(clickInfo:) withObject:_btnInfo];
}

- (void)showHideViewDisable:(BOOL)isHide
{
    _viewDisable.hidden = isHide;
}

//refresh language
- (void)refreshLanguage
{
    _lblFes.text = [MYLocalizedString(@"program.title", nil) uppercaseString];
    _lblNews.text = [MYLocalizedString(@"news.title", nil) uppercaseString];
    _lblLocation.text = [MYLocalizedString(@"location.title", nil) uppercaseString];
    _lblProfile.text = [MYLocalizedString(@"profile.title", nil) uppercaseString];
    
    NSString *sub = appDelegate.language;
    if(sub.length == 0)
        sub = @"vi";
    UIImage *img1 = nil;
    UIImage *img2 = nil;
    UIImage *img3 = nil;
    UIImage *img4 = nil;
    if(!IS_IPAD){
        img1 = [UIImage imageNamed:[NSString stringWithFormat:@"ct_tieudiem_%@.png", sub]];
        img2 = [UIImage imageNamed:[NSString stringWithFormat:@"ct_congdong_%@.png", sub]];
        img3 = [UIImage imageNamed:[NSString stringWithFormat:@"ct_lichdien_%@.png", sub]];
        img4 = [UIImage imageNamed:[NSString stringWithFormat:@"ct_nghesi_%@.png", sub]];
    } else {
        img1 = [UIImage imageNamed:[NSString stringWithFormat:@"ipad_ct_tieudiem_%@.png", sub]];
        img2 = [UIImage imageNamed:[NSString stringWithFormat:@"ipad_ct_congdong_%@.png", sub]];
        img3 = [UIImage imageNamed:[NSString stringWithFormat:@"ipad_ct_lichdien_%@.png", sub]];
        img4 = [UIImage imageNamed:[NSString stringWithFormat:@"ipad_ct_nghesi_%@.png", sub]];
    }
    
    [_btnTieuDiem setImage:img1 forState:UIControlStateNormal];
    [_btnCongDong setImage:img2 forState:UIControlStateNormal];
    [_btnLichDien setImage:img3 forState:UIControlStateNormal];
    [_btnNgheSi setImage:img4 forState:UIControlStateNormal];
    [_btnInfo setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"ic_about_%@.png", sub]] forState:UIControlStateNormal];
}

- (void)refreshImage:(int)tabIndex andId:(NSString *)_id andImage:(UIImage *)img
{
    if(tabIndex >= 0 && tabIndex <= 2)
        [fes refreshImage:tabIndex andId:_id];
    else if(tabIndex == 3 && place)//dia diem
        [place refreshImageLocation:_id andImage:img];
    else if(tabIndex == 4)//new
        [news refreshImage:_id andImage:img];
        
}

//change language
- (void)changeLanguage
{
    if(MyAppDelegate().isMenuShowed)
        return;
    [MyAppDelegate() switchLanguage];
    [MyAppDelegate().arrPrograms removeAllObjects];
    [MyAppDelegate().arrCalendar removeAllObjects];
    [MyAppDelegate().arrArts removeAllObjects];
    [MyAppDelegate().arrTickets removeAllObjects];
    [MyAppDelegate().arrLocations removeAllObjects];
    [MyAppDelegate().arrAboutList removeAllObjects];
    if(fes)
        [fes focusToTabIndex:0];
    //khởi tạo lại để request tab menu mới
    place = nil;
    //lấy lại danh sách các điểm diễn
    [self loadDataAtLocal];
}

- (IBAction) clickNews:(id)sender
{
    news = [[NewsViewController alloc] initWithDatas:@"NewsViewController" andType:0];
    news.menuSliderDelegate = self;
    [self.navigationController pushViewController:news animated:!sender?NO:YES];
}

- (IBAction) clickFes:(id)sender
{
    _viewFloatingBlur.hidden = NO;
    dispatch_block_t block = ^{
        [UIView animateWithDuration:0.25 delay:0.0 options: (int) UIViewAnimationCurveEaseOut
        animations:^{
            _viewFloatingBlur.frame = CGRectMake(0, 0, self.viewMain.frame.size.width, self.viewMain.frame.size.height);
        }
        completion:^(BOOL finished){
            [self callAnimationForSubView];
        }];
    };
    //
    if ([NSThread isMainThread]) {
        block();
    } else {
        dispatch_sync(dispatch_get_main_queue(), block);
    }
}

- (IBAction) clickLocation:(id)sender
{
    if(!place)
        place = [[PlaceViewController alloc] initWithNibName:@"PlaceViewController" bundle:nil];
    place.menuSliderDelegate = self;
    [self.navigationController pushViewController:place animated:!sender?NO:YES];
}

- (IBAction) clickProfile:(id)sender
{
    ProfileViewController *profile = [[ProfileViewController alloc] initWithDatas:@"ProfileViewController" andType:0];
    profile.menuSliderDelegate = self;
    [self.navigationController pushViewController:profile animated:!sender?NO:YES];
}

- (IBAction) clickInfo:(id)sender
{
    if(![CheckConnection hasConnectivity] && appDelegate.arrAboutList.count == 0){
        [UIAlertView showWithTitle:MYLocalizedString(@"error.internet.title", nil) message:MYLocalizedString(@"error.internet.content", nil) handler:nil];
        return;
    }
    [self showHideViewDisable:NO];
    about = [[AboutViewController alloc] initWithNibName:@"AboutViewController" bundle:nil];
    about.delegate = self;
    [self.revealSideViewController pushViewController:about onDirection:PPRevealSideDirectionRight withOffset:IS_IPAD?screenViewWidth/2:screenViewWidth*1/4 animated:YES];
}

- (void)clickItemOnMenu:(NSString *)title andRow:(int)row
{
    AboutDetailViewController *detail = [[AboutDetailViewController alloc] initWithDatas:@"AboutDetailViewController" andType:row andTitle:title];
    [self.navigationController pushViewController:detail animated:YES];
}

#pragma mark - menuSliderDelegate
- (void)callToNewController:(int)controllerIndex
{
    switch (controllerIndex) {
        case 2://call festival
            [self performSelector: @selector(clickTieuDiem:) withObject:nil];
            break;
        case 3://call locations
            [self performSelector: @selector(clickLocation:) withObject:nil];
            break;
        case 4://call news
            [self performSelector: @selector(clickNews:) withObject:nil];
            break;
        case 5://call profile
            [self performSelector: @selector(clickProfile:) withObject:nil];
            break;
        default:
            break;
    }
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////// Load Data At Local  /////////////////////////////
////////////////////////////////////////////////////////////////////////////////
- (void) loadDataAtLocal
{
    if(appDelegate.arrLocations.count == 0){
        NSMutableArray *arrDict = [[CouchbaseEvents sharedInstance] loadAllLocation];
        NSArray *arrSort = [arrDict sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *a, NSDictionary *b) {
            int pID1 = [[a objectForKey:@"arrange"] intValue];
            int pID2 = [[b objectForKey:@"arrange"] intValue];
            return pID1 > pID2;
        }];
        
        NSString *strJson = @"[";
        LocationObj *obj = nil;
        int i = 0;
        for(NSDictionary *d in arrSort){
            obj = [[LocationObj alloc] initWithDict:d];
            [appDelegate.arrLocations addObject:obj];
            //create string
            NSString *id_diadiem = [NSString stringWithFormat:@"{\"id_diadiem\":\"%@\"", obj.id_diadiem];
            NSString *md5 = [NSString stringWithFormat:@"\"md5\":\"%@\"},", obj.md5];
            if(i == arrDict.count - 1)
                md5 = [md5 stringByReplacingOccurrencesOfString:@"," withString:@""];
            strJson = [strJson stringByAppendingString:[NSString stringWithFormat:@"%@,%@", id_diadiem, md5]];
            i++;
        }
        strJson = [strJson stringByAppendingString:@"]"];//bổ sung mốc ] vào cuối
        //call load on server
        [self connectToServer:0 andDatas:strJson];
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////// Connect with Server ////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
- (void) connectToServer:(int)typeReg andDatas:(NSString *)datas
{
    if(![CheckConnection hasConnectivity])
        return;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    id successBlock = ^(AFHTTPRequestOperation *operation, id jsonResult)
    {
        //NSLog(@"%@", jsonResult);
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        switch (typeReg) {
            case 0:{//get địa điểm tổ chức
                int type = [[jsonResult objectForKey:@"type"] intValue];
                if(type == 1){
                    NSArray *arrDict = (NSArray *)[jsonResult objectForKey:@"list"];
                    if(arrDict.count == 0)
                        return;
                    
                    [[CouchbaseEvents sharedInstance] addLocationList:arrDict];
                    LocationObj *obj = nil;
                    for(NSDictionary *d in arrDict){
                        obj = [[LocationObj alloc] initWithDict:d];
                        if(obj.md5.length == 0)//remove item at local
                            continue;
                        
                        obj.loadImageDelegate = (id)appDelegate;
                        [appDelegate.arrLocations addObject:obj];
                    }
                }
                
                break;
            }
            default:
                break;
        }
    };
    
    id failureBlock = ^(AFHTTPRequestOperation *operation, NSError *error) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    };
    
    switch (typeReg) {
        case 0:{//lay danh sach cac diem bieu dien
            [[MyApiClient sharedClient] getAllLocation:datas andSuccesBlock:successBlock andFailBlock:failureBlock];
            break;
        }
            
        default:
            break;
    }
}

- (IBAction)clickTieuDiem:(id)sender
{
    fes = [[FesHomeViewController alloc] initWithTabIndex:0 nibName:@"FesHomeViewController"];
    fes.menuSliderDelegate = self;
    [self.navigationController pushViewController:fes animated:!sender?NO:YES];
    [self performSelector:@selector(clickCloseFloating:) withObject:nil];
}

- (IBAction)clickCongDong:(id)sender
{
    fes = [[FesHomeViewController alloc] initWithTabIndex:2 nibName:@"FesHomeViewController"];
    fes.menuSliderDelegate = self;
    [self.navigationController pushViewController:fes animated:!sender?NO:YES];
    [self performSelector:@selector(clickCloseFloating:) withObject:nil];
}

- (IBAction)clickLichDien:(id)sender
{
    fes = [[FesHomeViewController alloc] initWithTabIndex:1 nibName:@"FesHomeViewController"];
    fes.menuSliderDelegate = self;
    [self.navigationController pushViewController:fes animated:!sender?NO:YES];
    [self performSelector:@selector(clickCloseFloating:) withObject:nil];
}

- (IBAction)clickNgheSi:(id)sender
{
    fes = [[FesHomeViewController alloc] initWithTabIndex:3 nibName:@"FesHomeViewController"];
    fes.menuSliderDelegate = self;
    [self.navigationController pushViewController:fes animated:!sender?NO:YES];
    [self performSelector:@selector(clickCloseFloating:) withObject:nil];
}

//for floating view
- (void) callAnimationForSubView
{
    CGRect r = _btnClose.frame;
    if((appDelegate.screenHeight <= 480 || appDelegate.isIPhoneX) && r.size.width != r.size.height){
        float w = r.size.width;
        r.size.height = w;
        _btnClose.frame = r;
    }

    _btnTieuDiem.hidden = NO;
    [UIView animateWithDuration:0.1
                          delay:0.0
                        options: (int) UIViewAnimationCurveEaseOut
                     animations:^{
                         CGRect frame = _btnTieuDiem.frame;
                         frame.size.width = _btnClose.frame.size.width*2;
                         frame.size.height = _btnClose.frame.size.height*2;
                         frame.origin.x = _btnClose.frame.origin.x - _btnClose.frame.size.width/2;
                         frame.origin.y = _btnClose.frame.origin.y + _btnClose.frame.size.height/2 - 100 - frame.size.height;
                         _btnTieuDiem.frame = frame;
                         _btnTieuDiem.layer.cornerRadius = _btnTieuDiem.frame.size.width/2;
                     }
                     completion:^(BOOL finished){
                         _btnLichDien.hidden = NO;
                         [UIView animateWithDuration:0.1
                                               delay:0.0
                                             options: (int) UIViewAnimationCurveEaseOut
                                          animations:^{
                                              CGRect frame = _btnLichDien.frame;
                                              frame.size.width = _btnClose.frame.size.width*2;
                                              frame.size.height = _btnClose.frame.size.height*2;
                                              frame.origin.x = _btnClose.frame.origin.x + _btnClose.frame.size.width/2 + 25 + frame.size.width/2;
                                              frame.origin.y = _btnClose.frame.origin.y + _btnClose.frame.size.height/2 - 20 - frame.size.height;
                                              _btnLichDien.frame = frame;
                                              _btnLichDien.layer.cornerRadius = _btnLichDien.frame.size.width/2;
                                          }
                                          completion:^(BOOL finished){
                                              _btnCongDong.hidden = NO;
                                              [UIView animateWithDuration:0.1
                                                                    delay:0.0
                                                                  options: (int) UIViewAnimationCurveEaseOut
                                                               animations:^{
                                                                   CGRect frame = _btnCongDong.frame;
                                                                   frame.size.width = _btnClose.frame.size.width*2;
                                                                   frame.size.height = _btnClose.frame.size.height*2;
                                                                   frame.origin.x = _btnClose.frame.origin.x + _btnClose.frame.size.width/2 + 25 + frame.size.width/2;
                                                                   frame.origin.y = _btnClose.frame.origin.y + _btnClose.frame.size.height/2 + frame.size.height/2 - 25;
                                                                   _btnCongDong.frame = frame;
                                                                   _btnCongDong.layer.cornerRadius = _btnCongDong.frame.size.width/2;
                                                               }
                                                               completion:^(BOOL finished){
                                                                   _btnNgheSi.hidden = NO;
                                                                   [UIView animateWithDuration:0.1
                                                                                         delay:0.0
                                                                                       options: (int) UIViewAnimationCurveEaseOut
                                                                                    animations:^{
                                                                                        CGRect frame = _btnNgheSi.frame;
                                                                                        frame.size.width = _btnClose.frame.size.width*2;
                                                                                        frame.size.height = _btnClose.frame.size.height*2;
                                                                                        frame.origin.x = _btnClose.frame.origin.x - _btnClose.frame.size.width/2;
                                                                                        frame.origin.y = _btnClose.frame.origin.y + _btnClose.frame.size.height/2 + 100;
                                                                                        _btnNgheSi.frame = frame;
                                                                                        _btnNgheSi.layer.cornerRadius = _btnNgheSi.frame.size.width/2;
                                                                                    }
                                                                                    completion:^(BOOL finished){}];
                                                                
                                                               }];
                                          }];
                     }];
}

- (IBAction) clickCloseFloating:(id)sender
{
    [UIView animateWithDuration:0.1
                          delay:0.0
                        options: (int) UIViewAnimationCurveEaseOut
                     animations:^{
                         _btnNgheSi.frame = _btnClose.frame;
                     }
                     completion:^(BOOL finished){
                         _btnNgheSi.hidden = YES;
                         [UIView animateWithDuration:0.1
                                               delay:0.0
                                             options: (int) UIViewAnimationCurveEaseOut
                                          animations:^{
                                              _btnCongDong.frame = _btnClose.frame;
                                          }
                                          completion:^(BOOL finished){
                                              _btnCongDong.hidden = YES;
                                              [UIView animateWithDuration:0.1
                                                                    delay:0.0
                                                                  options: (int) UIViewAnimationCurveEaseOut
                                                               animations:^{
                                                                   _btnLichDien.frame = _btnClose.frame;
                                                               }
                                                               completion:^(BOOL finished){
                                                                   _btnLichDien.hidden = YES;
                                                                   [UIView animateWithDuration:0.1
                                                                                         delay:0.0
                                                                                       options: (int) UIViewAnimationCurveEaseOut
                                                                                    animations:^{
                                                                                        _btnTieuDiem.frame = _btnClose.frame;
                                                                                    }
                                                                                    completion:^(BOOL finished){
                                                                                        _btnTieuDiem.hidden = YES;
                                                                                        [UIView animateWithDuration:0.25
                                                                                                              delay:0.0
                                                                                                            options: (int) UIViewAnimationCurveEaseOut
                                                                                                         animations:^{
                                                                                                             CGRect frame = _viewFloatingBlur.frame;
                                                                                                             frame.origin.x = _viewBlur.frame.origin.x + _btnFes.frame.origin.x;
                                                                                                             frame.origin.y = _viewBlur.frame.origin.y + _btnFes.frame.origin.y;
                                                                                                             frame.size.width = _btnFes.frame.size.width;
                                                                                                             frame.size.height = _btnFes.frame.size.height;
                                                                                                             _viewFloatingBlur.frame = frame;
                                                                                                         }
                                                                                                         completion:^(BOOL finished){
                                                                                                             _viewFloatingBlur.hidden = YES;
                                                                                                         }];
                                                                                    }];
                                                               }];
                                          }];
                     }];}

#pragma common
- (void) showOLGhostAlertView: (NSString *) oltitle content:(NSString *) strContent time: (int) timeout
{
    OLGhostAlertView *olAlert = [[OLGhostAlertView alloc] initWithTitle:oltitle message:strContent timeout:timeout dismissible:YES];
    [olAlert show];
}

@end
