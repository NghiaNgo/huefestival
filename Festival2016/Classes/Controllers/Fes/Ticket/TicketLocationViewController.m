//
//  TicketLocationViewController.m
//  Festival2016
//
//  Created by Phan Phuoc Luong on 1/7/16.
//  Copyright © 2016 Phan Phuoc Luong. All rights reserved.
//

#import "TicketLocationViewController.h"
#import "MapViewController.h"
#import "CouchbaseEvents.h"
#import "LocationObj.h"
#import "SearchViewController.h"
#import "ProgramDetailViewController.h"

@interface TicketLocationViewController ()
{
    NSString *phone;
}
@end

@implementation TicketLocationViewController

- (id)initWithData:(NSString *)nibName andTypeList:(NSInteger)type
{
    if(self = [super initWithNibName:nibName bundle:nil]){
        arrDatas = [[NSMutableArray alloc] init];
        arrDatasSearch = [[NSMutableArray alloc] init];
        typeList = type;
        appDelegate = MyAppDelegate();
    }
    
    return self;
}

- (void)initSafeArea {
    _viewMain.translatesAutoresizingMaskIntoConstraints = NO;
    
    if (@available(iOS 11, *)) {
        UILayoutGuide *guide = self.view.safeAreaLayoutGuide;
        [_viewMain.leadingAnchor constraintEqualToAnchor:guide.leadingAnchor].active = YES;
        [_viewMain.trailingAnchor constraintEqualToAnchor:guide.trailingAnchor].active = YES;
        [_viewMain.topAnchor constraintEqualToAnchor:guide.topAnchor].active = YES;
        [_viewMain.bottomAnchor constraintEqualToAnchor:guide.bottomAnchor].active = YES;
    } else {
        UILayoutGuide *margins = self.view.layoutMarginsGuide;
        [_viewMain.leadingAnchor constraintEqualToAnchor:margins.leadingAnchor].active = YES;
        [_viewMain.trailingAnchor constraintEqualToAnchor:margins.trailingAnchor].active = YES;
        [_viewMain.topAnchor constraintEqualToAnchor:self.topLayoutGuide.bottomAnchor].active = YES;
        [_viewMain.bottomAnchor constraintEqualToAnchor:self.bottomLayoutGuide.topAnchor].active = YES;
    }
    // Refresh myView and/or main view
    [self.view layoutIfNeeded];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.frame = [UIScreen mainScreen].bounds;
    //
    [self initSafeArea];
    //
    //init langague
    [self refreshLanguage];
    if(typeList == 1){
        [self loadLocal];
    } else {
        [self connectToServer:1 andDatas:@"[]"];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if(typeList == 2){
        _btnSearch.hidden = YES;
        _lineSearch.hidden = YES;
        _ImgSearch.hidden = YES;
        _lblTitle.hidden = NO;
    } else {
        _lblTitle.hidden = YES;
    }
}

//refresh language
- (void)refreshLanguage
{
    [_btnSearch setTitle:MYLocalizedString(@"search", nil) forState:UIControlStateNormal];
    _lblTitle.text = MYLocalizedString(@"ticket.tour", nil);
}

- (IBAction)clickBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction) clickSearch:(id)sender
{
    SearchViewController *search = [[SearchViewController alloc] initWithDatas:@"SearchViewController" andType:search_diembanve andDatas:arrDatasSearch];
    [self.navigationController pushViewController:search animated:YES];
}

#pragma mark - UItableview
#pragma mark tableview Delegate method

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Indentifier";
    
    //chuong trinh
    TicketLocationCell *cell = (TicketLocationCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:(typeList == 1)?@"TicketLocationCell":@"TicketLocationCell1" owner:self options:nil];
        for(id currentObject in topLevelObjects) {
            if([currentObject isKindOfClass:[UITableViewCell class]])
            {
                cell = (TicketLocationCell *)currentObject;
                cell.backgroundColor = [UIColor clearColor];
                break;
            }
        }
    }
    if(typeList == 1){
        TicketLocationObj *obj = [arrDatasSearch objectAtIndex:indexPath.row];
        if(obj){
            NSString *name = [obj.title stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            CGRect r = cell.lblTitle.frame;
            //update vi tri title
            CGRect textRect = [name boundingRectWithSize:CGSizeMake(r.size.width - 10, 9999) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica Neue" size:15]} context:nil];
            int h = (int)textRect.size.height;
            
            if(h > 21){
                h = h + 2;
                if(h > 52)
                    h = 52;
                r.size.height = h;
                cell.lblTitle.frame = r;
            }
            
            cell.lblTitle.text = name;
            if(obj.pThumb){
                cell.imgThumb.image = obj.pThumb;
                [cell.progress stopAnimating];
            }
            [cell.lblCap1 setTitle:obj.address forState:UIControlStateNormal];
            [cell.lblCap2 setTitle:obj.phone forState:UIControlStateNormal];
            cell.lblCap1.tag = indexPath.row;
            cell.lblCap2.tag = indexPath.row;
            [cell.lblCap1 addTarget:self action:@selector(callLocation:) forControlEvents:UIControlEventTouchUpInside];
            [cell.lblCap2 addTarget:self action:@selector(callPhone:) forControlEvents:UIControlEventTouchUpInside];
        }
    } else if(typeList == 2){
        NSDictionary *dict = [arrDatasSearch objectAtIndex:indexPath.row];
        if(dict){
            NSString *title = [dict objectForKey:@"title"];
            NSString *price = [dict objectForKey:@"price"];
            if(!title || [title isKindOfClass:[NSNull class]])
                title = @"";
            if(!price || [price isKindOfClass:[NSNull class]])
                price = @"";
            cell.lblTitle.text = title;
            [cell.lblCap1 setTitle:[NSString stringWithFormat:@"%@ VND", price] forState:UIControlStateNormal];
        }
    }
    [cell bringSubviewToFront:cell.lblCap1];
    [cell bringSubviewToFront:cell.lblCap2];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void) callLocation:(id) sender
{
    int index = (int)((UIButton *)sender).tag;
    if(index >= 0 && index < arrDatasSearch.count){
        TicketLocationObj *obj = [arrDatasSearch objectAtIndex:index];
        if(obj)
            [self showMapView:obj];
    }
}

- (void) showMapView:(TicketLocationObj *)obj
{
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    LocationObj *lObj = [[LocationObj alloc] init];
    lObj.id_diadiem = obj.id_ticketlocation;
    lObj.diadiem_name = obj.title;
    lObj.diadiem_content = [NSString stringWithFormat:@"%@ (%@)", obj.address, obj.phone];
    lObj.pThumb = obj.pThumb;
    lObj.lat = obj.lat;
    lObj.lng = obj.lng;
    [arr addObject:lObj];
    
    [appDelegate.mapViewController setDatas:1 andDatas:arr];
    [self.navigationController pushViewController:appDelegate.mapViewController animated:YES];
}

- (void) callPhone:(id) sender
{
    int index = (int)((UIButton *)sender).tag;
    if(index >= 0 && index < arrDatasSearch.count){
        TicketLocationObj *obj = [arrDatasSearch objectAtIndex:index];
        phone = obj.phone;
        if(phone.length > 8){
            phone = [[[phone componentsSeparatedByString:@"-"] objectAtIndex:0] stringByReplacingOccurrencesOfString:@" " withString:@""];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:MYLocalizedString(@"art.alert.call.title", nil), phone] message:MYLocalizedString(@"art.alert.call.content", nil) delegate:self cancelButtonTitle:MYLocalizedString(@"nothanks", nil) otherButtonTitles:MYLocalizedString(@"call", nil), nil];
            [alert show];
        }
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1){
        phone = [phone stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", phone]]];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_tableView deselectRowAtIndexPath:indexPath animated:NO];
    if(typeList == 1){
        TicketLocationObj *obj = [arrDatasSearch objectAtIndex:indexPath.row];
        if(obj)
            [self showMapView:obj];
    } else {
        //ProgramObj *obj = [arrDatasSearch objectAtIndex:indexPath.row];
        //ProgramDetailViewController *detail = [[ProgramDetailViewController alloc] initWithObjectSelected:obj];
        //[self.navigationController pushViewController:detail animated:YES];
    }
}

#pragma TableDatasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(typeList == 1){
        return 120;
    } else if(typeList == 2){
        NSInteger hCell = 72;

        NSDictionary *d = [arrDatasSearch objectAtIndex:indexPath.row];
        NSString *name = [d objectForKey:@"title"];
        if(!name || [name isKindOfClass:[NSNull class]])
            name = @"";
        CGRect textRect = [name boundingRectWithSize:CGSizeMake(_tableView.frame.size.width - 22, 9999) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica Neue" size:15]} context:nil];
        
        CGSize size = textRect.size;
        float h = size.height;
        
        if(h <= 21)
            return hCell;
        else
            return hCell + (h - 21) + h/21;
    } else
        return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrDatasSearch.count;
}

//================================ Load Ticket At Local ================================//
- (void) loadLocal
{
    [arrDatas removeAllObjects];
    if(appDelegate.arrTickets.count == 0){
        //load data at local
        NSMutableArray *arrDict = [[CouchbaseEvents sharedInstance] loadAllTicketLocation];
        NSArray *arrSort = [arrDict sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *a, NSDictionary *b) {
            int pID1 = [[a objectForKey:@"arrange"] intValue];
            int pID2 = [[b objectForKey:@"arrange"] intValue];
            return pID1 > pID2;
        }];
        NSString *strJson = @"[";
        TicketLocationObj *obj = nil;
        int i = 0;
        for(NSDictionary *d in arrSort){
            obj = [[TicketLocationObj alloc] initWithDict:d andIndex:i];
            //======= load photo offline =====//
            UIImage *image = [[CouchbaseEvents sharedInstance] getTicketLocationPhoto:[NSString stringWithFormat:@"ticket_%@_%zd.jpg", obj.id_ticketlocation, 0] andLocationID:obj.id_ticketlocation];
            obj.pThumb = image;
            
            NSString *id_ = [NSString stringWithFormat:@"{\"id\":\"%@\"", obj.id_ticketlocation];
            NSString *md5 = [NSString stringWithFormat:@"\"md5\":\"%@\"},", obj.md5];
            if(i == arrSort.count - 1)
                md5 = [md5 stringByReplacingOccurrencesOfString:@"," withString:@""];
            strJson = [strJson stringByAppendingString:[NSString stringWithFormat:@"%@,%@", id_, md5]];
            i++;
            
            [arrDatas addObject:obj];
        }
        strJson = [strJson stringByAppendingString:@"]"];//bổ sung mốc ] vào cuối
        //call load on server
        [self connectToServer:0 andDatas:strJson];
    } else {
        [arrDatas addObjectsFromArray:appDelegate.arrTickets];
    }
    
    [arrDatasSearch removeAllObjects];
    [arrDatasSearch addObjectsFromArray:arrDatas];
    [_tableView reloadData];
}

//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////// Connect with Server /////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
- (void) connectToServer:(int)typeReg andDatas:(NSString *)datas
{
    if(![CheckConnection hasConnectivity]){
        if(arrDatas.count == 0)
            [UIAlertView showWithTitle:MYLocalizedString(@"error.internet.title", nil) message:MYLocalizedString(@"error.internet.content", nil) handler:nil];
        else {
            [appDelegate.arrTickets removeAllObjects];
            [appDelegate.arrTickets addObjectsFromArray:arrDatas];
        }
        return;
    }
    
    if(arrDatas.count == 0)
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    id successBlock = ^(AFHTTPRequestOperation *operation, id jsonResult)
    {
        //NSLog(@"%@", jsonResult);
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        switch (typeReg) {
            case 0:{//lấy danh sách các điểm bán vé
                int type = [[jsonResult objectForKey:@"type"] intValue];
                if(type == 1){
                    NSArray *arrDict = (NSArray *)[jsonResult objectForKey:@"list"];
                    if(arrDict.count == 0){
                        [appDelegate.arrTickets removeAllObjects];
                        [appDelegate.arrTickets addObjectsFromArray:arrDatas];
                        return;
                    }
                    NSArray *arrSort = [arrDict sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *a, NSDictionary *b) {
                        int pID1 = [[a objectForKey:@"arrange"] intValue];
                        int pID2 = [[b objectForKey:@"arrange"] intValue];
                        return pID1 > pID2;
                    }];
                    
                    [[CouchbaseEvents sharedInstance] addTicketLocationList:arrSort];
                    
                    TicketLocationObj *obj = nil;
                    for (NSDictionary *d in arrSort) {
                        obj = [[TicketLocationObj alloc] initWithDict:d andIndex:((int)appDelegate.arrTickets.count)];
                        
                        if(obj.md5.length == 0){
                            //remove item at local
                            [self removeItemAtLocal:obj.id_ticketlocation];
                            continue;
                        }
                        obj.loadImageDelegate = (id)appDelegate;
                        [appDelegate.arrTickets addObject:obj];
                    }
                
                    if(arrDatas.count == 0){
                        [arrDatas removeAllObjects];
                        [arrDatas addObjectsFromArray:appDelegate.arrTickets];
                    } else {
                        NSInteger index = 0;
                        for(TicketLocationObj *p1 in appDelegate.arrTickets){
                            BOOL isNew = YES;
                            for(TicketLocationObj *p2 in arrDatas){
                                if([p1.id_ticketlocation isEqualToString:p2.id_ticketlocation]){
                                    isNew = NO;
                                    [p2 updateNewData:p1];
                                    [self reloadDataTable:index];
                                    break;
                                }
                            }
                            if(isNew)
                                [arrDatas addObject:p1];
                            index++;
                        }
                        
                        [appDelegate.arrTickets removeAllObjects];
                        [appDelegate.arrTickets addObjectsFromArray:arrDatas];
                    }
                    [arrDatasSearch removeAllObjects];
                    [arrDatasSearch addObjectsFromArray:arrDatas];
                    [_tableView reloadData];
                }
                break;
            }
            case 1:{//lay danh sach gia ve
                int type = [[jsonResult objectForKey:@"type"] intValue];
                if(type == 1){
                    [arrDatas removeAllObjects];
                    NSArray *arrDict = (NSArray *)[jsonResult objectForKey:@"list"];
                    NSArray *arrSort = [arrDict sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *a, NSDictionary *b) {
                        int arr1 = [[a objectForKey:@"arrange"] intValue];
                        int arr2 = [[b objectForKey:@"arrange"] intValue];
                        return arr1 > arr2;
                    }];
                    for (NSDictionary *d in arrSort) {
                        [arrDatas addObject:d];
                    }
                    
                    [arrDatasSearch removeAllObjects];
                    [arrDatasSearch addObjectsFromArray:arrDatas];
                    [_tableView reloadData];
                }
                break;
            }
            default:
                break;
        }
    };
    
    id failureBlock = ^(AFHTTPRequestOperation *operation, NSError *error) {
        //NSLog(@"%@", error.description);
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        [UIAlertView showWithTitle:MYLocalizedString(@"error.fail.title", nil) message:MYLocalizedString(@"error.fail.content", nil) handler:nil];
    };
    
    switch (typeReg) {
        case 0:{//lấy danh sách các điểm bán vé
            [[MyApiClient sharedClient] getTicketLocations:datas andSuccesBlock:successBlock andFailBlock:failureBlock];
            break;
        }
        case 1:{//lấy danh sách giá vé
            [[MyApiClient sharedClient] getTicketPrice:datas andSuccesBlock:successBlock andFailBlock:failureBlock];
            break;
        }
        default:
            break;
    }
}


#pragma common
- (void) showOLGhostAlertView: (NSString *) title content:(NSString *) strContent time: (int) timeout
{
    OLGhostAlertView *olAlert = [[OLGhostAlertView alloc] initWithTitle:title message:strContent timeout:timeout dismissible:YES];
    [olAlert show];
}

- (void)reloadTable
{
    [_tableView reloadData];
}

- (void)reloadDataTable:(NSInteger) index
{
    NSString *strIndex = [NSString stringWithFormat:@"%zd", index];
    [self performSelectorOnMainThread:@selector(refreshTable:) withObject:strIndex waitUntilDone:NO];
}

- (void) refreshTable:(NSString *) strIndex
{
    NSInteger index = [strIndex integerValue];
    if(index >= arrDatasSearch.count || index < 0)
        return;
    
    [_tableView beginUpdates];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
    [_tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
    [_tableView endUpdates];
}

- (void) removeItemAtLocal:(NSString *)_id
{
    for(TicketLocationObj *obj in arrDatas){
        if([obj.id_ticketlocation isEqualToString:_id]){
            [arrDatas removeObject:obj];
            break;
        }
    }
}
    
- (void) removeProgramAtLocal:(NSString *)_id
{
    for(ProgramObj *obj in arrDatas){
        if([obj.pID isEqualToString:_id]){
            [arrDatas removeObject:obj];
                break;
        }
    }
}
@end
