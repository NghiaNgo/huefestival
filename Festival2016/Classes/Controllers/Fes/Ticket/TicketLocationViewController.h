//
//  TicketLocationViewController.h
//  Festival2016
//
//  Created by Phan Phuoc Luong on 1/7/16.
//  Copyright © 2016 Phan Phuoc Luong. All rights reserved.
//

#import "AppDelegate.h"
#import "TicketLocationCell.h"
#import "TicketLocationObj.h"
#import "ProgramObj.h"

@interface TicketLocationViewController : UIViewController<UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>
{
    AppDelegate *appDelegate;
    NSMutableArray *arrDatas;
    NSMutableArray *arrDatasSearch;
    
    NSInteger typeList;//1 theo địa điểm bán vé, 2 theo gia vé
}

- (id) initWithData:(NSString *)nibName andTypeList:(NSInteger) type;
- (void)reloadTable;
@property (nonatomic, weak) IBOutlet UIView *viewMain;
//
@property (nonatomic, weak) IBOutlet UITableView *tableView;
//for search
@property (nonatomic, weak) IBOutlet UILabel *lblTitle;
@property (nonatomic, weak) IBOutlet UIButton *btnSearch;
@property (nonatomic, weak) IBOutlet UIButton *ImgSearch;
@property (nonatomic, weak) IBOutlet UIImageView *lineSearch;


@end
