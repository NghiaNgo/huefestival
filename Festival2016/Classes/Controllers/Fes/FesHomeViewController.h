//
//  FesHomeViewController.h
//  Festival2016
//
//  Created by Phan Phuoc Luong on 11/9/15.
//  Copyright © 2015 Phan Phuoc Luong. All rights reserved.
//

#import "MyButton.h"
#import "RNFrostedSidebar.h"
#import "CouchbaseEvents.h"

@interface FesHomeViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, RNFrostedSidebarDelegate>
{
    AppDelegate *appDelegate;
    
    RNFrostedSidebar *menuLeft;
    
    NSMutableArray *arrDatas;
    NSMutableArray *arrDatasSearch;
}

- (id) initWithTabIndex:(int)index nibName:(NSString *)nibName;
@property (nonatomic, weak) IBOutlet UIView *viewMain;
//
@property (nonatomic, strong) id<MenuSliderDelegate> menuSliderDelegate;

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UIView *viewHeader;
@property (nonatomic, weak) IBOutlet UIScrollView *scrollTab;

//for search
@property (nonatomic, weak) IBOutlet UIButton *searchField;
@property (nonatomic, weak) IBOutlet UIButton *btnSearch;
@property (nonatomic, weak) IBOutlet UIImageView *iconLine;

@property (nonatomic, weak) IBOutlet MyButton *btnTieuDiem;
@property (nonatomic, weak) IBOutlet MyButton *btnCongDong;
@property (nonatomic, weak) IBOutlet MyButton *btnNgheSi;
@property (nonatomic, weak) IBOutlet MyButton *btnLichDien;
@property (nonatomic, weak) IBOutlet UILabel *lblTieuDiem;
@property (nonatomic, weak) IBOutlet UILabel *lblCongDong;
@property (nonatomic, weak) IBOutlet UILabel *lblNgheSi;
@property (nonatomic, weak) IBOutlet UILabel *lblLichDien;

//for slide
@property (nonatomic, strong) NSMutableIndexSet *optionIndices;

- (void) refreshImage:(int)tabIndex andId:(NSString *)_id;
- (void) focusToTabIndex:(int) tabIndex;

@end
