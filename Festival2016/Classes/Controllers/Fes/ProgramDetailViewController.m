//
//  ProgramDetailViewController.m
//  Festival2016
//
//  Created by Phan Phuoc Luong on 11/10/15.
//  Copyright © 2015 Phan Phuoc Luong. All rights reserved.
//

#import "ProgramDetailViewController.h"
#import "MapViewController.h"
#import "ProgramCell.h"
#import "LocationObj.h"
#import "PhotoDetailViewController.h"

@interface ProgramDetailViewController ()
{
    UIAlertViewHandler handler;
    DetailListObj *detailObjSelect;
}
@end

@implementation ProgramDetailViewController

- (id)initWithObjectSelected:(ProgramObj *)obj
{
    if(self = [super initWithNibName:@"ProgramDetailViewController" bundle:nil])
    {
        objSelected = obj;
        appDelegate = MyAppDelegate();
        
        arrDatas = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (void)initSafeArea {
    _viewMain.translatesAutoresizingMaskIntoConstraints = NO;
    
    if (@available(iOS 11, *)) {
        UILayoutGuide *guide = self.view.safeAreaLayoutGuide;
        [_viewMain.leadingAnchor constraintEqualToAnchor:guide.leadingAnchor].active = YES;
        [_viewMain.trailingAnchor constraintEqualToAnchor:guide.trailingAnchor].active = YES;
        [_viewMain.topAnchor constraintEqualToAnchor:guide.topAnchor].active = YES;
        [_viewMain.bottomAnchor constraintEqualToAnchor:guide.bottomAnchor].active = YES;
    } else {
        UILayoutGuide *margins = self.view.layoutMarginsGuide;
        [_viewMain.leadingAnchor constraintEqualToAnchor:margins.leadingAnchor].active = YES;
        [_viewMain.trailingAnchor constraintEqualToAnchor:margins.trailingAnchor].active = YES;
        [_viewMain.topAnchor constraintEqualToAnchor:self.topLayoutGuide.bottomAnchor].active = YES;
        [_viewMain.bottomAnchor constraintEqualToAnchor:self.bottomLayoutGuide.topAnchor].active = YES;
    }
    // Refresh myView and/or main view
    [self.view layoutIfNeeded];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.frame = [UIScreen mainScreen].bounds;
    //
    [self initSafeArea];
    //
    imageIndex = 0;
    _btnMorePlus.layer.cornerRadius = 25;
    //
    _pageControl.currentPage = 0;
    _pageControl.indicatorMargin = 3.0f;
    _pageControl.pageIndicatorTintColor = [[UIColor whiteColor] colorWithAlphaComponent:0.9f];
    _pageControl.currentPageIndicatorTintColor = [UIColorFromRGB(COLOR_RED) colorWithAlphaComponent:1.f];
    _pageControl.currentPageIndicatorImage = [UIImage imageNamed:@"ic_slide_focus.png"];
    _pageControl.pageIndicatorMaskImage = [UIImage imageNamed:@"ic_slide.png"];
    //init langague
    [self refreshLanguage];
    //touch on view lien quan
    _viewRelate.hidden = YES;
    UITapGestureRecognizer *tapShowRelate = [[UITapGestureRecognizer alloc] initWithTarget:self
                                               action:@selector(showRelateView)];
    [_viewBottom addGestureRecognizer:tapShowRelate];
    UITapGestureRecognizer *tabHideRelate = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                action:@selector(hideRelateView)];
    [_viewTopRelate addGestureRecognizer:tabHideRelate];
    
    UITapGestureRecognizer *tabScrollImage = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                    action:@selector(showPhotoDetail)];
    [_scrollImages addGestureRecognizer:tabScrollImage];
    
    handler = ^(UIAlertView *alertView, NSInteger buttonIndex) {
        if(buttonIndex == 1){
            [self callAddReminder:15];
        } else if(buttonIndex == 2){
            [self callAddReminder:30];
        } else if(buttonIndex == 3){
            [self callAddReminder:60];
        }
    };
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if(IS_IPAD){
        CGRect frame = _viewImages.frame;
        frame.size.height = 320;
        _viewImages.frame= frame;
        
        frame = _viewButton.frame;
        frame.origin.y = _viewImages.frame.origin.y + _viewImages.frame.size.height;
        _viewButton.frame = frame;
        
        frame = _scrollContent.frame;
        frame.origin.y = _viewButton.frame.origin.y + _viewButton.frame.size.height;
        frame.size.height = _scrollMain.frame.size.height - frame.origin.y;
        _scrollContent.frame = frame;
    }
    isPauseAuto = NO;
    [self loadScrollImages];
    [self loadContent];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return YES;
}

//refresh language
- (void)refreshLanguage
{
    _lblMap.text = MYLocalizedString(@"location.navigation", nil);
    _lblLike.text = MYLocalizedString(@"detail.like", nil);
    _lblSave.text = MYLocalizedString(@"detail.save", nil);
    _lblMorenews.text = MYLocalizedString(@"detail.more", nil);
    _lblMorenews2.text = MYLocalizedString(@"detail.more", nil);
    //for content
    _lblEvent.text = MYLocalizedString(@"event.title", nil);
    _lblLocationTitlee.text = MYLocalizedString(@"location.title", nil);
    _lblPriceTitle.text = MYLocalizedString(@"ticket.price.note", nil);
    _lblEventNote.text = MYLocalizedString(@"event.note", nil);
    _lblTimeTitle.text = MYLocalizedString(@"program.start", nil);
}

- (void) loadScrollImages
{
    CGRect r;
    float h = _scrollImages.frame.size.height;
    for(int i = 0; i < objSelected.arrImage.count; i++){
        r = CGRectMake(i*screenViewWidth, 0, screenViewWidth, h);
        UIImageView *imageview = [[UIImageView alloc] initWithFrame:r];
        imageview.contentMode = UIViewContentModeScaleAspectFill;
        imageview.image = [objSelected.arrImage objectAtIndex:i];
        [_scrollImages addSubview:imageview];
    }
    
    [_pageControl setNumberOfPages:objSelected.arrImage.count];
    _scrollImages.contentSize = CGSizeMake(objSelected.arrImage.count * screenViewWidth, _scrollImages.frame.size.height);
    _pageControl.currentPage = imageIndex;
    [_scrollImages setContentOffset:CGPointMake(imageIndex*screenViewWidth, 0) animated:NO];
    
    if(objSelected.arrImage.count > 1){
        if(!timer)
            timer = [NSTimer scheduledTimerWithTimeInterval:15.0
                                         target:self
                                       selector:@selector(autoMovePage)
                                       userInfo:nil
                                        repeats:YES];
    }
}

- (void) autoMovePage
{
    if(isPauseAuto)
        return;
    imageIndex++;
    if(imageIndex > objSelected.arrImage.count - 1)
        imageIndex = 0;
    
    [_scrollImages setContentOffset:CGPointMake(imageIndex*_scrollImages.frame.size.width, 0) animated:imageIndex == 0 ? NO: YES];
}

- (void) loadContent
{
    NSString *program_title = objSelected.pName;
    NSString *program_content = [objSelected.pContent stringByDecodingHTMLEntities];
    NSString *price = objSelected.pPrice;
    if(price.length == 0)
        price = @"0";
    
    float y = 5;
    CGRect r;
    r = [program_title boundingRectWithSize:CGSizeMake(screenViewWidth - 20, 9999)
                                                 options:NSStringDrawingUsesLineFragmentOrigin
                                              attributes:@{NSFontAttributeName:_lblTitle.font}
                                                 context:nil];
    CGRect frame = _lblTitle.frame;
    frame.origin.y = y;
    frame.size.height = r.size.height;
    _lblTitle.frame = frame;
    _lblTitle.text = program_title;
    y += r.size.height + 5;
    //set button like
    [_btnLike setSelected:[appDelegate checkProgramLike:objSelected.pID]];
    _lblLike.textColor = _btnLike.isSelected?UIColorFromRGB(COLOR_YELLOW):UIColorFromRGB(COLOR_WHITE);
    
    //đặt vị trí địa điểm
    NSString *strAddress = @"";
    NSString *strTime = @"";
    NSString *strDate = @"";
    //int i = 0;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    for(DetailListObj *d in objSelected.detail_list){
        strAddress = [strAddress stringByAppendingString:d.diadiem_name];
        strTime = d.time;
        NSDate *fdate = [formatter dateFromString:d.fdate];
        NSDate *tdate = [formatter dateFromString:d.tdate];
        [formatter setDateFormat:@"dd/MM"];
        if(![d.fdate isEqualToString:d.tdate])
            strDate = [NSString stringWithFormat:@"%@ - %@", [formatter stringFromDate:fdate], [formatter stringFromDate:tdate]];
        else
            strDate = [NSString stringWithFormat:@"%@", [formatter stringFromDate:fdate]];
        break;
    }
    
    frame = _viewTime.frame;
    frame.origin.y = y;
    _viewTime.frame = frame;
    
    if([strTime isEqualToString:@"00:00:00"])
        strTime = MYLocalizedString(@"program.time0", nil);
    else {
        NSArray *arr = [strTime componentsSeparatedByString:@":"];
        if(arr.count == 3){
            strTime = [NSString stringWithFormat:@"%@:%@", [arr objectAtIndex:0], [arr objectAtIndex:1]];
        }
    }
    _lblTimeNum.text = [NSString stringWithFormat:@"%@     %@", strTime, strDate];
    y += _viewTime.frame.size.height + 5;
    
    r = [strAddress boundingRectWithSize:CGSizeMake(_lblLocationValue.frame.size.width - 10, 9999)
                                    options:NSStringDrawingUsesLineFragmentOrigin
                                 attributes:@{NSFontAttributeName:_lblLocationValue.font}
                                    context:nil];
    r.size.height = r.size.height;
    CGSize size = r.size;
    float h = size.height;
    if(h <= 21)
        h = 25;
    else
        h = 25 + (h - 21) + h/21;
    frame = _viewLocation.frame;
    frame.origin.y = y;
    frame.size.height = h;
    _viewLocation.frame = frame;
    _lblLocationValue.text = strAddress;
    y += h + 5;
    
    //cập nhật cho view giá vé
    if(objSelected.pType != 2){
        frame = _viewPrice.frame;
        frame.origin.y = y;
        _viewPrice.frame = frame;
        if([price intValue] > 0){
            _lblPriceNum.text = [NSString stringWithFormat:@"%@ VND", price];
        } else {
            _lblPriceNum.text = MYLocalizedString(@"ticket.free", nil);
        }
        y += frame.size.height + 5;
        _viewPrice.hidden = NO;
    } else {
        _viewPrice.hidden = YES;
    }
    
    //cập nhật cho webview
    NSString *content = [program_content stringByReplacingOccurrencesOfString:@"\n" withString:@"<br/>"];
    content = [NSString stringWithFormat:@"<html>""<style type=\"text/css\">"
                                        "a:link{color:#000; text-decoration: none}"
                                        "body{background-color:transparent; font-size:15; font-family: Helvetica Neue; color: #000; text-align:justify; margin:0; padding:0}""</style>"
                                        "<body>%@</body></html>", content];
    [_webContent loadHTMLString:content baseURL: nil];
    _webContent.scrollView.scrollEnabled = NO;
    _webContent.scrollView.bounces = NO;
    _webContent.delegate = self;
    
    frame = _webContent.frame;
    frame.origin.y = y;
    _webContent.frame = frame;
    
    [self updateDownOfContent];
}

- (void) updateDownOfContent
{
    CGRect frame = _lblEvent.frame;
    frame.origin.y = _webContent.frame.origin.y + _webContent.frame.size.height + 5;
    _lblEvent.frame = frame;
    
    [self initCalendar];
    frame = _calendar.frame;
    frame.origin.y = _lblEvent.frame.origin.y + _lblEvent.frame.size.height + 10;
    _calendar.frame = frame;
    
    frame = _lblEventNote.frame;
    frame.origin.y = _calendar.frame.origin.y + _calendar.frame.size.height + 2;
    _lblEventNote.frame = frame;
    
    frame = _scrollContent.frame;
    frame.size.height = _lblEventNote.frame.origin.y + _lblEventNote.frame.size.height + 5;
    _scrollContent.frame = frame;
    
    frame = _scrollMain.frame;
    frame.size.height = self.viewMain.frame.size.height - (_viewHeader.frame.size.height + _viewBottom.frame.size.height);
    _scrollMain.frame = frame;
    //update content size to scroll
    _scrollMain.contentSize = CGSizeMake(screenViewWidth, _scrollImages.frame.size.height + _viewButton.frame.size.height + _scrollContent.frame.size.height);
}

- (void) initCalendar
{
    _calendar.delegate = self;
    _calendar.onlyShowCurrentMonth = NO;
    _dateFormatter = [[NSDateFormatter alloc] init];
    [_dateFormatter setDateFormat:@"dd/MM/yyyy"];
    _miniMumDate = [_dateFormatter dateFromString:@"01/01/2002"];

    _disableDate = [[NSMutableArray alloc] init];
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy-MM-dd"];
    for (DetailListObj *p in objSelected.detail_list) {
        NSString *fdate = p.fdate;
        NSString *tdate = p.tdate;
        
        NSDate *fd = [format dateFromString:fdate];
        fdate = [_dateFormatter stringFromDate:fd];
        fd = [_dateFormatter dateFromString:fdate];
        [_disableDate addObject:fd];
        
        NSDate *td = [format dateFromString:tdate];
        if(![fdate isEqualToString:tdate]){
            tdate = [_dateFormatter stringFromDate:td];
            td = [_dateFormatter dateFromString:tdate];
            
            NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
            NSUInteger unitFlags = NSMonthCalendarUnit | NSDayCalendarUnit;
            NSDateComponents *components = [calendar components:unitFlags fromDate:fd toDate:td options:0];
            NSInteger numDays = [components day];
            NSRange days = [calendar rangeOfUnit:NSDayCalendarUnit inUnit:NSMonthCalendarUnit forDate:fd];
            
            components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:fd];
            NSInteger fday = [components day];
            NSInteger fmonth = [components month];
            NSInteger fyear = [components year];
            components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:td];
            NSInteger tday = [components day];
            NSInteger tmonth = [components month];
            for(int i = 0; i < numDays; i++){
                fday += 1;
                if(fmonth == tmonth){
                    if(fday > tday)
                        break;
                } else if(fmonth < tmonth){
                    int daysInGivenMonth = (int)days.length;
                    if(fday > daysInGivenMonth){
                        fmonth = tmonth;
                        fday = 1;
                    }
                }
                
                NSString *strDate = [NSString stringWithFormat:@"%zd/%zd/%zd", fday, fmonth, fyear];
                NSDate *sDate = [_dateFormatter dateFromString:strDate];
                [_disableDate addObject:sDate];
            }
        }
        
        //ngày bắt đầu
        NSDateComponents *fcom = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:fd];
        NSInteger fmonth = [fcom month];
        NSInteger fyear = [fcom year];
        //ngày kết thúc
        NSDateComponents *tcom = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:td];
        NSInteger tmonth = [tcom month];
        NSInteger tyear = [tcom year];
        //ngày hiện tại
        NSDateComponents *ccom = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
        NSInteger cmonth = [ccom month];
        NSInteger cyear = [ccom year];
        
        //mặc đinh calendar focus vào ngày bắt đầu
        NSString *strDate = [NSString stringWithFormat:@"10/%zd/%zd", fmonth, fyear];
        if(fyear == tyear && fyear == cyear){//nếu cùng 1 năm
            if(fmonth < cmonth && cmonth <= tmonth){
                //và tháng hiện tại đã qua ngày bắt đầu nhưng chưa qua ngày kết thúc thì focus vào ngày hiện tại
                strDate = [NSString stringWithFormat:@"10/%zd/%zd", cmonth, cyear];
            }
        }
        NSDate *selectDate = [_dateFormatter dateFromString:strDate];
        [_calendar selectDate:selectDate makeVisible:YES];
    }
}

#pragma mark - Webview Delegate
-(void)webViewDidFinishLoad:(UIWebView *)webView {
    CGRect webViewFrame = webView.frame;
    webViewFrame.size.height = 1;
    webView.frame = webViewFrame;
    CGSize fittingSize = [webView sizeThatFits:CGSizeZero];
    webViewFrame.size = fittingSize;
    webView.frame = webViewFrame;
    
    [self updateDownOfContent];
}

#pragma mark - CKCalendar Delegate
#pragma mark -
#pragma mark - CKCalendarDelegate

- (void)calendar:(CKCalendarView *)calendar configureDateItem:(CKDateItem *)dateItem forDate:(NSDate *)date {
    // TODO: play with the coloring if we want to...
    if ([self dateIsDisabled:date]) {
        dateItem.backgroundColor = UIColorFromRGB(COLOR_RED);
        dateItem.textColor = [UIColor whiteColor];
    }
}

- (BOOL)calendar:(CKCalendarView *)calendar willSelectDate:(NSDate *)date {
    BOOL selected = ![self dateIsDisabled:date];
    if(!selected){
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setDateFormat:@"yyyy-MM-dd"];
        for(DetailListObj *detailObj in objSelected.detail_list){
            
            NSTimeInterval ids = [date timeIntervalSince1970];
            NSTimeInterval idf = [[format dateFromString:detailObj.fdate] timeIntervalSince1970];
            NSTimeInterval idt = [[format dateFromString:detailObj.tdate] timeIntervalSince1970];
            
            if(ids >= idf && ids <= idt){
                detailObjSelect = detailObj;
                detailObjSelect.day = [format stringFromDate:date];
                NSString *strTime = detailObj.time;
                if([strTime isEqualToString:@"00:00:00"])
                    strTime = MYLocalizedString(@"program.time0", nil);
                
                NSString *title = MYLocalizedString(@"reminder.title", nil);
                NSString *msg = [NSString stringWithFormat:MYLocalizedString(@"program.time.alert", nil), objSelected.pName, strTime, detailObj.diadiem_name];
                
                [UIAlertView showMyConfirmationDialogWithTitle:title message:msg andButton1:MYLocalizedString(@"close", nil) andButton2:MYLocalizedString(@"setreminder1", nil) andButton3:MYLocalizedString(@"setreminder2", nil) andButton4:MYLocalizedString(@"setreminder3", nil) handler:handler];
                break;
            }
        }
    }
    return selected;
}

- (void) callAddReminder:(int) before
{
    NSString *strTime = detailObjSelect.time;
    if([strTime isEqualToString:@"00:00:00"])
        strTime = MYLocalizedString(@"program.time0", nil);
    NSString *msg = [NSString stringWithFormat:MYLocalizedString(@"program.time.alert", nil), objSelected.pName, strTime, detailObjSelect.diadiem_name];
    
    NSString *_id = [NSString stringWithFormat:@"%@_%i_%@_%@_%@", objSelected.pID, detailObjSelect.id_doan, detailObjSelect.id_diadiem, detailObjSelect.day, detailObjSelect.time];
    NSString *time = [NSString stringWithFormat:@"%@ %@", detailObjSelect.day, detailObjSelect.time];
    //TEST by Time
//    NSDateFormatter *format = [[NSDateFormatter alloc] init];
//    [format setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//    time = [format stringFromDate:[NSDate date]];
    
    [appDelegate setReminder:_id andTitle:objSelected.pName andMessage:msg andTime:time andProgramID:objSelected.pID timeBefore:before];
}

- (void)calendar:(CKCalendarView *)calendar didSelectDate:(NSDate *)date {
    //NSLog(@"%@", [self.dateFormatter stringFromDate:date]);
}

- (BOOL)calendar:(CKCalendarView *)calendar willChangeToMonth:(NSDate *)date {
    if ([date laterDate:_miniMumDate] == date) {
        return YES;
    } else {
        return NO;
    }
}

- (void)calendar:(CKCalendarView *)calendar didLayoutInRect:(CGRect)frame {
    //NSLog(@"calendar layout: %@", NSStringFromCGRect(frame));
    CGRect r = _lblEventNote.frame;
    r.origin.y = _calendar.frame.origin.y + frame.size.height + 2;
    _lblEventNote.frame = r;
    
    r = _scrollContent.frame;
    r.size.height = _lblEventNote.frame.origin.y + _lblEventNote.frame.size.height + 5;
    _scrollContent.frame = r;
    
    _scrollMain.contentSize = CGSizeMake(screenViewWidth, _scrollImages.frame.size.height + _viewButton.frame.size.height + _scrollContent.frame.size.height);
}

- (BOOL)dateIsDisabled:(NSDate *)date {
    for (NSDate *disabledDate in _disableDate) {
        if ([disabledDate isEqualToDate:date])
            return YES;
    }
    return NO;
}

//============== Click Button ==============//
- (IBAction) clickBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction) clickShare:(id)sender
{
    NSArray *activityProviders = @[[NSURL URLWithString:BASE_URL]];
    UIActivityViewController *activityViewController =
    [[UIActivityViewController alloc] initWithActivityItems:activityProviders  applicationActivities:nil];
    if (SYSTEM_VERSION_GREATER_THAN(@"7.0")) {
        activityViewController.excludedActivityTypes = @[UIActivityTypeAddToReadingList,UIActivityTypePostToTencentWeibo, UIActivityTypeAirDrop,UIActivityTypePrint,  UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact];
    } else {
        activityViewController.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact];
    }
    
    if (!IS_IPAD) {
        activityViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        [self presentViewController:activityViewController animated:YES completion:nil];
    } else {
        UIButton *btn = (UIButton *)sender;
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:activityViewController];
        [popup presentPopoverFromRect:CGRectMake(btn.frame.origin.x + btn.frame.size.width/2, _viewHeader.frame.size.height, 0, 0) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
}

- (IBAction) clickMore:(UIButton *)sender
{
    if(!menuItems){
        KxMenuItem *item1 = [KxMenuItem menuItem:MYLocalizedString(@"detail.media", nil)
                                           image:[UIImage imageNamed:@"ic_media.png"]
                                          target:self
                                          action:@selector(pushMenuItem:)];
        item1.alignment = NSTextAlignmentLeft;
        item1.foreColor = [UIColor whiteColor];
        
        KxMenuItem *item2 = [KxMenuItem menuItem:MYLocalizedString(@"event.title", nil)
                                           image:[UIImage imageNamed:@"ic_event.png"]
                                          target:self
                                          action:@selector(pushMenuItem:)];
        item2.alignment = NSTextAlignmentLeft;
        item2.foreColor = [UIColor whiteColor];
        
        KxMenuItem *item3 = [KxMenuItem menuItem:MYLocalizedString(@"detail.info", nil)
                                           image:[UIImage imageNamed:@"ic_info.png"]
                                          target:self
                                          action:@selector(pushMenuItem:)];
        item3.alignment = NSTextAlignmentLeft;
        item3.foreColor = [UIColor whiteColor];
        
        KxMenuItem *item4 = [KxMenuItem menuItem:MYLocalizedString(@"location.title", nil)
                                           image:[UIImage imageNamed:@"ic_locations.png"]
                                          target:self
                                          action:@selector(showMapView)];
        item4.alignment = NSTextAlignmentLeft;
        item4.foreColor = [UIColor whiteColor];
        
        KxMenuItem *item5 = [KxMenuItem menuItem:MYLocalizedString(@"ticket.title", nil)
                                           image:[UIImage imageNamed:@"ic_ticket.png"]
                                          target:self
                                          action:@selector(pushMenuItem:)];
        item5.alignment = NSTextAlignmentLeft;
        item5.foreColor = [UIColor whiteColor];
        
        menuItems = [[NSMutableArray alloc] initWithObjects:item1, item2, item3, item4, item5, nil];
    }
    
    [KxMenu setTintColor:UIColorFromRGB(COLOR_RED)];
    [KxMenu showMenuInView:self.view
                  fromRect:sender.frame
                 menuItems:menuItems
                kxDelegate:self];
}

- (IBAction) clickSave:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:MYLocalizedString(@"program.save.alert.title", nil) message:MYLocalizedString(@"program.save.alert.content", nil) delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1){
        for(UIImage *img in objSelected.arrImage){
            UIImageWriteToSavedPhotosAlbum(img,nil,NULL,NULL);
        }
    }
}

- (IBAction) clickLike:(id)sender
{
    [_btnLike setSelected:!_btnLike.isSelected];
    _lblLike.textColor = _btnLike.isSelected?UIColorFromRGB(COLOR_YELLOW):UIColorFromRGB(COLOR_WHITE);
    [appDelegate addLikeProgram:_btnLike.isSelected andProgram:objSelected];
}

- (IBAction) clickLocation:(id)sender
{
    [self showMapView];
}

- (void) showRelateView
{
    if(!_viewRelate.isHidden)
        return;
    _viewRelate.hidden = NO;
    
    [arrDatas removeAllObjects];
    for (ProgramObj *p in appDelegate.arrPrograms) {
        if(arrDatas.count == 10)
            break;
        if([p.pID intValue] > [objSelected.pID intValue]){
            [arrDatas addObject:p];
        }
    }
    [_tableView reloadData];
    
    [UIView animateWithDuration:0.25
                          delay:0.0
                        options: (int) UIViewAnimationCurveEaseOut
                     animations:^{
                         CGRect frame = _viewRelate.frame;
                         frame.origin.y = 0;
                         _viewRelate.frame = frame;
                     }
                     completion:^(BOOL finished){}];
}

- (void) hideRelateView
{
    if(_viewRelate.isHidden)
        return;
    
    [UIView animateWithDuration:0.25
                          delay:0.0
                        options: (int) UIViewAnimationCurveEaseOut
                     animations:^{
                         CGRect frame = _viewRelate.frame;
                         frame.origin.y = self.viewMain.frame.size.height;
                         _viewRelate.frame = frame;
                     }
                     completion:^(BOOL finished){
                         _viewRelate.hidden = YES;
                     }];
}

- (void) showPhotoDetail
{
    isPauseAuto = YES;
    PhotoDetailViewController *photo = [[PhotoDetailViewController alloc] initWithData:objSelected.arrImage andIndex:imageIndex andNibName:@"PhotoDetailViewController"];
    [self.navigationController presentViewController:photo animated:NO completion:nil];
}

#pragma mark - KxMenuDelegate
- (void) showMapView
{
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    for(DetailListObj *o in objSelected.detail_list){
        for(LocationObj *lobj in appDelegate.arrLocations){
            if([lobj.id_diadiem isEqualToString:o.id_diadiem]){
                [arr addObject:lobj];
                break;
            }
        }
    }
    [appDelegate.mapViewController setDatas:1 andDatas:arr];
    [self.navigationController pushViewController:appDelegate.mapViewController animated:YES];
}

- (void) pushMenuItem:(id)sender
{
    KxMenuItem *item = (KxMenuItem *)sender;
    if(item){
        NSString *name = item.title;
        if([name isEqualToString:MYLocalizedString(@"detail.media", nil)]){
            [_scrollMain setContentOffset:CGPointMake(0, 0) animated:YES];
        } else if([name isEqualToString:MYLocalizedString(@"event.title", nil)]){
            [_scrollMain setContentOffset:CGPointMake(0, _calendar.frame.origin.y + 1) animated:YES];
        } else if([name isEqualToString:MYLocalizedString(@"detail.info", nil)]){
            [_scrollMain setContentOffset:CGPointMake(0, _webContent.frame.origin.y + 1) animated:YES];
        } else if([name isEqualToString:MYLocalizedString(@"ticket.title", nil)]){
            [_scrollMain setContentOffset:CGPointMake(0, _viewPrice.frame.origin.y + 1) animated:YES];
        }
    }
}

- (void)menuShow
{
    [UIView animateWithDuration:0.25
            animations:^(void) {
             _btnMorePlus.transform = CGAffineTransformMakeRotation(-(M_PI * 3)/ 4);
    } completion:^(BOOL completed) {}];
}

- (void)menuDismiss
{
    [UIView animateWithDuration:0.25
                     animations:^(void) {
                         _btnMorePlus.transform = CGAffineTransformMakeRotation(0);
                     } completion:^(BOOL completed) {}];
}

#pragma mark -
#pragma mark UIScrollViewDelegate methods
- (void)scrollViewDidScroll:(UIScrollView *)sender {
    // Switch the indicator when more than 50% of the previous/next page is visible
    CGFloat pageWidth = _scrollImages.frame.size.width;
    imageIndex = floor((_scrollImages.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    _pageControl.currentPage = imageIndex;
}

#pragma mark - UItableview
#pragma mark tableview Delegate method
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Indentifier";
    
    //chuong trinh
    ProgramCell *cell = (ProgramCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ProgramCell1" owner:self options:nil];
        for(id currentObject in topLevelObjects) {
            if([currentObject isKindOfClass:[UITableViewCell class]])
            {
                cell = (ProgramCell *)currentObject;
                cell.backgroundColor = [UIColor clearColor];
                break;
            }
        }
    }
    
    ProgramObj *obj = [arrDatas objectAtIndex:indexPath.row];
    cell.lblHueFes.text = [NSString stringWithFormat:@"%@, %@", MYLocalizedString(@"fes.title", nil), MYLocalizedString(@"program.title", nil)];
    if(obj){
        NSString *name = [obj.pName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        CGRect r = cell.lblTitle.frame;
        //update vi tri title
        CGRect textRect = [name boundingRectWithSize:CGSizeMake(r.size.width - 20, 9999) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica Neue" size:16]} context:nil];
        int h = (int)textRect.size.height;
            
        if(h > 21){
            h = h + 2;
            if(h > 80)
                h = 80;
            r.size.height = h;
            cell.lblTitle.frame = r;
        }
            
        cell.lblTitle.text = name;
        cell.imgThumb.image = [obj.arrImage objectAtIndex:0];
    }

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_tableView deselectRowAtIndexPath:indexPath animated:NO];
    [self hideRelateView];
    objSelected = [arrDatas objectAtIndex:indexPath.row];
    [self loadScrollImages];
    [self loadContent];
    [_scrollMain setContentOffset:CGPointMake(0, 0) animated:YES];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return nil;
}

#pragma TableDatasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrDatas.count;
}

#pragma common
- (void) showOLGhostAlertView: (NSString *) title content:(NSString *) strContent time: (int) timeout
{
    OLGhostAlertView *olAlert = [[OLGhostAlertView alloc] initWithTitle:title message:strContent timeout:timeout dismissible:YES];
    [olAlert show];
}

@end
