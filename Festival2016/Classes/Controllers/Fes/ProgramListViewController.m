//
//  ProgramListViewController.m
//  Festival2016
//
//  Created by Phan Phuoc Luong on 11/16/15.
//  Copyright © 2015 Phan Phuoc Luong. All rights reserved.
//

#import "ProgramListViewController.h"
#import "ProgramDetailViewController.h"
#import "ProgramCell.h"
#import "ProgramObj.h"
#import "CalendarEventObj.h"
#import "DetailListObj.h"
#import "LocationObj.h"
#import "MapViewController.h"

@interface ProgramListViewController ()
{
    UIAlertViewHandler handler;
    DetailListObj *detailObjSelect;
    ProgramObj *_pObj;
    NSString *artistID;
}
@end

@implementation ProgramListViewController

- (id)initWithData:(NSString *)nibName andArtID:(NSString *)art_id andTypeList:(NSInteger)type andData:(NSMutableArray *)datas
{
    if(self = [super initWithNibName:nibName bundle:nil]){
        arrDatas = [[NSMutableArray alloc] initWithArray:datas];
        typeList = type;
        artistID = art_id;
        appDelegate = MyAppDelegate();
    }
    
    return self;
}

- (void)initSafeArea {
    _viewMain.translatesAutoresizingMaskIntoConstraints = NO;
    
    if (@available(iOS 11, *)) {
        UILayoutGuide *guide = self.view.safeAreaLayoutGuide;
        [_viewMain.leadingAnchor constraintEqualToAnchor:guide.leadingAnchor].active = YES;
        [_viewMain.trailingAnchor constraintEqualToAnchor:guide.trailingAnchor].active = YES;
        [_viewMain.topAnchor constraintEqualToAnchor:guide.topAnchor].active = YES;
        [_viewMain.bottomAnchor constraintEqualToAnchor:guide.bottomAnchor].active = YES;
    } else {
        UILayoutGuide *margins = self.view.layoutMarginsGuide;
        [_viewMain.leadingAnchor constraintEqualToAnchor:margins.leadingAnchor].active = YES;
        [_viewMain.trailingAnchor constraintEqualToAnchor:margins.trailingAnchor].active = YES;
        [_viewMain.topAnchor constraintEqualToAnchor:self.topLayoutGuide.bottomAnchor].active = YES;
        [_viewMain.bottomAnchor constraintEqualToAnchor:self.bottomLayoutGuide.topAnchor].active = YES;
    }
    // Refresh myView and/or main view
    [self.view layoutIfNeeded];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.frame = [UIScreen mainScreen].bounds;
    //
    [self initSafeArea];
    //init langague
    [self refreshLanguage];
    if(typeList == 1){
        treeData = [[NSMutableArray alloc] init];
        _tableView.hidden = YES;
        _viewTree.hidden = NO;
        [self loadDataForTree];
    } else {
        _tableView.hidden = NO;
        _viewTree.hidden = YES;
        //phân loại in/off
        arrIN = [[NSMutableArray alloc] init];
        arrOFF = [[NSMutableArray alloc] init];
        for(DetailListObj *o in arrDatas){
            if(o.type_inoff == 2)//IN
                [arrIN addObject:o];
            else if(o.type_inoff == 1)//OFF
                [arrOFF addObject:o];
        }
        [_tableView reloadData];
    }
    
    handler = ^(UIAlertView *alertView, NSInteger buttonIndex) {
        if(buttonIndex == 1){
            [self callAddReminder:15];
        } else if(buttonIndex == 2){
            [self callAddReminder:30];
        } else if(buttonIndex == 3){
            [self callAddReminder:60];
        }
    };
}

- (void)loadDataForTree
{
    RADataObject *obj = nil;
    [treeData removeAllObjects];
    for(ProgramObj *pObj in arrDatas){
        NSMutableArray *subArr = [self loadSubObj:pObj];
        if(subArr.count > 0){
            RADataObject *sub = [subArr objectAtIndex:0];
            obj = [[RADataObject alloc] initWithName:pObj.pID andName:pObj.pName andIcon:[pObj.arrImage objectAtIndex:0] andDate:sub.strDate andTime:sub.strTime andAddress:sub.strAddress andLike:[appDelegate checkProgramLike:pObj.pID] children:subArr];
            obj.obj = pObj;
            [treeData addObject:obj];
        }
    }
    
    if(!_treeView){
        RATreeView *tree = [[RATreeView alloc] initWithFrame:_viewTree.bounds];
        [tree setBackgroundColor:[UIColor clearColor]];
        [tree setSeparatorStyle:RATreeViewCellSeparatorStyleNone];
        [tree setAllowsSelection:YES];
        tree.delegate = self;
        tree.dataSource = self;
        [_viewTree addSubview:tree];
        [tree reloadData];
        
        _treeView = tree;
    }
    [_treeView reloadData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if(typeList == 1 && _treeView && treeData.count > 0){
        RADataObject *obj = [treeData objectAtIndex:0];
        [_treeView expandRowForItem:obj withRowAnimation:RATreeViewRowAnimationFade];
        RATableViewCell *cell = (RATableViewCell *)[_treeView cellForItem:obj];
        NSInteger level = [_treeView levelForCellForItem:obj];
        if(level == 0){
            cell.isSelected = [_treeView isCellExpanded:cell];
            obj.isExpand = cell.isSelected;
            [cell refresh];
        }
    }
}

//refresh language
- (void)refreshLanguage
{
    _lblTitle.text = MYLocalizedString(@"program.list", nil);
}

- (IBAction)clickBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UItableview
#pragma mark tableview Delegate method
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Indentifier";
    NSInteger section = indexPath.section;
    //chuong trinh
    ProgramCell *cell = (ProgramCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:(typeList == 1)?@"ProgramCell1":@"ProgramCell2" owner:self options:nil];
        for(id currentObject in topLevelObjects) {
            if([currentObject isKindOfClass:[UITableViewCell class]])
            {
                cell = (ProgramCell *)currentObject;
                if(section != 0)
                    cell.backgroundColor = [UIColor clearColor];
                else
                    cell.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_cell.png"]];
                break;
            }
        }
    }
    if(typeList == 2){
        DetailListObj *obj = nil;
        if(section == 0)
            obj = [arrIN objectAtIndex:indexPath.row];
        else
            obj = [arrOFF objectAtIndex:indexPath.row];
        
        if(obj){
            cell.lblTitle.text = obj.chuongtrinh_name;
            cell.lblBegin.text = MYLocalizedString(@"program.start", nil);
            
            NSString *time = obj.time;
            if([time isEqualToString:@"00:00:00"])
                time = MYLocalizedString(@"program.time0", nil);
            cell.lblStartup.text = [NSString stringWithFormat:@"%@   %@", time, obj.day];
            
            //add cho muc dia chi
            cell.lblDiachiTitle.text = MYLocalizedString(@"location.title", nil);
            cell.lblDiaChiValue.text = obj.diadiem_name;
        }
    }
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_tableView deselectRowAtIndexPath:indexPath animated:NO];
    NSInteger proID = -1;
    DetailListObj *obj = nil;
    if(indexPath.section == 0)
        obj = [arrIN objectAtIndex:indexPath.row];
    else
        obj = [arrOFF objectAtIndex:indexPath.row];
    
    if(!obj)
        return;
    
    proID = [obj.id_chuongtrinh integerValue];
    //find program on delegate
    ProgramObj *pObj = nil;
    for(pObj in appDelegate.arrPrograms){
        if([pObj.pID integerValue] == proID)
            break;
    }
    if(pObj){
        ProgramDetailViewController *detail = [[ProgramDetailViewController alloc] initWithObjectSelected:pObj];
        [self.navigationController pushViewController:detail animated:YES];
    }
}

#pragma TableDatasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(typeList == 2){
        NSInteger hCell = 100;
        
        DetailListObj *obj = nil;
        if(indexPath.section == 0)
            obj = [arrIN objectAtIndex:indexPath.row];
        else
            obj = [arrOFF objectAtIndex:indexPath.row];
        
        if(!obj || obj.chuongtrinh_name.length == 0)
            return hCell;
        
        CGRect textRect = [obj.chuongtrinh_name boundingRectWithSize:CGSizeMake(_tableView.frame.size.width - 22, 9999) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica Neue" size:16]} context:nil];
        
        CGSize size = textRect.size;
        float h = size.height;
        
        if(h <= 21)
            return hCell;
        else
            return hCell + (h - 21) + h/21;
    } else
        return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return arrIN.count;
            break;
        case 1:
            return arrOFF.count;
            break;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    int h = 44;
    switch (section) {
        case 0:
            return arrIN.count > 0 ? h : 0;
        case 1:
            return arrOFF.count > 0 ? h : 0;
    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenViewWidth, 44)];
    view.backgroundColor = [UIColor colorWithRed:194/255.0 green:34/255.0 blue:35/255.0 alpha:1.f];
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, screenViewWidth - 20, view.frame.size.height)];
    lbl.backgroundColor = [UIColor clearColor];
    lbl.textColor = UIColorFromRGB(COLOR_WHITE);
    lbl.font = [UIFont boldSystemFontOfSize:15];
    switch (section) {
        case 0:
            lbl.text = [MYLocalizedString(@"program.in", nil) uppercaseString];
            break;
        case 1:
            lbl.text = [MYLocalizedString(@"program.off", nil) uppercaseString];
            break;
    }
    [view addSubview:lbl];
    
    return view;
}

////////////// TREE VIEW ///////////////
#pragma mark TreeView Delegate methods

- (CGFloat)treeView:(RATreeView *)treeView heightForRowForItem:(id)item
{
    NSInteger level = [_treeView levelForCellForItem:item];
    return level == 0 ? 130 : 80;
}

- (BOOL)treeView:(RATreeView *)treeView canEditRowForItem:(id)item
{
    return NO;
}

- (void)treeView:(RATreeView *)treeView didExpandRowForItem:(id)item
{
    RATableViewCell *cell = (RATableViewCell *)[treeView cellForItem:item];
    NSInteger level = [_treeView levelForCellForItem:item];
    if(level == 0){
        RADataObject *obj = item;
        cell.isSelected = [_treeView isCellExpanded:cell];
        obj.isExpand = cell.isSelected;
        [cell refresh];
    }
}

- (void)treeView:(RATreeView *)treeView didCollapseRowForItem:(id)item
{
    RATableViewCell *cell = (RATableViewCell *)[treeView cellForItem:item];
    NSInteger level = [_treeView levelForCellForItem:item];
    if(level == 0){
        RADataObject *obj = item;
        cell.isSelected = [_treeView isCellExpanded:cell];
        obj.isExpand = cell.isSelected;
        [cell refresh];
    }
}

- (void)treeView:(RATreeView *)treeView didSelectRowForItem:(id)item
{
    NSInteger level = [_treeView levelForCellForItem:item];
    [_treeView deselectRowForItem:item animated:NO];
    if(level > 0)
    {
    }
    else if(level == 0)
    {
    }
}

#pragma mark TreeView Data Source

- (UITableViewCell *)treeView:(RATreeView *)treeView cellForItem:(id)item
{
    RADataObject *obj = item;
    
    NSInteger level = [_treeView levelForCellForItem:item];
    static NSString *CellIdentifier = @"Identifier";
    RATableViewCell *cell = [_treeView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (nil == cell) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:level == 0 ? @"RATableViewCell" : @"RATableViewCell1" owner:nil options:nil];
        for(id currentObject in topLevelObjects){
            cell = (RATableViewCell *)currentObject;
            break;
        }
    }
    
    if(level == 0){
        cell.isSelected = obj.isExpand;
        [cell setupProgram:obj.pName icon:obj.imgIcon date:obj.strDate time:obj.strTime address:obj.strAddress isLiked:obj.isLike isExpand:obj.isCanTranfer andProgram:obj.obj];
        [cell bringSubviewToFront:cell.viewNext];
        [cell bringSubviewToFront:cell.btnLike];
    } else {
        [cell setupSub:obj.strDate time:obj.strTime address:obj.strAddress index:obj.subIndex isLast:obj.isLast diadiemID:obj.diadiem_id];
        cell.detail_obj = obj.detail_obj;
        cell.pObj = obj.obj;
        [cell bringSubviewToFront:cell.viewAddress];
        [cell bringSubviewToFront:cell.viewTime];
    }
    cell.delegate = self;

    return cell;
}

- (NSInteger)treeView:(RATreeView *)treeView numberOfChildrenOfItem:(id)item
{
    if (item == nil) {
        return [treeData count];
    }
    
    RADataObject *data = item;
    return [data.children count];
}

- (id)treeView:(RATreeView *)treeView child:(NSInteger)index ofItem:(id)item
{
    RADataObject *data = item;
    if (item == nil) {
        return [treeData objectAtIndex:index];
    }
    
    return data.children[index];
}

- (NSMutableArray *) loadSubObj:(ProgramObj *)pObj
{
    NSDateFormatter *_dateFormatter = [[NSDateFormatter alloc] init];
    [_dateFormatter setDateFormat:@"dd/MM/yyyy"];
    
    RADataObject *subObj = nil;
    NSMutableArray *listSub = [[NSMutableArray alloc] init];
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy-MM-dd"];
    for (DetailListObj *p in pObj.detail_list) {
        if(p.id_doan != [artistID intValue])
            continue;
        NSString *time = p.time;
        NSString *address = p.diadiem_name;
        NSString *fdate = p.fdate;
        NSString *tdate = p.tdate;
        
        NSDate *fd = [format dateFromString:fdate];
        fdate = [_dateFormatter stringFromDate:fd];
        fd = [_dateFormatter dateFromString:fdate];
        
        subObj = [[RADataObject alloc] initWithName:fdate andTime:time andAddress:address andDiaDiemID:p.id_diadiem andIndex:(int)listSub.count];
        subObj.detail_obj = p;
        subObj.obj = pObj;
        [listSub addObject:subObj];
        
        NSDate *td = [format dateFromString:tdate];
        if(![fdate isEqualToString:tdate]){
            tdate = [_dateFormatter stringFromDate:td];
            td = [_dateFormatter dateFromString:tdate];
            
            NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
            NSUInteger unitFlags = NSMonthCalendarUnit | NSDayCalendarUnit;
            NSDateComponents *components = [calendar components:unitFlags fromDate:fd toDate:td options:0];
            NSInteger numDays = [components day];
            NSRange days = [calendar rangeOfUnit:NSDayCalendarUnit inUnit:NSMonthCalendarUnit forDate:fd];
            
            components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:fd];
            NSInteger fday = [components day];
            NSInteger fmonth = [components month];
            NSInteger fyear = [components year];
            components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:td];
            NSInteger tday = [components day];
            NSInteger tmonth = [components month];
            for(int i = 0; i < numDays; i++){
                fday += 1;
                if(fmonth == tmonth){
                    if(fday > tday)
                        break;
                } else if(fmonth < tmonth){
                    int daysInGivenMonth = (int)days.length;
                    if(fday > daysInGivenMonth){
                        fmonth = tmonth;
                        fday = 1;
                    }
                }
                
                NSString *strDate = [NSString stringWithFormat:@"%@%zd/%@%zd/%zd", fday > 10?@"":@"0", fday, fmonth>10?@"":@"0",fmonth, fyear];
                
                subObj = [[RADataObject alloc] initWithName:strDate andTime:time andAddress:address andDiaDiemID:p.id_diadiem andIndex:(int)listSub.count];
                subObj.detail_obj = p;
                subObj.obj = pObj;
                [listSub addObject:subObj];
            }
        }
    }
    if(listSub.count > 0){
        subObj = [listSub objectAtIndex:listSub.count - 1];
        subObj.isLast = YES;
    }
    
    return listSub;
}

#pragma RATableCellDelegate
- (void)callDetail:(ProgramObj *)obj
{
    if(obj){
        ProgramDetailViewController *detail = [[ProgramDetailViewController alloc] initWithObjectSelected:obj];
        [self.navigationController pushViewController:detail animated:YES];
    }
}

- (void)callLocation:(NSString *)id_diadiem object:(ProgramObj *)obj
{
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    for(LocationObj *lobj in appDelegate.arrLocations){
        if([lobj.id_diadiem isEqualToString:id_diadiem]){
            [arr addObject:lobj];
            break;
        }
    }
    [appDelegate.mapViewController setDatas:1 andDatas:arr];
    [self.navigationController pushViewController:appDelegate.mapViewController animated:YES];
}

- (void)callNotification:(NSString *)strTime date:(NSString *)strDate address:(NSString *)address object:(ProgramObj *)obj detail_obj:(DetailListObj *)detail_obj
{
    _pObj = obj;
    detailObjSelect = detail_obj;
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"dd/MM/yyyy"];
    NSDate *date = [format dateFromString:strDate];
    [format setDateFormat:@"yyyy-MM-dd"];
    detailObjSelect.day = [format stringFromDate:date];

    if([strTime isEqualToString:@"00:00:00"])
        strTime = MYLocalizedString(@"program.time0", nil);
    NSString *msg = [NSString stringWithFormat:MYLocalizedString(@"program.time.alert", nil), obj.pName, strTime, address];
    
    [UIAlertView showMyConfirmationDialogWithTitle:MYLocalizedString(@"reminder.title", nil) message:msg andButton1:MYLocalizedString(@"close", nil) andButton2:MYLocalizedString(@"setreminder1", nil) andButton3:MYLocalizedString(@"setreminder2", nil) andButton4:MYLocalizedString(@"setreminder3", nil) handler:handler];
}

- (void) callAddReminder:(int) before
{
    NSString *strTime = detailObjSelect.time;
    if([strTime isEqualToString:@"00:00:00"])
        strTime = MYLocalizedString(@"program.time0", nil);
    NSString *msg = [NSString stringWithFormat:MYLocalizedString(@"program.time.alert", nil), _pObj.pName, strTime, detailObjSelect.diadiem_name];
    
    NSString *_id = [NSString stringWithFormat:@"%@_%i_%@_%@_%@", _pObj.pID, detailObjSelect.id_doan, detailObjSelect.id_diadiem, detailObjSelect.day, detailObjSelect.time];
    NSString *time = [NSString stringWithFormat:@"%@ %@", detailObjSelect.day, detailObjSelect.time];
    
    [appDelegate setReminder:_id andTitle:_pObj.pName andMessage:msg andTime:time andProgramID:_pObj.pID timeBefore:before];
}
@end
