//
//  ArtDetailViewController.m
//  Festival2016
//
//  Created by Phan Phuoc Luong on 11/16/15.
//  Copyright © 2015 Phan Phuoc Luong. All rights reserved.
//

#import "ArtDetailViewController.h"
#import "NSString+HTML.h"
#import "PhotoDetailViewController.h"

@interface ArtDetailViewController ()

@end

@implementation ArtDetailViewController

- (id)initWithObjectSelected:(ArtObj *)obj
{
    if(self = [super initWithNibName:@"ArtDetailViewController" bundle:nil])
    {
        objSelected = obj;
        appDelegate = MyAppDelegate();
    }
    
    return self;
}

- (void)initSafeArea {
    _viewMain.translatesAutoresizingMaskIntoConstraints = NO;
    
    if (@available(iOS 11, *)) {
        UILayoutGuide *guide = self.view.safeAreaLayoutGuide;
        [_viewMain.leadingAnchor constraintEqualToAnchor:guide.leadingAnchor].active = YES;
        [_viewMain.trailingAnchor constraintEqualToAnchor:guide.trailingAnchor].active = YES;
        [_viewMain.topAnchor constraintEqualToAnchor:guide.topAnchor].active = YES;
        [_viewMain.bottomAnchor constraintEqualToAnchor:guide.bottomAnchor].active = YES;
    } else {
        UILayoutGuide *margins = self.view.layoutMarginsGuide;
        [_viewMain.leadingAnchor constraintEqualToAnchor:margins.leadingAnchor].active = YES;
        [_viewMain.trailingAnchor constraintEqualToAnchor:margins.trailingAnchor].active = YES;
        [_viewMain.topAnchor constraintEqualToAnchor:self.topLayoutGuide.bottomAnchor].active = YES;
        [_viewMain.bottomAnchor constraintEqualToAnchor:self.bottomLayoutGuide.topAnchor].active = YES;
    }
    // Refresh myView and/or main view
    [self.view layoutIfNeeded];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.frame = [UIScreen mainScreen].bounds;
    //
    [self initSafeArea];
    //
    imageIndex = 0;
    _pageControl.currentPage = 0;
    _pageControl.indicatorMargin = 3.0f;
    _pageControl.pageIndicatorTintColor = [[UIColor whiteColor] colorWithAlphaComponent:0.9f];
    _pageControl.currentPageIndicatorTintColor = [UIColorFromRGB(COLOR_RED) colorWithAlphaComponent:1.f];
    _pageControl.currentPageIndicatorImage = [UIImage imageNamed:@"ic_slide_focus.png"];
    _pageControl.pageIndicatorMaskImage = [UIImage imageNamed:@"ic_slide.png"];
    
    UITapGestureRecognizer *tabScrollImage = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                     action:@selector(showPhotoDetail)];
    [_scrollImages addGestureRecognizer:tabScrollImage];
    //init langague
    [self refreshLanguage];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.view.frame = [UIScreen mainScreen].bounds;
    if(IS_IPAD){
        CGRect frame = _viewImages.frame;
        frame.size.height = 320;
        _viewImages.frame= frame;
        
        frame = _viewButton.frame;
        frame.origin.y = _viewImages.frame.origin.y + _viewImages.frame.size.height;
        _viewButton.frame = frame;
        
        frame = _viewContent.frame;
        frame.origin.y = _viewButton.frame.origin.y + _viewButton.frame.size.height;
        frame.size.height = _scrollMain.frame.size.height - frame.origin.y;
        _viewContent.frame = frame;
    }
    isPauseAuto = NO;
    [self loadScrollImages];
    [self loadContent];
}

//refresh language
- (void)refreshLanguage
{
    _lblLike.text = MYLocalizedString(@"detail.like", nil);
    _lblSave.text = MYLocalizedString(@"detail.save", nil);
    _lblHeaderTitle.text = MYLocalizedString(@"art.title", nil);
}

- (void) loadScrollImages
{
    CGRect r;
    float h = _scrollImages.frame.size.height;
    int i = 0;
    for(UIImage *img in objSelected.arrImage){
        r = CGRectMake(i*screenViewWidth, 0, screenViewWidth, h);
        UIImageView *imageview = [[UIImageView alloc] initWithFrame:r];
        imageview.contentMode = UIViewContentModeScaleAspectFill;
        imageview.image = img;
        [_scrollImages addSubview:imageview];
        i++;
    }
    
    [_pageControl setNumberOfPages:objSelected.arrImage.count];
    _scrollImages.contentSize = CGSizeMake(objSelected.arrImage.count * screenViewWidth, _scrollImages.frame.size.height);
    _pageControl.currentPage = imageIndex;
    [_scrollImages setContentOffset:CGPointMake(imageIndex*screenViewWidth, 0) animated:NO];
    
    if(objSelected.arrImage.count > 1){
        if(!timer)
            timer = [NSTimer scheduledTimerWithTimeInterval:15.0
                                         target:self
                                       selector:@selector(autoMovePage)
                                       userInfo:nil
                                        repeats:YES];
    }
}

- (void) autoMovePage
{
    if(isPauseAuto)
        return;
    imageIndex++;
    if(imageIndex > objSelected.arrImage.count - 1)
        imageIndex = 0;
    
    [_scrollImages setContentOffset:CGPointMake(imageIndex*_scrollImages.frame.size.width, 0) animated:imageIndex == 0 ? NO: YES];
}

- (void) loadContent
{
    NSString *art_name = objSelected.aName;
    NSString *art_content = [objSelected.aContent stringByDecodingHTMLEntities];
    //set button like
    [_btnLike setSelected:[appDelegate checkArtistLike:objSelected.aID]];
    _lblLike.textColor = _btnLike.isSelected?UIColorFromRGB(COLOR_YELLOW):UIColorFromRGB(COLOR_WHITE);
    
    CGRect r;
    r = [art_name boundingRectWithSize:CGSizeMake(screenViewWidth - 20, 9999)
                                    options:NSStringDrawingUsesLineFragmentOrigin
                                 attributes:@{NSFontAttributeName:_lblTitle.font}
                                    context:nil];
    CGRect frame = _lblTitle.frame;
    frame.size.height = r.size.height;
    _lblTitle.frame = frame;
    _lblTitle.text = art_name;
    
    NSString *content = art_content;
    content = [NSString stringWithFormat:@"<html>""<style type=\"text/css\">"
               "a:link{color:#000; text-decoration: none}"
               "body{background-color:transparent; font-size:15; font-family: Helvetica Neue; color: #000; text-align:justify; margin:0; padding:0}""</style>"
               "<body>%@</body></html>", content];
    [_webContent loadHTMLString:content baseURL: nil];
    _webContent.scrollView.scrollEnabled = NO;
    _webContent.scrollView.bounces = NO;
    _webContent.delegate = self;
    
    frame = _webContent.frame;
    frame.origin.y = _lblTitle.frame.origin.y + _lblTitle.frame.size.height + 5;
    _webContent.frame = frame;

    [self updateDownOfContent];
}

- (void) updateDownOfContent
{
    
    CGRect frame = _viewContent.frame;
    frame.size.height = _webContent.frame.origin.y + _webContent.frame.size.height + 5;
    _viewContent.frame = frame;
    
    frame = _scrollMain.frame;
    frame.size.height = appDelegate.screenHeight - _viewHeader.frame.size.height;
    _scrollMain.frame = frame;
    //update content size to scroll
    _scrollMain.contentSize = CGSizeMake(screenViewWidth, _viewContent.frame.origin.y + _viewContent.frame.size.height + 20);
}

#pragma mark - Webview Delegate
-(void)webViewDidFinishLoad:(UIWebView *)webView {
    CGSize mWebViewTextSize = [webView sizeThatFits:CGSizeMake(1.0f, 1.0f)]; // Pass about any size
    CGRect mWebViewFrame = webView.frame;
    mWebViewFrame.size.height = mWebViewTextSize.height;
    webView.frame = mWebViewFrame;
    
    [self updateDownOfContent];
}

//============== Click Button ==============//
- (IBAction) clickBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction) clickShare:(id)sender
{
    NSArray *activityProviders = @[[NSURL URLWithString:BASE_URL]];
    UIActivityViewController *activityViewController =
    [[UIActivityViewController alloc] initWithActivityItems:activityProviders  applicationActivities:nil];
    if (SYSTEM_VERSION_GREATER_THAN(@"7.0")) {
        activityViewController.excludedActivityTypes = @[UIActivityTypeAddToReadingList,UIActivityTypePostToTencentWeibo, UIActivityTypeAirDrop,UIActivityTypePrint,  UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact];
    } else {
        activityViewController.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact];
    }
    
    if (!IS_IPAD) {
        activityViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        [self presentViewController:activityViewController animated:YES completion:nil];
    } else {
        UIButton *btn = (UIButton *)sender;
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:activityViewController];
        [popup presentPopoverFromRect:CGRectMake(btn.frame.origin.x + btn.frame.size.width/2, _viewHeader.frame.size.height, 0, 0) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
}

- (IBAction) clickSave:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:MYLocalizedString(@"program.save.alert.title", nil) message:MYLocalizedString(@"program.save.alert.content", nil) delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1){
        for(UIImage *img in objSelected.arrImage){
            UIImageWriteToSavedPhotosAlbum(img,nil,NULL,NULL);
        }
    }
}

- (IBAction) clickLike:(id)sender
{
    [_btnLike setSelected:!_btnLike.isSelected];
    _lblLike.textColor = _btnLike.isSelected?UIColorFromRGB(COLOR_YELLOW):UIColorFromRGB(COLOR_WHITE);
    [appDelegate addLikeArtist:_btnLike.isSelected andArtObj:objSelected];
}

- (void) showPhotoDetail
{
    isPauseAuto = YES;
    PhotoDetailViewController *photo = [[PhotoDetailViewController alloc] initWithData:objSelected.arrImage andIndex:imageIndex andNibName:@"PhotoDetailViewController"];
    [self.navigationController presentViewController:photo animated:NO completion:nil];
}

#pragma mark -
#pragma mark UIScrollViewDelegate methods
- (void)scrollViewDidScroll:(UIScrollView *)sender {
    // Switch the indicator when more than 50% of the previous/next page is visible
    CGFloat pageWidth = _scrollImages.frame.size.width;
    imageIndex = floor((_scrollImages.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    _pageControl.currentPage = imageIndex;
}

#pragma common
- (void) showOLGhostAlertView: (NSString *) title content:(NSString *) strContent time: (int) timeout
{
    OLGhostAlertView *olAlert = [[OLGhostAlertView alloc] initWithTitle:title message:strContent timeout:timeout dismissible:YES];
    [olAlert show];
}

@end
