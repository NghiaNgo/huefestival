//
//  ArtDetailViewController.h
//  Festival2016
//
//  Created by Phan Phuoc Luong on 11/16/15.
//  Copyright © 2015 Phan Phuoc Luong. All rights reserved.
//

#import "SMPageControl.h"
#import "ArtObj.h"

@interface ArtDetailViewController : UIViewController<UIScrollViewDelegate, UIWebViewDelegate, UIAlertViewDelegate>
{
    ArtObj *objSelected;
    int imageIndex;
    BOOL isPauseAuto;
    NSTimer *timer;
    AppDelegate *appDelegate;
}
- (id) initWithObjectSelected:(ArtObj *)obj;
@property (nonatomic, weak) IBOutlet UIView *viewMain;
//
@property (nonatomic, weak) IBOutlet UILabel *lblHeaderTitle;
@property (nonatomic, weak) IBOutlet UIScrollView *scrollMain;
@property (nonatomic, weak) IBOutlet UIScrollView *scrollImages;
@property (nonatomic, weak) IBOutlet SMPageControl *pageControl;
@property (nonatomic, weak) IBOutlet UIView *viewContent;
@property (nonatomic, weak) IBOutlet UIView *viewButton;
@property (nonatomic, weak) IBOutlet UIView *viewHeader;
@property (nonatomic, weak) IBOutlet UIView *viewImages;

@property (nonatomic, weak) IBOutlet UIButton *btnLike;
@property (nonatomic, weak) IBOutlet UILabel *lblLike;
@property (nonatomic, weak) IBOutlet UILabel *lblSave;

//content
@property (nonatomic, weak) IBOutlet UILabel *lblTitle;
@property (nonatomic, weak) IBOutlet UIWebView *webContent;

@end
