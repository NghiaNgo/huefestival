//
//  ProgramListViewController.h
//  Festival2016
//
//  Created by Phan Phuoc Luong on 11/16/15.
//  Copyright © 2015 Phan Phuoc Luong. All rights reserved.
//
// Danh sách các chương trình biểu diễn do 1 đoàn nghệ sỹ nào đó thực hiện
#import "RATreeView.h"
#import "RATableViewCell.h"
#import "RADataObject.h"

@interface ProgramListViewController : UIViewController<RATreeViewDataSource, RATreeViewDelegate, UITableViewDataSource, UITableViewDelegate, RATableDelegate>
{
    AppDelegate *appDelegate;
    NSMutableArray *arrDatas;
    NSMutableArray *arrIN;
    NSMutableArray *arrOFF;
    //for tree
    NSMutableArray *treeData;
    
    NSInteger typeList;//1 theo doan nghe thuat, 2 theo lich dien
}

- (id) initWithData:(NSString *)nibName andArtID:(NSString *)art_id andTypeList:(NSInteger) type andData:(NSMutableArray *)datas;
@property (nonatomic, weak) IBOutlet UIView *viewMain;
//
@property (nonatomic, weak) IBOutlet UIView *viewTree;
@property (weak, nonatomic) RATreeView *treeView;

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UILabel *lblTitle;

@end
