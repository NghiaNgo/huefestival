//
//  FesHomeViewController.m
//  Festival2016
//
//  Created by Phan Phuoc Luong on 11/9/15.
//  Copyright © 2015 Phan Phuoc Luong. All rights reserved.
//

#import "FesHomeViewController.h"
#import "ProgramDetailViewController.h"
#import "ArtDetailViewController.h"
#import "ProgramListViewController.h"
#import "TicketLocationViewController.h"
#import "SearchViewController.h"
#import "ProgramCell.h"
#import "ProgramObj.h"
#import "EventCell.h"
#import "CalendarEventObj.h"
#import "ArtCell.h"
#import "ArtObj.h"
#import "TicketCell.h"

@interface FesHomeViewController ()
{
    int tabIndex;
}
@end

@implementation FesHomeViewController

- (id)initWithTabIndex:(int)index nibName:(NSString *)nibName
{
    if(self == [super initWithNibName:nibName bundle:nil]){
        tabIndex = index;
    }
    
    return self;
}

- (void)initSafeArea {
    _viewMain.translatesAutoresizingMaskIntoConstraints = NO;
    
    if (@available(iOS 11, *)) {
        UILayoutGuide *guide = self.view.safeAreaLayoutGuide;
        [_viewMain.leadingAnchor constraintEqualToAnchor:guide.leadingAnchor].active = YES;
        [_viewMain.trailingAnchor constraintEqualToAnchor:guide.trailingAnchor].active = YES;
        [_viewMain.topAnchor constraintEqualToAnchor:guide.topAnchor].active = YES;
        [_viewMain.bottomAnchor constraintEqualToAnchor:guide.bottomAnchor].active = YES;
    } else {
        UILayoutGuide *margins = self.view.layoutMarginsGuide;
        [_viewMain.leadingAnchor constraintEqualToAnchor:margins.leadingAnchor].active = YES;
        [_viewMain.trailingAnchor constraintEqualToAnchor:margins.trailingAnchor].active = YES;
        [_viewMain.topAnchor constraintEqualToAnchor:self.topLayoutGuide.bottomAnchor].active = YES;
        [_viewMain.bottomAnchor constraintEqualToAnchor:self.bottomLayoutGuide.topAnchor].active = YES;
    }
    // Refresh myView and/or main view
    [self.view layoutIfNeeded];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.frame = [UIScreen mainScreen].bounds;
    //
    [self initSafeArea];
    //
    appDelegate = MyAppDelegate();
    _optionIndices = [NSMutableIndexSet indexSetWithIndex:2];
    
    //color for button
    UIColor *normalColor = UIColorFromRGB(COLOR_RED);
    UIColor *normalColor1 = UIColorFromRGB(TAB_COLOR);
    UIColor *highLighColor = UIColorFromRGB(COLOR_YELLOW);
    
    _viewHeader.backgroundColor = normalColor;
    _scrollTab.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.8f];
    _scrollTab.scrollEnabled = NO;
    [_scrollTab setShowsHorizontalScrollIndicator:NO];
    [_scrollTab setShowsVerticalScrollIndicator:NO];
    
    _btnTieuDiem.colorNormal = normalColor1;
    _btnTieuDiem.colorHighlighted = highLighColor;
    [_btnTieuDiem setImage:[UIImage imageNamed:@"ic_program.png"] forState:UIControlStateSelected | UIControlStateHighlighted];
    _btnCongDong.colorNormal = normalColor1;
    _btnCongDong.colorHighlighted = highLighColor;
    [_btnCongDong setImage:[UIImage imageNamed:@"tab_icon_congdong.png"] forState:UIControlStateSelected | UIControlStateHighlighted];
    _btnNgheSi.colorNormal = normalColor1;
    _btnNgheSi.colorHighlighted = highLighColor;
    [_btnNgheSi setImage:[UIImage imageNamed:@"ic_art.png"] forState:UIControlStateSelected | UIControlStateHighlighted];
    _btnLichDien.colorNormal = normalColor1;
    _btnLichDien.colorHighlighted = highLighColor;
    [_btnLichDien setImage:[UIImage imageNamed:@"ic_event.png"] forState:UIControlStateSelected | UIControlStateHighlighted];
    //add touc on label
    _lblTieuDiem.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapTieuDiem = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickTieuDiem:)];
    [_lblTieuDiem addGestureRecognizer:tapTieuDiem];
    _lblLichDien.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapLichDien = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickLichDien:)];
    [_lblLichDien addGestureRecognizer:tapLichDien];
    _lblCongDong.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapCongDong = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickCongDong:)];
    [_lblCongDong addGestureRecognizer:tapCongDong];
    _lblNgheSi.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapNgheSi = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickNgheSi:)];
    [_lblNgheSi addGestureRecognizer:tapNgheSi];
    
    [self changeButtonSelected:tabIndex];

    arrDatas = [[NSMutableArray alloc] init];
    arrDatasSearch = [[NSMutableArray alloc] init];
    
    //handle swipse
//    UISwipeGestureRecognizer *leftSwipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipesLeft:)];
//    leftSwipeGestureRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
//    [self.view addGestureRecognizer:leftSwipeGestureRecognizer];
//    UISwipeGestureRecognizer *rightSwipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipesRight:)];
//    rightSwipeGestureRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
//    [self.view addGestureRecognizer:rightSwipeGestureRecognizer];
}

- (void)handleSwipesLeft:(UISwipeGestureRecognizer *)sender
{
    tabIndex++;
    if(tabIndex > 3)
        return;
    switch (tabIndex) {
        case 0:{
            [self performSelector:@selector(clickTieuDiem:) withObject:_btnTieuDiem];
            break;
        }
        case 1:{
            [self performSelector:@selector(clickLichDien:) withObject:_btnLichDien];
            break;
        }
        case 2:{
            [self performSelector:@selector(clickCongDong:) withObject:_btnCongDong];
            break;
        }
        case 3:{
            [self performSelector:@selector(clickNgheSi:) withObject:_btnNgheSi];
            break;
        }
    }
}

- (void)handleSwipesRight:(UISwipeGestureRecognizer *)sender
{
    tabIndex--;
    if(tabIndex < 0)
        return;
    switch (tabIndex) {
        case 0:
            [self performSelector:@selector(clickTieuDiem:) withObject:_btnTieuDiem];
            break;
        case 1:{
            [self performSelector:@selector(clickLichDien:) withObject:_btnLichDien];
            break;
        }
        case 2:{
            [self performSelector:@selector(clickCongDong:) withObject:_btnCongDong];
            break;
        }
        case 3:{
            [self performSelector:@selector(clickNgheSi:) withObject:_btnNgheSi];
            break;
        }
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //init langague
    [self refreshLanguage];
    [_optionIndices removeAllIndexes];
    [_optionIndices addIndex:2];
    [self initMenuLeft];
    
    switch (tabIndex) {
        case 0:{
            [self performSelector:@selector(clickTieuDiem:) withObject:_btnTieuDiem];
            break;
        }
        case 1:{
            [self performSelector:@selector(clickLichDien:) withObject:_btnLichDien];
            break;
        }
        case 2:{
            [self performSelector:@selector(clickCongDong:) withObject:_btnCongDong];
            break;
        }
        case 3:{
            [self performSelector:@selector(clickNgheSi:) withObject:_btnNgheSi];
            break;
        }
        default:
            break;
    }
    
    //cap nhạt vi trí các button tren tab
    float w = screenViewWidth/4;
    CGRect r = _btnTieuDiem.frame;
    r.origin.x = 0;
    r.size.width = w;
    _btnTieuDiem.frame = r;
    r = _lblTieuDiem.frame;
    r.origin.x = 0;
    r.size.width = w;
    _lblTieuDiem.frame = r;
    
    r = _btnLichDien.frame;
    r.origin.x = _btnTieuDiem.frame.origin.x + _btnTieuDiem.frame.size.width;
    r.size.width = w;
    _btnLichDien.frame = r;
    r = _lblLichDien.frame;
    r.origin.x = _btnLichDien.frame.origin.x;
    r.size.width = w;
    _lblLichDien.frame = r;
    
    r = _btnCongDong.frame;
    r.origin.x = _lblLichDien.frame.origin.x + _lblLichDien.frame.size.width;
    r.size.width = w;
    _btnCongDong.frame = r;
    r = _lblCongDong.frame;
    r.origin.x = _btnCongDong.frame.origin.x;
    r.size.width = w;
    _lblCongDong.frame = r;
    
    r = _btnNgheSi.frame;
    r.origin.x = _btnCongDong.frame.origin.x + _btnCongDong.frame.size.width;
    r.size.width = w;
    _btnNgheSi.frame = r;
    r = _lblNgheSi.frame;
    r.origin.x = _btnNgheSi.frame.origin.x;
    r.size.width = w;
    _lblNgheSi.frame = r;
}

//refresh language
- (void)refreshLanguage
{
    _lblTieuDiem.text = [MYLocalizedString(@"tab_tieudiem", nil) uppercaseString];
    _lblCongDong.text = [MYLocalizedString(@"tab_congdong", nil) uppercaseString];
    _lblNgheSi.text = [MYLocalizedString(@"tab_nghesi", nil) uppercaseString];
    _lblLichDien.text = [MYLocalizedString(@"tab_lichdien", nil) uppercaseString];
    [_searchField setTitle:MYLocalizedString(@"search", nil) forState:UIControlStateNormal];
}

- (void)refreshImage:(int)tabIndex andId:(NSString *)_id
{
    [_tableView reloadData];
}

- (void) initMenuLeft
{
    if(!menuLeft){
        NSString *sub = appDelegate.language;
        if(sub.length == 0)
            sub = @"vi";
        NSArray *images = @[[UIImage imageNamed:@"ic_menu_focus.png"],
                            [UIImage imageNamed:[NSString stringWithFormat:@"ic_home_%@.png", sub]],
                            [UIImage imageNamed:[NSString stringWithFormat:@"ic_program_%@.png", sub]],
                            [UIImage imageNamed:[NSString stringWithFormat:@"ic_place_%@.png", sub]],
                            [UIImage imageNamed:[NSString stringWithFormat:@"ic_news_%@.png", sub]],
                            [UIImage imageNamed:[NSString stringWithFormat:@"ic_profile_%@.png", sub]],
                            ];
    
        NSArray *colors = @[
                        [UIColor colorWithRed:215/255.f green:169/255.f blue:27/255.f alpha:0],
                        [UIColor colorWithRed:215/255.f green:169/255.f blue:27/255.f alpha:1],
                        [UIColor colorWithRed:215/255.f green:169/255.f blue:27/255.f alpha:1],
                        [UIColor colorWithRed:215/255.f green:169/255.f blue:27/255.f alpha:1],
                        [UIColor colorWithRed:215/255.f green:169/255.f blue:27/255.f alpha:1],
                        [UIColor colorWithRed:215/255.f green:169/255.f blue:27/255.f alpha:1],
                        ];

        menuLeft = [[RNFrostedSidebar alloc] initWithImages:images selectedIndices:_optionIndices borderColors:colors];
        menuLeft.delegate = self;
    }
}

#pragma mark - Menu Click
- (IBAction) clickMenuLeft:(id)sender
{
    [menuLeft setIsSingleSelect:YES];
    [menuLeft show];
}

- (IBAction) clickSearch:(id)sender
{
    SEARCH_TYPE type = search_program;
    if(tabIndex == 0 || tabIndex == 2)
        type = search_program;
    else if(tabIndex == 3)
        type = search_art;
    SearchViewController *search = [[SearchViewController alloc] initWithDatas:@"SearchViewController" andType:type andDatas:arrDatasSearch];
    [self.navigationController pushViewController:search animated:NO];
}

- (IBAction) clickTieuDiem:(id)sender
{
    _tableView.tag = 0;
    [self changeButtonSelected:0];
    
    [arrDatas removeAllObjects];
    if(appDelegate.arrPrograms.count == 0){
        //load data at local
        NSMutableArray *arrDict = [[CouchbaseEvents sharedInstance] loadAllProgram];
        NSArray *arrSort = [arrDict sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *a, NSDictionary *b) {
            int arr1 = [[a objectForKey:@"arrange"] intValue];
            int arr2 = [[b objectForKey:@"arrange"] intValue];
            return arr1 > arr2;
        }];
        NSString *strJson = @"[";
        ProgramObj *obj = nil;
        int i = 0;
        for(NSDictionary *d in arrSort){
            obj = [[ProgramObj alloc] initWithDict:d];
            obj.loadImageDelegate = (id)appDelegate;
            [appDelegate.arrPrograms addObject:obj];
            
            NSString *id_chuongtrinh = [NSString stringWithFormat:@"{\"id_chuongtrinh\":\"%@\"", obj.pID];
            NSString *md5 = [NSString stringWithFormat:@"\"md5\":\"%@\"},", obj.md5];
            if(i == arrSort.count - 1)
                md5 = [md5 stringByReplacingOccurrencesOfString:@"," withString:@""];
            strJson = [strJson stringByAppendingString:[NSString stringWithFormat:@"%@,%@", id_chuongtrinh, md5]];
            i++;
            //======= load photo offline =====//
            if(obj.arrLinkImage.count == 0){
                UIImage *image = [[CouchbaseEvents sharedInstance] getProgramPhoto:[NSString stringWithFormat:@"program_%@_%i.jpg", obj.pID, 0] andProgramID:obj.pID];
                if(obj.arrImage.count > 0)
                    [obj.arrImage replaceObjectAtIndex:0 withObject:image];
            } else {
                for(NSInteger i = 0; i < obj.arrLinkImage.count; i++){
                    UIImage *image = [[CouchbaseEvents sharedInstance] getProgramPhoto:[NSString stringWithFormat:@"program_%@_%zd.jpg", obj.pID, i] andProgramID:obj.pID];
                    if(i < obj.arrImage.count)
                        [obj.arrImage replaceObjectAtIndex:i withObject:image];
                }
            }
            if(obj.pType != 1)//chi load những chương trình tiêu điểm
                continue;
            [arrDatas addObject:obj];
        }
        strJson = [strJson stringByAppendingString:@"]"];//bổ sung mốc ] vào cuối
        //call load on server
        [self connectToServer:0 andDatas:strJson];
    } else {
        for(ProgramObj *o in appDelegate.arrPrograms){
            if(o.pType == 1)
                [arrDatas addObject:o];
        }
    }
    
    [arrDatasSearch removeAllObjects];
    [arrDatasSearch addObjectsFromArray:arrDatas];
    [_tableView reloadData];
}

- (IBAction) clickLichDien:(id)sender
{
    _tableView.tag = 1;
    [self changeButtonSelected:1];
    
    [arrDatas removeAllObjects];
    if(appDelegate.arrCalendar.count == 0){
        NSMutableArray *arrDict = [[CouchbaseEvents sharedInstance] loadAllCalendar];
        NSString *strJson = @"[";
        CalendarEventObj *obj = nil;
        int i = 0;
        for(NSDictionary *d in arrDict){
            obj = [[CalendarEventObj alloc] initWithDict:d];
            [arrDatas addObject:obj];
            //create string
            NSString *fdate = [NSString stringWithFormat:@"{\"fdate\":\"%@\"", obj.fDate];
            NSString *md5 = [NSString stringWithFormat:@"\"md5\":\"%@\"},", obj.md5];
            if(i == arrDict.count - 1)
                md5 = [md5 stringByReplacingOccurrencesOfString:@"," withString:@""];
            strJson = [strJson stringByAppendingString:[NSString stringWithFormat:@"%@,%@", fdate, md5]];
            i++;
        }
        strJson = [strJson stringByAppendingString:@"]"];//bổ sung mốc ] vào cuối
        //call load on server
        [self connectToServer:1 andDatas:strJson];
    } else {
        [arrDatas addObjectsFromArray:appDelegate.arrCalendar];
    }
    
    [arrDatasSearch removeAllObjects];
    [arrDatasSearch addObjectsFromArray:arrDatas];
    [_tableView reloadData];
}

- (IBAction) clickNgheSi:(id)sender
{
    _tableView.tag = 3;
    [self changeButtonSelected:3];
    
    [arrDatas removeAllObjects];
    if(appDelegate.arrArts.count == 0){
        NSMutableArray *arrDict = [[CouchbaseEvents sharedInstance] loadAllArt];
        NSArray *arrSort = [arrDict sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *a, NSDictionary *b) {
            int arr1 = [[a objectForKey:@"arrange"] intValue];
            int arr2 = [[b objectForKey:@"arrange"] intValue];
            return arr1 > arr2;
        }];
        
        NSString *strJson = @"[";
        ArtObj *obj = nil;
        int i = 0;
        for(NSDictionary *d in arrSort){
            obj = [[ArtObj alloc] initWithDict:d];
            obj.loadImageDelegate = (id)appDelegate;
            [arrDatas addObject:obj];
            //======= load photo offline =====//
            for(NSInteger i = 0; i < obj.arrImageLink.count; i++){
                UIImage *image = [[CouchbaseEvents sharedInstance] getArtPhoto:[NSString stringWithFormat:@"art_%@_%zd.jpg", obj.aID, i] andArtID:obj.aID];
                if(i < obj.arrImage.count)
                    [obj.arrImage replaceObjectAtIndex:i withObject:image];
            }

            //create string
            NSString *id_doan = [NSString stringWithFormat:@"{\"id_doan\":\"%@\"", obj.aID];
            NSString *md5 = [NSString stringWithFormat:@"\"md5\":\"%@\"},", obj.md5];
            if(i == arrDict.count - 1)
                md5 = [md5 stringByReplacingOccurrencesOfString:@"," withString:@""];
            strJson = [strJson stringByAppendingString:[NSString stringWithFormat:@"%@,%@", id_doan, md5]];
            i++;
        }
        strJson = [strJson stringByAppendingString:@"]"];//bổ sung mốc ] vào cuối
        //call load on server
        [self connectToServer:2 andDatas:strJson];
    } else {
        [arrDatas addObjectsFromArray:appDelegate.arrArts];
    }
    
    [arrDatasSearch removeAllObjects];
    [arrDatasSearch addObjectsFromArray:arrDatas];
    [_tableView reloadData];
}

- (IBAction) clickCongDong:(id)sender
{
    _tableView.tag = 2;
    [self changeButtonSelected:2];
    
    [arrDatas removeAllObjects];
    if(appDelegate.arrPrograms.count == 0){
        //load data at local
        NSMutableArray *arrDict = [[CouchbaseEvents sharedInstance] loadAllProgram];
        NSArray *arrSort = [arrDict sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *a, NSDictionary *b) {
            int arr1 = [[a objectForKey:@"arrange"] intValue];
            int arr2 = [[b objectForKey:@"arrange"] intValue];
            return arr1 > arr2;
        }];
        NSString *strJson = @"[";
        ProgramObj *obj = nil;
        int i = 0;
        for(NSDictionary *d in arrSort){
            obj = [[ProgramObj alloc] initWithDict:d];
            obj.loadImageDelegate = (id)appDelegate;
            [appDelegate.arrPrograms addObject:obj];
            
            NSString *id_chuongtrinh = [NSString stringWithFormat:@"{\"id_chuongtrinh\":\"%@\"", obj.pID];
            NSString *md5 = [NSString stringWithFormat:@"\"md5\":\"%@\"},", obj.md5];
            if(i == arrSort.count - 1)
                md5 = [md5 stringByReplacingOccurrencesOfString:@"," withString:@""];
            strJson = [strJson stringByAppendingString:[NSString stringWithFormat:@"%@,%@", id_chuongtrinh, md5]];
            i++;
            
            //======= load photo offline =====//
            if(obj.arrLinkImage.count == 0){
                UIImage *image = [[CouchbaseEvents sharedInstance] getProgramPhoto:[NSString stringWithFormat:@"program_%@_%i.jpg", obj.pID, 0] andProgramID:obj.pID];
                if(obj.arrImage.count > 0)
                    [obj.arrImage replaceObjectAtIndex:0 withObject:image];
            } else {
                for(NSInteger i = 0; i < obj.arrLinkImage.count; i++){
                    UIImage *image = [[CouchbaseEvents sharedInstance] getProgramPhoto:[NSString stringWithFormat:@"program_%@_%zd.jpg", obj.pID, i] andProgramID:obj.pID];
                    if(i < obj.arrImage.count)
                        [obj.arrImage replaceObjectAtIndex:i withObject:image];
                }
            }
            
            if(obj.pType != 3)//chi load những chương trình cong dong
                continue;
            [arrDatas addObject:obj];
        }
        strJson = [strJson stringByAppendingString:@"]"];//bổ sung mốc ] vào cuối
        //call load on server
        [self connectToServer:0 andDatas:strJson];
    } else {
        for(ProgramObj *o in appDelegate.arrPrograms){
            if(o.pType == 3)
                [arrDatas addObject:o];
        }
    }
    
    [arrDatasSearch removeAllObjects];
    [arrDatasSearch addObjectsFromArray:arrDatas];
    [_tableView reloadData];
}

- (void) changeButtonSelected:(int) index
{
    tabIndex = index;
    [_btnTieuDiem setSelected:NO];
    [_btnCongDong setSelected:NO];
    [_btnNgheSi setSelected:NO];
    [_btnLichDien setSelected:NO];
    //set title color
    _lblTieuDiem.textColor = [UIColor blackColor];
    _lblCongDong.textColor = [UIColor blackColor];
    _lblNgheSi.textColor = [UIColor blackColor];
    _lblLichDien.textColor = [UIColor blackColor];
    switch (index) {
        case 0://tieu diem
            _lblTieuDiem.textColor = UIColorFromRGB(TEXT_COLOR_FOCUS);
            [_btnTieuDiem setSelected:YES];
            if(_scrollTab.contentOffset.x > 0){
                float x = 0;
                [_scrollTab setContentOffset:CGPointMake(x, 0) animated:NO];
            }
            _btnSearch.hidden = NO;
            _searchField.hidden = NO;
            _iconLine.hidden = NO;
            break;
        case 1://lich dien
            _lblLichDien.textColor = UIColorFromRGB(TEXT_COLOR_FOCUS);
            [_btnLichDien setSelected:YES];
            _btnSearch.hidden = YES;
            _searchField.hidden = YES;
            _iconLine.hidden = YES;
            break;
        case 2://cong dong
            _lblCongDong.textColor = UIColorFromRGB(TEXT_COLOR_FOCUS);
            [_btnCongDong setSelected:YES];
            _btnSearch.hidden = NO;
            _searchField.hidden = NO;
            _iconLine.hidden = NO;
            break;
        case 3://nghe si
            _lblNgheSi.textColor = UIColorFromRGB(TEXT_COLOR_FOCUS);
            [_btnNgheSi setSelected:YES];
            _btnSearch.hidden = NO;
            _searchField.hidden = NO;
            _iconLine.hidden = NO;
            break;
        default:
            break;
    }
}

- (void)focusToTabIndex:(int)index
{
    tabIndex = index;
}

#pragma mark - RNFrostedSidebarDelegate

- (void)sidebar:(RNFrostedSidebar *)sidebar didTapItemAtIndex:(NSUInteger)index {
    [sidebar dismissAnimated:YES completion:^(BOOL finished) {
        if(index != 0 && index != 2){
            menuLeft = nil;
            [self.navigationController popToRootViewControllerAnimated:NO];
            [_menuSliderDelegate callToNewController:(int)index];
        }
    }];
}

- (void)sidebar:(RNFrostedSidebar *)sidebar didEnable:(BOOL)itemEnabled itemAtIndex:(NSUInteger)index {
    if (itemEnabled) {
        [self.optionIndices addIndex:index];
    } else {
        [self.optionIndices removeIndex:index];
    }
}

#pragma mark - UItableview
#pragma mark tableview Delegate method
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [_searchField resignFirstResponder];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Indentifier";

    switch (tableView.tag) {
        case 0://chuong trinh tieu diem
        case 2:{//chuong trinh cong dong
            ProgramCell *cell = (ProgramCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:IS_IPAD?@"ProgramCellIPad":@"ProgramCell" owner:self options:nil];
                for(id currentObject in topLevelObjects) {
                    if([currentObject isKindOfClass:[UITableViewCell class]])
                    {
                        cell = (ProgramCell *)currentObject;
                        cell.backgroundColor = [UIColor clearColor];
                        break;
                    }
                }
            }
            
            ProgramObj *obj = [arrDatasSearch objectAtIndex:indexPath.row];
            if(obj && [obj isKindOfClass:[ProgramObj class]]){
                cell.lblTitle.text = obj.pName;
                if(obj.arrImage.count > 0){
                    cell.imgThumb.image = [obj.arrImage objectAtIndex:0];
                    [cell.progress stopAnimating];
                } else
                    cell.imgThumb.image = [UIImage imageNamed:@"test_thumb0.png"];
            }
            cell.selectionStyle = UITableViewCellSelectionStyleDefault;
            return cell;
        }
        case 1:{// lich dien
            EventCell *cell = (EventCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"EventCell" owner:self options:nil];
                for(id currentObject in topLevelObjects) {
                    if([currentObject isKindOfClass:[UITableViewCell class]])
                    {
                        cell = (EventCell *)currentObject;
                        cell.backgroundColor = [UIColor clearColor];
                        break;
                    }
                }
            }
            
            CalendarEventObj *obj = [arrDatasSearch objectAtIndex:indexPath.row];
            if(obj && [obj isKindOfClass:[CalendarEventObj class]]){
                [cell setObject:obj];
            }
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
        case 3:{// nghe sy/ doan
            ArtCell *cell = (ArtCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:IS_IPAD?@"ArtCellIPad":@"ArtCell" owner:self options:nil];
                for(id currentObject in topLevelObjects) {
                    if([currentObject isKindOfClass:[UITableViewCell class]])
                    {
                        cell = (ArtCell *)currentObject;
                        cell.backgroundColor = [UIColor clearColor];
                        break;
                    }
                }
            }
            
            ArtObj *obj = [arrDatasSearch objectAtIndex:indexPath.row];
            if(obj && [obj isKindOfClass:[ArtObj class]]){
                cell.btnList.myDatas = obj;
                [cell.btnList addTarget:self action:@selector(showListProgramByArt:) forControlEvents:UIControlEventTouchUpInside];
                cell.lblTitle.text = obj.aName;
                if(obj.arrImage.count > 0){
                    cell.imgThumb.image = [obj.arrImage objectAtIndex:0];
                    [cell.progress stopAnimating];
                } else
                    cell.imgThumb.image = [UIImage imageNamed:@"test_thumb0.png"];
            }
            
            [cell bringSubviewToFront:cell.viewContent];
            [cell.viewContent bringSubviewToFront:cell.btnList];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
            break;
        }
    }
        
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_tableView deselectRowAtIndexPath:indexPath animated:NO];
    switch (tableView.tag) {
        case 0:
        case 2:{//cho tab chuong trinh
            ProgramObj *obj = [arrDatasSearch objectAtIndex:indexPath.row];
            ProgramDetailViewController *detail = [[ProgramDetailViewController alloc] initWithObjectSelected:obj];
            [self.navigationController pushViewController:detail animated:YES];
            break;
        }
        case 1:{//cho tab lịch diễn
            CalendarEventObj *obj = [arrDatasSearch objectAtIndex:indexPath.row];
            ProgramListViewController *programList = [[ProgramListViewController alloc] initWithData:@"ProgramListViewController" andArtID:obj.fDate andTypeList:2 andData:obj.calendar_detail_list];
            [self.navigationController pushViewController:programList animated:YES];
            break;
        }
        case 3:{//cho tab đoàn nghệ sỹ
            ArtObj *obj = [arrDatasSearch objectAtIndex:indexPath.row];
            ArtDetailViewController *detail = [[ArtDetailViewController alloc] initWithObjectSelected:obj];
            [self.navigationController pushViewController:detail animated:YES];
            break;
        }
        default:
            break;
    }
}

//show danh sach các chương trình do nghệ sỹ ??? biểu diễn
- (void) showListProgramByArt:(id)sender
{
    MyButton *btn = (MyButton *)sender;
    ArtObj *obj = (ArtObj *)btn.myDatas;
    
    NSMutableArray *data = [[NSMutableArray alloc] init];
    for(ProgramObj *p in appDelegate.arrPrograms){
        for(DetailListObj *pSub in p.detail_list){
            if(pSub.id_doan == [obj.aID intValue]){
                [data addObject:p];
                break;
            }
        }
    }
    
    ProgramListViewController *programList = [[ProgramListViewController alloc] initWithData:@"ProgramListViewController" andArtID:obj.aID andTypeList:1 andData:data];
    [self.navigationController pushViewController:programList animated:YES];
}

#pragma TableDatasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (tableView.tag) {
        case 0:
        case 2:{// tab tieu diem, nghe thuat, cong dong
            if(!IS_IPAD){
                float hCell = 200.0f;
                ProgramObj *obj = nil;
                obj = [arrDatasSearch objectAtIndex:indexPath.row];
                if(!obj || ![obj isKindOfClass:[ProgramObj class]])
                    return hCell;
                    
                CGRect textRect = [obj.pName boundingRectWithSize:CGSizeMake(_tableView.frame.size.width - 22, 9999)
                                                      options:NSStringDrawingUsesLineFragmentOrigin
                                                   attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica Neue" size:15]}
                                                      context:nil];
            
            
                CGSize size = textRect.size;
                float h = size.height;
            
                if(h <= 21)
                    return hCell;
                else
                    return hCell + (h - 21) + h/21;
            } else {
                float hCell = 400.0f;
                ProgramObj *obj = nil;
                obj = [arrDatasSearch objectAtIndex:indexPath.row];
                if(!obj || ![obj isKindOfClass:[ProgramObj class]])
                    return hCell;
                
                CGRect textRect = [obj.pName boundingRectWithSize:CGSizeMake(_tableView.frame.size.width - 22, 9999)
                                                          options:NSStringDrawingUsesLineFragmentOrigin
                                                       attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica Neue" size:24]}
                                                          context:nil];
                
                
                CGSize size = textRect.size;
                float h = size.height;
                
                if(h <= 50)
                    return hCell;
                else
                    return hCell + (h - 50) + h/50;
            }
        }
        case 1:{//for lịch diễn
            return 130;
        }
        case 3:{//doan nghe sy
            return IS_IPAD?400:240;
        }
        default:
            break;
    }
    
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrDatasSearch.count;
}

//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////// Connect with Server /////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
- (void) connectToServer:(int)typeReg andDatas:(NSString *)datas
{
    if(![CheckConnection hasConnectivity]){
        if(arrDatas.count == 0)
            [UIAlertView showWithTitle:MYLocalizedString(@"error.internet.title", nil) message:MYLocalizedString(@"error.internet.content", nil) handler:nil];
        else {
            switch (typeReg) {
                case 0:{
                    //[appDelegate.arrPrograms removeAllObjects];
                    //[appDelegate.arrPrograms addObjectsFromArray:arrDatas];
                    break;
                }
                case 1:{
                    [appDelegate.arrCalendar removeAllObjects];
                    [appDelegate.arrCalendar addObjectsFromArray:arrDatas];
                    break;
                }
                case 2:{
                    [appDelegate.arrArts removeAllObjects];
                    [appDelegate.arrArts addObjectsFromArray:arrDatas];
                    break;
                }
                case 3:{
                    
                    break;
                }
                default:
                    break;
            }
        }
        return;
    }
    
    if(arrDatas.count == 0)
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    id successBlock = ^(AFHTTPRequestOperation *operation, id jsonResult)
    {
        //NSLog(@"%@", jsonResult);
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        switch (typeReg) {
            case 0:{//get program
                int type = [[jsonResult objectForKey:@"type"] intValue];
                if(type == 1){
                    NSArray *arrDict = (NSArray *)[jsonResult objectForKey:@"list"];
                    if(arrDict.count == 0)
                        return;
    
                    NSArray *arrSort = [arrDict sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *a, NSDictionary *b) {
                        int arr1 = [[a objectForKey:@"arrange"] intValue];
                        int arr2 = [[b objectForKey:@"arrange"] intValue];
                        return arr1 > arr2;
                    }];

                    [[CouchbaseEvents sharedInstance] addProgramList:arrDict];                    
                
                    ProgramObj *obj = nil;
                    for (NSDictionary *d in arrSort) {
                        obj = [[ProgramObj alloc] initWithDict:d];
                        
                        if(obj.md5.length == 0){
                            //remove item at local
                            [self removeItemAtLocal:obj.pID andType:0];
                            continue;
                        }
                        obj.loadImageDelegate = (id)appDelegate;
                        if(![appDelegate checkProgram:obj.pID])
                            [appDelegate.arrPrograms addObject:obj];
                    }
                    
                    if(tabIndex == 0){//cho tieu diem
                        if(arrDatas.count == 0){
                            [arrDatas removeAllObjects];
                            for(ProgramObj *o in appDelegate.arrPrograms){
                                if(o.pType == 1)
                                    [arrDatas addObject:o];
                            }
                            [arrDatasSearch removeAllObjects];
                            [arrDatasSearch addObjectsFromArray:arrDatas];
                            [_tableView reloadData];
                        } else {
                            NSInteger index = 0;
                            for(ProgramObj *p1 in appDelegate.arrPrograms){
                                BOOL isNew = YES;
                                for(ProgramObj *p2 in arrDatas){
                                    if([p1.pID isEqualToString:p2.pID]){
                                        isNew = NO;
                                        [p2 updateNewData:p1];
                                        [self reloadDataTable:index];
                                        break;
                                    }
                                }
                                if(isNew && p1.pType == 1)
                                    [arrDatas addObject:p1];
                                index++;
                            }
                        }
                    } else if(tabIndex == 2){//cho cong dong
                        if(arrDatas.count == 0){
                            [arrDatas removeAllObjects];
                            for(ProgramObj *o in appDelegate.arrPrograms){
                                if(o.pType == 3)
                                    [arrDatas addObject:o];
                            }
                            [arrDatasSearch removeAllObjects];
                            [arrDatasSearch addObjectsFromArray:arrDatas];
                            [_tableView reloadData];
                        } else {
                            NSInteger index = 0;
                            for(ProgramObj *p1 in appDelegate.arrPrograms){
                                BOOL isNew = YES;
                                for(ProgramObj *p2 in arrDatas){
                                    if([p1.pID isEqualToString:p2.pID]){
                                        isNew = NO;
                                        [p2 updateNewData:p1];
                                        [self reloadDataTable:index];
                                        break;
                                    }
                                }
                                if(isNew && p1.pType == 3)
                                    [arrDatas addObject:p1];
                                index++;
                            }
                        }
                    } 
                }
                break;
            }
            case 1:{//lich dien
                if(appDelegate.arrPrograms.count == 0)
                    [self loadProgramExpandWhenClick];
                int type = [[jsonResult objectForKey:@"type"] intValue];
                if(type == 1){
                    NSArray *arrDict = (NSArray *)[jsonResult objectForKey:@"list"];
                    if(arrDict.count == 0){
                        [appDelegate.arrCalendar removeAllObjects];
                        [appDelegate.arrCalendar addObjectsFromArray:arrDatas];
                        return;
                    }
                    //store to local
                    [[CouchbaseEvents sharedInstance] addCalendarList:arrDict];
                    CalendarEventObj *cObj = nil;
                    for (NSDictionary *d in arrDict) {
                        cObj = [[CalendarEventObj alloc] initWithDict:d];
                        if(cObj.md5.length == 0){//remove item at local
                            [self removeItemAtLocal:cObj.fDate andType:1];
                            continue;
                        }
                        [appDelegate.arrCalendar addObject:cObj];
                    }
                    
                    if(tabIndex == 1){
                        if(arrDatas.count == 0){
                            [arrDatas removeAllObjects];
                            [arrDatas addObjectsFromArray:appDelegate.arrCalendar];
                            [arrDatasSearch removeAllObjects];
                            [arrDatasSearch addObjectsFromArray:arrDatas];
                            [_tableView reloadData];
                        } else {
                            NSInteger index = 0;
                            for(CalendarEventObj *c1 in appDelegate.arrCalendar){
                                BOOL isNew = YES;
                                for(CalendarEventObj *c2 in arrDatas){
                                    if([c1.fDate isEqualToString:c2.fDate]){
                                        isNew = NO;
                                        [c2 updateData:c1];
                                        [self reloadDataTable:index];
                                        break;
                                    }
                                }
                                if(isNew)
                                    [arrDatas addObject:c1];
                                index++;
                            }
                            
                            [appDelegate.arrCalendar removeAllObjects];
                            [appDelegate.arrCalendar addObjectsFromArray:arrDatas];
                        }
                    }
                }
                break;
            }
            case 2:{//doan nghe thuat
                if(appDelegate.arrPrograms.count == 0)
                    [self loadProgramExpandWhenClick];
                int type = [[jsonResult objectForKey:@"type"] intValue];
                if(type == 1){
                    NSArray *arrDict = (NSArray *)[jsonResult objectForKey:@"list"];
                    if(arrDict.count == 0){
                        [appDelegate.arrArts removeAllObjects];
                        [appDelegate.arrArts addObjectsFromArray:arrDatas];
                        return;
                    }
                    
                    NSArray *arrSort = [arrDict sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *a, NSDictionary *b) {
                        int arr1 = [[a objectForKey:@"arrange"] intValue];
                        int arr2 = [[b objectForKey:@"arrange"] intValue];
                        return arr1 > arr2;
                    }];
                    
                    //store to local
                    [[CouchbaseEvents sharedInstance] addArtList:arrDict];
                    ArtObj *aObj = nil;
                    for (NSDictionary *d in arrSort) {
                        aObj = [[ArtObj alloc] initWithDict:d];
                        if(aObj.md5.length == 0){//remove item at local
                            [self removeItemAtLocal:aObj.aID andType:2];
                            continue;
                        }
                        aObj.loadImageDelegate = (id)appDelegate;
                        [appDelegate.arrArts addObject:aObj];
                    }
                    
                    if(tabIndex == 3){
                        if(arrDatas.count == 0){
                            [arrDatas removeAllObjects];
                            [arrDatas addObjectsFromArray:appDelegate.arrArts];
                            [arrDatasSearch removeAllObjects];
                            [arrDatasSearch addObjectsFromArray:arrDatas];
                            [_tableView reloadData];
                        } else {
                            NSInteger index = 0;
                            for(ArtObj *a1 in appDelegate.arrArts){
                                BOOL isNew = YES;
                                for(ArtObj *a2 in arrDatas){
                                    if([a1.aID isEqualToString:a2.aID]){
                                        isNew = NO;
                                        [a2 updateNewData:a1];
                                        [self reloadDataTable:index];
                                        break;
                                    }
                                }
                                if(isNew)
                                    [arrDatas addObject:a1];
                                index++;
                            }
                        
                            [appDelegate.arrArts removeAllObjects];
                            [appDelegate.arrArts addObjectsFromArray:arrDatas];
                        }
                    }
                }
                break;
            }
            default:
                break;
        }
    };
    
    id failureBlock = ^(AFHTTPRequestOperation *operation, NSError *error) {
        //NSLog(@"%@", error.description);
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        if(arrDatas.count == 0)
            [UIAlertView showWithTitle:MYLocalizedString(@"error.fail.title", nil) message:MYLocalizedString(@"error.fail.content", nil) handler:nil];
    };
    
    switch (typeReg) {
        case 0:{//get all program
            [[MyApiClient sharedClient] getAllPrograms:datas andSuccesBlock:successBlock andFailBlock:failureBlock];
            break;
        }
        case 1:{//lay danh sach lich dien
            [[MyApiClient sharedClient] getAllCalendarEvent:datas andSuccesBlock:successBlock andFailBlock:failureBlock];
            break;
        }
        case 2:{//lay danh sach cac doan nghe thuat
            [[MyApiClient sharedClient] getAllArt:datas andSuccesBlock:successBlock andFailBlock:failureBlock];
            break;
        }
        default:
            break;
    }
}

#pragma common
- (void) showOLGhostAlertView: (NSString *) title content:(NSString *) strContent time: (int) timeout
{
    OLGhostAlertView *olAlert = [[OLGhostAlertView alloc] initWithTitle:title message:strContent timeout:timeout dismissible:YES];
    [olAlert show];
}

- (void)reloadDataTable:(NSInteger) index
{
    NSString *strIndex = [NSString stringWithFormat:@"%zd", index];
    [self performSelectorOnMainThread:@selector(refreshTable:) withObject:strIndex waitUntilDone:NO];
}

- (void) refreshTable:(NSString *) strIndex
{
    NSInteger index = [strIndex integerValue];
    if(index >= arrDatasSearch.count || index < 0)
        return;
    
    [_tableView beginUpdates];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
    [_tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
    [_tableView endUpdates];
}

- (void) removeItemAtLocal:(NSString *)_id andType:(int)tab
{
    if(tab == tabIndex){
        switch (tabIndex) {
            case 0:{//xoa 1 program
                for(ProgramObj *obj in arrDatas){
                    if([obj.pID isEqualToString:_id]){
                        [arrDatas removeObject:obj];
                        break;
                    }
                }
                break;
            }
            case 1:{//xoa 1 lich dien
                for(CalendarEventObj *obj in arrDatas){
                    if([obj.fDate isEqualToString:_id]){
                        [arrDatas removeObject:obj];
                        break;
                    }
                }
                break;
            }
            case 2:{//xoa 1 đoàn nghệ thuật
                for(ArtObj *obj in arrDatas){
                    if([obj.aID isEqualToString:_id]){
                        [arrDatas removeObject:obj];
                        break;
                    }
                }
                break;
            }
            case 3:{//xoa 1 diem ban ve
                
                break;
            }
            default:
                break;
        }
    }
}


//load program list when user tap on Lich dien or Nghe Si
- (void) loadProgramExpandWhenClick
{
    if(appDelegate.arrPrograms.count == 0){
        NSMutableArray *arrDict = [[CouchbaseEvents sharedInstance] loadAllProgram];
        NSArray *arrSort = [arrDict sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *a, NSDictionary *b) {
            int pID1 = [[a objectForKey:@"id_chuongtrinh"] intValue];
            int pID2 = [[b objectForKey:@"id_chuongtrinh"] intValue];
            return pID1 > pID2;
        }];
        NSString *strJson = @"[";
        ProgramObj *obj = nil;
        int i = 0;
        for(NSDictionary *d in arrSort){
            obj = [[ProgramObj alloc] initWithDict:d];
            [appDelegate.arrPrograms addObject:obj];
            
            NSString *id_chuongtrinh = [NSString stringWithFormat:@"{\"id_chuongtrinh\":\"%@\"", obj.pID];
            NSString *md5 = [NSString stringWithFormat:@"\"md5\":\"%@\"},", obj.md5];
            if(i == arrSort.count - 1)
                md5 = [md5 stringByReplacingOccurrencesOfString:@"," withString:@""];
            strJson = [strJson stringByAppendingString:[NSString stringWithFormat:@"%@,%@", id_chuongtrinh, md5]];
            i++;
            //======= load photo offline =====//
            if(obj.arrLinkImage.count == 0){
                UIImage *image = [[CouchbaseEvents sharedInstance] getProgramPhoto:[NSString stringWithFormat:@"program_%@_%i.jpg", obj.pID, 0] andProgramID:obj.pID];
                if(obj.arrImage.count > 0)
                    [obj.arrImage replaceObjectAtIndex:0 withObject:image];
            } else {
                for(NSInteger i = 0; i < obj.arrLinkImage.count; i++){
                    UIImage *image = [[CouchbaseEvents sharedInstance] getProgramPhoto:[NSString stringWithFormat:@"program_%@_%zd.jpg", obj.pID, i] andProgramID:obj.pID];
                    if(i < obj.arrImage.count)
                        [obj.arrImage replaceObjectAtIndex:i withObject:image];
                }
            }
        }
        strJson = [strJson stringByAppendingString:@"]"];//bổ sung mốc ] vào cuối
        //call load on server
        [self connectToServer:0 andDatas:strJson];
    }
}
@end
