//
//  ProgramDetailViewController.h
//  Festival2016
//
//  Created by Phan Phuoc Luong on 11/10/15.
//  Copyright © 2015 Phan Phuoc Luong. All rights reserved.
//
#import "KxMenu.h"
#import "CKCalendarView.h"
#import "ProgramObj.h"
#import "SMPageControl.h"
#import "NSString+HTML.h"

@interface ProgramDetailViewController : UIViewController<UIScrollViewDelegate, KxMenuDeleate, CKCalendarDelegate, UIWebViewDelegate, UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate>
{
    AppDelegate *appDelegate;
    NSMutableArray *menuItems;
    ProgramObj *objSelected;
    NSMutableArray *arrDatas;
    int imageIndex;
    BOOL isPauseAuto;
    NSTimer *timer;
}

- (id) initWithObjectSelected:(ProgramObj *)obj;
@property (nonatomic, weak) IBOutlet UIView *viewMain;
//
@property (nonatomic, weak) IBOutlet UIScrollView *scrollMain;
@property (nonatomic, weak) IBOutlet UIButton *btnMorePlus;
@property (nonatomic, weak) IBOutlet UIScrollView *scrollImages;
@property (nonatomic, weak) IBOutlet SMPageControl *pageControl;
@property (nonatomic, weak) IBOutlet UIScrollView *scrollContent;
@property (nonatomic, weak) IBOutlet UIView *viewButton;
@property (nonatomic, weak) IBOutlet UIView *viewHeader;
@property (nonatomic, weak) IBOutlet UIView *viewBottom;
@property (nonatomic, weak) IBOutlet UIView *viewImages;

@property (nonatomic, weak) IBOutlet UIButton *btnLike;
@property (nonatomic, weak) IBOutlet UILabel *lblLike;
@property (nonatomic, weak) IBOutlet UILabel *lblSave;
@property (nonatomic, weak) IBOutlet UILabel *lblMap;
@property (nonatomic, weak) IBOutlet UILabel *lblMorenews;
//content
@property (nonatomic, weak) IBOutlet UILabel *lblTitle;

//location
@property (nonatomic, weak) IBOutlet UIView *viewLocation;
@property (nonatomic, weak) IBOutlet UILabel *lblLocationTitlee;
@property (nonatomic, weak) IBOutlet UILabel *lblLocationValue;

//view giá
@property (nonatomic, weak) IBOutlet UIView *viewPrice;
@property (nonatomic, weak) IBOutlet UILabel *lblPriceTitle;
@property (nonatomic, weak) IBOutlet UILabel *lblPriceNum;

//view
@property (nonatomic, weak) IBOutlet UIView *viewTime;
@property (nonatomic, weak) IBOutlet UILabel *lblTimeTitle;
@property (nonatomic, weak) IBOutlet UILabel *lblTimeNum;

//web content
@property (nonatomic, weak) IBOutlet UIWebView *webContent;

//calendar
@property (nonatomic, weak) IBOutlet UILabel *lblEvent;
@property (nonatomic, weak) IBOutlet UILabel *lblEventNote;
@property (nonatomic, weak) IBOutlet CKCalendarView *calendar;
@property(nonatomic, strong) NSDateFormatter *dateFormatter;
@property(nonatomic, strong) NSDate *miniMumDate;
@property(nonatomic, strong) NSMutableArray *disableDate;

//view các tin liên quan
@property (nonatomic, weak) IBOutlet UIView *viewRelate;
@property (nonatomic, weak) IBOutlet UIView *viewTopRelate;
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UILabel *lblMorenews2;
@end
