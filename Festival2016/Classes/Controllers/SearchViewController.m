//
//  SearchViewController.m
//  Festival2016
//
//  Created by Phan Phuoc Luong on 1/20/16.
//  Copyright © 2016 Phan Phuoc Luong. All rights reserved.
//

#import "SearchViewController.h"
#import "NewsObj.h"
#import "ProgramObj.h"
#import "ArtObj.h"

#import "ProgramDetailViewController.h"
#import "ArtDetailViewController.h"
#import "NewsDetailViewController.h"
//cell
#import "SearchCell.h"
#import "TicketLocationObj.h"

#import "LocationObj.h"
#import "MapViewController.h"
//
#import "PlaceDetailViewController.h"
#import <objc/runtime.h>

@interface SearchViewController ()

@end

@implementation SearchViewController

- (id)initWithDatas:(NSString *)nibName andType:(SEARCH_TYPE)type andDatas:(NSMutableArray *)datas
{
    if(self = [super initWithNibName:nibName bundle:nil]){
        appDelegate = MyAppDelegate();
        search_type = type;
        arrDatas = [[NSMutableArray alloc] initWithArray:datas];
        arrDatasSearch = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (void)initSafeArea {
    _viewMain.translatesAutoresizingMaskIntoConstraints = NO;
    
    if (@available(iOS 11, *)) {
        UILayoutGuide *guide = self.view.safeAreaLayoutGuide;
        [_viewMain.leadingAnchor constraintEqualToAnchor:guide.leadingAnchor].active = YES;
        [_viewMain.trailingAnchor constraintEqualToAnchor:guide.trailingAnchor].active = YES;
        [_viewMain.topAnchor constraintEqualToAnchor:guide.topAnchor].active = YES;
        [_viewMain.bottomAnchor constraintEqualToAnchor:guide.bottomAnchor].active = YES;
    } else {
        UILayoutGuide *margins = self.view.layoutMarginsGuide;
        [_viewMain.leadingAnchor constraintEqualToAnchor:margins.leadingAnchor].active = YES;
        [_viewMain.trailingAnchor constraintEqualToAnchor:margins.trailingAnchor].active = YES;
        [_viewMain.topAnchor constraintEqualToAnchor:self.topLayoutGuide.bottomAnchor].active = YES;
        [_viewMain.bottomAnchor constraintEqualToAnchor:self.bottomLayoutGuide.topAnchor].active = YES;
    }
    // Refresh myView and/or main view
    [self.view layoutIfNeeded];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.frame = [UIScreen mainScreen].bounds;
    //
    [self initSafeArea];
    //
    [_txtSearch addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    _txtSearch.delegate = self;
    [self refreshLanguage];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    Ivar ivar =  class_getInstanceVariable([UITextField class], "_placeholderLabel");
    UILabel *placeholderLabel = object_getIvar(_txtSearch, ivar);
    placeholderLabel.textColor = [UIColor colorWithWhite:1.0 alpha:0.8f];
    //
    [_txtSearch becomeFirstResponder];
}

//refresh language
- (void)refreshLanguage
{
    NSString *place_holder = MYLocalizedString(@"search_detail", nil);
    switch (search_type) {
        case search_program:
            place_holder = [place_holder stringByAppendingString:MYLocalizedString(@"program.title", nil)];
            break;
        case search_art:
            place_holder = [place_holder stringByAppendingString:MYLocalizedString(@"art.title", nil)];
            break;
        case search_news:
            place_holder = [place_holder stringByAppendingString:MYLocalizedString(@"news.title", nil)];
            break;
        case search_diembanve:
        case search_content:
        default:
            place_holder = [place_holder stringByAppendingString:MYLocalizedString(@"search_content", nil)];
            break;
    }
    [_txtSearch setPlaceholder:[place_holder lowercaseString]];
}

//============== Click Button ==============//
- (IBAction) clickBack:(id)sender
{
    [_txtSearch resignFirstResponder];
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction) clickSearch:(id)sender
{
    [_txtSearch becomeFirstResponder];
}

#pragma TexyField Search Delegate
-(void)textFieldDidChange :(UITextField *)theTextField
{
    NSString *str = [theTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    [arrDatasSearch removeAllObjects];
    if ([str length] > 0) {
        switch (search_type) {
            case search_program:{
                for (ProgramObj *obj in arrDatas){
                    NSRange nameRange = [obj.pName rangeOfString:str options:NSCaseInsensitiveSearch];
                    if(nameRange.location != NSNotFound){
                        [arrDatasSearch addObject:obj];
                    }
                }

                break;
            }
            case search_art:{
                for (ArtObj *obj in arrDatas){
                    NSRange nameRange = [obj.aName rangeOfString:str options:NSCaseInsensitiveSearch];
                    if(nameRange.location != NSNotFound){
                        [arrDatasSearch addObject:obj];
                    }
                }

                break;
            }
            case search_news:{
                for (NewsObj *obj in arrDatas){
                    NSRange nameRange = [obj.nTitle rangeOfString:str options:NSCaseInsensitiveSearch];
                    if(nameRange.location != NSNotFound){
                        [arrDatasSearch addObject:obj];
                    }
                }

                break;
            }
            case search_diembanve:{
                for (TicketLocationObj *obj in arrDatas){
                    NSRange nameRange = [obj.title rangeOfString:str options:NSCaseInsensitiveSearch];
                    if(nameRange.location != NSNotFound){
                        [arrDatasSearch addObject:obj];
                    }
                }
                
                break;
            }
            case search_content:
            default:
                for (TicketLocationObj *obj in arrDatas){
                    NSRange nameRange = [obj.title rangeOfString:str options:NSCaseInsensitiveSearch];
                    if(nameRange.location != NSNotFound){
                        [arrDatasSearch addObject:obj];
                    }
                }
                break;
        }
    }
    
    [_tableView reloadData];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_txtSearch resignFirstResponder];
    return YES;
}

#pragma mark - UItableview
#pragma mark tableview Delegate method
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [_txtSearch resignFirstResponder];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Indentifier";
    
    SearchCell *cell = (SearchCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:IS_IPAD?@"SearchCellIPad":@"SearchCell" owner:self options:nil];
        for(id currentObject in topLevelObjects) {
            if([currentObject isKindOfClass:[UITableViewCell class]])
            {
                cell = (SearchCell *)currentObject;
                cell.backgroundColor = [UIColor clearColor];
                break;
            }
        }
    }
    
    UIImage *thumb = [UIImage imageNamed:@"test0.png"];
    NSString *huefes = MYLocalizedString(@"hue.fes.name", nil);
    NSString *title = @"";
    switch (search_type) {
        case search_program:{
            ProgramObj *obj = [arrDatasSearch objectAtIndex:indexPath.row];
            title = obj.pName;
            huefes = [huefes stringByAppendingString:MYLocalizedString(@"program.title", nil)];
            if(obj.arrImage.count > 0)
                thumb = [obj.arrImage objectAtIndex:0];
            break;
        }
        case search_art:{
            ArtObj *obj = [arrDatasSearch objectAtIndex:indexPath.row];
            title = obj.aName;
            huefes = [huefes stringByAppendingString:MYLocalizedString(@"art.title", nil)];
            if(obj.arrImage.count > 0)
                thumb = [obj.arrImage objectAtIndex:0];
            break;
        }
        case search_news:{
            NewsObj *obj = [arrDatasSearch objectAtIndex:indexPath.row];
            title = obj.nTitle;
            huefes = [huefes stringByAppendingString:MYLocalizedString(@"news.title", nil)];
            if(obj.newsThumb)
                thumb = obj.newsThumb;
            break;
        }
        case search_diembanve:{
            TicketLocationObj *obj = [arrDatasSearch objectAtIndex:indexPath.row];
            title = obj.title;
            huefes = [huefes stringByAppendingString:MYLocalizedString(@"ticket.title", nil)];
            if(obj.pThumb)
                thumb = obj.pThumb;
            break;
        }
        case search_content:
        default:{
            TicketLocationObj *obj = [arrDatasSearch objectAtIndex:indexPath.row];
            title = obj.title;
            huefes = [huefes stringByAppendingString:MYLocalizedString(@"location.title", nil)];
            if(obj.pThumb)
                thumb = obj.pThumb;
            break;
        }
    }
    
    cell.lblHueFes.text = huefes;
    cell.lblTitle.text = title;
    cell.iconView.image = thumb;
    
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_txtSearch resignFirstResponder];
    [_tableView deselectRowAtIndexPath:indexPath animated:NO];
    switch (search_type) {
        case search_program:{
            ProgramObj *obj = [arrDatasSearch objectAtIndex:indexPath.row];
            ProgramDetailViewController *detail = [[ProgramDetailViewController alloc] initWithObjectSelected:obj];
            [self.navigationController pushViewController:detail animated:YES];
            break;
        }
        case search_art:{
            ArtObj *obj = [arrDatasSearch objectAtIndex:indexPath.row];
            ArtDetailViewController *detail = [[ArtDetailViewController alloc] initWithObjectSelected:obj];
            [self.navigationController pushViewController:detail animated:YES];
            break;
        }
        case search_news:{
            NewsObj *obj = [arrDatasSearch objectAtIndex:indexPath.row];
            NewsDetailViewController *detail = [[NewsDetailViewController alloc] initWithObject:@"NewsDetailViewController" andObj:obj andArr:arrDatas];
            [self.navigationController pushViewController:detail animated:YES];
            break;
        }
        case search_diembanve:{
            TicketLocationObj *obj = [arrDatasSearch objectAtIndex:indexPath.row];
            //show map for this location
            if(obj)
                [self showMapView:obj];
            break;
        }
        case search_content:
        default:{
            TicketLocationObj *obj = [arrDatasSearch objectAtIndex:indexPath.row];
            PlaceDetailViewController *detail = [[PlaceDetailViewController alloc] initWithObject:@"PlaceDetailViewController" andObj:obj andArr:arrDatas];
            [self.navigationController pushViewController:detail animated:YES];
            break;
        }
    }
}

- (void) showMapView:(TicketLocationObj *)obj
{
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    LocationObj *lObj = [[LocationObj alloc] init];
    lObj.id_diadiem = obj.id_ticketlocation;
    lObj.diadiem_name = obj.title;
    lObj.diadiem_content = [NSString stringWithFormat:@"%@ (%@)", obj.address, obj.phone];
    lObj.pThumb = obj.pThumb;
    lObj.lat = obj.lat;
    lObj.lng = obj.lng;
    [arr addObject:lObj];
    
    [appDelegate.mapViewController setDatas:1 andDatas:arr];
    [self.navigationController pushViewController:appDelegate.mapViewController animated:YES];
}


#pragma TableDatasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!IS_IPAD){
        return 60;
    } else {
        return 100;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return arrDatasSearch.count > 0?0:200;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrDatasSearch.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _tableView.frame.size.width, 200)];
    view.backgroundColor = UIColorFromRGB(COLOR_WHITE);
    
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, view.frame.size.width - 40, 180)];
    lbl.numberOfLines = 0;
    lbl.text = MYLocalizedString(@"search.noresult", nil);
    lbl.textColor = UIColorFromRGB(COLOR_BLACK);
    lbl.textAlignment = NSTextAlignmentCenter;
    [view addSubview:lbl];
    
    return view;
}

- (void)reloadDataTable:(NSInteger) index
{
    NSString *strIndex = [NSString stringWithFormat:@"%zd", index];
    [self performSelectorOnMainThread:@selector(refreshTable:) withObject:strIndex waitUntilDone:NO];
}

- (void) refreshTable:(NSString *) strIndex
{
    NSInteger index = [strIndex integerValue];
    if(index >= arrDatasSearch.count || index < 0)
        return;
    
    [_tableView beginUpdates];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
    [_tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
    [_tableView endUpdates];
}
@end
