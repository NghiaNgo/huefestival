//
//  PlaceDetailViewController.h
//  Festival2016
//
//  Created by Phan Phuoc Luong on 2/17/16.
//  Copyright © 2016 Phan Phuoc Luong. All rights reserved.
//

#import "SMPageControl.h"
#import "NSString+HTML.h"
#import "TicketLocationObj.h"

@interface PlaceDetailViewController : UIViewController<UIScrollViewDelegate, UIWebViewDelegate, UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate>
{
    AppDelegate *appDelegate;
    TicketLocationObj *objSelect;
    NSMutableArray *arrDatas;
}

- (id) initWithObject:(NSString *)nibName andObj:(TicketLocationObj *)obj andArr:(NSMutableArray *)data;
@property (nonatomic, weak) IBOutlet UIView *viewMain;
//
@property (nonatomic, weak) IBOutlet UIScrollView *scrollMain;
@property (nonatomic, weak) IBOutlet UIScrollView *scrollImages;
@property (nonatomic, weak) IBOutlet SMPageControl *pageControl;
@property (nonatomic, weak) IBOutlet UIScrollView *scrollContent;
@property (nonatomic, weak) IBOutlet UIView *viewButton;
@property (nonatomic, weak) IBOutlet UIView *viewHeader;
@property (nonatomic, weak) IBOutlet UIView *viewBottom;
@property (nonatomic, weak) IBOutlet UIView *viewImages;

@property (nonatomic, weak) IBOutlet UIButton *btnLike;
@property (nonatomic, weak) IBOutlet UILabel *lblLike;
@property (nonatomic, weak) IBOutlet UILabel *lblSave;
@property (nonatomic, weak) IBOutlet UILabel *lblMore;
@property (nonatomic, weak) IBOutlet UILabel *lblMap;
//content
@property (nonatomic, weak) IBOutlet UIView *viewAddress;
@property (nonatomic, weak) IBOutlet UIView *viewPhone;
@property (nonatomic, weak) IBOutlet UIView *viewWebsite;
@property (nonatomic, weak) IBOutlet UILabel *lblTitle;//tieu de bai viet
@property (nonatomic, weak) IBOutlet UILabel *lblAddressTitle;//dia chi
@property (nonatomic, weak) IBOutlet UILabel *lblAddressValue;//dia chi
@property (nonatomic, weak) IBOutlet UILabel *lblPhoneTitle;//dien thoai
@property (nonatomic, weak) IBOutlet UILabel *lblPhoneValue;//dien thoai value
@property (nonatomic, weak) IBOutlet UILabel *lblWebsiteValue;//dia chi website
@property (nonatomic, weak) IBOutlet UIWebView *webContent;

//view các tin liên quan
@property (nonatomic, weak) IBOutlet UIView *viewRelate;
@property (nonatomic, weak) IBOutlet UIView *viewTopRelate;
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UILabel *lblMore2;

@end
