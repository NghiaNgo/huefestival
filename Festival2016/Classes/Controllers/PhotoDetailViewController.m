//
//  PhotoDetailViewController.m
//  Festival2016
//
//  Created by Phan Phuoc Luong on 2/25/16.
//  Copyright © 2016 Phan Phuoc Luong. All rights reserved.
//

#import "PhotoDetailViewController.h"

@interface PhotoDetailViewController ()

@end

@implementation PhotoDetailViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(self){
        _isPresented = self;
    }
    
    return self;
}

- (id)initWithData:(NSMutableArray *)arrPhotos andIndex:(int)index andNibName:(NSString *)nibName
{
    self = [super initWithNibName:nibName bundle:nil];
    if(self){
        _isPresented = YES;
        _arrPhotos = [[NSMutableArray alloc] initWithArray:arrPhotos];
        indexSelected = index;
        
        appDelegate = MyAppDelegate();
    }
    
    return self;
}

- (void)initSafeArea {
    _viewMain.translatesAutoresizingMaskIntoConstraints = NO;
    
    if (@available(iOS 11, *)) {
        UILayoutGuide *guide = self.view.safeAreaLayoutGuide;
        [_viewMain.leadingAnchor constraintEqualToAnchor:guide.leadingAnchor].active = YES;
        [_viewMain.trailingAnchor constraintEqualToAnchor:guide.trailingAnchor].active = YES;
        [_viewMain.topAnchor constraintEqualToAnchor:guide.topAnchor].active = YES;
        [_viewMain.bottomAnchor constraintEqualToAnchor:guide.bottomAnchor].active = YES;
    } else {
        UILayoutGuide *margins = self.view.layoutMarginsGuide;
        [_viewMain.leadingAnchor constraintEqualToAnchor:margins.leadingAnchor].active = YES;
        [_viewMain.trailingAnchor constraintEqualToAnchor:margins.trailingAnchor].active = YES;
        [_viewMain.topAnchor constraintEqualToAnchor:self.topLayoutGuide.bottomAnchor].active = YES;
        [_viewMain.bottomAnchor constraintEqualToAnchor:self.bottomLayoutGuide.topAnchor].active = YES;
    }
    // Refresh myView and/or main view
    [self.view layoutIfNeeded];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.frame = [UIScreen mainScreen].bounds;
    //
    [self initSafeArea];
    //
    _pageControl.indicatorMargin = 3.0f;
    _pageControl.pageIndicatorTintColor = [[UIColor whiteColor] colorWithAlphaComponent:0.9f];
    _pageControl.currentPageIndicatorTintColor = [UIColorFromRGB(COLOR_RED) colorWithAlphaComponent:1.f];
    _pageControl.currentPageIndicatorImage = [UIImage imageNamed:@"ic_slide_focus.png"];
    _pageControl.pageIndicatorMaskImage = [UIImage imageNamed:@"ic_slide.png"];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self loadScrollImages];
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (void) loadScrollImages
{
    CGRect r = _scrollImages.frame;
    r.origin.x = 0;
    r.origin.y = 0;
    _scrollImages.frame = r;
    float w = r.size.width;
    float h = r.size.height;
    for(int i = 0; i < _arrPhotos.count; i++){
        r = CGRectMake(i*w, 0, w, h);
        UIImageView *imageview = [[UIImageView alloc] initWithFrame:r];
        imageview.contentMode = UIViewContentModeScaleAspectFit;//IS_IPAD ? UIViewContentModeScaleToFill : UIViewContentModeScaleAspectFill;
        imageview.image = [_arrPhotos objectAtIndex:i];
        [_scrollImages addSubview:imageview];
    }
    
    [_pageControl setNumberOfPages:_arrPhotos.count];
    _scrollImages.contentSize = CGSizeMake(_arrPhotos.count * w, _scrollImages.frame.size.height);
    _pageControl.currentPage = indexSelected;
    [_scrollImages setContentOffset:CGPointMake(indexSelected*w, 0) animated:NO];
}

#pragma mark -
#pragma mark UIScrollViewDelegate methods
- (void)scrollViewDidScroll:(UIScrollView *)sender {
    CGFloat pageWidth = _scrollImages.frame.size.width;
    indexSelected = floor((_scrollImages.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    _pageControl.currentPage = indexSelected;
}

- (IBAction)clickBack:(id)sender
{
    _isPresented = NO;
    [self dismissViewControllerAnimated:NO completion:nil];
}
@end
