//
//  ProfileViewController.h
//  Festival2016
//
//  Created by Phan Phuoc Luong on 11/6/15.
//  Copyright © 2015 Phan Phuoc Luong. All rights reserved.
//

#import "RNFrostedSidebar.h"

@interface ProfileViewController : UIViewController<RNFrostedSidebarDelegate>
{

    int typeHeader;//để xác định nút back hay menu
    RNFrostedSidebar *menuLeft;
    AppDelegate *appDelegate;
}

- (id) initWithDatas:(NSString *)nibName andType:(int)typeHeader;
@property (nonatomic, weak) IBOutlet UIView *viewMain;
//
@property (nonatomic, strong) id<MenuSliderDelegate> menuSliderDelegate;

@property (nonatomic, weak) IBOutlet UILabel *lblTitle;
@property (nonatomic, weak) IBOutlet UIButton *btnMenu;
@property (nonatomic, weak) IBOutlet UIButton *btnBack;

//for slide
@property (nonatomic, strong) NSMutableIndexSet *optionIndices;
//for search
@property (nonatomic, weak) IBOutlet UITextField *txtSearch;
//
@property (nonatomic, weak) IBOutlet UIView *viewMap;
@property (nonatomic, weak) IBOutlet UIImageView *imgMap;
@property (nonatomic, weak) IBOutlet UILabel *lblMap;

@property (nonatomic, weak) IBOutlet UIView *viewAlarm;
@property (nonatomic, weak) IBOutlet UIView *viewAlarmImg;
@property (nonatomic, weak) IBOutlet UIImageView *imgA1;
@property (nonatomic, weak) IBOutlet UIImageView *imgA2;
@property (nonatomic, weak) IBOutlet UIImageView *imgA3;
@property (nonatomic, weak) IBOutlet UILabel *lblA1;
@property (nonatomic, weak) IBOutlet UILabel *lblA2;

@property (nonatomic, weak) IBOutlet UIView *viewLiked;
@property (nonatomic, weak) IBOutlet UIView *viewLikedImg;
@property (nonatomic, weak) IBOutlet UIImageView *imgL1;
@property (nonatomic, weak) IBOutlet UIImageView *imgL2;
@property (nonatomic, weak) IBOutlet UIImageView *imgL3;
@property (nonatomic, weak) IBOutlet UILabel *lblL1;
@property (nonatomic, weak) IBOutlet UILabel *lblL2;

- (void) refreshLanguage;
- (void) loadListAlarm;
@end
