//
//  ProfileViewController.m
//  Festival2016
//
//  Created by Phan Phuoc Luong on 11/6/15.
//  Copyright © 2015 Phan Phuoc Luong. All rights reserved.
//

#import "ProfileViewController.h"
#import "MapViewController.h"
#import "LocalPushManager.h"
#import "CouchbaseEvents.h"
#import "ProgramObj.h"
#import "TicketLocationObj.h"
#import "ArtObj.h"
#import "NewsObj.h"
#import <objc/runtime.h>

#import "AlarmViewController.h"
#import "LikeViewController.h"

@interface ProfileViewController ()
{
    NSMutableArray *arrNewsLiked;
    NSMutableArray *arrDiaDiemLiked;
    NSMutableArray *arrArtistLiked;
    NSMutableArray *arrProgramLiked;
    NSMutableArray *arrAlarms;
    NSMutableArray *arrImageThumbs;
    LocalPushManager *localPushManager;
}
@end

@implementation ProfileViewController

- (id)initWithDatas:(NSString *)nibName andType:(int)type
{
    if(self = [super initWithNibName:nibName bundle:nil]){
        typeHeader = type;
        appDelegate = MyAppDelegate();
        
        arrImageThumbs = [[NSMutableArray alloc] init];
        arrAlarms = [[NSMutableArray alloc] init];
        arrProgramLiked = [[NSMutableArray alloc] init];
        arrArtistLiked = [[NSMutableArray alloc] init];
        arrDiaDiemLiked = [[NSMutableArray alloc] init];
        arrNewsLiked = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (void)initSafeArea {
    _viewMain.translatesAutoresizingMaskIntoConstraints = NO;
    
    if (@available(iOS 11, *)) {
        UILayoutGuide *guide = self.view.safeAreaLayoutGuide;
        [_viewMain.leadingAnchor constraintEqualToAnchor:guide.leadingAnchor].active = YES;
        [_viewMain.trailingAnchor constraintEqualToAnchor:guide.trailingAnchor].active = YES;
        [_viewMain.topAnchor constraintEqualToAnchor:guide.topAnchor].active = YES;
        [_viewMain.bottomAnchor constraintEqualToAnchor:guide.bottomAnchor].active = YES;
    } else {
        UILayoutGuide *margins = self.view.layoutMarginsGuide;
        [_viewMain.leadingAnchor constraintEqualToAnchor:margins.leadingAnchor].active = YES;
        [_viewMain.trailingAnchor constraintEqualToAnchor:margins.trailingAnchor].active = YES;
        [_viewMain.topAnchor constraintEqualToAnchor:self.topLayoutGuide.bottomAnchor].active = YES;
        [_viewMain.bottomAnchor constraintEqualToAnchor:self.bottomLayoutGuide.topAnchor].active = YES;
    }
    // Refresh myView and/or main view
    [self.view layoutIfNeeded];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.frame = [UIScreen mainScreen].bounds;
    //
    [self initSafeArea];
    //
    _optionIndices = [NSMutableIndexSet indexSetWithIndex:5];
    //init menu left
    [self initMenuLeft];
    //init langague
    [self refreshLanguage];
    
    float corner = 6;
    _viewAlarm.layer.cornerRadius = corner;
    _viewAlarmImg.layer.cornerRadius = corner;
    _viewLiked.layer.cornerRadius = corner;
    _viewLikedImg.layer.cornerRadius = corner;
    _viewMap.layer.cornerRadius = corner;
    _imgMap.layer.cornerRadius = corner;
    
    UITapGestureRecognizer *tapAlarm = [[UITapGestureRecognizer alloc] initWithTarget:self
                                        action:@selector(showAlarm)];
    [_viewAlarm addGestureRecognizer:tapAlarm];
    UITapGestureRecognizer *tabLiked = [[UITapGestureRecognizer alloc] initWithTarget:self
                                        action:@selector(showLiked)];
    [_viewLiked addGestureRecognizer:tabLiked];
    UITapGestureRecognizer *tabMap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                        action:@selector(showMap)];
    [_viewMap addGestureRecognizer:tabMap];
    
    localPushManager = [[LocalPushManager alloc] init];
}

- (void) loadListAlarm
{
    NSMutableArray *arrLocalDict = [localPushManager getAllEvent];
    _imgA1.image = nil;
    _imgA2.image = nil;
    _imgA3.image = nil;
    if(arrLocalDict.count > 0){
        int i = 2;
        for(int index = (int)arrLocalDict.count - 1; index >= 0; --index){
            NSDictionary *d = [arrLocalDict objectAtIndex:index];
            NSString *pID = [d objectForKey:@"program_id"];
            UIImage *image = [[CouchbaseEvents sharedInstance] getProgramPhoto:[NSString stringWithFormat:@"program_%@_%i.jpg", pID, 0] andProgramID:pID];
            if(image){
                switch (i) {
                    case 0:
                        _imgA3.image = image;
                        break;
                    case 1:
                        _imgA2.image = image;
                        break;
                    case 2:
                        _imgA1.image = image;
                        break;
                    default:
                        break;
                }
            }
            
            --i;
            if(i < 0)
                break;
        }
    }
    [arrAlarms removeAllObjects];
    [arrAlarms addObjectsFromArray:arrLocalDict];
    _lblA2.text = [NSString stringWithFormat:MYLocalizedString(@"profile.alarm2", nil), arrLocalDict.count];
}

- (void) loadProgramLiked
{
    [arrImageThumbs removeAllObjects];
    [arrProgramLiked removeAllObjects];
    for(NSString *pID in appDelegate.arrLikedID){
        NSDictionary *dict = [[CouchbaseEvents sharedInstance] getProgramByID:pID];
        if(dict){
            ProgramObj *obj = [[ProgramObj alloc] initWithDict:dict];
            //======= load photo offline =====//
            if(obj.arrLinkImage.count == 0){
                UIImage *image = [[CouchbaseEvents sharedInstance] getProgramPhoto:[NSString stringWithFormat:@"program_%@_%i.jpg", obj.pID, 0] andProgramID:obj.pID];
                if(obj.arrImage.count > 0)
                    [obj.arrImage replaceObjectAtIndex:0 withObject:image];
            } else {
                for(NSInteger i = 0; i < obj.arrLinkImage.count; i++){
                    UIImage *image = [[CouchbaseEvents sharedInstance] getProgramPhoto:[NSString stringWithFormat:@"program_%@_%zd.jpg", obj.pID, i] andProgramID:obj.pID];
                    if(i < obj.arrImage.count)
                        [obj.arrImage replaceObjectAtIndex:i withObject:image];
                }
            }
            [arrProgramLiked addObject:obj];
        }
    }
    
    for(int index = 0; index < arrProgramLiked.count; ++index){
        ProgramObj *obj = [arrProgramLiked objectAtIndex:index];
        UIImage *image = [obj.arrImage objectAtIndex:0];
        if(arrImageThumbs.count < 3)
            [arrImageThumbs addObject:image];
    }
    [self loadArtistLiked];
}

- (void) loadArtistLiked
{
    [arrArtistLiked removeAllObjects];
    for(NSString *aID in appDelegate.arrLikedArtistID){
        NSDictionary *dict = [[CouchbaseEvents sharedInstance] getArtByID:aID];
        if(dict){
            ArtObj *obj = [[ArtObj alloc] initWithDict:dict];
            //======= load photo offline =====//
            for(NSInteger i = 0; i < obj.arrImageLink.count; i++){
                UIImage *image = [[CouchbaseEvents sharedInstance] getArtPhoto:[NSString stringWithFormat:@"art_%@_%zd.jpg", obj.aID, i] andArtID:obj.aID];
                if(i < obj.arrImage.count)
                    [obj.arrImage replaceObjectAtIndex:i withObject:image];
            }
            [arrArtistLiked addObject:obj];
        }
    }
    for(int index = 0; index < arrArtistLiked.count; ++index){
        ArtObj *obj = [arrArtistLiked objectAtIndex:index];
        UIImage *image = [obj.arrImage objectAtIndex:0];
        if(arrImageThumbs.count < 3)
            [arrImageThumbs addObject:image];
    }
    [self loadNewsLiked];
}

- (void) loadNewsLiked
{
    if(appDelegate.arrNewsLikeList.count == 0){
        NSMutableArray *arrDict = [[CouchbaseEvents sharedInstance] loadAllNewsLiked];
        for(NSDictionary *d in arrDict){
            NewsObj *obj = [[NewsObj alloc] initWithDict:d];
            [appDelegate.arrNewsLikeList addObject:obj];
        }
    }
    [arrNewsLiked removeAllObjects];
    [arrNewsLiked addObjectsFromArray:appDelegate.arrNewsLikeList];
    for(int index = 0; index < arrNewsLiked.count; ++index){
        NewsObj *obj = [arrNewsLiked objectAtIndex:index];
        obj.newsThumb = [[CouchbaseEvents sharedInstance] getNewsPhoto:[NSString stringWithFormat:@"%@.jpg", obj.nTitle] andID:obj.nID];
        if(arrImageThumbs.count < 3 && obj.newsThumb)
            [arrImageThumbs addObject:obj.newsThumb];
    }
    [self loadDiaDiemLike];
}

- (void) loadDiaDiemLike
{
    if(appDelegate.arrDiaDiemLikeList.count == 0){
        NSMutableArray *arrDict = [[CouchbaseEvents sharedInstance] loadAllDiaDiemLiked];
        for(NSDictionary *d in arrDict){
            TicketLocationObj *obj = [[TicketLocationObj alloc] initWithDict:d andIndex:appDelegate.arrDiaDiemLikeList.count];
            obj.typedata = [[d objectForKey:@"stypedata"] intValue];
            [appDelegate.arrDiaDiemLikeList addObject:obj];
        }
    }
    [arrDiaDiemLiked removeAllObjects];
    [arrDiaDiemLiked addObjectsFromArray:appDelegate.arrDiaDiemLikeList];
    for(int index = 0; index < arrDiaDiemLiked.count; ++index){
        TicketLocationObj *obj = [arrDiaDiemLiked objectAtIndex:index];
        obj.pThumb = [[CouchbaseEvents sharedInstance] getDiaDiemPhoto:obj.pLinkImage andID:obj.id_ticketlocation];
        if(arrImageThumbs.count < 3 && obj.pThumb)
            [arrImageThumbs addObject:obj.pThumb];
    }
    [self showThumbAndLabel];
}

- (void) showThumbAndLabel
{
    _imgL1.image = nil;
    _imgL2.image = nil;
    _imgL3.image = nil;

    for(int index = 0; index < arrImageThumbs.count; ++index){
        UIImage *image = [arrImageThumbs objectAtIndex:index];
        if(image){
            switch (index) {
                case 0:
                    _imgL1.image = image;
                    break;
                case 1:
                    _imgL2.image = image;
                    break;
                case 2:
                    _imgL3.image = image;
                    break;
                default:
                    break;
            }
        }
    }
    _lblL2.text = [NSString stringWithFormat:MYLocalizedString(@"profile.liked2", nil), arrProgramLiked.count + arrArtistLiked.count + arrDiaDiemLiked.count + arrNewsLiked.count];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    Ivar ivar =  class_getInstanceVariable([UITextField class], "_placeholderLabel");
    UILabel *placeholderLabel = object_getIvar(_txtSearch, ivar);
    placeholderLabel.textColor = [UIColor colorWithWhite:1.0 alpha:0.8f];
    //
    [_optionIndices removeAllIndexes];
    [_optionIndices addIndex:5];
    if(typeHeader == 0){
        [_btnMenu setHidden:NO];
        [_btnBack setHidden:YES];
    } else {
        [_btnMenu setHidden:YES];
        [_btnBack setHidden:NO];
    }
    
    //update layout for 3.5'
    if(appDelegate.screenHeight <= 480){
        CGRect r = _viewAlarm.frame;
        r.origin.y = 62;
        r.size.height = 162;
        _viewAlarm.frame = r;
        
        r = _viewLiked.frame;
        r.origin.y = 62;
        r.size.height = 162;
        _viewLiked.frame = r;
        
        r = _viewMap.frame;
        r.origin.y = _viewAlarm.frame.origin.y + _viewAlarm.frame.size.height + 10;
        r.size.height = 162;
        _viewMap.frame = r;
    } else {
        CGRect r = _viewAlarm.frame;
        r.origin.y = 62;
        _viewAlarm.frame = r;
        
        r = _viewLiked.frame;
        r.origin.y = 62;
        _viewLiked.frame = r;
        
        r = _viewMap.frame;
        r.origin.y = _viewAlarm.frame.origin.y + _viewAlarm.frame.size.height + 10;
        _viewMap.frame = r;
    }
    
    //lấy danh sách các chuong trình đã đặt thông báo
    [self loadListAlarm];
    //lấy danh các chương trình yêu thích
    [self loadProgramLiked];
}

//refresh language
- (void)refreshLanguage
{
    _lblTitle.text = MYLocalizedString(@"profile.title", nil);
    [_txtSearch setPlaceholder:MYLocalizedString(@"search", nil)];
    
    _lblMap.text = MYLocalizedString(@"profile.map", nil);
    _lblA1.text = MYLocalizedString(@"profile.alarm1", nil);
    _lblA2.text = [NSString stringWithFormat:MYLocalizedString(@"profile.alarm2", nil), 0];
    _lblL1.text = MYLocalizedString(@"profile.liked1", nil);
    _lblL2.text = [NSString stringWithFormat:MYLocalizedString(@"profile.liked2", nil), 0];
}

- (void) showAlarm
{
    if(arrAlarms.count == 0)
        return;
    AlarmViewController *alarmView = [[AlarmViewController alloc] initWithDatas:@"AlarmViewController" andData:arrAlarms];
    [self.navigationController pushViewController:alarmView animated:NO];
}

- (void) showLiked
{
    if(arrProgramLiked.count == 0 && arrArtistLiked.count == 0 && arrDiaDiemLiked.count == 0 && arrNewsLiked.count == 0)
        return;
    LikeViewController *likeView = [[LikeViewController alloc] initWithDatas:@"LikeViewController" andData:arrProgramLiked andData1:arrArtistLiked andData2:arrNewsLiked andData3:arrDiaDiemLiked];
    [self.navigationController pushViewController:likeView animated:NO];
}

- (void) showMap
{
    [appDelegate.mapViewController setDatas:2 andDatas:appDelegate.arrLocations];
    [self.navigationController pushViewController:appDelegate.mapViewController animated:YES];
}

- (IBAction) clickBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction) clickSearch:(id)sender
{
}

- (IBAction) clickMenuLeft:(id)sender
{
    [menuLeft setIsSingleSelect:YES];
    [menuLeft show];
}

- (void) initMenuLeft
{
    NSString *sub = appDelegate.language;
    if(sub.length == 0)
        sub = @"vi";
    NSArray *images = @[[UIImage imageNamed:@"ic_menu_focus.png"],
                        [UIImage imageNamed:[NSString stringWithFormat:@"ic_home_%@.png", sub]],
                        [UIImage imageNamed:[NSString stringWithFormat:@"ic_program_%@.png", sub]],
                        [UIImage imageNamed:[NSString stringWithFormat:@"ic_place_%@.png", sub]],
                        [UIImage imageNamed:[NSString stringWithFormat:@"ic_news_%@.png", sub]],
                        [UIImage imageNamed:[NSString stringWithFormat:@"ic_profile_%@.png", sub]],
                        ];
    
    NSArray *colors = @[
                        [UIColor colorWithRed:215/255.f green:169/255.f blue:27/255.f alpha:0],
                        [UIColor colorWithRed:215/255.f green:169/255.f blue:27/255.f alpha:1],
                        [UIColor colorWithRed:215/255.f green:169/255.f blue:27/255.f alpha:1],
                        [UIColor colorWithRed:215/255.f green:169/255.f blue:27/255.f alpha:1],
                        [UIColor colorWithRed:215/255.f green:169/255.f blue:27/255.f alpha:1],
                        [UIColor colorWithRed:215/255.f green:169/255.f blue:27/255.f alpha:1],
                        ];
    
    
    menuLeft = [[RNFrostedSidebar alloc] initWithImages:images selectedIndices:_optionIndices borderColors:colors];
    menuLeft.delegate = self;
}

#pragma mark - RNFrostedSidebarDelegate

- (void)sidebar:(RNFrostedSidebar *)sidebar didTapItemAtIndex:(NSUInteger)index {
    [sidebar dismissAnimated:YES completion:^(BOOL finished) {
        if(index != 0 && index != 5){
            [self.navigationController popToRootViewControllerAnimated:NO];
            [_menuSliderDelegate callToNewController:(int)index];
        }
    }];
}

- (void)sidebar:(RNFrostedSidebar *)sidebar didEnable:(BOOL)itemEnabled itemAtIndex:(NSUInteger)index {
    if (itemEnabled) {
        [self.optionIndices addIndex:index];
    } else {
        [self.optionIndices removeIndex:index];
    }
}

@end
