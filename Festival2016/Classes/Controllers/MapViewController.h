//
//  MapViewController.h
//  Festival2016
//
//  Created by Phan Phuoc Luong on 11/4/15.
//  Copyright © 2015 Phan Phuoc Luong. All rights reserved.
//
#import "MyButton.h"
//for callout in map
#import "SMCalloutView.h"

@import GoogleMaps;
@interface MapViewController : UIViewController<GMSMapViewDelegate, CLLocationManagerDelegate, SMCalloutViewDelegate, UIActionSheetDelegate>
{
    GMSMarker *myMarker;
    NSMutableArray *arrMarkers;
    GMSPolyline *polyline;
    //app delegate
    AppDelegate *appDelegate;
    //for title
    NSString *title;
    int typeHeader;//để xác định nút back hay menu
    //0: hien thi danh sach cac dia diem bieu dien
    //1: ..
    NSMutableArray *arrDatas;
    NSString *id_diadiem_selected;
    
    //dùng để vẽ góc xoay
    BOOL isBearing;
    float angle;
    float direction;
    MyButton *btnBearing;
    
    UIImage *icPinNoBearing;
    UIImage *icPinBearing;
    UIImage *pinIconOnMap;
}

@property (nonatomic, strong) id<MenuSliderDelegate> menuSliderDelegate;
@property (nonatomic, weak) IBOutlet UIView *viewMain;
//
@property (nonatomic, weak) IBOutlet GMSMapView *mapView;
@property (nonatomic, strong) CLLocationManager *locationManager;
//call out
@property (nonatomic, strong) SMCalloutView *calloutView;
@property (strong, nonatomic) UIView *emptyCalloutView;
//label title
@property (nonatomic, weak) IBOutlet UILabel *lblTitle;
@property (nonatomic, weak) IBOutlet UIButton *btnBack;
@property (nonatomic, weak) IBOutlet UIButton *btnMoreProgram;

//for search
@property (nonatomic, weak) IBOutlet UITextField *txtSearch;

- (void) setDatas:(int)type andDatas:(NSMutableArray *)datas;

@end
