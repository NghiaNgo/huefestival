//
//  SearchViewController.h
//  Festival2016
//
//  Created by Phan Phuoc Luong on 1/20/16.
//  Copyright © 2016 Phan Phuoc Luong. All rights reserved.
//

typedef enum {
    search_program = 0,
    search_art = 1,
    search_news = 2,
    search_content = 3,
    search_diembanve= 4,
} SEARCH_TYPE;

@interface SearchViewController : UIViewController<UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource>
{
    AppDelegate *appDelegate;
    int search_type;//để xác định search cho loại nào
    NSInteger page_index;
    
    NSMutableArray *arrDatas;
    NSMutableArray *arrDatasSearch;
}

- (id) initWithDatas:(NSString *)nibName andType:(SEARCH_TYPE)type andDatas:(NSMutableArray *)datas;
@property (nonatomic, weak) IBOutlet UIView *viewMain;
//
@property (nonatomic, weak) IBOutlet UITextField *txtSearch;
@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end
