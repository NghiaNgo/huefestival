//
//  AlarmViewController.m
//  Festival2016
//
//  Created by Phan Phuoc Luong on 2/18/16.
//  Copyright © 2016 Phan Phuoc Luong. All rights reserved.
//

#import "AlarmViewController.h"
#import "AlarmCell.h"
#import "LocalPushManager.h"

@interface AlarmViewController ()
{
    NSMutableArray *arrDatas;
    BOOL isUpdated;
    LocalPushManager *localPush;
    NSMutableArray *arrIndexPath;
}
@end

@implementation AlarmViewController

- (id)initWithDatas:(NSString *)nibName andData:(NSMutableArray *)arrAlarms
{
    if(self = [super initWithNibName:nibName bundle:nil]){
        arrDatas = [[NSMutableArray alloc] init];
        arrIndexPath = [[NSMutableArray alloc] init];
        [arrDatas addObjectsFromArray:arrAlarms];
        appDelegate = MyAppDelegate();
        localPush = [[LocalPushManager alloc] init];
    }
    
    return self;
}

- (void)initSafeArea {
    _viewMain.translatesAutoresizingMaskIntoConstraints = NO;
    
    if (@available(iOS 11, *)) {
        UILayoutGuide *guide = self.view.safeAreaLayoutGuide;
        [_viewMain.leadingAnchor constraintEqualToAnchor:guide.leadingAnchor].active = YES;
        [_viewMain.trailingAnchor constraintEqualToAnchor:guide.trailingAnchor].active = YES;
        [_viewMain.topAnchor constraintEqualToAnchor:guide.topAnchor].active = YES;
        [_viewMain.bottomAnchor constraintEqualToAnchor:guide.bottomAnchor].active = YES;
    } else {
        UILayoutGuide *margins = self.view.layoutMarginsGuide;
        [_viewMain.leadingAnchor constraintEqualToAnchor:margins.leadingAnchor].active = YES;
        [_viewMain.trailingAnchor constraintEqualToAnchor:margins.trailingAnchor].active = YES;
        [_viewMain.topAnchor constraintEqualToAnchor:self.topLayoutGuide.bottomAnchor].active = YES;
        [_viewMain.bottomAnchor constraintEqualToAnchor:self.bottomLayoutGuide.topAnchor].active = YES;
    }
    // Refresh myView and/or main view
    [self.view layoutIfNeeded];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.frame = [UIScreen mainScreen].bounds;
    //
    [self initSafeArea];
    //
    isUpdated = NO;
    _viewBlur.blurRadius = 15;
    _viewEdit.blurRadius = 50;
    //init langague
    [self refreshLanguage];
    [_tableView reloadData];
    _viewEdit.hidden = YES;
    _myDatePicker.hidden = YES;
    _lblContentEdit.hidden = YES;
    _lblDateEdit.hidden = YES;
    _lblTimeEdit.hidden = YES;
    [_myDatePicker addTarget:self action:@selector(dateIsChanged:) forControlEvents:UIControlEventValueChanged];
}

//refresh language
- (void)refreshLanguage
{
    _lblTitle.text = MYLocalizedString(@"profile.alarm1", nil);
}

- (IBAction) clickBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)dateIsChanged:(id)sender{
    isUpdated = YES;
    NSDate *date = [_myDatePicker date];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    _lblTimeEdit.text = [dateFormatter stringFromDate:date];
}

#pragma mark - UItableview
#pragma mark tableview Delegate method
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Indentifier";
    
    //chuong trinh
    AlarmCell *cell = (AlarmCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"AlarmCell" owner:self options:nil];
        for(id currentObject in topLevelObjects) {
            if([currentObject isKindOfClass:[UITableViewCell class]])
            {
                cell = (AlarmCell *)currentObject;
                cell.backgroundColor = [UIColor clearColor];
                break;
            }
        }
    }
    
    NSDictionary *dict = [arrDatas objectAtIndex:indexPath.row];
    if(dict){
        NSString *content = [dict objectForKey:@"content"];
        if(!content || [content isKindOfClass:[NSNull class]])
            content = @"";
        NSString *time = [dict objectForKey:@"strTime"];
        if(!time || [time isKindOfClass:[NSNull class]])
            time = @"";
        NSString *day = [dict objectForKey:@"strDay"];
        if(!day || [day isKindOfClass:[NSNull class]])
            day = @"";
        cell.lblContent.text = content;
        cell.lblTime.text = time;
        cell.lblDate.text = day;
        cell.btnDelete.tag = indexPath.row;
        cell.btnEdit.tag = indexPath.row;
        [cell.btnDelete addTarget:self action:@selector(callDelete:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnEdit addTarget:self action:@selector(callEdit:) forControlEvents:UIControlEventTouchUpInside];
        
        //kiem tra ngay da qua chua
        if(day.length > 0 && time.length > 0){
            NSDateFormatter *format = [[NSDateFormatter alloc] init];
            [format setDateFormat:@"dd/MM/yyyy HH:mm"];
            NSString *strDate = [NSString stringWithFormat:@"%@ %@", day, time];
            NSDate *d1  = [format dateFromString:strDate];
            NSDate *d2  = [NSDate date];
            NSTimeInterval todayTimeInterval = [d2 timeIntervalSince1970];
            NSTimeInterval previousTimeInterval = [d1 timeIntervalSince1970];
            
            if(previousTimeInterval < todayTimeInterval){
                UIColor *color = [UIColor lightGrayColor];
                cell.lblContent.textColor   = color;
                cell.lblDate.textColor      = color;
                cell.lblTime.textColor      = color;
                [cell.btnEdit setEnabled:NO];
                
                BOOL isOk = NO;
                for(NSIndexPath *ip in arrIndexPath){
                    if(ip.row == indexPath.row){
                        isOk = YES;
                        break;
                    }
                }
                if(!isOk)
                    [arrIndexPath addObject:indexPath];
            }
        }
    }
    
    [cell bringSubviewToFront:cell.btnEdit];
    [cell bringSubviewToFront:cell.btnDelete];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_tableView deselectRowAtIndexPath:indexPath animated:NO];
    NSInteger index = indexPath.row;
    for(NSIndexPath *ip in arrIndexPath){
        if(ip.row == index)
            return;
    }
    
    if(index >= 0 && index < arrDatas.count){
        NSDictionary *dict = [arrDatas objectAtIndex:index];
        [self showViewEdit:dict andIndex:(int)index];
    }
}

- (void) callDelete:(id) sender
{
    UIButton *btn = (UIButton *)sender;
    int index = (int)btn.tag;
    if(index >= 0 && index < arrDatas.count){
        NSDictionary *dict = [arrDatas objectAtIndex:index];
        NSString *nID = [dict objectForKey:@"id"];
        [appDelegate removeReminder:nID];
        [arrDatas removeObject:dict];
        [arrIndexPath removeAllObjects];
        [_tableView reloadData];
    }
}

- (void) callEdit:(id) sender
{
    UIButton *btn = (UIButton *)sender;
    int index = (int)btn.tag;
    if(index >= 0 && index < arrDatas.count){
        NSDictionary *dict = [arrDatas objectAtIndex:index];
        [self showViewEdit:dict andIndex:index];
    }
}

- (void) showViewEdit:(NSDictionary *)dict andIndex:(int)index
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    AlarmCell *cell = (AlarmCell *)[_tableView cellForRowAtIndexPath:indexPath];
    NSString *content = [dict objectForKey:@"content"];
    if(!content || [content isKindOfClass:[NSNull class]])
        content = @"";
    NSString *time = [dict objectForKey:@"strTime"];
    if(!time || [time isKindOfClass:[NSNull class]])
        time = @"";
    NSString *day = [dict objectForKey:@"strDay"];
    if(!day || [day isKindOfClass:[NSNull class]])
        day = @"";
    _lblContentEdit.text = content;
    _lblDateEdit.text = day;
    _lblTimeEdit.text = time;
    CGRect textRect = [content boundingRectWithSize:CGSizeMake(_lblContentEdit.frame.size.width - 5, 9999)
                                            options:NSStringDrawingUsesLineFragmentOrigin
                                         attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica Neue" size:15]}
                                            context:nil];
    
    
    CGSize size = textRect.size;
    float h = size.height;
    if(h <= 21)
         h = 21;
    textRect = _lblContentEdit.frame;
    textRect.size.height = h;
    _lblContentEdit.frame = textRect;
    
    textRect = cell.frame;
    textRect.origin.y += textRect.size.height/2;
    _viewEdit.frame = textRect;
    _viewEdit.hidden = NO;
    _btnOK.tag = index;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm dd/MM/yyyy"];
    NSDate *now = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@ %@", time, day]];
    _myDatePicker.date = now;
    [UIView animateWithDuration:0.25
                          delay:0.0
                        options: (int) UIViewAnimationCurveEaseOut
                     animations:^{
                         _viewEdit.frame = self.view.frame;
                     }
                     completion:^(BOOL finished){
                         _myDatePicker.hidden = NO;
                         _lblContentEdit.hidden = NO;
                         _lblDateEdit.hidden = NO;
                         _lblTimeEdit.hidden = NO;
                     }];
}

- (IBAction)hideViewEdit:(id)sender
{
    _myDatePicker.hidden = YES;
    _lblContentEdit.hidden = YES;
    _lblDateEdit.hidden = YES;
    _lblTimeEdit.hidden = YES;
    NSInteger index = _btnOK.tag;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    AlarmCell *cell = (AlarmCell *)[_tableView cellForRowAtIndexPath:indexPath];
    [UIView animateWithDuration:0.25
                          delay:0.0
                        options: (int) UIViewAnimationCurveEaseOut
                     animations:^{
                         CGRect textRect = cell.frame;
                         textRect.origin.y += textRect.size.height/2;
                         _viewEdit.frame = textRect;
                     }
                     completion:^(BOOL finished){
                         _viewEdit.hidden = YES;
                         if(isUpdated){
                             isUpdated = NO;
                             NSDictionary *d = [arrDatas objectAtIndex:index];
                             NSMutableDictionary *mutableDict = [d mutableCopy];
                             [mutableDict setObject:_lblTimeEdit.text forKey:@"strTime"];
                             d = [mutableDict mutableCopy];
                             [arrDatas replaceObjectAtIndex:index withObject:d];
                             [self reloadDataTable:index];
                         
                             NSString *_id = [d objectForKey:@"id"];
                             NSString *msg = [d objectForKey:@"content"];
                             NSString *pid = [d objectForKey:@"program_id"];
                             //set to local push
                             NSString *strDate = [NSString stringWithFormat:@"%@ %@", _lblTimeEdit.text, _lblDateEdit.text];
                             [localPush updateEvent:_id andDate:strDate alertBody:msg andProgramID:pid];
                         }
                     }];
}

- (void)reloadDataTable:(NSInteger) index
{
    NSString *strIndex = [NSString stringWithFormat:@"%zd", index];
    [self performSelectorOnMainThread:@selector(refreshTable:) withObject:strIndex waitUntilDone:NO];
}

- (void) refreshTable:(NSString *) strIndex
{
    NSInteger index = [strIndex integerValue];
    if(index >= arrDatas.count || index < 0)
        return;
    
    [_tableView beginUpdates];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
    [_tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
    [_tableView endUpdates];
}

#pragma TableDatasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    float hCell = 80.0f;
    NSDictionary *obj = nil;
    obj = [arrDatas objectAtIndex:indexPath.row];
    if(!obj)
        return hCell;
    
    NSString *content = [obj objectForKey:@"content"];
    CGRect textRect = [content boundingRectWithSize:CGSizeMake(_tableView.frame.size.width - 110, 9999)
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica Neue" size:13]}
                                                  context:nil];
        
        
    CGSize size = textRect.size;
    float h = size.height;
        
    if(h <= 21)
        return hCell;
    else
        return hCell + (h - 21) + h/21;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrDatas.count;
}

@end
