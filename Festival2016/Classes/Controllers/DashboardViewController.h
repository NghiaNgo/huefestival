//
//  DashboardViewController.h
//  Festival2016
//
//  Created by Phan Phuoc Luong on 11/9/15.
//  Copyright © 2015 Phan Phuoc Luong. All rights reserved.
//

#import "MyButton.h"
#import "FXBlurView.h"

@interface DashboardViewController : UIViewController<MenuSliderDelegate>
{
    AppDelegate *appDelegate;
}

@property (nonatomic, weak) IBOutlet UIView *viewMain;

@property (nonatomic, weak) IBOutlet MyButton *btnNews;
@property (nonatomic, weak) IBOutlet MyButton *btnFes;
@property (nonatomic, weak) IBOutlet MyButton *btnLocations;
@property (nonatomic, weak) IBOutlet MyButton *btnProfile;
@property (nonatomic, weak) IBOutlet UILabel *lblNews;
@property (nonatomic, weak) IBOutlet UILabel *lblFes;
@property (nonatomic, weak) IBOutlet UILabel *lblLocation;
@property (nonatomic, weak) IBOutlet UILabel *lblProfile;

@property (nonatomic, weak) IBOutlet UIView *viewBlur;
@property (nonatomic, weak) IBOutlet UIImageView *iconLogo;

//view floating
@property (nonatomic, weak) IBOutlet UIButton *btnClose;
@property (nonatomic, weak) IBOutlet MyButton *btnTieuDiem;
@property (nonatomic, weak) IBOutlet MyButton *btnCongDong;
@property (nonatomic, weak) IBOutlet MyButton *btnLichDien;
@property (nonatomic, weak) IBOutlet MyButton *btnNgheSi;
@property (nonatomic, weak) IBOutlet FXBlurView *viewFloatingBlur;

//info
@property (nonatomic, weak) IBOutlet UIView *viewDisable;
@property (nonatomic, weak) IBOutlet UIButton *btnInfo;

@property (nonatomic, weak) IBOutlet UISegmentedControl *segLanguage;

- (void) refreshLanguage;
- (void) refreshImage:(int) tabIndex andId:(NSString *)_id andImage:(UIImage *)img;
- (void) showHideViewDisable:(BOOL) isHide;
@end
