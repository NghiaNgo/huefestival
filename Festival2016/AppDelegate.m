//
//  AppDelegate.m
//  Festival2016
//
//  Created by Phan Phuoc Luong on 9/22/15.
//  Copyright (c) 2015 Phan Phuoc Luong. All rights reserved.
//

#import "AppDelegate.h"
#import "LanguagesManager.h"
#import "DashboardViewController.h"
#import "CouchbaseEvents.h"
#import "ProgramObj.h"
#import "ArtObj.h"
#import "LocalPushManager.h"
#import "TicketLocationViewController.h"
#import "MapViewController.h"
#import "ProfileViewController.h"
#import "PhotoDetailViewController.h"

@import GoogleMaps;
static dispatch_once_t firstLaunch;
@interface AppDelegate ()<LoadImageDelegate>
{
    DashboardViewController *dashboard;
    LocalPushManager *localPushManager;
}

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [GMSServices provideAPIKey:GOOGLE_MAP_KEY];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    _language = [userDefault objectForKey:@"language"];
    if(!_language || [_language isKindOfClass:[NSNull class]])
        _language = @"";
    
    if(_language.length == 0){
        dispatch_once(&firstLaunch, ^{
            [[LanguagesManager sharedInstance] setSupportedLanguages:@[@"vi", @"en"]];
            //[[LanguagesManager sharedInstance] setNotificationEnable:NO];
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            [userDefault setObject:@"vi" forKey:@"language"];
        });
    } else {
        [[LanguagesManager sharedInstance] setLanguage:_language];
    }

    _window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    _isMenuShowed = NO;
    _screenWidth =  CGRectGetWidth([UIScreen mainScreen].bounds);
    _screenHeight = _window.frame.size.height;
    
    _arrAboutList = [[NSMutableArray alloc] init];
    _arrPrograms = [[NSMutableArray alloc] init];
    _arrTickets = [[NSMutableArray alloc] init];
    _arrLocations = [[NSMutableArray alloc] init];
    _arrArts = [[NSMutableArray alloc] init];
    _arrCalendar = [[NSMutableArray alloc] init];
    //lấy danh sách các chương trình đã đánh dấu like
    _arrLikedID = [[NSMutableArray alloc] init];
    NSString *strLiked = [userDefault objectForKey:@"program_liked"];
    if(strLiked && strLiked.length > 0){
        NSArray *arr = [strLiked componentsSeparatedByString: @","];
        [_arrLikedID addObjectsFromArray:arr];
    }
    //lấy danh sách các nghệ sĩ đã đánh dấu like
    _arrLikedArtistID = [[NSMutableArray alloc] init];
    strLiked = [userDefault objectForKey:@"artist_liked"];
    if(strLiked && strLiked.length > 0){
        NSArray *arr = [strLiked componentsSeparatedByString: @","];
        [_arrLikedArtistID addObjectsFromArray:arr];
    }
    _arrNewsLikeList = [[NSMutableArray alloc] init];
    _arrDiaDiemLikeList = [[NSMutableArray alloc] init];
    
    _mapViewController = [[MapViewController alloc] initWithNibName:@"MapViewController" bundle:nil];
    //init local push manager
    localPushManager = [[LocalPushManager alloc] init];
    //[localPushManager canceAllEvent];
    
    dashboard = [[DashboardViewController alloc] initWithNibName:IS_IPAD?@"DashboardViewControllerIPad":@"DashboardViewController" bundle:nil];
    _nav = [[UINavigationController alloc] initWithRootViewController:dashboard];
    _nav.navigationBarHidden = YES;
    _revealSideViewController = [[PPRevealSideViewController alloc] initWithRootViewController:_nav];
    _revealSideViewController.delegate = self;
    [self.window setRootViewController:_revealSideViewController];

    //regis notification for app
    [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    //
    [_window makeKeyAndVisible];
    
    return YES;
}

- (void)switchLanguage
{
    if([[[LanguagesManager sharedInstance] getCurrentLanguage] isEqualToString:@"vi"]) {
        _language = @"en";
        [[LanguagesManager sharedInstance] setLanguage:@"en"];
    }
    else
    {
        _language = @"vi";
        [[LanguagesManager sharedInstance] setLanguage:@"vi"];
    }
    
    [dashboard refreshLanguage];
    //save language cache
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:_language forKey:@"language"];
}

#pragma mark - PPRevealSideViewController delegate

- (void) pprevealSideViewController:(PPRevealSideViewController *)controller willPushController:(UIViewController *)pushedController {
    //PPRSLog(@"%@", pushedController);
}

- (void) pprevealSideViewController:(PPRevealSideViewController *)controller didPushController:(UIViewController *)pushedController {
    //PPRSLog(@"pushedController");
    [dashboard showHideViewDisable:NO];
}

- (void) pprevealSideViewController:(PPRevealSideViewController *)controller willPopToController:(UIViewController *)centerController {
    //PPRSLog(@"%@", willPopToController);
}

- (void) pprevealSideViewController:(PPRevealSideViewController *)controller didPopToController:(UIViewController *)centerController {
    //PPRSLog(@"didPopToController");
    [dashboard showHideViewDisable:YES];
}

- (void) pprevealSideViewController:(PPRevealSideViewController *)controller didChangeCenterController:(UIViewController *)newCenterController {
    //PPRSLog(@"%@", newCenterController);
}

- (BOOL) pprevealSideViewController:(PPRevealSideViewController *)controller shouldDeactivateDirectionGesture:(UIGestureRecognizer*)gesture forView:(UIView*)view
{
    return YES;
}

- (PPRevealSideDirection)pprevealSideViewController:(PPRevealSideViewController*)controller directionsAllowedForPanningOnView:(UIView*)view {
    
    if ([view isKindOfClass:NSClassFromString(@"UIWebBrowserView")]) return PPRevealSideDirectionLeft | PPRevealSideDirectionRight;
    
    return PPRevealSideDirectionLeft | PPRevealSideDirectionRight | PPRevealSideDirectionTop | PPRevealSideDirectionBottom;
}

#pragma mark - Unloading tests

- (void) unloadRevealFromMemory
{
    self.revealSideViewController = nil;
    self.window.rootViewController = nil;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    application.applicationIconBadgeNumber = 0;
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    application.applicationIconBadgeNumber = 0;
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSString *str = [NSString stringWithFormat:@"%@",deviceToken];
    str = [str stringByReplacingOccurrencesOfString:@"<" withString:@""];
    str = [str stringByReplacingOccurrencesOfString:@">" withString:@""];
    str = [str stringByReplacingOccurrencesOfString:@" " withString:@""];
    //NSLog(@"token %@", str);
    //call save token
    //[UIAlertView showWithTitle:@"Token Device" message:str handler:nil];
    //[self callSaveToken:str];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    //[[[OLGhostAlertView alloc] initWithTitle:@"" message:error.debugDescription timeout:3 dismissible:YES] show];
}

//support lanscapse for view photo detail
- (UIInterfaceOrientationMask) application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    if ([self.window.rootViewController.presentedViewController isKindOfClass: [PhotoDetailViewController class]]) {
        PhotoDetailViewController *photoViewDetail = (PhotoDetailViewController *) self.window.rootViewController.presentedViewController;
        //
        if (photoViewDetail.isPresented)
            return UIInterfaceOrientationMaskPortrait;
        else
            return UIInterfaceOrientationMaskPortrait;
    } else
        return UIInterfaceOrientationMaskPortrait;
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSDictionary *aps = [userInfo objectForKey:@"aps"];
    NSString *message = [[aps objectForKey:@"alert"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    OLGhostAlertView *olAlert = [[OLGhostAlertView alloc] initWithCustomTitle:@"Hue Festival" message:message timeout:3 dismissible:YES];
    [olAlert showCustom];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    NSDictionary *aps = [userInfo objectForKey:@"aps"];
    NSString *message = [[aps objectForKey:@"alert"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    OLGhostAlertView *olAlert = [[OLGhostAlertView alloc] initWithCustomTitle:@"Hue Festival" message:message timeout:3 dismissible:YES];
    [olAlert showCustom];
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    NSDictionary *aps = notification.userInfo;
    NSString *message = [[aps objectForKey:@"content"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    OLGhostAlertView *olAlert = [[OLGhostAlertView alloc] initWithCustomTitle:@"Hue Festival" message:message timeout:3 dismissible:YES];
    [olAlert showCustom];
    
    //refresh dữ liệu nếu đang ở profile
    NSString *nID = [aps objectForKey:@"id"];
    [localPushManager canceEvent:nID];
   
    NSArray *viewContrlls=[_nav viewControllers];
    UIViewController *currentViewController = [viewContrlls lastObject];
    if([currentViewController isKindOfClass:[ProfileViewController class]]){
        ProfileViewController *pView = (ProfileViewController *)currentViewController;
        [pView loadListAlarm];
    }
}

#pragma mark - like a program
- (void)addLikeProgram:(BOOL)isLike andProgram:(ProgramObj *)programObj
{
    NSString *pID = programObj.pID;
    if(isLike){
        [_arrLikedID addObject:pID];
    } else {
        [_arrLikedID removeObject:pID];
    }
    
    NSString *strID = @"";
    int index = 0;
    for(NSString *sID in _arrLikedID){
        if(index < _arrLikedID.count - 1)
            strID = [strID stringByAppendingString:[NSString stringWithFormat:@"%@,", sID]];
        else
            strID = [strID stringByAppendingString:[NSString stringWithFormat:@"%@", sID]];
    }
    //update
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:strID forKey:@"program_liked"];
}

- (BOOL)checkProgramLike:(NSString *)pID
{
    for(NSString *sID in _arrLikedID){
        if([sID isEqualToString:pID])
            return YES;
    }
    return NO;
}

#pragma mark - like a program
- (void) addLikeArtist:(BOOL)isLike andArtObj:(ArtObj *)obj
{
    NSString *_id = obj.aID;
    if(isLike){
        [_arrLikedArtistID addObject:_id];
    } else {
        [_arrLikedArtistID removeObject:_id];
    }
    
    NSString *strID = @"";
    int index = 0;
    for(NSString *sID in _arrLikedArtistID){
        if(index < _arrLikedArtistID.count - 1)
            strID = [strID stringByAppendingString:[NSString stringWithFormat:@"%@,", sID]];
        else
            strID = [strID stringByAppendingString:[NSString stringWithFormat:@"%@", sID]];
    }
    //update
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:strID forKey:@"artist_liked"];
}

- (BOOL)checkArtistLike:(NSString *)aID
{
    for(NSString *_ID in _arrLikedArtistID){
        if([aID isEqualToString:_ID])
            return YES;
    }
    return NO;
}

//kiem tra 1 program da co trong danh sach chua
- (BOOL)checkProgram:(NSString *)pID
{
    for(ProgramObj *obj in _arrPrograms){
        if([pID isEqualToString:obj.pID])
            return YES;
    }
    return NO;
}

//======================================================//
//================= Cài đạt thông báo ==================//
//======================================================//
- (void)setReminder:(NSString *)_id andTitle:(NSString *)title andMessage:(NSString *)message andTime:(NSString *)time andProgramID:(NSString *)pID timeBefore:(int)before
{
    [localPushManager addNewEvent:_id andDate:time alertTitle:time alertBody:message action:MYLocalizedString(@"view", nil) andProgramID:pID andBefore:before];
}

- (void)removeReminder:(NSString *)_id
{
    [localPushManager canceEvent:_id];
}

////////
#pragma mark - Load Image Delegate
- (void)loadImageProgramFinish:(NSString *)_idProgram andIndex:(NSInteger)index andImage:(UIImage *)img
{
    [dashboard refreshImage:0 andId:_idProgram andImage:img];
    [[CouchbaseEvents sharedInstance] saveProgramPhoto:[NSString stringWithFormat:@"program_%@_%zd.jpg", _idProgram, index] andImage:img andProgramID:_idProgram];
}

- (void)loadImageArtFinish:(NSString *)_idArt andIndex:(NSInteger)index andImage:(UIImage *)img
{
    [dashboard refreshImage:2 andId:_idArt andImage:img];
    [[CouchbaseEvents sharedInstance] saveArtPhoto:[NSString stringWithFormat:@"art_%@_%zd.jpg", _idArt, index] andImage:img andArtID:_idArt];
}

- (void)loadImageLocationFinish:(NSString *)_idLocation andIndex:(NSInteger)index andImage:(UIImage *)img
{
    [dashboard refreshImage:3 andId:_idLocation andImage:img];
    [[CouchbaseEvents sharedInstance] saveLocationPhoto:[NSString stringWithFormat:@"location_%@_%zd.jpg", _idLocation, index] andImage:img andLocationID:_idLocation];
}

- (void)loadImageNewsFinish:(NSString *)_idNews andIndex:(NSInteger)index andImage:(UIImage *)img
{
    [dashboard refreshImage:4 andId:_idNews andImage:img];
}

- (void)loadImageTicketLocationFinish:(NSString *)_id andIndex:(NSInteger)index andImage:(UIImage *)img
{
    NSArray *viewContrlls = [_nav viewControllers];
    UIViewController *currentViewController = [viewContrlls lastObject];
    if ([currentViewController isKindOfClass:[TicketLocationViewController class]]){
        TicketLocationViewController *ticket = (TicketLocationViewController *)currentViewController;
        [ticket reloadTable];
    }
    [[CouchbaseEvents sharedInstance] saveTicketLocationPhoto:[NSString stringWithFormat:@"ticket_%@_%i.jpg", _id, 0] andImage:img andTicketLocationID:_id];
}

//change statusbar color
- (void)setStatusbarColor:(UIColor *)color
{
    UIView *statusBar;
    if (@available(iOS 13, *)) {
        NSInteger tag = 01012020;
        statusBar = [[UIApplication sharedApplication].keyWindow viewWithTag:tag];
        if(!statusBar) {
            statusBar = [[UIView alloc] initWithFrame:[UIApplication sharedApplication].keyWindow.windowScene.statusBarManager.statusBarFrame];
            statusBar.tag = tag;
            [[UIApplication sharedApplication].keyWindow addSubview:statusBar];
        }
        //
        if(statusBar){
            [statusBar setHidden:NO];
            statusBar.backgroundColor = color;
            [statusBar bringSubviewToFront:[UIApplication sharedApplication].keyWindow];
        }
    } else {
        statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
        if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
            statusBar.backgroundColor = color;
        }
    }
}

@end
