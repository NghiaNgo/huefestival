//
//  Created by Phan Phuoc Luong on 1/5/15.
//  Copyright (c) 2015 Phan Phuoc Luong. All rights reserved.
//

#ifndef Appzitov_Constants_h
#define Appzitov_Constants_h

#define VERSION @"1.0.9"

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPhone)
#define IS_IPHONE (!IS_IPAD)

#define SYSTEM_VERSION_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

//define color
//#define HEADER_COLOR 0xdc3c3c
//color
#define TAB_COLOR 0xc22223
#define COLOR_RED 0xdb3c3c
#define COLOR_YELLOW 0xd7a91b
#define COLOR_WHITE 0xFFFFFF
#define COLOR_BLACK 0x000000
#define TEXT_COLOR 0x758DB0
#define TEXT_COLOR_FOCUS 0xc63031

//GOOGLE MAP KEY       
#define GOOGLE_MAP_KEY @"AIzaSyBskcxdws_Z-Fh2H17TNzs7X_gtcdjuyN8"
#define GOOGLE_API_KEY @"AIzaSyAuXqPaY6D9l832pIlIn9BRWeveA7CIvbI"
// App View Basic
//#define screenViewHeight CGRectGetHeight([UIScreen mainScreen].applicationFrame)
#define screenViewWidth  CGRectGetWidth([UIScreen mainScreen].applicationFrame)

#define KNotification @"kNetworkReachabilityChangedNotification"
#define kReceiveLocalNotification @"didReceiveLocalNotification"

#define RadiansToDegrees(radians)(radians * 180.0/M_PI)
#define DegreesToRadians(degrees)(degrees * M_PI / 180.0)

#define KEY_API @"88888888"
#define PAGE_COUNT 50
//============= for dev ================
#define BASE_URL @"http://www.huefestival.com"


#endif
